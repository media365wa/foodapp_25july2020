<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintReport extends Model
{
    protected $table = 'complaint_report';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'unique_order_id','issue_type', 'issue_description', 'issue_status', 'is_open'
	];
}
