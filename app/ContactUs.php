<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = 'contact_us';
    public function contactUs()
    {
        return $this->belongsTo('App\ContactUs');
    }
}
