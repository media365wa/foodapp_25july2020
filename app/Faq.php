<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';
    public function faq()
    {
        return $this->belongsTo('App\Faq');
    }
}
