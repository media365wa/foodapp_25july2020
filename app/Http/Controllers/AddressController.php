<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\User;

class AddressController extends Controller
{
    public function getAddresses($id)
    {
        $addresses = Address::where('user_id', $id)->orderBy("id", "DESC")->get();
        return response()->json($addresses);
    }

    public function saveAddress(Request $request)
    {
        $address = new Address();
        $address->user_id = $request->user_id;
        $address->address = $request->address["address"];
        $address->house = $request->address["house"];
        $address->landmark = $request->address["landmark"];
        $address->tag = $request->address["tag"];
        $address->save();

        $addresses = Address::where('user_id', $request->user_id)->orderBy("id", "DESC")->get();
        return response()->json($addresses);
    }

    public function deleteAddress(Request $request)
    {
        $address = Address::where('id', $request->address_id)->first();
        $address->delete();

        $addresses = Address::where('user_id', $request->user_id)->orderBy("id", "DESC")->get();
        return response()->json($addresses);
    }

    public function setDefaultAddress(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        $user->default_address_id = $request->address_id;
        $user->save();

        $addresses = Address::where('user_id', $request->user_id)->orderBy("id", "DESC")->get();
        return response()->json($addresses);
    }

    public function updateAddress(Request $request)
    {
        try {
            $address = Address::where('id', $request->address_id)->first();
            $address->address = $request->address_address;
            $address->house = $request->address_house;
            $address->landmark = $request->address_landmark;
            $address->tag = $request->address_tag;
            $address->save();

            return response()->json(array("Success"));
        }
        catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th,
            ], 401);
        }
        
    }
}
