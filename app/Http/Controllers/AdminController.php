<?php

namespace App\Http\Controllers;

use App\Addon;
use App\AddonCategory;
use App\ComplaintReport;
use App\ContactUs;
use App\Faq;
use App\Item;
use App\ItemCategory;
use App\Kitchen;
use App\Location;
use App\Order;
use App\Orderitem;
use App\Orderstatus;
use App\Page;
use App\PromoSlider;
use App\Restaurant;
use App\RestaurantPayout;
use App\Slide;
use App\User;
use App\Wallet;
use App\WalletTransactions;
use Auth;
use Carbon\Carbon;
use Exception;
use Exporter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{

    /**
     * @return mixed
     */
    public function dashboard()
    {
        $displayUsers = User::all();
        $displayUsers = count($displayUsers);

        $displayRestaurants = Restaurant::all();
        $displayRestaurants = count($displayRestaurants);

        $displaySales = Order::where('orderstatus_id', 5)->get();
        $displaySales = count($displaySales);

        $displayEarnings = Order::where('orderstatus_id', 5)->get();
        $total = 0;
        foreach ($displayEarnings as $de) {
            $total += $de->total;
        }
        $displayEarnings = $total;

        $orders = Order::orderBy('id', 'DESC')->take(10)->get();
        $users = User::orderBy('id', 'DESC')->take(10)->get();

        $todaysDate = Carbon::now()->format('Y-m-d');

        $orderStatusesName = '[';

        $orderStatuses = Orderstatus::get(['name'])
            ->pluck('name')
            ->toArray();
        foreach ($orderStatuses as $key => $value) {
            $orderStatusesName .= "'" . $value . "' ,";
        }
        $orderStatusesName = rtrim($orderStatusesName, ' ,');
        $orderStatusesName = $orderStatusesName . ']';

        $orderStatusOrders = Order::all();

        $orderStatusOrders = $orderStatusOrders->groupBy('orderstatus_id')->map(function ($orderCount) {
            return $orderCount->count();
        });

        $orderStatusesData = '[';
        foreach ($orderStatusOrders as $key => $value) {
            if ($key == 1) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Order Placed'},";
            }
            if ($key == 2) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Preparing Order'},";
            }
            if ($key == 3) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Delivery Guy Assigned'},";
            }
            if ($key == 4) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Order Picked Up'},";
            }
            if ($key == 5) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Delivered'},";
            }
            if ($key == 6) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Cancelled'},";
            }
            if ($key == 7) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Paid Via Wallet'},";
            }
            if ($key == 8) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Declined'},";
            }
        }
        $orderStatusesData = rtrim($orderStatusesData, ',');
        $orderStatusesData .= ']';

        $ifAnyOrders = Order::all()->count();
        if ($ifAnyOrders == 0) {
            $ifAnyOrders = false;
        } else {
            $ifAnyOrders = true;
        }

        return view('admin.dashboard', array(
            'displayUsers' => $displayUsers,
            'displayRestaurants' => $displayRestaurants,
            'displaySales' => $displaySales,
            'displayEarnings' => $displayEarnings,
            'orders' => $orders,
            'users' => $users,
            'todaysDate' => $todaysDate,
            'orderStatusesName' => $orderStatusesName,
            'orderStatusesData' => $orderStatusesData,
            'ifAnyOrders' => $ifAnyOrders,
        ));
    }

    public function users()
    {
        $count = User::count();
        $users = User::orderBy('id', 'DESC')->paginate('20');
        $roles = Role::all();
        return view('admin.users', array(
            'count' => $count,
            'users' => $users,
            'roles' => $roles,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewUser(Request $request)
    {
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'roll_number' => $request->roll_number,
                'delivery_pin' => strtoupper(str_random(5)),
                'password' => bcrypt($request->password),
            ]);

            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }

            $wallet = new Wallet();
            //$wallets = Wallet::where('user_id', $user->id)->first();

            $wallet->user_id = $user->id;
            $wallet->is_active = 1;
            $wallet->balance = 0;
            $wallet->save();
            $walletId = $wallet->save();

            return response()->json([
                'success' => false,
                'name' => $request->name,
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th,
            ], 401);
        }
    }

    /**
     * @param Request $request
     */
    public function postSearchUsers(Request $request)
    {
        $query = $request['query'];
        $users = User::where('name', 'LIKE', '%' . $query . '%')
            ->orWhere('email', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();

        $roles = Role::all();

        $count = count($users);

        return view('admin.users', array(
            'users' => $users,
            'query' => $query,
            'count' => $count,
            'roles' => $roles,
        ));
    }

    /**
     * @param $id
     */
    public function getEditUser($id)
    {
        $user = User::where('id', $id)->first();
        $roles = Role::get();
        return view('admin.editUser', array(
            'user' => $user,
            'roles' => $roles,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateUser(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        try {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->roll_number = $request->roll_number;
            if ($request->has('password')) {
                $user->password = bcrypt($request->password);
            }
            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }
            $user->save();
            return redirect()->back()->with(['success' => 'User Updated']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    public function manageDeliveryGuys()
    {
        $users = User::role('Delivery Guy')->orderBy('id', 'DESC')->paginate();
        $count = count($users);
        return view('admin.manageDeliveryGuys', array(
            'users' => $users,
            'count' => $count,
        ));
    }

    /**
     * @param $id
     */
    public function getManageDeliveryGuysRestaurants($id)
    {
        $user = User::where('id', $id)->first();
        if ($user->hasRole('Delivery Guy')) {
            $userRestaurants = $user->restaurants;

            $allRestaurants = Restaurant::get();

            return view('admin.manageDeliveryGuysRestaurants', array(
                'user' => $user,
                'userRestaurants' => $userRestaurants,
                'allRestaurants' => $allRestaurants,
            ));
        }
    }

    /**
     * @param Request $request
     */
    public function updateDeliveryGuysRestaurants(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $user->restaurants()->sync($request->user_restaurants);
        $user->save();
        return redirect()->back()->with(['success' => 'Delivery Guy Updated']);
    }

    public function manageRestaurantOwners()
    {
        $users = User::role('Restaurant Owner')->orderBy('id', 'DESC')->paginate();
        $count = count($users);
        return view('admin.manageRestaurantOwners', array(
            'users' => $users,
            'count' => $count,
        ));
    }

    public function getKitchen(Request $request)
    {

        $fp = fopen('/home/qwickpay/public_html/app/Http/Controllers/lidn.txt', 'w');
        fwrite($fp, $request);
        fclose($fp);
        //if ($request->ajax()) {
        $kitchens = Kitchen::whereIn('restaurant_id', $request->id)->get();
        return view('admin.tems', array(
            'success' => true,
            'kitchens' => $kitchens,
        ));
        // $response = [
        //     'success' => true,
        //     'data' => $kitchens,
        // ];

        // return response()->json($response);
        //}
        // $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)
        //   ->orderBy('id', 'DESC')
        //   ->get();

        // $response = [
        //     'success' => true,
        //     'data' => $kitchens,
        // ];

        // return response()->json($response);

        //return redirect()->back()->with(['success' => 'Delivery Guy Updated']);
    }

    /**
     * @param $id
     */
    public function getManageRestaurantOwnersRestaurants($id)
    {
        $user = User::where('id', $id)->first();
        if ($user->hasRole('Restaurant Owner')) {
            $userRestaurants = $user->restaurants;

            $allRestaurants = Restaurant::get();

            return view('admin.manageDeliveryGuysRestaurants', array(
                'user' => $user,
                'userRestaurants' => $userRestaurants,
                'allRestaurants' => $allRestaurants,
            ));
        }
    }

    /**
     * @param Request $request
     */
    public function updateManageRestaurantOwnersRestaurants(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $user->restaurants()->sync($request->user_restaurants);
        $user->save();
        return redirect()->back()->with(['success' => 'Restaurant Owner Updated']);
    }

    public function orders()
    {
        $count = Order::count();

        $restaurants = Restaurant::get();
        $orders = Order::orderBy('id', 'DESC')->with('accept_delivery.user')->paginate('20');
        // dd($orders);
        return view('admin.orders', array(
            'restaurants' => $restaurants,
            'orders' => $orders,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function postSearchOrders(Request $request)
    {
        $query = $request['query'];
        $restaurants = Restaurant::get();
        $orders = Order::where('unique_order_id', 'LIKE', '%' . $query . '%')
            ->take('100')->paginate();
        $count = count($orders);
        return view('admin.orders', array(
            'restaurants' => $restaurants,
            'orders' => $orders,
            'count' => $count,
        ));
    }

    /**
     * @param $order_id
     */
    public function viewOrder($order_id)
    {
        $order = Order::where('unique_order_id', $order_id)->first();
        // ->update(['quantity' => quantity-$oderquantity])

        if ($order) {
            return view('admin.viewOrder', array(
                'order' => $order,

            ));
        } else {
            return redirect()->route('admin.orders');
        }
    }

    public function sliders()
    {
        $sliders = PromoSlider::orderBy('id', 'DESC')->get();
        $count = count($sliders);
        $locations = Location::all();
        return view('admin.sliders', array(
            'sliders' => $sliders,
            'count' => $count,
            'locations' => $locations,
        ));
    }

    /**
     * @param $id
     */
    public function getEditSlider($id)
    {
        $slider = PromoSlider::where('id', $id)->first();

        if ($slider) {
            return view('admin.editSlider', array(
                'slider' => $slider,
                'slides' => $slider->slides,
            ));
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param Request $request
     */
    public function createSlider(Request $request)
    {
        $slider = new PromoSlider();
        $slider->name = $request->name;
        $slider->location_id = $request->location_id;
        $slider->save();
        return redirect()->back()->with(['success' => 'New Slider Created']);
    }

    /**
     * @param $id
     */
    public function disableSlider($id)
    {
        $slider = PromoSlider::where('id', $id)->first();
        if ($slider) {
            $slider->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param $id
     */
    public function deleteSlider($id)
    {
        $slider = PromoSlider::where('id', $id)->first();
        if ($slider) {
            $slides = $slider->slides;
            foreach ($slides as $slide) {
                $slide->delete();
            }
            $slider->delete();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param Request $request
     */
    public function saveSlide(Request $request)
    {
        $url = url('/');
        $url = substr($url, 0, strrpos($url, '/')); //this will give url without "/public"

        $slide = new Slide();
        $slide->promo_slider_id = $request->promo_slider_id;
        $slide->name = $request->name;
        $slide->url = $request->url;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();

        Image::make($image)
            ->resize(384, 384)
            ->save(base_path('assets/img/slider/' . $filename));
        $slide->image = '/assets/img/slider/' . $filename;

        Image::make($image)
            ->resize(20, 20)
            ->save(base_path('assets/img/slider/small/' . $filename_sm));
        $slide->image_placeholder = '/assets/img/slider/small/' . $filename_sm;

        $slide->save();

        return redirect()->back()->with(['success' => 'New Slide Created']);
    }

    /**
     * @param $id
     */
    public function deleteSlide($id)
    {
        $slide = Slide::where('id', $id)->first();
        if ($slide) {
            $slide->delete();
            return redirect()->back()->with(['success' => 'Deleted']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param $id
     */
    public function disableSlide($id)
    {
        $slide = Slide::where('id', $id)->first();
        if ($slide) {
            $slide->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    public function restaurants()
    {
        $count = Restaurant::all()->count();
        $restaurants = Restaurant::orderBy('id', 'DESC')->where('is_accepted', '1')
            ->paginate(10);
        $locations = Location::get();

        return view('admin.restaurants', array(
            'restaurants' => $restaurants,
            'count' => $count,
            'locations' => $locations,

        ));
    }

    public function pendingAcceptance()
    {
        $count = Restaurant::all()->count();
        $restaurants = Restaurant::orderBy('id', 'DESC')->where('is_accepted', '0')->paginate(10000);
        $count = count($restaurants);
        $locations = Location::get();

        return view('admin.restaurants', array(
            'restaurants' => $restaurants,
            'count' => $count,
            'locations' => $locations,
        ));
    }

    /**
     * @param $id
     */
    public function acceptRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $restaurant->toggleAcceptance()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.restaurants');
        }
    }

    /**
     * @param Request $request
     */
    public function searchRestaurants(Request $request)
    {
        $query = $request['query'];
        $restaurants = Restaurant::where('name', 'LIKE', '%' . $query . '%')
            ->orWhere('sku', 'LIKE', '%' . $query . '%')
            ->take('1000')
            ->paginate();
        $count = count($restaurants);
        $locations = Location::get();

        return view('admin.restaurants', array(
            'restaurants' => $restaurants,
            'query' => $query,
            'count' => $count,
            'locations' => $locations,
        ));
    }

    /**
     * @param $id
     */
    public function disableRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $restaurant->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.restaurants');
        }
    }

    /**
     * @param $id
     */
    public function deleteRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $items = $restaurant->items;
            foreach ($items as $item) {
                $item->delete();
            }
            $restaurant->delete();
            return redirect()->route('admin.restaurants');
        } else {
            return redirect()->route('admin.restaurants');
        }
    }

    /**
     * @param Request $request
     */
    public function saveNewRestaurant(Request $request)
    {
        $opens_at = date("H:i:s", strtotime(($request->opens_at))) . '.000000';
        $closes_at = date("H:i:s", strtotime(($request->closes_at))) . '.000000';
        // dd($opens_at, $closes_at);
        // $opens_at24 = date("H:i:s", strtotime($opens_at));
        // $closes_at24 = date("H:i:s", strtotime($closes_at));
        // dd($opens_at, $closes_at, $request->opens_at);
        // dd($opens_at, $closes_at);
        $restaurant = new Restaurant();

        $restaurant->name = $request->name;
        $restaurant->code = $request->code;
        $restaurant->opens_at = $opens_at;
        $restaurant->closed_at = $closes_at;

        $restaurant->max_cap = $request->limit;
        $restaurant->order_limit = $request->maxOrders;
        $restaurant->description = $request->description;
        $restaurant->location_id = $request->location_id;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(160, 117)
            ->save(base_path('assets/img/restaurants/' . $filename));
        $restaurant->image = '/assets/img/restaurants/' . $filename;
        Image::make($image)
            ->resize(20, 20)
            ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
        $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;

        $restaurant->rating = $request->rating;
        $restaurant->delivery_time = $request->delivery_time;
        $restaurant->price_range = $request->price_range;

        if ($request->is_pureveg == 'true') {
            $restaurant->is_pureveg = true;
        } else {
            $restaurant->is_pureveg = false;
        }

        if ($request->is_featured == 'true') {
            $restaurant->is_featured = true;
        } else {
            $restaurant->is_featured = false;
        }

        $restaurant->slug = str_slug($request->name) . '-' . str_random(15);
        $restaurant->certificate = $request->certificate;

        $restaurant->address = $request->address;
        $restaurant->pincode = $request->pincode;
        $restaurant->landmark = $request->landmark;
        $restaurant->latitude = $request->latitude;
        $restaurant->longitude = $request->longitude;

        $restaurant->restaurant_charges = $request->restaurant_charges;
        $restaurant->delivery_charges = $request->delivery_charges;
        $restaurant->commission_rate = $request->commission_rate;

        $restaurant->sku = time() . str_random(10);
        $restaurant->is_active = 0;
        $restaurant->is_accepted = 1;
        try {
            $restaurant->save();
            return redirect()->back()->with(['success' => 'Restaurant Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function getEditRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        $locations = Location::get();

        return view('admin.editRestaurant', array(
            'restaurant' => $restaurant,
            'locations' => $locations,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateRestaurant(Request $request)
    {
        $restaurant = Restaurant::where('id', $request->id)->first();

        if ($restaurant) {
            $restaurant->name = $request->name;
            $restaurant->max_cap = $request->limit;
            $restaurant->order_limit = $request->maxOrders;
            $restaurant->description = $request->description;
            $restaurant->location_id = $request->location_id;

            if ($request->image == null) {
                $restaurant->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(160, 117)
                    ->save(base_path('assets/img/restaurants/' . $filename));
                $restaurant->image = '/assets/img/restaurants/' . $filename;
                Image::make($image)
                    ->resize(20, 20)
                    ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
                $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;
            }

            $restaurant->rating = $request->rating;
            $restaurant->delivery_time = $request->delivery_time;
            $restaurant->price_range = $request->price_range;

            if ($request->is_pureveg == 'true') {
                $restaurant->is_pureveg = true;
            } else {
                $restaurant->is_pureveg = false;
            }

            if ($request->is_featured == 'true') {
                $restaurant->is_featured = true;
            } else {
                $restaurant->is_featured = false;
            }

            $restaurant->certificate = $request->certificate;

            $restaurant->address = $request->address;
            $restaurant->pincode = $request->pincode;
            $restaurant->landmark = $request->landmark;
            $restaurant->latitude = $request->latitude;
            $restaurant->longitude = $request->longitude;

            $restaurant->restaurant_charges = $request->restaurant_charges;
            $restaurant->delivery_charges = $request->delivery_charges;
            $restaurant->commission_rate = $request->commission_rate;

            try {
                $restaurant->save();
                return redirect()->back()->with(['success' => 'Restaurant Saved']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function items()
    {
        $count = Item::all()->count();
        $items = Item::orderBy('id', 'DESC')->paginate(20);
        $restaurants = Restaurant::all();
        $kitchens = Kitchen::all();
        /*$kitchens = Kitchen::where('restaurant_id', '4')->get();*/
        $itemCategories = ItemCategory::orderBy('id', 'DESC')->where('is_enabled', '1')->paginate(20);
        $itemCategoriesCount = count($itemCategories);
        $addonCategories = AddonCategory::all();

        return view('admin.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'itemCategoriesCount' => $itemCategoriesCount,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchItems(Request $request)
    {
        $query = $request['query'];
        $items = Item::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($items);
        $restaurants = Restaurant::get();
        $kitchens = Kitchen::get();
        $itemCategories = ItemCategory::where('is_enabled', '1')->get();
        $addonCategories = AddonCategory::all();

        return view('admin.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'query' => $query,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewItem(Request $request)
    {
        // dd($request->day);
        $days = $request->input('day');
        $days = implode(',', $days);

        $item = new Item();

        $item->name = $request->name;
        $item->day = $days;
        $item->price = $request->price;
        $item->restaurant_id = $request->restaurant_id;
        $item->from_time = $request->from_time;
        $item->to_time = $request->to_time;
        $item->kitchen_id = isset($request->kitchen_id) ? $request->kitchen_id : 0;
        $item->item_category_id = $request->item_category_id;
        $item->quantity = $request->quantity;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(162, 118)
            ->save(base_path('assets/img/items/' . $filename));
        $item->image = '/assets/img/items/' . $filename;
        Image::make($image)
            ->resize(25, 18)
            ->save(base_path('assets/img/items/small/' . $filename_sm));
        $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;

        if ($request->is_recommended == 'true') {
            $item->is_recommended = true;
        } else {
            $item->is_recommended = false;
        }

        if ($request->is_popular == 'true') {
            $item->is_popular = true;
        } else {
            $item->is_popular = false;
        }

        if ($request->is_new == 'true') {
            $item->is_new = true;
        } else {
            $item->is_new = false;
        }
        try {
            $item->save();
            if (isset($request->addon_category_item)) {
                $item->addon_categories()->sync($request->addon_category_item);
            }
            return redirect()->back()->with(['success' => 'Item Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }

    }

    /**
     * @param $id
     */
    public function getEditItem($id)
    {
        $item = Item::where('id', $id)->first();
        $restaurants = Restaurant::get();
        $kitchens = Kitchen::where('is_active', '1')->get();
        $itemCategories = ItemCategory::where('is_enabled', '1')->get();
        $addonCategories = AddonCategory::all();

        return view('admin.editItem', array(
            'item' => $item,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param $id
     */
    public function disableItem($id)
    {
        $item = Item::where('id', $id)->first();
        if ($item) {
            $item->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.items');
        }
    }

    /**
     * @param $id
     */
    public function deleteItem($id)
    {
        $item = Item::where('id', $id)->first();
        if ($item) {
            $item->delete();
            return redirect()->back()->with(['success' => 'Deleted Successfully']);
        } else {
            return redirect()->route('admin.items');
        }
    }
    /**
     * @param Request $request
     */
    public function updateItem(Request $request)
    {
        $item = Item::where('id', $request->id)->first();

        if ($item) {
            $days = $request->input('day');
            $days = implode(',', $days);

            $item->name = $request->name;
            $item->day = $days;
            $item->restaurant_id = $request->restaurant_id;
            $item->kitchen_id = isset($request->kitchen_id) ? $request->kitchen_id : 0;
            $item->item_category_id = $request->item_category_id;
            $item->from_time = $request->from_time;
            $item->to_time = $request->to_time;
            $item->quantity = $request->quantity;
            if ($request->image == null) {
                $item->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(162, 118)
                    ->save(base_path('assets/img/items/' . $filename));
                $item->image = '/assets/img/items/' . $filename;
                Image::make($image)
                    ->resize(25, 18)
                    ->save(base_path('assets/img/items/small/' . $filename_sm));
                $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;
            }

            $item->price = $request->price;

            if ($request->is_recommended == 'true') {
                $item->is_recommended = true;
            } else {
                $item->is_recommended = false;
            }

            if ($request->is_popular == 'true') {
                $item->is_popular = true;
            } else {
                $item->is_popular = false;
            }

            if ($request->is_new == 'true') {
                $item->is_new = true;
            } else {
                $item->is_new = false;
            }

            try {
                $item->save();
                if (isset($request->addon_category_item)) {
                    $item->addon_categories()->sync($request->addon_category_item);
                }
                return redirect()->back()->with(['success' => 'Item Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function addonCategories()
    {
        $count = AddonCategory::all()->count();
        $addonCategories = AddonCategory::orderBy('id', 'DESC')->paginate(20);

        return view('admin.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddonCategories(Request $request)
    {
        $query = $request['query'];
        $addonCategories = AddonCategory::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')->paginate();
        $count = count($addonCategories);

        return view('admin.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddonCategory(Request $request)
    {
        $addonCategory = new AddonCategory();

        $addonCategory->name = $request->name;
        $addonCategory->type = $request->type;
        $addonCategory->user_id = Auth::user()->id;

        try {
            $addonCategory->save();
            return redirect()->back()->with(['success' => 'Addon Category Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function getEditAddonCategory($id)
    {
        $addonCategory = AddonCategory::where('id', $id)->first();
        return view('admin.editAddonCategory', array(
            'addonCategory' => $addonCategory,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateAddonCategory(Request $request)
    {
        $addonCategory = AddonCategory::where('id', $request->id)->first();

        if ($addonCategory) {

            $addonCategory->name = $request->name;
            $addonCategory->type = $request->type;

            try {
                $addonCategory->save();
                return redirect()->back()->with(['success' => 'Addon Category Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function addons()
    {
        $count = Addon::all()->count();
        $addons = Addon::orderBy('id', 'DESC')->paginate(20);
        $addonCategories = AddonCategory::all();

        return view('admin.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddons(Request $request)
    {
        $query = $request['query'];
        $addons = Addon::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')->paginate();
        $count = count($addons);
        $addonCategories = AddonCategory::all();

        return view('admin.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddon(Request $request)
    {
        // dd($request->all());
        $addon = new Addon();

        $addon->name = $request->name;
        $addon->price = $request->price;
        $addon->user_id = Auth::user()->id;
        $addon->addon_category_id = $request->addon_category_id;

        try {
            $addon->save();
            return redirect()->back()->with(['success' => 'Addon Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }

    }

    /**
     * @param $id
     */
    public function getEditAddon($id)
    {
        $addon = Addon::where('id', $id)->first();
        $addonCategories = AddonCategory::all();
        return view('admin.editAddon', array(
            'addon' => $addon,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateAddon(Request $request)
    {
        $addon = Addon::where('id', $request->id)->first();

        if ($addon) {

            $addon->name = $request->name;
            $addon->price = $request->price;
            $addon->addon_category_id = $request->addon_category_id;

            try {
                $addon->save();
                return redirect()->back()->with(['success' => 'Addon Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function itemcategories()
    {
        $itemCategories = ItemCategory::orderBy('id', 'DESC')->paginate(1000);
        $count = count($itemCategories);

        return view('admin.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
        ));
    }

    public function searchitemcategories(Request $request)
    {
        $query = $request['query'];
        $itemCategories = ItemCategory::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($itemCategories);

        return view('admin.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
            'query' => $query,
        ));
    }

    // public function searchitemcategories(Request $request)
    // {
    //     $query = $request['query'];
    //     $items = ItemCategory::where('name', 'LIKE', '%' . $query . '%')
    //         ->take('100')
    //         ->paginate();
    //     $count = count($items);
    //     $itemCategories = ItemCategory::where('is_enabled', '1')->get();

    //     return view('admin.itemcategories', array(
    //         'itemCategories' => $itemCategories,
    //         'count' => $count,
    //     ));
    // }

    /**
     * @param Request $request
     */
    public function createItemCategory(Request $request)
    {
        $itemCategory = new ItemCategory();

        $itemCategory->name = $request->name;
        $itemCategory->user_id = Auth::user()->id;

        try {
            $itemCategory->save();
            return redirect()->back()->with(['success' => 'Category Created']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function disableCategory($id)
    {
        $itemCategory = ItemCategory::where('id', $id)->first();
        if ($itemCategory) {
            $itemCategory->toggleEnable()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.itemcategories');
        }
    }

    public function locations()
    {
        $locations = Location::orderBy('id', 'DESC')->paginate(1000);
        $locationsAll = Location::all();
        $count = count($locationsAll);
        return view('admin.locations', array(
            'locations' => $locations,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewLocation(Request $request)
    {
        // dd($request->all());

        $location = new Location();

        $location->name = $request->name;
        $location->description = $request->description;

        if ($request->is_popular == 'true') {
            $location->is_popular = true;
        } else {
            $location->is_popular = false;
        }

        if ($request->is_active == 'true') {
            $location->is_active = true;
        } else {
            $location->is_active = false;
        }

        try {
            $location->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function disableLocation($id)
    {
        $location = Location::where('id', $id)->first();
        if ($location) {
            $location->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.locations');
        }
    }

    /**
     * @param $id
     */
    public function editLocation($id)
    {
        $location = Location::where('id', $id)->first();
        if ($location) {
            return view('admin.editLocation', array(
                'location' => $location,
            ));
        } else {
            return redirect()->route('admin.editLocation');
        }
    }

    /**
     * @param Request $request
     */
    public function updateLocation(Request $request)
    {
        $location = Location::where('id', $request->id)->first();

        if ($location) {
            $location->name = $request->name;
            $location->description = $request->description;
            if ($request->is_popular == 'true') {
                $location->is_popular = true;
            } else {
                $location->is_popular = false;
            }

            if ($request->is_active == 'true') {
                $location->is_active = true;
            } else {
                $location->is_active = false;
            }

            try {
                $location->save();
                return redirect()->back()->with(['success' => 'Operation Successful']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function pages()
    {
        $pages = Page::all();
        return view('admin.pages', array(
            'pages' => $pages,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewPage(Request $request)
    {
        $page = new Page();
        $page->name = $request->name;
        $page->slug = $request->slug;
        $page->body = $request->body;

        try {
            $page->save();
            return redirect()->back()->with(['success' => 'New Page Created']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function getEditPage($id)
    {
        $page = Page::where('id', $id)->first();

        if ($page) {
            return view('admin.editPage', array(
                'page' => $page,
            ));
        } else {
            return redirect()->route('admin.pages');
        }
    }

    /**
     * @param Request $request
     */
    public function updatePage(Request $request)
    {
        $page = Page::where('id', $request->id)->first();

        if ($page) {
            $page->name = $request->name;
            $page->slug = $request->slug;
            $page->body = $request->body;
            try {
                $page->save();
                return redirect()->back()->with(['success' => 'Page Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        } else {
            return redirect()->route('admin.pages');
        }
    }

    /**
     * @param $id
     */
    public function deletePage($id)
    {
        $page = Page::where('id', $id)->first();
        if ($page) {
            $page->delete();
            return redirect()->back()->with(['success' => 'Deleted']);
        } else {
            return redirect()->route('admin.pages');
        }
    }

    public function restaurantpayouts()
    {
        $count = RestaurantPayout::all()->count();

        $restaurantPayouts = RestaurantPayout::paginate('20');

        return view('admin.restaurantPayouts', array(
            'restaurantPayouts' => $restaurantPayouts,
            'count' => $count,
        ));
    }

    /**
     * @param $id
     */
    public function viewRestaurantPayout($id)
    {
        $restaurantPayout = RestaurantPayout::where('id', $id)->first();

        if ($restaurantPayout) {
            return view('admin.viewRestaurantPayout', array(
                'restaurantPayout' => $restaurantPayout,
            ));
        }
    }

    /**
     * @param Request $request
     */
    public function updateRestaurantPayout(Request $request)
    {
        $restaurantPayout = RestaurantPayout::where('id', $request->id)->first();

        if ($restaurantPayout) {
            $restaurantPayout->status = $request->status;
            $restaurantPayout->transaction_mode = $request->transaction_mode;
            $restaurantPayout->transaction_id = $request->transaction_id;
            $restaurantPayout->message = $request->message;
            try {
                $restaurantPayout->save();
                return redirect()->back()->with(['success' => 'Restaurant Payout Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }

        }
    }

    public function getComplaints()
    {
        $openComplaints = ComplaintReport::select('complaint_report.*', 'us.name')
            ->leftJoin('users AS us', 'us.id', '=', 'complaint_report.user_id')
            ->where('complaint_report.is_open', 0)
            ->orderBy('complaint_report.created_at', 'DESC')
            ->paginate(5);
        $closeComplaints = ComplaintReport::select('complaint_report.*', 'us.name')
            ->leftJoin('users AS us', 'us.id', '=', 'complaint_report.user_id')
            ->where('complaint_report.is_open', 1)
            ->orderBy('complaint_report.updated_at', 'DESC')
            ->take(5)
            ->get();
        $status = DB::table('issue_type')->select('*')->get();
        return view('admin.complaint', array(
            'openComplaints' => $openComplaints,
            'closeComplaints' => $closeComplaints,
            'status' => $status,
        ));
    }

    public function updateComplaintStatus(Request $request)
    {
        $complaint = ComplaintReport::where('id', $request->input('id'))->first();
        // dd($complaint->is_open == $request->input('is_open'));
        if ($complaint->is_open == $request->input('is_open')) {
            return redirect()->back()->with(['message' => 'Check the Status again']);
        }
        $complaint->is_open = $request->input('is_open');
        $complaint->save();
        return redirect()->back()->with(['success' => 'Complaint Closed']);
    }

    public function getWalletTransaction($id, $orderid, $ticketno)
    {
        // dd($id, $orderid);
        $wallet = Wallet::where('user_id', $id)->first();
        $walletId = $wallet->id;
        $user = User::where('id', $id)->first();
        $transactions = WalletTransactions::where('wallet_id', $walletId)->orderBy('updated_at', 'DESC')->take(20)->paginate(25);
        return view('admin.userWalletTransactions', array(
            'transactions' => $transactions,
            'user' => $user,
            'walletId' => $walletId,
            'orderid' => $orderid,
            'ticketno' => $ticketno,
        ));
    }

    public function refundAndUpdateComplaintStatus(Request $request)
    {
        if ($request->input('refundAmount')) {

            $wallet = Wallet::where('id', $request->input('walletId'))->first();
            $balance = $wallet->balance;
            $refund = $balance + $request->input('refundAmount');
            $wallet->balance = $refund;

            $WalletTransactions = new WalletTransactions();
            $WalletTransactions->order_reference = 'RETKT_' . $request->input('ticketno');
            $WalletTransactions->wallet_id = $request->input('walletId');
            $WalletTransactions->amount = $request->input('refundAmount');
            $WalletTransactions->transaction_type = 'R';

            try {
                $wallet->save();
                $WalletTransactions->save();
                return response()->json([
                    'success' => true,
                    'userName' => $request->input('name'),
                ], 201);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function exportOrders(Request $request)
    {
        $from = explode('T', $request->from);
        $fromDate = $from[0] . ' ' . $from[1] . ':00';

        $to = explode('T', $request->to);
        $toDate = $to[0] . ' ' . $to[1] . ':00';
        $orderList = Order::select('users.name', 'orders.unique_order_id', DB::raw('group_concat(orderitems.name, " * ", orderitems.quantity SEPARATOR ", ") as names'), 'orders.created_at', 'orders.total')
            ->whereBetween('orders.created_at', [$fromDate, $toDate])
            ->whereIn('orders.orderstatus_id', [5, 7])
            ->where('orders.restaurant_id', $request->restaurantId)
            ->leftjoin('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->groupBy('orders.id')
            ->orderBy('orders.created_at', 'asc')
            ->get();
        $total = Order::select(DB::raw("SUM(orders.total) as total"))
            ->whereBetween('orders.created_at', [$fromDate, $toDate])
            ->where('orders.restaurant_id', $request->restaurantId)
            ->whereIn('orders.orderstatus_id', [5, 7])
            ->get();
        $totalItems = Orderitem::where('orders.restaurant_id', $request->restaurantId)
            ->join('orders', 'orders.id', '=', 'orderitems.order_id')
            ->whereBetween('orders.created_at', [$fromDate, $toDate])
            ->whereIn('orders.orderstatus_id', [5, 7])
            ->count();
        $arr = collect($orderList);
        $fileName = 'applicants.xlsx';

        $arr->push([' ', 'Total Items ', $totalItems, 'Sum:', $total[0]->total]);
        return $collection = Exporter::make('Excel')->load($arr)->stream($fileName);
    }

    public function getContactUs()
    {
        $details = ContactUs::where('is_active', 1)->get();
        return response()->json([
            'success' => true,
            'mail' => $details,
        ], 201);
    }

    public function getFaq()
    {
        $details = Faq::where('is_active', 1)->get();
        return response()->json([
            'success' => true,
            'faq' => $details,
        ], 201);
    }

    public function rawData(Request $request)
    {
        $restaurants = Restaurant::get();
        if($request->restaurantId) {
            $from = explode('T', $request->from);
            $fromDate = $from[0].' '.$from[1].':00';
            $to = explode('T', $request->to);
            $toDate = $to[0].' '.$to[1].':00';

            $orderList = [];
            $arr = [];
            $arr = collect($orderList);
            if ($request->report === 'user_spend') {
                $restaurants = Order::select ('restaurants.name as shopname')
                    ->whereBetween('orders.created_at', [$fromDate,$toDate])
                    ->where('users.email', 'like', '%iimb.ac.in')
                    ->whereIn('orders.orderstatus_id', [5,7])
                    // ->leftjoin(DB::raw("(SELECT * FROM orderitems WHERE orderitems.declined = 0) as orderitems"), 'orderitems.order_id', '=', 'orders.id')
                    ->join('users', 'users.id', '=', 'orders.user_id')
                    ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
                    ->groupBy('orders.user_id')
                    ->orderBy('orders.user_id')
                    ->orderBy('orders.restaurant_id')
                    ->orderBy('orders.created_at', 'asc')
                    ->distinct()
                    ->get();
                    $header = ["Roll Number", "User Name"];
                    $shopnames = [];
                    $shop_total = [];
                    $shop_total[0] = null;
                    $shop_total[1] = null;
                    foreach ($restaurants as $data) {
                        array_push($shopnames, $data['shopname']);
                        array_push($header, $data['shopname']);
                        array_push($shop_total, 0);
                    }
                    $shop_total[count($shopnames) + 1] = 0;
                    array_push($header, "Total spend by user from $fromDate to $toDate");
                    $arr[0] = $header;
                    $users = Order::select('users.roll_number','users.id','users.name')
                        ->whereBetween('orders.created_at', [$fromDate,$toDate])
                        ->where('users.email', 'like', '%iimb.ac.in')
                        ->whereIn('orders.orderstatus_id', [5,7])
                        // ->leftjoin(DB::raw("(SELECT * FROM orderitems WHERE orderitems.declined = 0) as orderitems"), 'orderitems.order_id', '=', 'orders.id')
                        ->join('users', 'users.id', '=', 'orders.user_id')
                        ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
                        ->groupBy('orders.user_id')
                        ->orderBy('orders.user_id')
                        ->orderBy('orders.restaurant_id')
                        ->orderBy('orders.created_at', 'asc')
                        ->distinct()
                        ->get();
                    $index = 1;
                    foreach ($users as $data) {
                        $sub_total = Order::select('orders.user_id','users.name', 'restaurants.name',DB::raw("SUM(orders.total) as total"))
                            ->whereBetween('orders.created_at', [$fromDate,$toDate])
                            ->where('users.email', 'like', '%iimb.ac.in')
                            ->whereIn('orders.orderstatus_id', [5,7])
                            ->where('orders.user_id', '=', $data['id'])
                            ->join('users', 'users.id', '=', 'orders.user_id')
                            ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
                            ->groupBy('orders.user_id')
                            ->groupBy('restaurants.id')
                            ->orderBy('orders.restaurant_id')
                            ->distinct()
                            ->get();
                        $refers = array();
                        $user_id = null;
                        $grand_total = 0;
                        foreach ($sub_total as $data1) {
                            $shop_total[0] = $data['roll_number'];
                            $shop_total[1] = $data['name'];
                            for($cnt = 0; $cnt < count($shopnames); $cnt++) {
                                $user_id = $data1['user_id'];
                                if ($shopnames[$cnt] === $data1['name']) {
                                    $shop_total[$cnt + 2] = $data1['total'];
                                    $grand_total += $data1['total'];
                                }
                            }
                        }
                    $shop_total[count($shopnames) + 2] = $grand_total;
                    $arr[$index] = $shop_total;
                    $index = $index + 1;
                    $grand_total = 0;
                    $shop_total = [];
                    $shop_total[0] = null;
                    $shop_total[1] = null;
                    foreach ($shopnames as $data) {
                        array_push($shop_total, 0);
                    }
                    $shop_total[count($shopnames) + 2] = 0;
                }
            }
            else if ($request->report === 'order') {
                $orderList = Order::select('users.roll_number', 'users.name as username', 'users.phone', 'users.email', 'restaurants.name as shopname', 'orders.id', DB::raw("IFNULL(group_concat(orderitems.name, ' * ', orderitems.quantity SEPARATOR ', '), 'Paid via Wallet') as product_name "), 'orders.created_at', 'orders.total')
                    ->whereBetween('orders.created_at', [$fromDate,$toDate])
                    ->whereIn('orders.restaurant_id', $request->restaurantId)
                    ->whereIn('orders.orderstatus_id', [5,7])
                    ->where('users.email', 'like', '%iimb.ac.in')
                    // ->whereIn('orders.restaurant_id', [57, 60, 63])
                    ->leftjoin(DB::raw("(SELECT * FROM orderitems WHERE orderitems.declined = 0) as orderitems"), 'orderitems.order_id', '=', 'orders.id')
                    ->join('users', 'users.id', '=', 'orders.user_id')
                    ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
                    ->groupBy('orders.id')
                    ->orderBy('orders.created_at', 'asc')
                    ->get();
                $arr = collect($orderList);
            }
            else if ($request->report === 'vendor_sales') {
                $sales = Order::select ('restaurants.name as shopname',DB::raw("SUM(orders.total) as total"))
                    ->whereBetween('orders.created_at', [$fromDate,$toDate])
                    ->whereIn('orders.restaurant_id', $request->restaurantId)
                    ->where('users.email', 'like', '%iimb.ac.in')
                    ->whereIn('orders.orderstatus_id', [5,7])
                    ->join('users', 'users.id', '=', 'orders.user_id')
                    ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
                    ->groupBy('restaurants.id')
                    ->orderBy('restaurants.id')
                    ->orderBy('orders.restaurant_id')
                    ->orderBy('orders.created_at', 'asc')
                    ->distinct()
                    ->get();

                $header = ["Date"];
                $shopnames = [];
                $shop_total = [];
                $total_sales = ["Total Sales"];
                foreach ($sales as $sale) {
                    array_push($shopnames, $sale['shopname']);
                    array_push($header, $sale['shopname']);
                    array_push($total_sales, $sale['total']);
                }
                $arr[0] = $header;

                $st_date = $from[0];
                $end_date = $to[0];

                // $aDays = [];
                $sCurrentDate = date('Y-m-d', strtotime($st_date . ' -1 day'));
                $index = 1;
                // While the current date is less than the end date
                while($sCurrentDate < $end_date){
                    // Add a day to the current date
                    $sCurrentDate = date('Y-m-d', strtotime($sCurrentDate . ' +1 day'));
                    // Add this new day to the aDays array
                    $aDays[] = $sCurrentDate;

                    $vendors = Order::select('restaurants.name', DB::raw("DATE(orders.created_at) as created_dt"), DB::raw("SUM(orders.total) as total"))
                        ->whereBetween('orders.created_at', [$fromDate,$toDate])
                        ->where('users.email', 'like', '%iimb.ac.in')
                        ->where('orders.created_at', 'like', "$sCurrentDate%")
                        ->whereIn('orders.orderstatus_id', [5,7])
                        // ->leftjoin(DB::raw("(SELECT * FROM orderitems WHERE orderitems.declined = 0) as orderitems"), 'orderitems.order_id', '=', 'orders.id')
                        ->join('users', 'users.id', '=', 'orders.user_id')
                        ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
                        ->groupBy('created_dt')
                        ->groupBy('restaurants.id')
                        ->orderBy('orders.restaurant_id')
                        ->distinct()
                        ->get();
                    $shop_total[0] = date('d-m-Y', strtotime($sCurrentDate));
                    foreach ($sales as $sale) {
                        array_push($shop_total, 0);
                    }
                    foreach ($vendors as $data1) {
                        // dump($data1['total']);
                        for($cnt = 0; $cnt < count($shopnames); $cnt++) {
                            if ($shopnames[$cnt] === $data1['name']) {
                                $shop_total[$cnt + 1] = $data1['total'];
                            }
                        }
                    }
                    $arr[$index] = $shop_total;
                    $index = $index + 1;
                    $shop_total = [];
            }
            $arr[count($arr)] = $total_sales;
        }
        else if ($request->report === 'pending_refund') {
                $pr = WalletTransactions::select('users.roll_number', 'wallet_transactions.amount' , 'users.name', 'users.email', 'wallet_transactions.updated_at')
                    ->whereBetween('wallet_transactions.updated_at', [$fromDate,$toDate])
                    // ->where('users.email', 'like', '%iimb.ac.in')
                    ->join('wallets', 'wallets.id', '=', 'wallet_transactions.wallet_id')
                    ->join('users', 'users.id', '=', 'wallets.user_id')
                    ->where('wallet_transactions.transaction_type', 'P')
                    ->orderBy('wallet_transactions.updated_at', 'DESC')
                    ->get();

                $header = ["Roll Number", "Amount", "User Name", "Email", "Date"];
                $arr[0] = $header;
                $index = 1;
                foreach ($pr as $row) {
                  $arr[$index] = $row;
                  $index += 1;
                }
        }

        $fileName = "$request->report.xlsx";
            // Storage::disk('local')->put(Exporter::make('Excel')->load($arr)->stream($fileName), 'real_public');
            return $collection = Exporter::make('Excel')->load($arr)->stream($fileName);
        }

        return view('admin.rawData', array(
            'restaurants' => $restaurants,
        ));
    }

}
