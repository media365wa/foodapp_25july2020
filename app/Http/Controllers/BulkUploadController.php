<?php

namespace App\Http\Controllers;

use App\Wallet;
use App\WalletTransactions;
use App\Item;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use Image;
use Importer;
use App\User;

class BulkUploadController extends Controller
{
    public function restaurantBulkUpload(Request $request)
    {
        if ($request->hasFile('restaurant_csv')) {
            $filepath = $request->file('restaurant_csv')->getRealPath();
            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();
            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {
                    if ($key["name"] == "NULL") {
                        $name = null;
                    } else {
                        $name = $key["name"];
                    }
                    if ($key["description"] == "NULL") {
                        $description = null;
                    } else {
                        $description = $key["description"];
                    }
                    if ($key["location_id"] == "NULL") {
                        $location_id = null;
                    } else {
                        $location_id = $key["location_id"];
                    }
                    // if ($key["image"] == "NULL") {
                    //     $image = null;
                    // } else {
                    //     $imageName = $key["image"];
                    //     $rand_name = time() . str_random(10);
                    //
                    //     $filename = $rand_name . '.' . $imageName;
                    //     $filename_sm = $rand_name . '-sm.' . $imageName;
                    //
                    //     Image::make(public_path('assets/img/restaurants/bulk-upload/' . $imageName))
                    //         ->resize(160, 117)
                    //         ->save(public_path('assets/img/restaurants/' . $filename));
                    //
                    //     $image = "/assets/img/restaurants/" . $filename;
                    //
                    //     Image::make(public_path('assets/img/restaurants/bulk-upload/' . $imageName))
                    //         ->resize(20, 20)
                    //         ->save(public_path('assets/img/restaurants/small/' . $filename_sm));
                    //
                    //     $placeholder_image = "/assets/img/restaurants/small/" . $filename;
                    // }
                    if ($key["rating"] == "NULL") {
                        $rating = null;
                    } else {
                        $rating = $key["rating"];
                    }
                    if ($key["delivery_time"] == "NULL") {
                        $delivery_time = null;
                    } else {
                        $delivery_time = $key["delivery_time"];
                    }
                    if ($key["price_range"] == "NULL") {
                        $price_range = null;
                    } else {
                        $price_range = $key["price_range"];
                    }
                    if ($key["address"] == "NULL") {
                        $address = null;
                    } else {
                        $address = $key["address"];
                    }
                    if ($key["pincode"] == "NULL") {
                        $pincode = null;
                    } else {
                        $pincode = $key["pincode"];
                    }
                    if ($key["landmark"] == "NULL") {
                        $landmark = null;
                    } else {
                        $landmark = $key["landmark"];
                    }
                    if ($key["latitude"] == "NULL") {
                        $latitude = null;
                    } else {
                        $latitude = $key["latitude"];
                    }
                    if ($key["longitude"] == "NULL") {
                        $longitude = null;
                    } else {
                        $longitude = $key["longitude"];
                    }
                    if ($key["certificate"] == "NULL") {
                        $certificate = null;
                    } else {
                        $certificate = $key["certificate"];
                    }
                    if ($key["restaurant_charges"] == "NULL") {
                        $restaurant_charges = null;
                    } else {
                        $restaurant_charges = $key["restaurant_charges"];
                    }
                    if ($key["delivery_charges"] == "NULL") {
                        $delivery_charges = null;
                    } else {
                        $delivery_charges = $key["delivery_charges"];
                    }
                    if ($key["is_accepted"] == "NULL") {
                        $is_accepted = null;
                    } else {
                        $is_accepted = $key["is_accepted"];
                    }
                    if ($key["is_pureveg"] == "NULL") {
                        $is_pureveg = null;
                    } else {
                        $is_pureveg = $key["is_pureveg"];
                    }
                    // if ($key["is_featured"] == "NULL") {
                    //     $is_featured = null;
                    // } else {
                    //     $is_featured = $key["is_featured"];
                    // }
                    $slug = str_slug($name) . '-' . str_random(15);
                    $sku = time() . str_random(10);
                    $insert[] = [
                        'name' => $name,
                        'description' => $description,
                        'location_id' => $location_id,
                        // 'image' => $image,
                        'rating' => $rating,
                        'delivery_time' => $delivery_time,
                        'price_range' => $price_range,
                        'address' => $address,
                        'pincode' => $pincode,
                        'landmark' => $landmark,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'certificate' => $certificate,
                        'restaurant_charges' => $restaurant_charges,
                        'delivery_charges' => $delivery_charges,
                        'is_pureveg' => $is_pureveg,
                        'slug' => $slug,
                        'is_accepted' => $is_accepted,
                        // 'placeholder_image' => $placeholder_image,
                        'sku' => $sku,
                        'created_at' => date("Y-m-d H:m:s"),
                        'updated_at' => date("Y-m-d H:m:s"),
                    ];
                }
                if (!empty($insert)) {
                    try {
                        DB::table('restaurants')->insert($insert);
                        return redirect()->back()->with(['success' => 'Operation Successful']);
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
            }
        }
    }

    public function itemBulkUpload(Request $request)
    {
        if ($request->hasFile('item_csv')) {
            $filepath = $request->file('item_csv')->getRealPath();

            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {

                    if ($key["name"] == "NULL") {
                        $name = null;
                    } else {
                        $name = $key["name"];
                    }

                    if ($key["restaurant_id"] == "NULL") {
                        $restaurant_id = null;
                    } else {
                        $restaurant_id = $key["restaurant_id"];
                    }

                    if ($key["item_category_id"] == "NULL") {
                        $item_category_id = null;
                    } else {
                        $item_category_id = $key["item_category_id"];
                    }

                    if ($key["price"] == "NULL") {
                        $price = null;
                    } else {
                        $price = $key["price"];
                    }

                    if ($key["kitchen_id"] == "NULL") {
                        $kitchen_id = null;
                    } else {
                        $kitchen_id = $key["kitchen_id"];
                    }
                    if ($key["from_time"] == "NULL") {
                        $from_time = null;
                    } else {
                        $from_time = $key["from_time"];
                    }
                    if ($key["to_time"] == "NULL") {
                        $to_time = null;
                    } else {
                        $to_time = $key["to_time"];
                    }
                    if ($key["day"] == "NULL") {
                        $day = null;
                    } else {
                        $day = $key["day"];
                    }
                    if ($key["is_active"] == "NULL") {
                        $is_active = null;
                    } else {
                        $is_active = $key["is_active"];
                    }
                    if ($key["quantity"] == "NULL") {
                        $quantity = null;
                    } else {
                        $quantity = $key["quantity"];
                    }

                    if ($key["image"] == "NULL") {
                        $image = null;
                        $placeholder_image = null;
                    } else {
                        $imageName = $key["image"];
                        $rand_name = time() . str_random(10);

                        $filename = $rand_name . '.' . $imageName;
                        $filename_sm = $rand_name . '-sm.' . $imageName;

                        Image::make(public_path('assets/img/items/bulk-upload/' . $imageName))
                            ->resize(162, 118)
                            ->save(public_path('assets/img/items/' . $filename));

                        $image = "/assets/img/items/" . $filename;

                        Image::make(public_path('assets/img/items/bulk-upload/' . $imageName))
                            ->resize(25, 18)
                            ->save(public_path('assets/img/items/small/' . $filename_sm));

                        $placeholder_image = "/assets/img/items/small/" . $filename;
                    }

                    if ($key["is_recommended"] == "NULL") {
                        $is_recommended = null;
                    } else {
                        $is_recommended = $key["is_recommended"];
                    }

                    if ($key["is_popular"] == "NULL") {
                        $is_popular = null;
                    } else {
                        $is_popular = $key["is_popular"];
                    }

                    if ($key["is_new"] == "NULL") {
                        $is_new = null;
                    } else {
                        $is_new = $key["is_new"];
                    }

                    $insert[] = [
                        'name' => $name,
                        'restaurant_id' => $restaurant_id,
                        'item_category_id' => $item_category_id,
                        'price' => $price,
                        'kitchen_id' => $kitchen_id,
                        'from_time' => $from_time,
                        'to_time' => $to_time,
                        'day' => $day,
                        'is_active' => $is_active,
                        'quantity' => $quantity,
                        'image' => $image,
                        'placeholder_image' => $placeholder_image,
                        'is_recommended' => $is_recommended,
                        'is_popular' => $is_popular,
                        'is_new' => $is_new,
                        // 'created_at' => date("Y-m-d H:m:s"),
                        // 'updated_at' => date("Y-m-d H:m:s"),
                    ];
                }
                if (!empty($insert)) {
                    try {
                        DB::table('items')->insert($insert);
                        return redirect()->back()->with(['success' => 'Operation Successful']);
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
            }
        }
    }

    public function locationBulkUpload(Request $request)
    {
        if ($request->hasFile('location_csv')) {
            $filepath = $request->file('location_csv')->getRealPath();

            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {

                    if ($key["name"] == "NULL") {
                        $name = null;
                    } else {
                        $name = $key["name"];
                    }

                    if ($key["description"] == "NULL") {
                        $description = null;
                    } else {
                        $description = $key["description"];
                    }

                    if ($key["is_popular"] == "NULL") {
                        $is_popular = null;
                    } else {
                        $is_popular = $key["is_popular"];
                    }

                    if ($key["is_active"] == "NULL") {
                        $is_active = null;
                    } else {
                        $is_active = $key["is_active"];
                    }

                    $insert[] = [
                        'name' => $name,
                        'description' => $description,
                        'is_active' => $is_active,
                    ];
                }
                if (!empty($insert)) {
                    try {
                        DB::table('locations')->insert($insert);
                        return redirect()->back()->with(['success' => 'Operation Successful']);
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
            }
        }
    }

    public function itemBulkUploadFromRestaurant(Request $request)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        if ($request->hasFile('item_csv')) {
            $filepath = $request->file('item_csv')->getRealPath();

            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {

                    if ($key["name"] == "NULL") {
                        $name = null;
                    } else {
                        $name = $key["name"];
                    }

                    if ($key["restaurant_id"] == "NULL") {
                        $restaurant_id = null;
                    } else {
                        $restaurant_id = $key["restaurant_id"];
                    }

                    if ($key["item_category_id"] == "NULL") {
                        $item_category_id = null;
                    } else {
                        $item_category_id = $key["item_category_id"];
                    }

                    if ($key["price"] == "NULL") {
                        $price = null;
                    } else {
                        $price = $key["price"];
                    }

                    // if ($key["kitchen_id"] == "NULL") {
                    //     $kitchen_id = null;
                    // } else {
                    //     $kitchen_id = '0';
                    // }
                    // if ($key["from_time"] == "NULL") {
                    //     $from_time = "0000";
                    // } else {
                    //     $from_time = "0000";
                    // }
                    // if ($key["to_time"] == "NULL") {
                    //     $to_time = "2359";
                    // } else {
                    //     $to_time = "2359";
                    // }
                    // if ($key["day"] == "NULL") {
                    //     $day = null;
                    // } else {
                    //     $day = $key["day"];
                    // }
                    // if ($key["is_active"] == "NULL") {
                    //     $is_active = null;
                    // } else {
                    //     $is_active = 1;
                    // }
                    if ($key["quantity"] == "NULL") {
                        $quantity = null;
                    } else {
                        $quantity = $key["quantity"];
                    }

                    // if ($key["image"] == "NULL") {
                    //     $image = null;
                    //     $placeholder_image = null;
                    // } else {
                    //     $imageName = null;
                    //     $rand_name = time() . str_random(10);

                    //     $filename = $rand_name . '.' . $imageName;
                    //     $filename_sm = $rand_name . '-sm.' . $imageName;

                    //     Image::make(public_path('assets/img/items/bulk-upload/' . $imageName))
                    //         ->resize(162, 118)
                    //         ->save(public_path('assets/img/items/' . $filename));

                    //     $image = "/assets/img/items/" . $filename;

                    //     Image::make(public_path('assets/img/items/bulk-upload/' . $imageName))
                    //         ->resize(25, 18)
                    //         ->save(public_path('assets/img/items/small/' . $filename_sm));

                    //     $placeholder_image = null;
                    // }

                    // if ($key["is_recommended"] == "NULL") {
                    //     $is_recommended = null;
                    // } else {
                    //     $is_recommended = 1;
                    // }

                    // if ($key["is_popular"] == "NULL") {
                    //     $is_popular = null;
                    // } else {
                    //     $is_popular = 1;
                    // }

                    // if ($key["is_new"] == "NULL") {
                    //     $is_new = null;
                    // } else {
                    //     $is_new = 1;
                    // }
                    if(!empty($name) || !empty($restaurant_id) || !empty($item_category_id) || !empty($restaurant_id) ) {
                        $insert = [
                            'name' => $name,
                            'restaurant_id' => $restaurant_id,
                            'item_category_id' => $item_category_id,
                            'price' => $price,
                            'kitchen_id' => 0,
                            'from_time' => "0000",
                            'to_time' => "2359",
                            'day' => 'monday,tuesday,wednesday,thursday,friday,saturday,sunday',
                            'is_active' => 1,
                            'quantity' => $quantity,
                            'image' => '/assets/img/items/1614775403oBTNuJPIzF.png',
                            'placeholder_image' => '/assets/img/items/1614775403oBTNuJPIzF.png',
                            'is_recommended' => 1,
                            'is_popular' => 1,
                            'is_new' => 1,
                            'created_at' => date("Y-m-d H:m:s"),
                            'updated_at' => date("Y-m-d H:m:s"),
                        ];
                        DB::table('items')->insert($insert);
                    } else {
                        return redirect()->back()->with(['message' => 'Please check the data again']);
                    }
                }

                if (!empty($insert)) {
                    try {
                        return redirect()->back()->with(['success' => 'Operation Successful']);
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => $qe->getMessage()]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
            }
        }
    }

    public function walletBulkUpload(Request $request) {

        if ($request->hasFile('WalletImport_csv')) {
            $filepath = $request->file('WalletImport_csv')->getRealPath();

            $excel = Importer::make('Csv');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {

                    if ($key["user_id"] == "NULL") {
                        $userId = null;
                    } else {
                        $userId = $key["user_id"];
                    }
                    if ($key["amount"] == "NULL") {
                        $amount = null;
                    } else {
                        $amount = $key["amount"];
                    }

                    try {
                        $walletId = 0;
                        $wallet = new Wallet();
                        $WalletTransactions = new WalletTransactions();
                        $wallets = Wallet::where('user_id', $userId)->first();
                        if (!empty($wallets)) {
                            $wallets->user_id = $userId;
                            $wallets->is_active = 1;
                            $wallets->balance = $amount;
                            $wallets->save();
                            $walletId = $wallets->save();
                        }

                        $WalletTransactions = new WalletTransactions();
                        $wallets = Wallet::where('user_id', $userId)->first();
                        $today = date("md");
                        $WalletTransactions->wallet_id = $wallets->id;
                        $WalletTransactions->order_reference = 'CREDITED-ON-'.$today;
                        $WalletTransactions->amount = 13000;
                        $WalletTransactions->transaction_type = 'C';
                        $WalletTransactions->save();

                        $response = [
                            'success' => true,
                            'data' => [
                                'id' => $user->id,
                                'amount' => $amount,
                            ],
                            'running_order' => null,
                        ];

                    } catch (\Throwable $e) {
                        continue;
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
            }
            return redirect()->back()->with(['success' => 'Operation Successful']);
        }
        return redirect()->back()->with(['message' => "Upload 'WalletImport_csv' only"]);
    }
  //   public function walletBulkUpload(Request $request) {
  // if ($request->hasFile('Wallet_addition_csv')) {
    // return redirect()->back()->with(['message' => "Before user fetch"]);
    //
    //       $filepath = $request->file('Wallet_addition_csv')->getRealPath();
    //
    //       $excel = Importer::make('Csv');
    //       $excel->hasHeader(true);
    //       $excel->load($filepath);
    //       $data = $excel->getCollection();
    //
    //       if (!empty($data) && $data->count()) {
    //           foreach ($data as $key) {
    //
    //               if ($key["email"] == "NULL") {
    //                   $email = null;
    //               } else {
    //                   $email = $key["email"];
    //               }
    //               return redirect()->back()->with(['message' => "Before user fetch"]);
    //
    //               $user = User::where('email', $email)->first();
    //
    //               //loop for no email
    //               if($user) {
    //                 try {
    //                     $userId = $user->id;
    //                     $walletId = 0;
    //                     $wallet = new Wallet();
    //                     $WalletTransactions = new WalletTransactions();
    //                     $wallets = Wallet::where('user_id', $userId)->first();
    //                     if (!empty($wallets)) {
    //                         $wallets->user_id = $userId;
    //                         $wallets->is_active = 1;
    //                         $wallets->balance = 13000;
    //                         $wallets->save();
    //                         $walletId = $wallets->save();
    //                     }
    //
    //                     $WalletTransactions = new WalletTransactions();
    //                     $wallets = Wallet::where('user_id', $userId)->first();
    //                     $today = date("md");
    //                     $WalletTransactions->wallet_id = $wallets->id;
    //                     $WalletTransactions->order_reference = 'CREDITED-ON-'.$today;
    //                     $WalletTransactions->amount = 13000;
    //                     $WalletTransactions->transaction_type = 'C';
    //                     $WalletTransactions->save();
    //
    //                     $response = [
    //                         'success' => true,
    //                         'data' => [
    //                             'id' => $user->id,
    //                             'amount' => $amount,
    //                         ],
    //                         'running_order' => null,
    //                     ];
    //
    //                 } catch (\Throwable $e) {
    //                     continue;
    //               }
    //             } else {
    //                       array_push($emailNotFound, $roll_number.'-'.$email);
    //                       continue;
    //                   }
    //                 }
    //
    //                 $noMail = 'email doesnt exist: '.implode(",",$emailNotFound);
    //                 $data = json_encode($noMail);
    //                 $file = 'issues_file.txt';
    //                 $destinationPath=public_path()."/upload/";
    //                 if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
    //                 File::put($destinationPath.$file,$data);
    //
    //                 return response()->download($destinationPath.$file);
    //
    //             }
    //
    //         return redirect()->back()->with(['message' => "Uploaded successfully"]);
    //     }
    //     return redirect()->back()->with(['message' => "Something went wrong"]);
    // }

    public function disableItemsBulk(Request $request)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $itemIds = array();
        $itemCategoryIds = array();
        $disablingItems = array();
        // dd($itemCategoryIds);
        if ($request->hasFile('disable_item_csv')) {
            $filepath = $request->file('disable_item_csv')->getRealPath();

            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {

                foreach ($data as $key) {
                    if ($key["item_id"] == "") {
                        return redirect()->back()->with(['message' => "Please check the item list again"]);
                    } else {
                        array_push($itemIds,$key["item_id"]);
                    }
                    if ($key["category_id"] == "") {
                        // dd($key["category_id"]);
                        return redirect()->back()->with(['message' => "Please check the category list again"]);
                    } else {
                        // dd($key["category_id"]);
                        array_push($itemCategoryIds,$key["category_id"]);
                    }
                }
                // dd($itemIds, $itemCategoryIds);

                if($itemIds && $itemCategoryIds) {
                    $checkArray = Item::select('*')->whereIn('id', $itemIds)->whereIn('restaurant_id', $restaurantIds)->distinct()->get();

                    if(count($checkArray)) {
                        foreach ($checkArray as $key => $value) {
                            array_push($disablingItems, $value->id);
                        }
                    } else {
                        return redirect()->back()->with(['message' => "Please check the item ids again"]);
                    }
                    if (!empty($disablingItems)) {
                        try {
                            $update  = Item::whereIn('id', $disablingItems)
                                            ->update(['is_active' => false]);
                            return redirect()->back()->with(['success' => 'Operation Successful']);
                        } catch (\Illuminate\Database\QueryException $qe) {
                            return redirect()->back()->with(['message' => $qe->getMessage()]);
                        } catch (Exception $e) {
                            return redirect()->back()->with(['message' => "Something went wrong."]);
                        } catch (\Throwable $th) {
                            return redirect()->back()->with(['message' => $th]);
                        }
                    }
                } else {
                    return redirect()->back()->with(['message' => "Sheet is empty"]);
                }
            } else {
                return redirect()->back()->with(['message' => "Sheet is empty"]);
            }
        }
    }
}
