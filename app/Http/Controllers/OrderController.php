<?php
namespace App\Http\Controllers;
use App\Addon;
use App\Coupon;
use App\GpsTable;
use App\Item;
use App\Order;
use App\Orderitem;
use App\OrderItemAddon;
use App\Orderstatus;
use App\Restaurant;
use App\User;
use App\Wallet;
use App\Kitchen;
use App\WalletTransactions;
use Hashids;
use Importer;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Omnipay\Omnipay;

class OrderController extends Controller
{
    /**
     * @param Request $request
     */
    public function placeOrder(Request $request)
    {
        // dd($request->all());
        $newOrder = new Order();

        $checkingIfEmpty = Order::count();

        $lastOrder = Order::orderBy('id', 'desc')->first();

        if ($lastOrder)
        {
            $lastOrderId = $lastOrder->id;
            $newId = $lastOrderId + 1;
            $uniqueId = Hashids::encode($newId);
        }
        else
        {
            //first order
            $newId = 1;
        }

        $restaurantCode = $request['restaurantCode'];
        $houseAddress = $request['user']['data']['default_address']['house'];


        $uniqueId = Hashids::encode($newId);
        $orderId = str_pad($newId, 4, '0', STR_PAD_LEFT);
        $unique_order_id = $restaurantCode . date('md') . $orderId . $request['user']['data']['default_address']['house'];
        $newOrder->unique_order_id = $unique_order_id;

        $newOrder->user_id = $request['user']['data']['id'];

        $newOrder->orderstatus_id = '1';

        $newOrder->location = $request['location'];
        $newOrder->pickup_location_id = $request['pickup_location'];
        $full_address = $request['user']['data']['default_address']['house'] . ', ' . $request['user']['data']['default_address']['address'] . ', ' . $request['user']['data']['default_address']['landmark'];
        $newOrder->address = $full_address;

        //get restaurant charges
        $restaurant_id = $request['order'][0]['restaurant_id'];
        $restaurant = Restaurant::where('id', $restaurant_id)->first();

        $newOrder->restaurant_charge = $restaurant->restaurant_charges;
        $newOrder->delivery_charge = $restaurant->delivery_charges;

        $orderTotal = 0;
        foreach ($request['order'] as $oI)
        {
            $originalItem = Item::where('id', $oI['id'])->first();
            $orderTotal += ($originalItem->price * $oI['quantity']);

            if (isset($oI['selectedaddons']))
            {
                foreach ($oI['selectedaddons'] as $selectedaddon)
                {
                    $addon = Addon::where('id', $selectedaddon['addon_id'])->first();
                    if ($addon)
                    {
                        $orderTotal += $addon->price * $oI['quantity'];
                    }
                }
            }
        }
        $orderTotal = $orderTotal + $restaurant->restaurant_charges + $restaurant->delivery_charges;

        if ($request->coupon)
        {
            $coupon = Coupon::where('code', strtoupper($request['coupon']['code']))->first();
            if ($coupon)
            {
                $newOrder->coupon_name = $request['coupon']['code'];
                if ($coupon->discount_type == 'PERCENTAGE')
                {
                    $orderTotal = $orderTotal - (($coupon->discount / 100) * $orderTotal);
                }
                if ($coupon->discount_type == 'AMOUNT')
                {
                    $orderTotal = $orderTotal - $coupon->discount;
                }
                $coupon->count = $coupon->count + 1;
                $coupon->save();
            }
        }

        if (config('settings.taxApplicable') == 'true')
        {
            $newOrder->tax = config('settings.taxPercentage');
            $orderTotal = $orderTotal + (float)(((float)config('settings.taxPercentage') / 100) * $orderTotal);
        }

        $newOrder->total = $orderTotal;

        $newOrder->order_comment = $request['order_comment'];
        $newOrder->delivery_time = $request['delivery_time'];

        $newOrder->payment_mode = $request['method'];

        $newOrder->restaurant_id = $request['order'][0]['restaurant_id'];

        //process stripe payment
        if ($request['method'] == 'STRIPE')
        {
            //successfuly received user token
            if ($request['payment_token'])
            {
                $gateway = Omnipay::create('Stripe');
                $gateway->setApiKey(config('settings.stripeSecretKey'));

                $token = $request['payment_token']['id'];

                $response = $gateway->purchase(['amount' => $orderTotal, 'currency' => config('settings.currencyId') , 'token' => $token, 'metadata' => ['OrderId' => $unique_order_id, 'Name' => $request['user']['data']['name'], 'Email' => $request['user']['data']['email'], ], ])->send();

                if ($response->isSuccessful())
                {
                    //when success then save order
                    $newOrder->save();

                    foreach ($request['order'] as $orderItem)
                    {
                        $item = new Orderitem();
                        $item->order_id = $newOrder->id;
                        $item->item_id = $orderItem['id'];
                        $item->name = $orderItem['name'];
                        $item->quantity = $orderItem['quantity'];
                        $item->price = $orderItem['price'];
                        $item->kitchen_id = $orderItem['kitchen_id'];
                        $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                            ->select('name')
                            ->first() ['name'];
                        $item->save();

                        if (isset($orderItem['selectedaddons']))
                        {
                            foreach ($orderItem['selectedaddons'] as $selectedaddon)
                            {
                                $addon = new OrderItemAddon();
                                $addon->orderitem_id = $item->id;
                                $addon->addon_category_name = $selectedaddon['addon_category_name'];
                                $addon->addon_name = $selectedaddon['addon_name'];
                                $addon->addon_price = $selectedaddon['price'];
                                $addon->save();
                            }
                        }
                    }

                    $gps_table = new GpsTable();
                    $gps_table->order_id = $newOrder->id;
                    $gps_table->save();

                    $response = ['success' => true, 'data' => $newOrder, ];

                    return response()->json($response);

                }
                elseif ($response->isRedirect())
                {

                }
                else
                {

                    $response = ['success' => false, 'data' => null, ];

                    return response()->json($response);
                }
            }
        }

        //process paypal payment
        if ($request['method'] == 'PAYPAL' || ['method'] == 'PAYSTACK')
        {
            //successfuly received payment
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $item = new Orderitem();
                $item->order_id = $newOrder->id;
                $item->item_id = $orderItem['id'];
                $item->name = $orderItem['name'];
                $item->quantity = $orderItem['quantity'];
                $item->price = $orderItem['price'];
                $item->kitchen_id = $orderItem['kitchen_id'];
                $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                    ->select('name')
                    ->first() ['name'];
                $item->save();
                if (isset($orderItem['selectedaddons']))
                {
                    foreach ($orderItem['selectedaddons'] as $selectedaddon)
                    {
                        $addon = new OrderItemAddon();
                        $addon->orderitem_id = $item->id;
                        $addon->addon_category_name = $selectedaddon['addon_category_name'];
                        $addon->addon_name = $selectedaddon['addon_name'];
                        $addon->addon_price = $selectedaddon['price'];
                        $addon->save();
                    }
                }
            }

            $gps_table = new GpsTable();
            $gps_table->order_id = $newOrder->id;
            $gps_table->save();

            $response = ['success' => true, 'data' => $newOrder, ];

            return response()->json($response);

        }

        if ($request['method'] == 'WALLET')
        {
            $walletDetails = Wallet::where('user_id', $newOrder->user_id)
                ->first();

            //write code here to check the wallet balance and deduct
            $orderItemsIds = [];

            if ($walletDetails->balance >= $orderTotal)
            {

                //code start
                foreach ($request['order'] as $orderItem)
                {
                    $orderItemsIds[] = $orderItem['id'];
                }
                //var_dump($orderItemsIds);
                $itemsOG = Item::whereIn('id', $orderItemsIds)->get();

                foreach ($itemsOG as $itemog)
                {
                    foreach ($request['order'] as $orderItem)
                    {
                        //  echo $orderItem['quantity'];
                        //  echo $itemog->quantity ;
                        if ($itemog->id == $orderItem['id'])
                        {

                            if ($itemog->quantity >= $orderItem['quantity'])
                            {
                                // $itemsOG = Item::where('id', $orderItemsIds)->first();
                                // $itemsOG->save();
                                // $itemog->quantity =$itemog->quantity-$orderItem['quantity'];
                                $itemog['quantity'] = $itemog['quantity'] - $orderItem['quantity'];
                                //  echo $itemog->quantity ;
                                // $itemsOG->quantity=$itemog;
                                //  $itemsOG->save();
                                //  $item = new Orderitem();


                                $itemog->name = $itemog['name'];
                                $itemog->quantity = $itemog['quantity'];
                                $itemog->price = $itemog['price'];
                                $itemog->kitchen_id = $itemog['kitchen_id'];
                                $itemog->save();

                                //  $itemog->quantity = $itemog->quantity-$orderItem['quantity'];

                            }
                            else
                            {
                                $response = ['success' => false, 'data' => null, ];

                                return response()->json($response);
                            }
                        }
                    }

                }
                //code end
                $WalletTransactions = new WalletTransactions();

                //save the wallet WalletTransactions
                $WalletTransactions->wallet_id = $walletDetails->id;
                $WalletTransactions->order_reference = $unique_order_id;
                $WalletTransactions->amount = $orderTotal;
                $WalletTransactions->transaction_type = 'D';
                $WalletTransactions->save();

                $walletDetails->user_id = $newOrder->user_id;
                $walletDetails->balance = $walletDetails->balance - $orderTotal;
                $walletDetails->save();

                //when success then save order
                $newOrder->save();

                foreach ($request['order'] as $orderItem)
                {

                    $item = new Orderitem();
                    $item->order_id = $newOrder->id;
                    $item->item_id = $orderItem['id'];
                    $item->name = $orderItem['name'];
                    $item->quantity = $orderItem['quantity'];
                    $item->price = $orderItem['price'];
                    $item->kitchen_id = $orderItem['kitchen_id'];
                    $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                        ->select('name')
                        ->first() ['name'];
                    $item->save();

                    if (isset($orderItem['selectedaddons']))
                    {
                        foreach ($orderItem['selectedaddons'] as $selectedaddon)
                        {
                            $addon = new OrderItemAddon();
                            $addon->orderitem_id = $item->id;
                            $addon->addon_category_name = $selectedaddon['addon_category_name'];
                            $addon->addon_name = $selectedaddon['addon_name'];
                            $addon->addon_price = $selectedaddon['price'];
                            $addon->save();
                        }
                    }
                }

                $gps_table = new GpsTable();
                $gps_table->order_id = $newOrder->id;
                $gps_table->save();

                $response = ['success' => true, 'data' => $newOrder, ];

                return response()->json($response);

            }
            else
            {

                $response = ['success' => false, 'data' => null, ];

                return response()->json($response);
            }

        }

        if ($request['method'] == 'QRPAYMENT')
        {
            $newOrder->orderstatus_id = '0';
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $orderItemsIds[] = $orderItem['id'];
            }
            $itemsOG = Item::whereIn('id', $orderItemsIds)->get();
            foreach ($itemsOG as $itemog)
            {
                foreach ($request['order'] as $orderItem)
                {
                    if ($itemog->id == $orderItem['id'])
                    {
                        if ($itemog->quantity >= $orderItem['quantity'])
                        {
                            $itemog['quantity'] = $itemog['quantity'] - $orderItem['quantity'];
                            $itemog->name = $itemog['name'];
                            $itemog->quantity = $itemog['quantity'];
                            $itemog->price = $itemog['price'];
                            $itemog->kitchen_id = $itemog['kitchen_id'];
                            $itemog->save();
                        }
                        else {
                            $response = ['success' => false, 'data' => null, ];
                            return response()->json($response);
                        }
                    }
                }

            }
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $item = new Orderitem();
                $item->order_id = $newOrder->id;
                $item->item_id = $orderItem['id'];
                $item->name = $orderItem['name'];
                $item->quantity = $orderItem['quantity'];
                $item->price = $orderItem['price'];
                $item->kitchen_id = $orderItem['kitchen_id'];
                $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                    ->select('name')
                    ->first() ['name'];
                $item->save();

                if (isset($orderItem['selectedaddons']))
                {
                    foreach ($orderItem['selectedaddons'] as $selectedaddon)
                    {
                        $addon = new OrderItemAddon();
                        $addon->orderitem_id = $item->id;
                        $addon->addon_category_name = $selectedaddon['addon_category_name'];
                        $addon->addon_name = $selectedaddon['addon_name'];
                        $addon->addon_price = $selectedaddon['price'];
                        $addon->save();
                    }
                }
            }
        }
        //if new payment gateway is added, write elseif here
        else
        {
            $newOrder->orderstatus_id = '0';
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $item = new Orderitem();
                $item->order_id = $newOrder->id;
                $item->item_id = $orderItem['id'];
                $item->name = $orderItem['name'];
                $item->quantity = $orderItem['quantity'];
                $item->price = $orderItem['price'];
                $originalItem = Item::where('id', $item->item_id)
                    ->first();
                $item->kitchen_id = $originalItem->kitchen_id;
                $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                    ->select('name')
                    ->first() ['name'];
                $item->save();
                if (isset($orderItem['selectedaddons']))
                {
                    foreach ($orderItem['selectedaddons'] as $selectedaddon)
                    {
                        $addon = new OrderItemAddon();
                        $addon->orderitem_id = $item->id;
                        $addon->addon_category_name = $selectedaddon['addon_category_name'];
                        $addon->addon_name = $selectedaddon['addon_name'];
                        $addon->addon_price = $selectedaddon['price'];
                        $addon->save();
                    }
                }
            }

            $gps_table = new GpsTable();
            $gps_table->order_id = $newOrder->id;
            $gps_table->save();

            $response = ['success' => true, 'data' => $newOrder, ];

            return response()->json($response);
        }
    }
    /**
     * @param Request $request
     */

    public function messQr(Request $request) {
        //Initializing request parameters
        $userId = $request->u;
        $restaurant_id = $request->restaurant_id;
        $item_id = $request->item_id;
        $quantity = $request->quantity;

        //Initializing address
        $addressDetails = DB::table('addresses')
                            ->where('user_id', $userId)
                            ->get();

        // //OrderID Generation
        $lastOrder = Order::orderBy('id', 'desc')->first();
        if ($lastOrder)
        {
            $lastOrderId = $lastOrder->id;
            $newId = $lastOrderId + 1;
            $uniqueId = Hashids::encode($newId);
        }
        else
        {
            $newId = 1;
        }

        //Item Initialization
        $originalItem = Item::where('id', $item_id)->first();

        // //Restaurant Details
        $restaurant = Restaurant::where('id', $restaurant_id)->first();
        $restaurantCode = $restaurant->code;
        $uniqueId = Hashids::encode($newId);
        $orderId = str_pad($newId, 4, '0', STR_PAD_LEFT);
        $unique_order_id = $restaurantCode . date('md') . $orderId . $addressDetails[0]->house;

        // //New Order Initialization
        $newOrder = new Order();
        $newOrder->unique_order_id = $unique_order_id;
        $newOrder->user_id = $userId;
        $newOrder->orderstatus_id = '5';
        $newOrder->	deliver_status = '1';
        $newOrder->location = 'Campus';
        $newOrder->address = 'IIMB Campus';
        $newOrder->restaurant_charge = $restaurant->restaurant_charges;
        $newOrder->delivery_charge = $restaurant->delivery_charges;
        $orderTotal = 0;
        $orderTotal += ($originalItem->price * $quantity);
        $orderTotal = $orderTotal + $restaurant->restaurant_charges + $restaurant->delivery_charges;

        //Wallet initialization
        $walletDetails = Wallet::where('id', $request->w)->first();
        if ($walletDetails->balance >= $orderTotal) {

            if (config('settings.taxApplicable') == 'true')
            {
                $newOrder->tax = config('settings.taxPercentage');
                $orderTotal = $orderTotal + (float)(((float)config('settings.taxPercentage') / 100) * $orderTotal);
            }
            $newOrder->total = $orderTotal;
            $newOrder->order_comment = 'Mess Extras';
            $newOrder->delivery_time = NULL;
            $newOrder->payment_mode = 'QR Code - MESS';
            $newOrder->restaurant_id = $restaurant_id;
            $newOrder->orderstatus_id = '5';
            // if($request->payment_method == 'messQrCode') {


            if ($originalItem->quantity >= $quantity) {

                $originalItem['quantity'] = $originalItem['quantity'] - $quantity;
                $originalItem->quantity = $originalItem['quantity'];
                // $originalItem->name = $originalItem['name'];
                // $originalItem->price = $originalItem['price'];
                // $originalItem->kitchen_id = $originalItem['kitchen_id'];


                $item = new Orderitem();
                $item->order_id = $newId;
                $item->item_id = $item_id;
                $item->name = $originalItem->name;
                $item->quantity = $quantity;
                $item->price = $originalItem->price;
                $item->kitchen_id = NULL;
                $item->kitchen_name = NULL;


                $WalletTransactions = new WalletTransactions();

                //save the wallet WalletTransactions
                $WalletTransactions->wallet_id = $walletDetails->id;
                $WalletTransactions->order_reference = $unique_order_id;
                $WalletTransactions->amount = $orderTotal;
                $WalletTransactions->transaction_type = 'D';


                $walletDetails->user_id = $newOrder->user_id;
                $walletDetails->balance = $walletDetails->balance - $orderTotal;

                $gps_table = new GpsTable();
                $gps_table->order_id = $newId;

                $newOrder->save();
                $originalItem->save();
                $item->save();
                $WalletTransactions->save();
                $walletDetails->save();
                $gps_table->save();
                $data2['unique_order_id'] = $unique_order_id;
                $data2['restaurantName'] = $restaurant->name;
                $data2['itemName'] =  $originalItem->name;
                $data2['itemPrice'] = $originalItem->price;
                $data2['quantity'] = $quantity;
                $data2['total'] = $orderTotal;
                $data2['payment_mode'] = $newOrder->payment_mode;
                $data2['created_at'] = $newOrder->created_at;

                $response = ['success' => true, 'data' => $data2, 'message' => 'order_placed'];
                return response()->json($response);
            }
            else {
                $response = ['success' => false, 'message' => 'less_quantity', ];
                return response()->json($response);
            }
            // $newOrder->save();
        }
        else {
            $response = ['success' => false, 'message' => 'less_balance', ];
            return response()->json($response);
        }
    }
    /**
     * @param Request $request
     */
    public function getOrders(Request $request)
    {
        $orders = Order::where('user_id', $request->user_id)
            ->with('orderitems', 'orderitems.order_item_addons')
            ->orderBy('id', 'DESC')
            ->get();
        return response()
            ->json($orders);
    }

    /**
     * @param Request $request
     */
    public function getOrderByID($order_id)
    {
        $orders = Order::where('unique_order_id', $order_id)->get();
        return response()
            ->json($orders);
    }

    /**
     * @param Request $request
     */
    public function getOrderItems(Request $request)
    {
        $items = Orderitem::where('order_id', $request->order_id)
            ->get();
        return response()
            ->json($items);
    }

    public function getWaitingList(Request $request) {

        try {
            $statuses = array(1, 2);
            $orderLimit = Restaurant::where('id', $request->restaurantId)->get();
            $limitOrders = Order::distinct()->where('restaurant_id', $request->restaurantId)
                                ->whereIn('orderstatus_id', $statuses)
                                ->where('deliver_status',0)
                                ->count();
            if($limitOrders > $orderLimit[0]->order_limit) {
                $response = ['success' => false, 'data' => $limitOrders ];
                return response()->json($response);
            } else {
                $response = ['success' => true, 'data' => 0, 'limit' => $limitOrders ];
                return response()->json($response);
            }

        } catch (\Throwable $th) {
            $response = ['success' => false, 'data' => $th ];
            return response()->json($response);
        }  catch (\Illuminate\Database\QueryException $qe) {
            $response = ['success' => false, 'data' => 'Something went wrong. Please check your form and try again.' ];
            return response()->json($response);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function userAmountDeduction(Request $request) {
        if ($request->hasFile('deduction_csv')) {
            $filepath = $request->file('deduction_csv')->getRealPath();
            $excel = Importer::make('Csv');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();
            $lowBalance = [];
            $otherIssue = [];
            $emailNotFound = [];

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {
                    if ($key["restaurant_id"] == "NULL") {
                        $restaurant_id = null;
                    } else {
                        $restaurant_id = $key["restaurant_id"];
                    }
                    if ($key["email"] == "NULL") {
                        $email = null;
                    } else {
                        $email = $key["email"];
                    }
                    if ($key["amount"] == "NULL") {
                        $amount = null;
                    } else {
                        $amount = $key["amount"];
                    }
                    if ($key["order_comment"] == "NULL") {
                        $order_comment = null;
                    } else {
                        $order_comment = $key["order_comment"];
                    }
                    if ($key["item_id"] == "NULL") {
                        $item_id = null;
                    } else {
                        $item_id = $key["item_id"];
                    }
                    if ($key["name"] == "NULL") {
                        $username = null;
                    } else {
                        $username = $key["name"];
                    }
                    if ($key["roll_number"] == "NULL") {
                        $roll_number = null;
                    } else {
                        $roll_number = $key["roll_number"];
                    }

                    $user = User::where('email', $email)->first();

                    //loop for no email
                    if($user) {
                        $restaurant = Restaurant::where('id', $restaurant_id)->first();
                        $newOrder = new Order();

                        //Unique Order Number
                        $lastOrder = Order::orderBy('id', 'desc')->first();
                        $lastOrderId = $lastOrder->id;
                        $newId = $lastOrderId + 1;

                        //Genarate UniqueOrderId
                        $orderId = str_pad($newId, 4, '0', STR_PAD_LEFT);
                        $unique_order_id = $restaurant->code . date('md') . $orderId . $user->id;

                        //QueryElements for new order
                        $newOrder->unique_order_id = $unique_order_id;
                        $newOrder->orderstatus_id = '5';
                        $newOrder->deliver_status = '1';
                        $newOrder->user_id = $user->id;
                        $newOrder->coupon_name = '';
                        $newOrder->location = 'Campus';
                        $newOrder->address = 'IIMB';
                        $newOrder->tax = '';
                        $newOrder->restaurant_charge = '';
                        $newOrder->delivery_charge = '';
                        $newOrder->total = $amount;
                        $newOrder->order_comment = $order_comment;
                        $newOrder->delivery_time = null;
                        $newOrder->payment_mode = 'WALLET';
                        $newOrder->restaurant_id = $restaurant_id;

                        if ($newOrder->payment_mode == 'WALLET') {
                            $walletDetails = Wallet::where('user_id', $newOrder->user_id)->first();
                            if ($walletDetails->balance >= $amount)
                            {
                                //save the wallet WalletTransactions
                                $WalletTransactions = new WalletTransactions();
                                $WalletTransactions->wallet_id = $walletDetails->id;
                                $WalletTransactions->order_reference = $unique_order_id;
                                $WalletTransactions->amount = $amount;
                                $WalletTransactions->transaction_type = 'D';
                                $WalletTransactions->save();
                                $walletDetails->user_id = $newOrder->user_id;
                                $walletDetails->balance = $walletDetails->balance - $amount;
                                $walletDetails->save();

                                //save the wallet OrderItem
                                $item_name = Item::where('id', $item_id)->first();
                                $item = new Orderitem();
                                $item->order_id = $newId;
                                $item->item_id = $item_id;
                                $item->name = $item_name->name;
                                $item->quantity = '1';
                                $item->price = $amount;
                                $item->item_status = 3;
                                $item->price = $amount;
                                $item->kitchen_id = '';
                                $item->kitchen_name = null;
                                $item->save();
                            }
                            else
                            {
                                array_push($lowBalance, $roll_number.'-'.$username.'-'.$user->email.'-'.$amount);
                                continue;
                            }
                        }
                    } else {
                        array_push($emailNotFound, $roll_number.'-'.$username.'-'.$email.'-'.$amount);
                        continue;
                    }
                    try {
                        $newOrder->save();
                    } catch (\Throwable $e) {
                        array_push($otherIssue, $roll_number.'-'.$username.'-'.$user->email.'-'.$amount);
                        continue;
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }

                $balList = 'insufficient_balance: '.implode(",",$lowBalance);
                $noMail = 'email doesnt exist: '.implode(",",$emailNotFound);
                $data = json_encode($balList.$noMail);
                $file = 'issues_file.txt';
                $destinationPath=public_path()."/upload/";
                if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
                File::put($destinationPath.$file,$data);

                return response()->download($destinationPath.$file);
            }
        }
        return redirect()->back()->with(['message' => "Upload 'deduction_csv' only"]);
    }
    public function userAmountAddition(Request $request) {
          if ($request->hasFile('deduction_csv')) {
            $filepath = $request->file('deduction_csv')->getRealPath();

            $excel = Importer::make('Csv');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                $emailNotFound = [];
                foreach ($data as $key) {

                    if ($key["email"] == "NULL") {
                        $email = null;
                    } else {
                        $email = $key["email"];
                    }

                    $user = User::where('email', $email)->first();

                     //loop for no email
                    if($user) {
                      try {
                          $userId = $user->id;
                          $walletId = 0;
                          $wallet = new Wallet();
                          $WalletTransactions = new WalletTransactions();
                          $wallets = Wallet::where('user_id', $userId)->first();
                          if (!empty($wallets)) {
                              $WalletTransactions = new WalletTransactions();
                              $wallets = Wallet::where('user_id', $userId)->first();
                              $today = date("md");
                              $WalletTransactions->wallet_id = $wallets->id;
                              $WalletTransactions->order_reference = 'PENDINGREFUND-'.$today;
                              $WalletTransactions->amount = $wallets->balance;
                              $WalletTransactions->transaction_type = 'P';
                              $WalletTransactions->save();

                              $wallets->user_id = $userId;
                              $wallets->is_active = 1;
                              $wallets->balance = 13000;
                              $wallets->save();
                              $walletId = $wallets->save();
                          }

                          $WalletTransactions = new WalletTransactions();
                          $wallets = Wallet::where('user_id', $userId)->first();
                          $today = date("md");
                          $WalletTransactions->wallet_id = $wallets->id;
                          $WalletTransactions->order_reference = 'CREDITED-ON-'.$today;
                          $WalletTransactions->amount = 13000;
                          $WalletTransactions->transaction_type = 'C';
                          $WalletTransactions->save();

                          $response = [
                              'success' => true,
                              'data' => [
                                  'id' => $user->id,
                                  'amount' => $amount,
                              ],
                              'running_order' => null,
                          ];

                      } catch (\Throwable $e) {
                          continue;
                    }
                  } else {
                            array_push($emailNotFound, $email);
                            continue;
                        }
                      }

                      $noMail = 'email doesnt exist: '.implode(",",$emailNotFound);
                      $data = json_encode($noMail);
                      $file = 'issues_file.txt';
                      $destinationPath=public_path()."/upload/";
                      if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
                      File::put($destinationPath.$file,$data);

                      return response()->download($destinationPath.$file);

                  }

              return redirect()->back()->with(['message' => "Uploaded successfully"]);
          }
          return redirect()->back()->with(['message' => "Upload deduction_csv file only"]);
      }

}
