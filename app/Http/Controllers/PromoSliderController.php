<?php

namespace App\Http\Controllers;

use App\Location;
use App\PromoSlider;
use App\Slide;
use Illuminate\Http\Request;

class PromoSliderController extends Controller
{
    /**
     * @param Request $request
     */
    public function promoSlider(Request $request)
    {
        //check if admin has set promo slider for all restaurants (any location_id = 0 and is_active = 1)
        $allLocation = PromoSlider::where('location_id', 0)->where('is_active', 1)->first();

        // if present then return that for all locations
        if ($allLocation) {
            $slides = Slide::where('promo_slider_id', $allLocation->id)
                ->where('is_active', '1')
                ->get();
            return response()->json($slides);
        } else {
            // else check the location name and get the corresponding promo slider
            $location = Location::where('name', $request->location_name)->first();
            if ($location) {
                $location_id = $location->id;
                $promo_slider = PromoSlider::where('is_active', '1')
                    ->where('location_id', $location->id)
                    ->first();
                if ($promo_slider) {
                    $slides = Slide::where('promo_slider_id', $promo_slider->id)
                        ->where('is_active', '1')
                        ->get();
                    return response()->json($slides);
                }
            }
        }
    }
}
