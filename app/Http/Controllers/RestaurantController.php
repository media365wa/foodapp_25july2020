<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Item;
use App\Location;
use App\Restaurant;
use App\Order;
use App\Orderitem;
use App\ImportantNotice;
use DB;
use Exporter;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

// from update
class RestaurantController extends Controller
{
    /**
     * @param $location
     */
    public function getRestaurants($location)
    {
        // $mytime = Carbon::now();
        $mytime = date('H:i:s');
        $location = Location::where('name', $location)->first();
        $location_id = $location->id;
        $restaurants = Restaurant::where('location_id', $location_id)
            ->where('is_accepted', '1')
            ->where('is_active', '1')
            // ->where('opens_at','<=', $mytime.'.000000')
            // ->where('closed_at','>=', $mytime.'.000000')
            ->paginate(10);
        return response()->json($restaurants);
    }

    /**
     * @param $slug
     */
    public function getRestaurantInfo($slug)
    {
        $restaurant = Restaurant::where('slug', $slug)->first();
        return response()->json($restaurant);
    }
    /**
     * @param $id
     */
    public function getRestaurantInfoById($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        return response()->json($restaurant);
    }

    /**
     * @param $slug
     */
    public function getRestaurantItems($slug)
    {
        
        $t=date("Hi");
        $mytime=Carbon::now();
        $date=$mytime->toRfc850String();
        $today= substr($date, 0, strrpos($date, ","));
        // dd( $today);
        //echo $t;
        
        $restaurant = Restaurant::where('slug', $slug)->first();
        
        $recommended = Item::where('restaurant_id', $restaurant->id)->where('is_recommended', '1')
            ->where('is_active', '1')
            ->where('quantity', '>', 0)
            ->where('from_time', '<=', $t)
            ->where('to_time', '>=', $t)
            ->where('day', 'LIKE', '%' . strtolower($today) . '%' )
            ->with('addon_categories', 'addon_categories.addons')
            ->toSql();
            
            

        // $items = Item::with('add')
        $items = Item::where('restaurant_id', $restaurant->id)
            ->join('item_categories', 'items.item_category_id', '=', 'item_categories.id')
            ->where('is_enabled', '1')
            ->where('is_active', '1')
            ->where('quantity', '>', 0)
            ->where('from_time', '<=', $t)
            ->where('to_time', '>=', $t)
            ->where('day', 'LIKE', '%' . strtolower($today) . '%' )
            ->with('addon_categories', 'addon_categories.addons')
            ->get(array('items.*', 'item_categories.name as category_name'));

        $items = json_decode($items, true);

        $array = [];
        foreach ($items as $item) {
            $item['originalQuantity'] = $item['quantity'];
            $array[$item['category_name']][] = $item;
        }
        // $recommendedArray = [];
        // foreach ($recommended as $item) {
        //     $item['originalQuantity'] = $item['quantity'];
        //     $recommendedArray[] = $item;
        // }
        // $fp = fopen('/home/qwickpay/public_html/app/Http/Controllers/res.txt', 'w');
        // fwrite($fp, json_encode($array));
        // fclose($fp);
        // sleep(3);
        return response()->json(array(
            'recommended' => $restaurant,
            'items' => $array,
        ));
    }
    /**
     * @param Request $request
     */
    public function searchRestaurants(Request $request)
    {
        $location = Location::where('name', $request->location)->first();
        $location_id = $location->id;

        $query = $request->q;
        $restaurants = Restaurant::where('name', 'LIKE', "%$query%")
            ->where('location_id', $location_id)
            ->where('is_active', '1')
            ->get();
        return response()->json($restaurants);
    }

    public function selectRestaurant()
    {
        $restaurants =DB::table('restaurants')->distinct()->get();
        return response()->json($restaurants);
    }

    public function getTotalWithMaxLimit(Request $request) {
        $limitFrom = $request->maxLimitFrom . " 00:00:00";
        $restaurantId = $request->restaurantId;
        $userId = $request->userId;
        $sum = Order::select(DB::raw('sum(`total`) as sum, `restaurant_id`'))
                    ->where('restaurant_id', $restaurantId)
                    ->where('user_id', $userId)
                    ->where('created_at', '>=', $limitFrom)
                    ->get();
        return response()->json($sum);
    }
    
    public function exportOrders(Request $request)
    {   
        $from = explode('T', $request->from);
        $fromDate = $from[0].' '.$from[1].':00';

        $to = explode('T', $request->to);
        $toDate = $to[0].' '.$to[1].':00';
        $orderList = Order::select('users.name', 'orders.unique_order_id' , DB::raw("IFNULL(group_concat(orderitems.name, ' * ', orderitems.quantity SEPARATOR ', '), 'Paid via Wallet') as names "), 'orders.created_at', 'orders.total')  
                            ->whereBetween('orders.created_at', [$fromDate,$toDate])
                            ->where('orders.restaurant_id', $request->restaurantId)
                            ->whereIn('orders.orderstatus_id', [5,7])
                            ->leftjoin(DB::raw("(SELECT * FROM orderitems WHERE orderitems.declined = 0) as orderitems"), 'orderitems.order_id', '=', 'orders.id')
                            ->join('users', 'users.id', '=', 'orders.user_id')
                            ->groupBy('orders.id')
                            ->orderBy('orders.created_at', 'asc')
                            ->get();
        $total  = Order::select(DB::raw("SUM(orders.total) as total"))
                            ->whereBetween('orders.created_at', [$fromDate,$toDate])
                            ->where('orders.restaurant_id', $request->restaurantId)
                            ->whereIn('orders.orderstatus_id', [5,7])
                            ->get();
        $totalItems = Orderitem::where('orders.restaurant_id', $request->restaurantId)
                            ->join('orders', 'orders.id', '=', 'orderitems.order_id')
                            ->whereBetween('orders.created_at', [$fromDate,$toDate])
                            ->whereIn('orders.orderstatus_id', [5,7])
                            ->count();
        $arr = collect($orderList);
        $fileName = 'applicants.xlsx';
        
        $arr->push([' ','Total Items ',$totalItems,'Sum:', $total[0]->total]);
        return $collection = Exporter::make('Excel')->load($arr)->stream($fileName);
    }

    public function getNotice() {
        $notices = ImportantNotice::get();
        return response()->json(array(
             $notices
        ));
    }
}
