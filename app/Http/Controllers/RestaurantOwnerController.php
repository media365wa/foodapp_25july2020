<?php

namespace App\Http\Controllers;

use App\Addon;
use App\AddonCategory;
use App\ComplaintReport;
use App\Item;
use App\ItemCategory;
use App\Kitchen;
use App\Location;
use App\Logs;
use App\Order;
use App\Orderitem;
use App\PickupLocation;
use App\PushNotify;
use App\Restaurant;
use App\RestaurantEarning;
use App\RestaurantPayout;
use App\RestaurantUser;
use App\User;
use App\Wallet;
use App\WalletTransactions;
use Auth;
use Carbon\Carbon;
use Exception;
use Exporter;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Image;

class RestaurantOwnerController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user();

        $restaurant = $user->restaurants;

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $newOrders = Order::whereIn('orders.restaurant_id', $restaurantIds)
            ->select('*', DB::raw("concat(orderitems.name, ' * ', orderitems.quantity) as itemNames, orderitems.id as orderitemsId, orderitems.price as ip"), 'users.name as userName', 'orders.id as orderId', 'pickup_location.name as pickupLocation')
            ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->leftJoin('pickup_location', 'pickup_location.id', '=', 'orders.pickup_location_id')
            ->where('orders.orderstatus_id', '1')
            ->where('declined', 0)
            ->orderBy('orders.id', 'DESC')
            ->with('restaurant', 'restaurant.location')
            ->paginate(10);
        
        $newAllOrdersIds = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', '1')
            ->get();
        $acceptAllOrders = [];
        foreach ($newAllOrdersIds as $state) {
            array_push($acceptAllOrders, $state->id);
        }

        $newOrdersJS = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', '1')
            ->with('restaurant', 'restaurant.location')
            ->get();

        $acceptedOrders = Order::whereIn('orders.restaurant_id', $restaurantIds)
            ->select('orders.*', 'orderitems.*', DB::raw('concat(orderitems.name, " * ", orderitems.quantity ) as names'), 'orders.id as orderId', 'pickup_location.name as pickupLocation')
            ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->leftJoin('pickup_location', 'pickup_location.id', '=', 'orders.pickup_location_id')
            ->where('orderstatus_id', '2')
            ->where('declined', 0)
            ->orderBy('orders.id')
            ->paginate(10);

        $acceptedAllOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', '2')
            ->get();

        $ordersReadyArray = [];
        foreach ($acceptedAllOrders as $state) {
            array_push($ordersReadyArray, $state->id);
        }
        // dd($ordersReadyArray);
        $completedOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->select('orders.*', 'orderitems.*', DB::raw('group_concat(orderitems.name, " * ", orderitems.quantity SEPARATOR ", ") as names'), 'orders.id as orderId')
            ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->where('orderstatus_id', "5")
            ->where('deliver_status', "0")
            ->where('declined', 0)
            ->orderBy('orders.created_at', 'DESC')
            ->groupBy('orders.id')
            ->limit(10)
            ->get();

        $completedAllOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', "5")
            ->where('deliver_status', "0")
            ->get();

        $ordersDeliverArray = [];
        foreach ($completedAllOrders as $state) {
            array_push($ordersDeliverArray, $state->id);
        }

        $recentOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->take(10)
            ->get();

        $allOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', "5")
            ->where('deliver_status', "1")
            ->with('orderitems')
            ->get();

        $ordersCount = count($allOrders);

        $orderItemsCount = 0;
        foreach ($allOrders as $order) {
            $orderItemsCount += count($order->orderitems);
        }

        $allCompletedOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->whereIn('orderstatus_id', ['5', '7'])
            ->with('orderitems')
            ->get();

        $totalEarning = 0;
        settype($var, 'float');

        $displayorder = Order::with('orderitems', 'orderitems.order_item_addons')
            ->orderBy('id', 'DESC')
            ->limit(10)
            ->get();

        foreach ($allCompletedOrders as $completedOrder) {
            $totalEarning += $completedOrder->total;
        }
        if ($restaurantIds[0] == 57) {
            /*today data begin*/
            $allOrdersToday = Order::whereIn('restaurant_id', $restaurantIds)
                ->where('orderstatus_id', "5")
                ->with('orderitems')
                ->whereRaw('DATE(DATE_SUB(created_at, INTERVAL 2 HOUR)) = DATE_FORMAT(convert_tz(NOW(),"+00:00","-05:30"),"%Y-%m-%d")')
                ->get();
            $ordersCountToday = count($allOrdersToday);

            $orderItemsCountToday = 0;
            foreach ($allOrdersToday as $orderToday) {
                $orderItemsCountToday += count($orderToday->orderitems);
            }
            $allCompletedOrdersToday = Order::whereIn('restaurant_id', $restaurantIds)
                ->whereIn('orderstatus_id', ['5', '7'])
                ->with('orderitems')
                ->whereRaw('DATE(DATE_SUB(created_at, INTERVAL 2 HOUR)) = DATE_FORMAT(convert_tz(NOW(),"+00:00","-05:30"),"%Y-%m-%d")')
                ->get();
            $totalEarningToday = 0;
            settype($var, 'float');

            $displayorderToday = Order::with('orderitems', 'orderitems.order_item_addons')
                ->whereRaw('DATE(DATE_SUB(created_at, INTERVAL 2 HOUR)) = DATE_FORMAT(convert_tz(NOW(),"+00:00","-05:30"),"%Y-%m-%d")')
                ->orderBy('id', 'DESC')
                ->limit(10)
                ->get();
            foreach ($allCompletedOrdersToday as $completedOrderToday) {
                $totalEarningToday += $completedOrderToday->total;
            }
            /*today data end*/
        } else {
            /*today data begin*/

            $allOrdersToday = Order::whereIn('restaurant_id', $restaurantIds)
                ->where('orderstatus_id', "5")
                ->with('orderitems')
                ->whereRaw('Date(created_at) = CURDATE()')
                ->get();
            $ordersCountToday = count($allOrdersToday);

            $orderItemsCountToday = 0;
            foreach ($allOrdersToday as $orderToday) {
                $orderItemsCountToday += count($orderToday->orderitems);
            }
            $allCompletedOrdersToday = Order::whereIn('restaurant_id', $restaurantIds)
                ->whereIn('orderstatus_id', ['5', '7'])
                ->with('orderitems')
                ->whereRaw('Date(created_at) = CURDATE()')
                ->get();
            $totalEarningToday = 0;
            settype($var, 'float');

            $displayorderToday = Order::with('orderitems', 'orderitems.order_item_addons')
                ->whereRaw('Date(created_at) = CURDATE()')
                ->orderBy('id', 'DESC')
                ->limit(10)
                ->get();
            foreach ($allCompletedOrdersToday as $completedOrderToday) {
                $totalEarningToday += $completedOrderToday->total;
            }
            /*today data end*/
        }
        // dd($restaurantIds);

        return view('restaurantowner.dashboard', array(
            'restaurantsCount' => count($user->restaurants),
            'ordersCount' => $ordersCount,
            'orderItemsCount' => $orderItemsCount,
            'totalEarning' => $totalEarning,
            'newOrders' => $newOrders,
            'newOrdersJS' => $newOrdersJS,
            'acceptedOrders' => $acceptedOrders,
            'aCO' => $allCompletedOrders,
            'completedOrders' => $completedOrders,
            'ordersCountToday' => $ordersCountToday,
            'orderItemsCountToday' => $orderItemsCountToday,
            'totalEarningToday' => $totalEarningToday,
            'acceptAllOrders' => $acceptAllOrders,
            'restaurantIds' => $restaurantIds,
            'ordersReadyArray' => $ordersReadyArray,
            'ordersDeliverArray' => $ordersDeliverArray,
        ));
    }

    public function getNewOrders()
    {
        $user = Auth::user();

        $restaurant = $user->restaurants;

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $newOrders = Order::whereIn('orders.restaurant_id', $restaurantIds)
                    ->select('*', DB::raw("concat(orderitems.name, ' * ', orderitems.quantity) as itemNames, orderitems.id as orderitemsId, orderitems.price as ip"), 'users.name as userName', 'orders.id as orderId', 'pickup_location.name as pickupLocation')
                    ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
                    ->join('users', 'users.id', '=', 'orders.user_id')
                    ->leftJoin('pickup_location', 'pickup_location.id', '=', 'orders.pickup_location_id')
                    ->where('orders.orderstatus_id', '1')
                    ->where('declined', 0)
                    ->orderBy('orders.id', 'DESC')
                    ->with('restaurant', 'restaurant.location')
                    ->paginate(10);
        $arrIds = [];
        // dd($newOrders);
        foreach ($newOrders as $state) {
            array_push($arrIds, $state->orderId);
        }
        return response()->json([$newOrders, $arrIds]);
    }

    /**
     * @param $id
     */
    public function acceptOrder($id)
    {
        // dd($id);
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $order = Order::where('unique_order_id', $id)->whereIn('restaurant_id', $restaurantIds)->first();
        $orderid = Order::where('unique_order_id', $id)->first();
        // dd($order);
        DB::table('orderitems')
            ->where('order_id', $orderid->id)
            ->update(array('item_status' => 2));

        if ($order->orderstatus_id == '1') {
            $order->orderstatus_id = 2;
            $order->save();

            if (config('settings.enablePushNotificationOrders') == 'true') {

                //to user
                $notify = new PushNotify();
                $notify->sendPushNotification('2', $order->user_id);
            }

            return redirect()->back()->with(array('success' => 'Order Accepted'));
        } else {
            return redirect()->back()->with(array('message' => 'Something went wrong.'));
        }
    }

    public function acceptAllOrders(Request $request)
    {
        $ids = [];
        $ids = array('ids' => json_decode($request->ids));
        // dd($ids);
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $order = Order::whereIn('id', $ids['ids'])->whereIn('restaurant_id', $restaurantIds)->get();
        // $orderid = Order::where('unique_order_id', $id)->first();
        // dd($ids, $restaurantIds);
        // dd($order);
        DB::table('orderitems')
            ->whereIn('order_id', $ids['ids'])
            ->update(array('item_status' => 2));

        foreach ($order as $state) {

            if ($state->orderstatus_id == '1') {
                $state->orderstatus_id = 2;
                $state->save();
                if (config('settings.enablePushNotificationOrders') == 'true') {

                    //to user
                    $notify = new PushNotify();
                    $notify->sendPushNotification('2', $order->user_id);
                }
            } else {
                return redirect()->back()->with(array('message' => 'Something went wrong.'));
            }
        }
        return redirect()->back()->with(array('success' => 'Order Accepted'));

    }

    public function declineItem($id)
    {
        // dd($id);
        $itemId = $id;
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $orderItem = Orderitem::where('id', $itemId)->first();
        // dd($orderItem);
        $orderId = $orderItem->order_id;
        $order = Order::where('id', $orderId)->get();
        // dd($order);
        $userId = $order[0]->user_id;
        $orderItemPrice = $orderItem->price * $orderItem->quantity;
        $totalPrice = Order::where('id', $orderId)
            ->select('total')
            ->get();
        $totalPriceAfterDeduction = $totalPrice[0]->total - $orderItemPrice;

        if ($orderItem->item_status == '0') {
            $itemId = $id;
            $restaurantIds = $user->restaurants->pluck('id')->toArray();
            $orderItem = Orderitem::where('id', $itemId)->first();
            $orderId = $orderItem->order_id;
            $order = Order::where('id', $orderId)->get();

            $orderItem->item_status = 5;
            $orderItem->declined = 1;
            $orderItem->save();

            $totalPriceUpdate = Order::where('id', $orderId)
                ->update(['total' => $totalPriceAfterDeduction]);

            $userDetails = Wallet::where('user_id', $userId)->first();
            $userWalletBalance = $userDetails->balance;
            $updateUserWallet = $orderItemPrice + $userWalletBalance;
            $userDetails->balance = $updateUserWallet;
            $userDetails->save();

            $WalletTransactions = new WalletTransactions();
            $WalletTransactions->order_reference = $order[0]->unique_order_id;
            $WalletTransactions->wallet_id = $userDetails->id;
            $WalletTransactions->amount = $orderItemPrice;
            $WalletTransactions->transaction_type = 'R';
            $WalletTransactions->save();

            $totalItems = Orderitem::where('order_id', $orderId)->where('declined', '=', 0)->count();
            $totalItemsCompleted = Orderitem::where('order_id', $orderId)
                ->where('item_status', '=', '3')
                ->count();
            $totalItemsInOrder = Orderitem::where('order_id', $orderId)->count();
            $totalDeclinedItems = Orderitem::where('order_id', $orderId)->where('declined', '=', 1)->count();

            if ($totalItemsInOrder == $totalDeclinedItems) {
                $ss = Order::where('id', $orderId)
                    ->update(['orderstatus_id' => 8]);
            } else if ($totalItems == $totalItemsCompleted) {
                $ss = Order::where('id', $orderId)
                    ->update(['orderstatus_id' => 5]);
            }

            return redirect()->back()->with(array('message' => 'Item Removed'));

        } else {
            return redirect()->back()->with(array('message' => 'Something went wrong.'));
        }

    }

    public function markAllItemsAsReady(Request $request)
    {
        $ids = [];
        $ids = array('ids' => json_decode($request->ids));
        // dd($ids);
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        // dd($ids);
        $orderItemList = Orderitem::whereIn('order_id', $ids['ids'])->get();
        // dd($orderItemList);
        if (count($orderItemList) > 0) {
            Orderitem::whereIn('order_id', $ids['ids'])
                ->update(array('item_status' => 3));
            Order::whereIn('id', $ids['ids'])
                ->update(array('orderstatus_id' => 5));

            return redirect()->back()->with(array('success' => 'All Orders Completed'));
        } else {
            return redirect()->back()->with(array('message' => 'No Orders.'));
        }
        return redirect()->back()->with(array('message' => 'Something went wrong.'));
    }

    public function markAllItemsAsDelivered(Request $request)
    {
        $ids = [];
        $ids = array('ids' => json_decode($request->ids));
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $orderList = Order::whereIn('id', $ids['ids'])->get();
        // dd($ids, $orderList);
        if (count($orderList) > 0) {
            Order::whereIn('id', $ids['ids'])
                ->update(array('deliver_status' => 1));
            return redirect()->back()->with(array('success' => 'All Orders Completed'));
        } else {
            return redirect()->back()->with(array('message' => 'No Orders.'));
        }
        return redirect()->back()->with(array('message' => 'Something went wrong.'));
    }

    public function deliverOrder($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $order = Order::where('unique_order_id', $id)->whereIn('restaurant_id', $restaurantIds)->first();
        $orderid = Order::where('unique_order_id', $id)->first();

        if ($order->deliver_status == '0') {
            $order->deliver_status = 1;
            $order->save();

            return redirect()->back()->with(array('success' => 'Order Delivered'));
        } else {
            return redirect()->back()->with(array('message' => 'Something went wrong.'));
        }
    }

    public function restaurants()
    {
        $user = Auth::user();
        $restaurants = $user->restaurants;
        $locations = Location::all();
        $showChart = false;

        return view('restaurantowner.restaurants', array(
            'restaurants' => $restaurants,
            'locations' => $locations,
            'showChart' => $showChart,
        ));
    }

    /**
     * @param $id
     */
    public function getEditRestaurant($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $restaurant = Restaurant::where('id', $id)->whereIn('id', $restaurantIds)->first();
        $dateFrom = $restaurant->max_cap_from;
        $dateTo = $restaurant->max_cap_to;
        $newDateFromFormat = date("m-d-Y", strtotime($dateFrom));
        $newDateFromTo = date("m-d-Y", strtotime($dateTo));
        // dd( $newDateFromFormat, $newDateFromTo);

        if ($restaurant) {
            $locations = Location::get();

            return view('restaurantowner.editRestaurant', array(
                'restaurant' => $restaurant,
                'newDateFromFormat' => $newDateFromFormat,
                'newDateFromTo' => $newDateFromTo,
                'locations' => $locations,
            ));
        } else {
            return redirect()->route('restaurant.restaurants')->with(array('message' => 'Access Denied'));
        }

    }

    /**
     * @param $id
     */
    public function disableRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $restaurant->toggleActive()->save();
            return redirect()->back()->with(array('success' => 'Operation Successful'));
        } else {
            return redirect()->route('restaurant.restaurants');
        }
    }

//  public function searchItems(Request $request)
    //     {
    //         $user = Auth::user();

//         $restaurantIds = $user->restaurants->pluck('id')->toArray();

//         $query = $request['query'];
    //         $items = Item::whereIn('restaurant_id', $restaurantIds)
    //             ->where('name', 'LIKE', '%' . $query . '%')
    //             ->take('100')
    //             ->paginate();
    //         $count = count($items);
    //         $restaurants = Restaurant::get();
    //         $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
    //         $itemCategories = ItemCategory::where('is_enabled', '1')->get();
    //         $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

//         return view('restaurantowner.items', array(
    //             'items' => $items,
    //             'count' => $count,
    //             'restaurants' => $restaurants,
    //             'query' => $query,
    //             'kitchens' => $kitchens,
    //             'itemCategories' => $itemCategories,
    //             'addonCategories' => $addonCategories,
    //         ));
    //     }

    /**
     * @param Request $request
     */
    public function saveNewRestaurant(Request $request)
    {
        dd($request->opens_at, $request->closes_at);
        $dateFrom = $request->limitFrom;
        $dateTo = $request->limitTill;
        $newDateFromFormat = date("Y-m-d", strtotime($dateFrom));
        $newDateFromTo = date("Y-m-d", strtotime($dateTo));

        $restaurant = new Restaurant();

        $restaurant->name = $request->name;
        $restaurant->code = $request->code;
        $restaurant->max_cap = $request->limit;
        $restaurant->order_limit = $request->maxOrders;
        $restaurant->max_cap_from = $newDateFromFormat;
        $restaurant->max_cap_to = $newDateFromTo;
        $restaurant->description = $request->description;
        $restaurant->location_id = $request->location_id;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(150, 150)
            ->save(base_path('assets/img/restaurants/' . $filename));
        $restaurant->image = '/assets/img/restaurants/' . $filename;
        Image::make($image)
            ->resize(20, 20)
            ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
        $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;

        $restaurant->delivery_time = $request->delivery_time;
        $restaurant->price_range = $request->price_range;

        if ($request->is_pureveg == 'true') {
            $restaurant->is_pureveg = true;
        } else {
            $restaurant->is_pureveg = false;
        }

        $restaurant->slug = str_slug($request->name) . '-' . str_random(15);
        $restaurant->certificate = $request->certificate;

        $restaurant->address = $request->address;
        $restaurant->pincode = $request->pincode;
        $restaurant->landmark = $request->landmark;
        $restaurant->latitude = $request->latitude;
        $restaurant->longitude = $request->longitude;

        $restaurant->restaurant_charges = $request->restaurant_charges;
        $restaurant->delivery_charges = $request->delivery_charges;

        $restaurant->sku = time() . str_random(10);

        $restaurant->is_active = 0;

        try {
            $restaurant->save();
            $user = Auth::user();
            $user->restaurants()->attach($restaurant);
            return redirect()->back()->with(array('success' => 'Restaurant Saved'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     */
    public function updateRestaurant(Request $request)
    {
        $restaurant = Restaurant::where('id', $request->id)->first();
        $dateFrom = $request->limitFrom;
        $dateTo = $request->limitTill;
        $newDateFromFormat = date("Y-m-d", strtotime($dateFrom));
        $newDateFromTo = date("Y-m-d", strtotime($dateTo));
        if ($restaurant) {
            $restaurant->name = $request->name;
            $restaurant->max_cap = $request->limit;
            $restaurant->order_limit = $request->maxOrders;
            $restaurant->max_cap_from = $newDateFromFormat;
            $restaurant->max_cap_to = $newDateFromTo;
            $restaurant->description = $request->description;
            $restaurant->location_id = $request->location_id;

            if ($request->image == null) {
                $restaurant->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(150, 150)
                    ->save(base_path('assets/img/restaurants/' . $filename));
                $restaurant->image = '/assets/img/restaurants/' . $filename;
                Image::make($image)
                    ->resize(20, 20)
                    ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
                $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;
            }

            $restaurant->delivery_time = $request->delivery_time;
            $restaurant->price_range = $request->price_range;

            if ($request->is_pureveg == 'true') {
                $restaurant->is_pureveg = true;
            } else {
                $restaurant->is_pureveg = false;
            }

            $restaurant->certificate = $request->certificate;

            $restaurant->address = $request->address;
            $restaurant->pincode = $request->pincode;
            $restaurant->landmark = $request->landmark;
            $restaurant->latitude = $request->latitude;
            $restaurant->longitude = $request->longitude;

            $restaurant->restaurant_charges = $request->restaurant_charges;
            $restaurant->delivery_charges = $request->delivery_charges;

            try {
                $restaurant->save();
                return redirect()->back()->with(array('success' => 'Restaurant Saved'));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }
        }
    }

    public function itemcategories()
    {
        $itemCategories = ItemCategory::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        $count = count($itemCategories);

        return view('restaurantowner.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
        ));
    }

    public function searchitemcategories(Request $request)
    {

        $user = Auth::user();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $query = $request['query'];
        $itemCategories = ItemCategory::where('user_id', Auth::user()->id)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($itemCategories);

        return view('restaurantowner.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
            'query' => $query,
        ));
    }

    /**
     * @param Request $request
     */
    public function createItemCategory(Request $request)
    {
        $itemCategory = new ItemCategory();

        $itemCategory->name = $request->name;
        $itemCategory->user_id = Auth::user()->id;

        try {
            $itemCategory->save();
            return redirect()->back()->with(array('success' => 'Category Created'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }
    }

    /**
     * @param $id
     */
    public function disableCategory($id)
    {
        $itemCategory = ItemCategory::where('id', $id)->first();
        if ($itemCategory) {
            $itemCategory->toggleEnable()->save();
            return redirect()->back()->with(array('success' => 'Operation Successful'));
        } else {
            return redirect()->route('restaurant.itemcategories');
        }
    }

    public function items()
    {
        $user = Auth::user();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $items = Item::whereIn('restaurant_id', $restaurantIds)
        // ->where('quantity', '>', 0)
            ->orderBy('id', 'DESC')
            ->paginate(20);

        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)
            ->orderBy('id', 'DESC')
            ->get();

        $count = Item::whereIn('restaurant_id', $restaurantIds)->count();

        $restaurants = $user->restaurants;

        $itemCategories = ItemCategory::where('is_enabled', '1')
            ->where('user_id', Auth::user()->id)
            ->get();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchItems(Request $request)
    {
        $user = Auth::user();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $query = $request['query'];
        $items = Item::whereIn('restaurant_id', $restaurantIds)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($items);
        $restaurants = Restaurant::get();
        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
        $itemCategories = ItemCategory::where('is_enabled', '1')->get();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'query' => $query,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewItem(Request $request)
    {
        $user = Auth::user();
        $days = $request->input('day');
        $days = implode(',', $days);

        $item = new Item();

        $item->name = $request->name;
        $item->price = $request->price;
        if ($days == "all") {
            $days = "monday,  tuesday,  wednesday,  thursday,  friday,  saturday,  sunday";
            $item->day = $days;
        } else {
            $item->day = $days;
        }
        $item->day = $days;
        $item->restaurant_id = $request->restaurant_id;
        $item->kitchen_id = isset($request->kitchen_id) ? $request->kitchen_id : 0;
        $item->item_category_id = $request->item_category_id;
        $item->from_time = $request->from_time;
        $item->to_time = $request->to_time;
        $item->quantity = $request->quantity;
        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(162, 118)
            ->save(base_path('assets/img/items/' . $filename));
        $item->image = '/assets/img/items/' . $filename;
        Image::make($image)
            ->resize(25, 18)
            ->save(base_path('assets/img/items/small/' . $filename_sm));
        $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;

        if ($request->is_recommended == 'true') {
            $item->is_recommended = true;
        } else {
            $item->is_recommended = false;
        }

        if ($request->is_popular == 'true') {
            $item->is_popular = true;
        } else {
            $item->is_popular = false;
        }

        if ($request->is_new == 'true') {
            $item->is_new = true;
        } else {
            $item->is_new = false;
        }

        $now = now()->toDateTimeString();
        $existingLog = Logs::where('user_id', $user->id)->first();
        $arrayData = (array) $existingLog;
        // dd($request->prev_name);
        if (!$arrayData) {
            $log = new Logs();
            $log->user_id = $user->id;
            $log->restaurant_id = $request->restaurant_id;
            $logData = array(
                'action' => 'new',
                'name' => $request->prev_name . ' - ' . $request->name,
                'price' => $request->prev_price . ' - ' . $request->price,
                'quantity' => $request->prev_quantity . ' - ' . $request->quantity,
                'changedOn' => now()->toDateTimeString(),
            );
            $log->value_data = (array($logData));
            $log->save();
        } else {
            $treailingLog = $existingLog->value_data;
            $newLog = array(
                'action' => 'new',
                'name' => $request->prev_name != null ? $request->prev_name . ' - ' . $request->name : 'New Item - ' . $request->name,
                'price' => $request->prev_price != null ? $request->prev_price . ' - ' . $request->price : 'New Item - ' . $request->price,
                'quantity' => $request->prev_quantity != null ? $request->prev_quantity . ' - ' . $request->quantity : 'New Item - ' . $request->quantity,
                'changedOn' => now()->toDateTimeString(),
            );
            $newLog = ($newLog);
            array_unshift($treailingLog, $newLog);
            $affected = DB::table('logs')
                ->where('id', $existingLog->id)
                ->update(array('value_data' => json_encode($treailingLog)));
        }

        try {
            $item->save();
            if (isset($request->addon_category_item)) {
                $item->addon_categories()->sync($request->addon_category_item);
            }
            return redirect()->back()->with(array('success' => 'Item Saved'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }

    }

    /**
     * @param $id
     */
    public function getEditItem($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $item = Item::where('id', $id)
            ->whereIn('restaurant_id', $restaurantIds)
            ->first();
        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)
            ->orderBy('name', 'DESC')
            ->get();

        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();
        $restaurantUsers = RestaurantUser::select('user_id')
            ->whereIn('restaurant_id', $restaurantIds)
            ->get();
        $userIdsCategory = [];
        foreach ($restaurantUsers as $key => $value) {
            array_push($userIdsCategory, $value->user_id);
        }
        if ($item) {
            $restaurants = $user->restaurants;
            $itemCategories = ItemCategory::where('is_enabled', '1')
                ->whereIn('user_id', $userIdsCategory)
                ->get();

            return view('restaurantowner.editItem', array(
                'item' => $item,
                'restaurants' => $restaurants,
                'kitchens' => $kitchens,
                'itemCategories' => $itemCategories,
                'addonCategories' => $addonCategories,
            ));
        } else {
            return redirect()->route('restaurant.items')->with(array('message' => 'Access Denied'));
        }

    }

    /**
     * @param $id
     */
    public function disableItem($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $item = Item::where('id', $id)
            ->whereIn('restaurant_id', $restaurantIds)
            ->first();
        if ($item) {
            $item->toggleActive()->save();
            return redirect()->back()->with(array('success' => 'Operation Successful'));
        } else {
            return redirect()->route('restaurant.items');
        }
    }

    /**
     * @param Request $request
     */
    public function updateItem(Request $request)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $item = Item::where('id', $request->id)
            ->whereIn('restaurant_id', $restaurantIds)
        // ->where('quantity', '>', 0)
            ->first();
        $days = $request->input('day');
        $days = implode(',', $days);
        // dd($days);
        if ($item) {
            if ($days == "all") {
                $days = "monday,  tuesday,  wednesday,  thursday,  friday,  saturday,  sunday";
                $item->day = $days;
            } else {
                $item->day = $days;
            }
            $item->name = $request->name;
            $item->day = $days;
            $item->restaurant_id = $request->restaurant_id;
            $item->kitchen_id = isset($request->kitchen_id) ? $request->kitchen_id : 0;
            $item->item_category_id = $request->item_category_id;
            $item->from_time = $request->from_time;
            $item->to_time = $request->to_time;
            $item->quantity = $request->quantity;
            if ($request->image == null) {
                $item->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(162, 118)
                    ->save(base_path('assets/img/items/' . $filename));
                $item->image = '/assets/img/items/' . $filename;
                Image::make($image)
                    ->resize(25, 18)
                    ->save(base_path('assets/img/items/small/' . $filename_sm));
                $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;
            }

            $item->price = $request->price;

            if ($request->is_recommended == 'true') {
                $item->is_recommended = true;
            } else {
                $item->is_recommended = false;
            }

            if ($request->is_popular == 'true') {
                $item->is_popular = true;
            } else {
                $item->is_popular = false;
            }

            if ($request->is_new == 'true') {
                $item->is_new = true;
            } else {
                $item->is_new = false;
            }
            $now = now()->toDateTimeString();
            $existingLog = Logs::where('user_id', $user->id)->first();
            $arrayData = (array) $existingLog;
            if (!$arrayData) {
                $log = new Logs();
                $log->user_id = $user->id;
                $log->restaurant_id = $request->restaurant_id;
                $logData = array(
                    'action' => 'edit',
                    'name' => $request->prev_name . ' - ' . $request->name,
                    'price' => $request->prev_price . ' - ' . $request->price,
                    'quantity' => $request->prev_quantity . ' - ' . $request->quantity,
                    'changedOn' => now()->toDateTimeString(),
                );
                $log->value_data = (array($logData));
                $log->save();
            } else {

                $treailingLog = $existingLog->value_data;
                $newLog = array(
                    'action' => 'edit',
                    'name' => $request->prev_name . ' - ' . $request->name,
                    'price' => $request->prev_price . ' - ' . $request->price,
                    'quantity' => $request->prev_quantity . ' - ' . $request->quantity,
                    'changedOn' => now()->toDateTimeString(),
                );
                $newLog = ($newLog);
                array_unshift($treailingLog, $newLog);
                $affected = DB::table('logs')
                    ->where('id', $existingLog->id)
                    ->update(array('value_data' => json_encode($treailingLog)));
            }
            try {
                $item->save();
                if (isset($request->addon_category_item)) {
                    $item->addon_categories()->sync($request->addon_category_item);
                }
                return redirect()->back()->with(array('success' => 'Item Saved'));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }
        }
    }

    public function getLogs()
    {
        $user = Auth::user();
        $restaurant = $user->restaurants;
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $logs = Logs::select('logs.id', 'users.name', 'value_data')
        // ->where('user_id', $user->id)
            ->whereIn('restaurant_id', $restaurantIds)
            ->join('users', 'users.id', '=', 'logs.user_id')
            ->orderBy('logs.updated_at', 'desc')
            ->get();
        return view('restaurantowner.logs', array(
            'logs' => $logs,
        ));
    }

    public function addonCategories()
    {
        $count = AddonCategory::all()->count();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return view('restaurantowner.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddonCategories(Request $request)
    {
        $query = $request['query'];
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($addonCategories);

        return view('restaurantowner.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddonCategory(Request $request)
    {
        $addonCategory = new AddonCategory();

        $addonCategory->name = $request->name;
        $addonCategory->type = $request->type;
        $addonCategory->user_id = Auth::user()->id;

        try {
            $addonCategory->save();
            return redirect()->back()->with(array('success' => 'Addon Category Saved'));
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        } catch (Exception $e) {
            return redirect()->back()->with(array('message' => $e->getMessage()));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }
    }

    /**
     * @param $id
     */
    public function getEditAddonCategory($id)
    {
        $addonCategory = AddonCategory::where('id', $id)->first();
        if ($addonCategory) {
            if ($addonCategory->user_id == Auth::user()->id) {
                return view('restaurantowner.editAddonCategory', array(
                    'addonCategory' => $addonCategory,
                ));
            } else {
                return redirect()
                    ->route('restaurant.editAddonCategory')
                    ->with(array('message' => 'Access Denied'));
            }
        } else {
            return redirect()
                ->route('restaurant.editAddonCategory')
                ->with(array('message' => 'Access Denied'));
        }

    }

    /**
     * @param Request $request
     */
    public function updateAddonCategory(Request $request)
    {
        $addonCategory = AddonCategory::where('id', $request->id)->first();

        if ($addonCategory) {

            $addonCategory->name = $request->name;
            $addonCategory->type = $request->type;

            try {
                $addonCategory->save();
                return redirect()->back()->with(array('success' => 'Addon Category Updated'));
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
            } catch (Exception $e) {
                return redirect()->back()->with(array('message' => $e->getMessage()));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }
        }
    }

    public function addons()
    {
        $addons = Addon::where('user_id', Auth::user()->id)->paginate();
        $count = Addon::where('user_id', Auth::user()->id)->count();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddons(Request $request)
    {
        $query = $request['query'];
        $addons = Addon::where('user_id', Auth::user()->id)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($addons);
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddon(Request $request)
    {
        $addon = new Addon();

        $addon->name = $request->name;
        $addon->price = $request->price;
        $addon->user_id = Auth::user()->id;
        $addon->addon_category_id = $request->addon_category_id;

        try {
            $addon->save();
            return redirect()->back()->with(array('success' => 'Addon Saved'));
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        } catch (Exception $e) {
            return redirect()->back()->with(array('message' => $e->getMessage()));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }

    }

    /**
     * @param $id
     */
    public function getEditAddon($id)
    {
        $addon = Addon::where('id', $id)
            ->where('user_id', Auth::user()->id)
            ->first();

        if ($addon) {
            return view('restaurantowner.editAddon', array(
                'addon' => $addon,
            ));
        } else {
            return redirect()->route('restaurant.addons')->with(array('message' => 'Access Denied'));
        }
    }

    /**
     * @param Request $request
     */
    public function updateAddon(Request $request)
    {
        $addon = Addon::where('id', $request->id)->first();

        if ($addon) {
            if ($addon->user_id == Auth::user()->id) {
                $addon->name = $request->name;
                $addon->price = $request->price;
                $addon->addon_category_id = $request->addon_category_id;

                try {
                    $addon->save();
                    return redirect()->back()->with(array('success' => 'Addon Updated'));
                } catch (\Illuminate\Database\QueryException $qe) {
                    return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
                } catch (Exception $e) {
                    return redirect()->back()->with(array('message' => $e->getMessage()));
                } catch (\Throwable $th) {
                    return redirect()->back()->with(array('message' => $th));
                }
            } else {
                return redirect()->route('restaurant.addons')->with(array('message' => 'Access Denied'));
            }
        } else {
            return redirect()->route('restaurant.addons')->with(array('message' => 'Access Denied'));
        }
    }

    public function orders()
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $itemSold = Orderitem::select('name', DB::raw("SUM(quantity) as totalQuantity"), 'price')
            ->where('item_status', 3)
            ->where('declined', 0)
            ->groupBy('item_id')
            ->orderBy('totalQuantity', 'desc')
            ->limit(10)
            ->get();
        // dd($restaurantIds);
        $count = Order::whereIn('restaurant_id', $restaurantIds)->count();
        $orders = Order::orderBy('id', 'DESC')
            ->whereIn('restaurant_id', $restaurantIds)
            ->with('accept_delivery.user')
            ->paginate(10);
        return view('restaurantowner.orders', array(
            'orders' => $orders,
            'count' => $count,
            'itemSold' => $itemSold,
            'restaurantId' => $restaurantIds,
        ));
    }

    /**
     * @param Request $request
     */
    public function postSearchOrders(Request $request)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $query = $request['query'];
        $orders = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('unique_order_id', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($orders);
        return view('restaurantowner.orders', array(
            'orders' => $orders,
            'count' => $count,
        ));

    }

    /**
     * @param $order_id
     */
    public function viewOrder($order_id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $order = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('unique_order_id', $order_id)
            ->first();

        $orderIds = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('unique_order_id', $order_id)
            ->get();
        $orderId = $orderIds[0]->id;
        $declinedItems = Orderitem::where('order_id', $orderId)
            ->where('declined', 1)
            ->get();
        $orderedBy = User::select('users.name', 'users.id', 'users.email', 'users.phone')->where('id', $order->user_id)->get();
        // dd($orderedBy);
        if ($order) {
            return view('restaurantowner.viewOrder', array(
                'order' => $order,
                'orderedBy'=> $orderedBy,
                'declinedItems' => $declinedItems,
            ));
        } else {
            return redirect()->route('restaurantowner.orders');
        }
    }

    public function viewOrderById($order_id)
    {
        $user = Auth::user();
        // $restaurantIds = $user->restaurants->pluck('id')->toArray();
        try {
            $order = Order::where('unique_order_id', $order_id)
                ->get();
            $orderId = $order[0]->id;
            $orderItems = Order::where('orders.id', $orderId)
                ->leftJoin('orderitems', 'orderitems.order_id', '=', 'orders.id')
                ->get();
            if (count($order) > 0) {
                return response()->json($orderItems);

            } else {
                $response = ['success' => false, 'data' => 'Try Again'];
                return response()->json($response, 201);
            }
        } catch (\Illuminate\Database\QueryException $qe) {
            return response()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        } catch (Exception $e) {
            return response()->json('No items found', 201);
        } catch (\Throwable $th) {
            return response()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        }
    }
    /**
     * @param $restaurant_id
     */
    public function earnings(Request $request)
    {

        $restaurant_id = $request->restaurant_id;
        $showChart = false;
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $restaurant = Restaurant::where('id', $restaurantIds)->first();
        // dd($restaurant);
        $balance = RestaurantEarning::where('restaurant_id', $restaurant->id)
            ->first();

        $allCompletedOrders = Order::where('restaurant_id', $restaurant->id)
            ->whereIn('orderstatus_id', [5, 7])
            ->whereRaw('Date(created_at) = CURDATE()')
        // ->orderBy("created_at", 'desc')
        // ->take(10)
            ->get();

        $totalEarning = 0;
        settype($var, 'float');

        foreach ($allCompletedOrders as $completedOrder) {
            $totalEarning += $completedOrder->total;
        }

        $latestTenOrders = Order::where('restaurant_id', $restaurant->id)
            ->whereIn('orderstatus_id', [5, 7])
            ->orderBy("created_at", 'desc')
            ->take(10)
            ->get();
        // dd($latestTenOrders);

        // $daysChartOffline = Order::where('restaurant_id',$restaurantIds)
        //                         ->where('orderstatus_id',  5)
        //                         // ->where(DB::raw('LEFT(`created_at`, 10)'),'=', date("Y-m-d", time()))
        //                         ->where('created_at', ">", DB::raw('NOW() - INTERVAL 1 WEEK'))
        //                         // ->where('payment_mode', 'QRPAYMENT')
        //                         ->sum('total');
        // ->toSql();

        $balanceBeforeCommission = $totalEarning;
        // dd($balanceBeforeCommission);
        $balanceAfterCommission = ($totalEarning - ($restaurant->commission_rate / 100) * $totalEarning);
        $balanceAfterCommission = number_format((float) $balanceAfterCommission, 2, '.', '');

        // $chartDataPresentDay = '[';
        // $chartDataPresentDay .= '{value:' . $daysChartOnline . ", name: `Online`}," . '{value:'. $daysChartOffline . ", name: `Offline`}";
        // $chartDataPresentDay .= ']';
        // dd();

        if ($restaurant_id) {
            // dd($request);
            $user = Auth::user();
            $restaurant = $user->restaurants;
            $restaurantIds = $user->restaurants->pluck('id')->toArray();

            $restaurant = Restaurant::where('id', $restaurant_id)->first();
            // check if restaurant exists
            if ($restaurant) {
                $dateRange = $request->datetimes;
                $from1 = explode("-", $dateRange)[0];
                $from = substr($from1, 0, -1);
                $to1 = explode("-", $dateRange)[1];
                $to = substr($to1, 1);
                // dd($from, $to);

                $showChart = true;
                //check if restaurant belongs to the auth user
                $contains = Arr::has($restaurantIds, $restaurant->id);
                // if ($contains) {
                //true
                $allCompletedOrders = Order::where('restaurant_id', $restaurant->id)
                    ->where('orderstatus_id', '5')
                    ->get();

                $totalEarning = 0;
                settype($var, 'float');

                foreach ($allCompletedOrders as $completedOrder) {
                    $totalEarning += $completedOrder->total;
                }

                // Build an array of the dates we want to show, oldest first
                $dates = collect();
                foreach (range(-30, 0) as $i) {
                    $date = Carbon::now()->addDays($i)->format('Y-m-d');
                    $dates->put($date, 0);
                }

                // Get the post counts
                $posts = Order::where('restaurant_id', $restaurant->id)
                    ->where('orderstatus_id', '5')
                    ->where('created_at', '>=', $dates->keys()->first())
                    ->groupBy('date')
                    ->orderBy('date')
                    ->get([
                        DB::raw('DATE( created_at ) as date'),
                        DB::raw('SUM( total ) as "total"'),
                    ])
                    ->pluck('total', 'date');

                // Merge the two collections; any results in `$posts` will overwrite the zero-value in `$dates`
                $dates = $dates->merge($posts);

                // dd($dates);
                $monthlyDate = '[';
                $monthlyEarning = '[';
                foreach ($dates as $date => $value) {
                    $monthlyDate .= "'" . $date . "' ,";
                    $monthlyEarning .= "'" . $value . "' ,";
                }

                $monthlyDate = rtrim($monthlyDate, ' ,');
                $monthlyDate = $monthlyDate . ']';

                $monthlyEarning = rtrim($monthlyEarning, ' ,');
                $monthlyEarning = $monthlyEarning . ']';
                /*=====  End of Monthly Post Analytics  ======*/

                $balance = RestaurantEarning::where('restaurant_id', $restaurant->id)
                    ->where('is_requested', "=", 0)
                    ->first();

                $totalFromOnline = Order::where('restaurant_id', $restaurant->id)
                    ->where('orderstatus_id', '=', 5)
                    ->whereBetween('created_at', [$from, $to])
                    ->where('payment_mode', 'WALLET')
                    ->sum('total');

                $totalFromOffline = Order::where('restaurant_id', $restaurant->id)
                    ->where('orderstatus_id', '=', 5)
                    ->whereBetween('created_at', [$from, $to])
                    ->where('payment_mode', 'QRPAYMENT')
                    ->sum('total');

                // $chartData =
                $chartData = '[';
                $chartData .= '{value:' . $totalFromOnline . ", name: `Online`}," . '{value:' . $totalFromOffline . ", name: `Offline`}";
                $chartData .= ']';
                // dd($chartData);

                // if (!$balance) {
                //     $balanceBeforeCommission = 0;
                //     $balanceAfterCommission = 0;
                // } else {
                $balanceBeforeCommission = $totalFromOnline + $totalFromOffline;
                $balanceAfterCommission = (($totalFromOnline + $totalFromOffline) - ($restaurant->commission_rate / 100) * ($totalFromOnline + $totalFromOffline));
                $balanceAfterCommission = number_format((float) $balanceAfterCommission, 2, '.', '');
                // }

                $payoutRequests = RestaurantPayout::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->get();

                return view('restaurantowner.earnings', array(
                    '$restaurant_id' => $restaurant_id,
                    'restaurant' => $restaurant,
                    'totalEarning' => $totalEarning,
                    'monthlyDate' => $monthlyDate,
                    'monthlyEarning' => $monthlyEarning,
                    'balanceBeforeCommission' => $balanceBeforeCommission,
                    'balanceAfterCommission' => $balanceAfterCommission,
                    'payoutRequests' => $payoutRequests,
                    'chartData' => $chartData,
                    'showChart' => $showChart,
                    'fromDate' => $from,
                    'toDate' => $to,
                ));
                // } else {
                //     return redirect()->route('restaurant.earnings')->with(array('message' => 'Access D'));
                // }
            } else {
                return redirect()->route('restaurant.earnings')->with(array('message' => 'Access Denied'));
            }

        } else {
            $user = Auth::user();
            $restaurants = $user->restaurants;

            return view('restaurantowner.earnings', array(
                'restaurants' => $restaurants,
                // 'chartDataPresentDay' => $chartDataPresentDay,
                'balanceBeforeCommission' => $balanceBeforeCommission,
                'balanceAfterCommission' => $balanceAfterCommission,
                'totalEarning' => $totalEarning,
                'latestTenOrders' => $latestTenOrders,
            ));
        }

    }

    public function postearningsDateRange(Request $request)
    {

    }

    /**
     * @param Request $request
     */
    public function sendPayoutRequest(Request $request)
    {
        $user = Auth::user();
        $restaurant = DB::table('restaurants')->where('id', $request->restaurant_id)->first();

        $earning = RestaurantEarning::where('restaurant_id', $request->restaurant_id)
            ->where('is_requested', 0)
            ->first();

        $balance = RestaurantEarning::where('restaurant_id', $request->restaurant_id)
            ->where('is_requested', 0)
            ->first();

        if ($earning) {
            $payoutRequest = new RestaurantPayout;
            $payoutRequest->restaurant_id = $request->restaurant_id;
            $payoutRequest->restaurant_earning_id = $earning->id;
            $payoutRequest->amount = ($balance->amount - ((float) ($restaurant->commission_rate) / 100) * $balance->amount);
            $payoutRequest->status = 'PENDING';
            try {
                $payoutRequest->save();
                $earning->is_requested = 1;
                $earning->restaurant_payout_id = $payoutRequest->id;
                $earning->save();
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
            } catch (Exception $e) {
                return redirect()->back()->with(array('message' => $e->getMessage()));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }

            return redirect()->back()->with(array('success' => 'Payout Request Sent'));
        } else {
            return redirect()->route('restaurant.earnings')->with(array('message' => 'Access Denied'));
        }
    }

    public function pos()
    {
        $user = Auth::user();
        $t = date("Hi");
        $mytime = Carbon::now();
        $date = $mytime->toRfc850String();
        $today = substr($date, 0, strrpos($date, ","));
        // dd(strtolower($today));

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        //$userIds = $user->users->pluck('id')->toArray();

        $items = Item::whereIn('restaurant_id', $restaurantIds)
            ->where('is_active', 1)
            ->where('quantity', '>', 0)
            ->where('from_time', '<=', $t)
            ->where('to_time', '>=', $t)
            ->where('day', 'LIKE', '%' . strtolower($today) . '%')
            ->orderBy('id', 'DESC')
            ->get();

        $displayUsers = User::select('users.name', 'users.id', 'users.email', 'users.auth_token', 'ad.house')
            ->leftJoin('restaurant_user AS ru', 'ru.user_id', '=', 'users.id')
            ->leftJoin('model_has_roles AS mr', 'mr.model_id', '=', 'users.id')
            ->leftJoin('addresses AS ad', 'ad.user_id', '=', 'users.id')
            ->where('mr.role_id', '4')
            ->whereNull('ru.user_id')
            ->get();

        $displayResName = Restaurant::all();

        $displayItemCategory = Item::join('item_categories', 'item_categories.id', '=', 'items.item_category_id')
            ->whereIn('items.restaurant_id', $restaurantIds)
            ->select('item_categories.id', 'item_categories.name')
            ->distinct('item_categories.id')
            ->get();

        $displayUniqueOrderID = Order::all();

        $displayWalletBalance = Wallet::all();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $restaurantC = $user->restaurants->pluck('code')->toArray();
        $restaurantIdFirst = head($restaurantIds);
        $restaurantCode = head($restaurantC);

        $kitchen = Kitchen::get();
        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
        $kitchenIds = Kitchen::whereIn('restaurant_id', $restaurantIds)->select('id')->get()->toArray();

        $count = Order::whereIn('restaurant_id', $restaurantIds)->count();
        $orders = Order::orderBy('id', 'DESC')
            ->whereIn('restaurant_id', $restaurantIds)
            ->with('accept_delivery.user')
            ->paginate('20');

        return view('restaurantowner.pos', array(
            'items' => $items,
            'orders' => $orders,
            'count' => $count,
            'users' => $displayUsers,
            'restaurants' => $displayResName,
            'itemCategories' => $displayItemCategory,
            'unique_order_id' => $displayUniqueOrderID,
            'walletbalance' => $displayWalletBalance,
            'restaurantIdFirst' => $restaurantIdFirst,
            'kitcheniId' => $kitchenIds,
            'restaurantCode' => $restaurantCode,
        ));
    }
    public function getComplaints()
    {
        $user = Auth::user();
        $restaurantId = RestaurantUser::where('user_id', $user->id)->first();
        $id = $restaurantId->restaurant_id;
        $openComplaintsMainArray = ComplaintReport::select('complaint_report.*', 'us.name')
            ->leftJoin('users AS us', 'us.id', '=', 'complaint_report.user_id')
            ->where('complaint_report.is_open', 0)
            ->orderBy('complaint_report.created_at', 'DESC')
            ->get();
        $filteredOpenComplaints = $user->restaurants->filter(function ($value) use ($id) {
            return $value['id'] == $id;
        });
        $restaurantCode = $filteredOpenComplaints[0]->code;

        $openComplaints = $openComplaintsMainArray->filter(function ($value) use ($restaurantCode) {
            $itemArrayCode = substr($value->unique_order_id, 0, 2);
            return $itemArrayCode == $restaurantCode;
        });
        $closeComplaintsMainArray = ComplaintReport::select('complaint_report.*', 'us.name')
            ->leftJoin('users AS us', 'us.id', '=', 'complaint_report.user_id')
            ->where('complaint_report.is_open', 1)
            ->take(5)
            ->get();
        $filteredClosedComplaints = $user->restaurants->filter(function ($value) use ($id) {
            return $value['id'] == $id;
        });
        $restaurantCode = $filteredClosedComplaints[0]->code;
        $closeComplaints = $closeComplaintsMainArray->filter(function ($value) use ($restaurantCode) {
            $itemArrayCode = substr($value->unique_order_id, 0, 2);
            return $itemArrayCode == $restaurantCode;
        });

        return view('restaurantowner.complaint', array(
            'openComplaints' => $openComplaints,
            'closeComplaints' => $closeComplaints,
        ));
    }

    public function downloadAllItems()
    {
        $user = Auth::user();
        $restaurantId = RestaurantUser::where('user_id', $user->id)->first();
        $itemsList = Item::select('items.id', 'items.item_category_id', 'items.name', 'items.price', DB::raw("(CASE WHEN items.is_active='1' THEN 'Enabled' ELSE 'Disabled' END) as is_active"))
            ->whereIn('items.restaurant_id', $restaurantId)
            ->get();
        $fileName = 'items.xlsx';
        $arr = collect($itemsList);
        // $arr->push([' ', 'Total Items ', $totalItems, 'Sum:', $total[0]->total]);
        return $collection = Exporter::make('Excel')->load($arr)->stream($fileName);
    }

    public function downloadAllItemsCategories()
    {
        $user = Auth::user();
        $restaurantId = RestaurantUser::where('user_id', $user->id)->first();
        $itemCategories = ItemCategory::select('id', 'name', DB::raw("(CASE WHEN is_enabled='1' THEN 'Enabled' ELSE 'Disabled' END) as is_active"))
            ->where('is_enabled', '1')
            ->where('user_id', Auth::user()->id)
            ->get();
        $fileName = 'items.xlsx';
        $arr = collect($itemCategories);
        // $arr->push([' ', 'Total Items ', $totalItems, 'Sum:', $total[0]->total]);
        return $collection = Exporter::make('Excel')->load($arr)->stream($fileName);
    }

    public function generateQrcode()
    {
        $user = Auth::user();
        $restaurants = $user->restaurants;
        $items = Item::where('restaurant_id', $restaurants[0]->id)
            ->where('is_active', 1)
            ->orderBy('id', 'DESC')
            ->get();
        return view('restaurantowner.generateQrcode', array(
            'restaurants' => $restaurants,
            'items' => $items,
        ));
    }

    public function deliverySettings()
    {
        $user = Auth::user();
        $restaurants = $user->restaurants;
        $pickupLocation = PickupLocation::where('restaurant_id', $restaurants[0]->id)->get();
        return view('restaurantowner.deliverySettings', array(
            'pickupLocationData' => $pickupLocation,
        ));
    }

    public function togglePickupLocation(Request $request)
    {
        $pickupLocation = PickupLocation::where('id', $request->id)->first();
        if ($pickupLocation->is_active == 1) {
            $is_active = PickupLocation::where('id', $request->id)->update(['is_active' => 0]);
            $success = true;
            return response()->json($success, 200);
        } else if ($pickupLocation->is_active == 0) {
            $is_active = PickupLocation::where('id', $request->id)->update(['is_active' => 1]);
            $success = true;
            return response()->json($success, 200);
        } else {
            $success = false;
            return response()->json($success, 401);
        }
    }
    public function getPickupLocation(Request $request)
    {

        $pickupLocation = PickupLocation::where('restaurant_id', $request->restaurant_id)
            ->where('is_active', 1)
            ->get();

        $response = ['success' => true, 'data' => $pickupLocation];
        return response()->json($response, 200);
    }
}
