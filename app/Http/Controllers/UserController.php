<?php
namespace App\Http\Controllers;

use App\Errorlog;
use App\ComplaintReport;
use App\User;
use App\Wallet;
use App\WalletTransactions;
use App\Otp;
use DB;
use Importer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use JWTAuthException;

class UserController extends Controller
{
    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    private function getToken($email, $password)
    {
        $token = null;
        //$credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt(['email' => $email, 'password' => $password])) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'Password or email is invalid..',
                    'token' => $token,
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'Token creation failed',
            ]);
        }
        return $token;
    }
    /** save error log
     * @param $user
     * @param $error
     */
    public function saveErrorLog($user, $error)
    {
      $errorlog = new Errorlog();
      $errorlog->user = $user;
      $errorlog->error = $error;
      $errorlog->save();
    }
    /**
     * @param Request $request
     */
    public function login(Request $request)
    {
        $user = \App\User::where('email', $request->email)->get()->first();
        $isActive = null;
        if($user)
          $isActive = $user->is_active;
        else {
          $response = ['success' => false, 'data' => 'Email is invalid'];

          self::saveErrorLog($request->email, "email invalid");

          return response()->json($response, 201);
        }

        if($isActive == 1 ) {
            if ($request->has('password')) {
                if ($user && \Hash::check($request->password, $user->password)) // The passwords match...
                {
                    $token = self::getToken($request->email, $request->password);
                    $user->auth_token = $token;
                    $user->save();
                    if ($user->default_address_id !== 0) {
                        $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                    } else {
                        $default_address = null;
                    }

                    $running_order = \App\Order::where('user_id', $user->id)->where('orderstatus_id', '1')->first();

                    $response = [
                        'success' => true,
                        'data' => [
                            'id' => $user->id,
                            'auth_token' => $user->auth_token,
                            'name' => $user->name,
                            'email' => $user->email,
                            'phone' => $user->phone,
                            'default_address_id' => $user->default_address_id,
                            'default_address' => $default_address,
                            'delivery_pin' => $user->delivery_pin,
                            'roll_number' => $user->roll_number,
                        ],
                        'isActive' => $user->is_active,
                        'running_order' => $running_order,
                    ];
                } else {
                    $response = ['success' => false, 'data' => 'Password is invalid'];
                    self::saveErrorLog($request->email, "Password invalid");
        }
            } else if ($request->has('accessToken')) {
                //if user exists with the email address, update the user JWT token
                if ($user) {
                    $token = JWTAuth::fromUser($user);
                    $user->auth_token = $token;
                    $user->save();
                    if ($user->default_address_id !== 0) {
                        $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                    } else {
                        $default_address = null;
                    }

                    $running_order = \App\Order::where('user_id', $user->id)->where('orderstatus_id', '1')->first();
                    $response = [
                        'success' => true,
                        'data' => [
                            'id' => $user->id,
                            'auth_token' => $user->auth_token,
                            'name' => $user->name,
                            'email' => $user->email,
                            'phone' => $user->phone,
                            'default_address_id' => $user->default_address_id,
                            'default_address' => $default_address,
                            'delivery_pin' => $user->delivery_pin,
                            'roll_number' => $user->roll_number,
                        ],
                        'isActive' => $user->is_active,
                        'running_order' => $running_order,
                    ];
                }
                //if user doesnt exists, create the user.
                else {
                    $user = new User();
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = \Hash::make(str_random(8));
                    $user->delivery_pin = strtoupper(str_random(5));

                    try {
                        $user->save();
                        $user->assignRole('Customer');
                        $token = JWTAuth::fromUser($user);
                        $user->auth_token = $token;
                        $user->save();

                    } catch (\Throwable $e) {
                        $response = ['success' => false, 'data' => 'Something went wrong. Please try again.'];
                        return response()->json($response, 201);
                    }

                    if ($user->default_address_id !== 0) {
                        $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                    } else {
                        $default_address = null;
                    }

                    $running_order = \App\Order::where('user_id', $user->id)->where('orderstatus_id', '1')->first();

                    $response = [
                        'success' => true,
                        'data' => [
                            'id' => $user->id,
                            'auth_token' => $user->auth_token,
                            'name' => $user->name,
                            'email' => $user->email,
                            'phone' => $user->phone,
                            'default_address_id' => $user->default_address_id,
                            'default_address' => $default_address,
                            'delivery_pin' => $user->delivery_pin,
                            'roll_number' => $user->roll_number,
                        ],
                        'isActive' => $user->is_active,
                        'running_order' => $running_order,
                    ];
                }
            }
        } else {
          self::saveErrorLog($request->email, "Account Inactive");
          $response = [
              'success' => false,
              'data' => 'Account Inactive'
          ];
        }
        return response()->json($response, 201);
    }

    /**
     * @param Request $request
     */
    public function register(Request $request)
    {
        $payload = [
            'password' => \Hash::make($request->password),
            'email' => $request->email,
            'name' => $request->name,
            'phone' => $request->phone,
            'delivery_pin' => strtoupper(str_random(5)),
            'auth_token' => '',
        ];

        try {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6'],
                'phone' => ['required'],
            ]);

            $user = new \App\User($payload);
            if ($user->save()) {

                $token = self::getToken($request->email, $request->password); // generate user token

                if (!is_string($token)) {
                    return response()->json(['success' => false, 'data' => 'Token generation failed'], 201);
                }

                $user = \App\User::where('email', $request->email)->get()->first();

                $user->auth_token = $token; // update user token

                $user->save();
                $user->assignRole('Customer');

                $wallet = new Wallet();
                //$wallets = Wallet::where('user_id', $user->id)->first();

                $wallet->user_id = $user->id;
                $wallet->is_active = 1;
                $wallet->balance = 0;
                $wallet->save();
                $walletId = $wallet->save();

                if ($user->default_address_id !== 0) {
                    $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                } else {
                    $default_address = null;
                }

                $response = [
                    'success' => true,
                    'data' => [
                        'id' => $user->id,
                        'auth_token' => $user->auth_token,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'default_address_id' => $user->default_address_id,
                        'default_address' => $default_address,
                        'delivery_pin' => $user->delivery_pin,
                        'roll_number' => $user->roll_number,
                    ],
                    'running_order' => null,
                ];
            } else {
                $response = ['success' => false, 'data' => 'Couldnt register user'];
            }
        } catch (\Throwable $e) {
            $response = ['success' => false, 'data' => 'Couldnt register user'];
            return response()->json($response, 201);
        }

        return response()->json($response, 201);
    }

    /**
     * @param Request $request
     */
    public function updateUserInfo(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        if ($user->default_address_id !== 0) {
            $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
        } else {
            $default_address = null;
        }

        $running_order = \App\Order::where('user_id', $request->user_id)
            ->whereIn('orderstatus_id', ['1', '2', '3', '4','5','6','8'])
            ->where('deliver_status', 0)
            ->with('restaurant')
            ->latest('id')
            ->first();

        $response = [
            'success' => true,
            'data' => [
                'id' => $user->id,
                'auth_token' => $user->auth_token,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'default_address_id' => $user->default_address_id,
                'default_address' => $default_address,
                'delivery_pin' => $user->delivery_pin,
                'roll_number' => $user->roll_number,
            ],
            'running_order' => $running_order,
        ];

        return response()->json($response);
    }
    /**
     * @param Request $request
     */
    public function updateUserPassword(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        $password = \Hash::make($request->password);
        $token = null;
        $token = self::getToken($request->email, $request->password);

        try {
            $user->password = $password;
            $user->save();

        } catch (\Throwable $e) {
            $response = ['success' => false, 'data' => 'Please check the password again'];
            return response()->json($response, 201);
        }
        $response = [
            'success' => true,
            'data' => [
                'id' => $user->id,
            ],
        ];
        return response()->json($response);
    }

    public function checkUserPassword(Request $request)
    {
        $user = User::where('id', $request->userId)->first();
        $password = \Hash::make($request->password);

        // try {
        if ($user && \Hash::check($request->password, $user->password)) {
            $response = [
                'success' => true,
                'data' => [
                    'id' => $user->id,
                ],
            ];
            return response()->json($response);
        } else {
            $response = ['success' => false, 'data' => 'Please check the password again'];
            // return response()->json($response, 201);
            return response()->json($response);
        }

        // }
        // catch (\Throwable $e) {
        //     $response = ['success' => false, 'data' => 'Please check the password again'];
        //     return response()->json($response, 201);
        // }

    }

    public function updateUserPersonalDetails(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();

        $name = $request->name;
        $phone = $request->phone;
        $email = $request->email;
        $roll_number = $request->roll_number;

        try {
            $user->name = $name;
            $user->phone = $phone;
            $user->email = $email;
            $user->roll_number = $roll_number;
            $user->save();

        } catch (\Throwable $e) {
            $response = ['success' => false, 'data' => 'Please check the details again'];
            return response()->json($response, 201);
        }
        $response = [
            'success' => true,
            'data' => [
                'id' => $user->id,
            ],
        ];
        return response()->json($response);
    }
    public function reportIssue(Request $request)
    {
        $newComplaint = new ComplaintReport();
        $latestComplaint = ComplaintReport::orderBy('id', 'DESC')->first();
        if($latestComplaint) {
            $latestComplaintId = $latestComplaint->id+1;
        } else {
            $latestComplaintId = 0;
        }
        $ticketId = str_pad($latestComplaintId, 4, '0', STR_PAD_LEFT);


        $existingTicket =ComplaintReport::where('unique_order_id', $request->uniqueOrderId)
                                        ->orderBy('id', 'DESC')
                                        ->get();
        if(count($existingTicket) > 0) {
            $response = ['success' => 'exist', 'id' => $existingTicket[0]->unique_ticket_id];
            return response()->json($response);
        }
        $newComplaint->user_id = $request->userId;
        $newComplaint->unique_ticket_id = 'TKT'.$ticketId;
        $newComplaint->unique_order_id = $request->uniqueOrderId;
        $newComplaint->issue_type = $request->issueType['value'];
        $newComplaint->issue_description = $request->issueDescription;
        $newComplaint->total = $request->total;
        $newComplaint->issue_status = 'open';
        $newComplaint->is_open = 0;
        try {
            $newComplaint->save();

            $response = ['success' => true, 'id' => $newComplaint->unique_ticket_id];
            return response()->json($response);
        }
        catch (\Throwable $e) {
            $response = ['success' => false, 'data' => 'Please check the details again'];
            return response()->json($response, 201);
        }

    }

    public function getTickets(Request $request) {
        $tickets = ComplaintReport::where('user_id', $request->userId)->orderBy('id', 'DESC')->get();
        $response = ['success' => true, 'data' => $tickets];
        return response()->json($response);
    }

    public function checkMail(Request $request) {
        $emailId = $request->email;
        $user = User::where('email', $emailId)->get();
        if(count($user) > 0) {
            $otp= rand(100000, 999999);
            $name = $user[0]->name;
            $checkOtpExist = Otp::where('user_id', $user[0]->id)->get();
            if(count($checkOtpExist) > 0) {
                DB::table('otp_verification')
                    ->where('id', $checkOtpExist[0]->id)
                    ->update(['otp' => $otp]);
            } else {
                DB::table('otp_verification')->insert(
                    ['user_id' => $user[0]->id,
                    'otp' => $otp]
                );
            }
            $data = [ 'msg' => 'Your OTP is '.$otp.'. Please do not share.' ];
            Mail::send('mail', $data, function($message) use($emailId, $name)
            {
                $message->to($emailId, $name)->subject('Qwickpay - OTP');
            });
            $response = ['success' => true];
            return response()->json($response);
        }
        $response = ['success' => false, 'data' => $emailId];
        return response()->json($response);
    }

    public function verifyOtp(Request $request) {
        $emailId = $request->email;
        $otp = $request->otp;
        $user = User::where('email', $emailId)->get();
        if(count($user) > 0) {
            $checkOtpExist = Otp::where('user_id', $user[0]->id)->get();
            if($otp == $checkOtpExist[0]->otp) {
                $response = ['success' => true,'data' => $emailId];
            }  else {
                $response = ['success' => false,'data' => $emailId];
            }
            // $data = [ 'msg' => 'Your OTP is '.$otp.'. Please do not share.' ];
            // Mail::send('mail', $data, function($message) use($emailId, $name)
            // {
            //     $message->to($emailId, $name)->subject('Qwickpay - OTP');
            // });
            // $response = ['success' => true];
            return response()->json($response);
        }
        $response = ['success' => false, 'data' => $emailId];
        return response()->json($response);
    }

    public function changePassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $password = \Hash::make($request->password);
        $token = null;
        $token = self::getToken($request->email, $request->password);

        try {
            $user->password = $password;
            $user->save();

        } catch (\Throwable $e) {
            $response = ['success' => false, 'data' => 'Please check the password again'];
            return response()->json($response, 201);
        }
        $response = [
            'success' => true,
            'data' => 'successfully changed the password'
        ];
        return response()->json($response);
    }


    public function userBulkUpload(Request $request)
    {
        if ($request->hasFile('users_csv')) {
            $filepath = $request->file('users_csv')->getRealPath();

            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {

                    if ($key["name"] == "NULL") {
                        $name = null;
                    } else {
                        $name = $key["name"];
                    }

                    if ($key["email"] == "NULL") {
                        $email = null;
                    } else {
                        $email = $key["email"];
                    }

                    if ($key["password"] == "NULL") {
                        $password = null;
                    } else {
                        $password = $key["password"];
                    }

                    if ($key["phone"] == "NULL") {
                        $phone = null;
                    } else {
                        $phone = $key["phone"];
                    }
                    if ($key["rollnumber"] == "NULL") {
                        $rollnumber = null;
                    } else {
                        $rollnumber = $key["rollnumber"];
                    }

                    $payload = [
                        'password' => \Hash::make($password),
                        'email' => $email,
                        'name' => $name,
                        'phone' => $phone,
                        'roll_number' => $rollnumber,
                        'delivery_pin' => strtoupper(str_random(5)),
                        'auth_token' => '',
                    ];

                    try {

                        $user = new \App\User($payload);
                        $user->save();
                        $token = self::getToken($email, $password); // generate user token
                        $user = \App\User::where('email', $email)->get()->first();

                        $user->auth_token = $token; // update user token

                        $user->save();
                        $user->assignRole('Customer');

                        $wallet = new Wallet();

                        $wallet->user_id = $user->id;
                        $wallet->is_active = 1;
                        $wallet->balance = 13000;
                        $wallet->save();
                        $walletId = $wallet->save();

                        $WalletTransactions = new WalletTransactions();
                        $wallets = Wallet::where('user_id', $user->id)->first();
                        $today = date("md");
                        $WalletTransactions->wallet_id = $wallets->id;
                        $WalletTransactions->order_reference = 'CREDITED-ON-'.$today;
                        $WalletTransactions->amount = 13000;
                        $WalletTransactions->transaction_type = 'C';
                        $WalletTransactions->save();

                        if ($user->default_address_id !== 0) {
                            $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                        } else {
                            $default_address = null;
                        }

                        $response = [
                            'success' => true,
                            'data' => [
                                'id' => $user->id,
                                'auth_token' => $user->auth_token,
                                'name' => $user->name,
                                'email' => $user->email,
                                'phone' => $user->phone,
                                'default_address_id' => $user->default_address_id,
                                'default_address' => $default_address,
                                'delivery_pin' => $user->delivery_pin,
                                'roll_number' => $user->roll_number,
                            ],
                            'running_order' => null,
                        ];

                    } catch (\Throwable $e) {
                        continue;
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
            }
            return redirect()->back()->with(['success' => 'Operation Successful']);
        }
        return redirect()->back()->with(['message' => "Upload 'users_csv' only"]);
    }


    public function userBulkMailUpload(Request $request)
    {
        // dd($request);
        if ($request->hasFile('email_csv')) {
            // dd("data");
            $filepath = $request->file('email_csv')->getRealPath();

            $excel = Importer::make('CSV');
            $excel->hasHeader(true);
            $excel->load($filepath);
            $data = $excel->getCollection();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key) {

                    if ($key["name"] == "NULL") {
                        $name = null;
                    } else {
                        $name = $key["name"];
                    }
                    if ($key["email"] == "NULL") {
                        $email = null;
                    } else {
                        $email = $key["email"];
                    }
                    if ($key["password"] == "NULL") {
                        $password = null;
                    } else {
                        $password = $key["password"];
                    }
                    if ($key["phone"] == "NULL") {
                        $phone = null;
                    } else {
                        $phone = $key["phone"];
                    }

                    try {

                        $data = [ 'msg' => 'Hey '.$name.'. '."\n"."Please use your Email: ".$email." and Password: ".$password." to login to Qwickpay.
                        \nDownload Qwickpay for Android from Playstore: https://play.google.com/store/apps/details?id=com.prod.qwickpay and for iOS devices, please visit https://qwickpay.in in the browser." ];
                        Mail::send('mail', $data, function($message) use($email, $name)
                        {
                            $message->to($email, $name)->subject('Welcome to Qwickpay');
                            $message->attach('Welcome to Qwickpay');
                        });
                    } catch (\Throwable $e) {
                        $response = ['success' => false, 'data' => 'Couldnt send mail'];
                        return response()->json($response, 201);
                    } catch (\Illuminate\Database\QueryException $qe) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (Exception $e) {
                        return redirect()->back()->with(['message' => "Something went wrong."]);
                    } catch (\Throwable $th) {
                        return redirect()->back()->with(['message' => $th]);
                    }
                }
                return redirect()->back()->with(['success' => 'Operation Successful']);
            }
        }
        return redirect()->back()->with(['message' => "Upload 'email_csv' only"]);

    }

    public function getUserDetails(Request $request) {
        $user = User::select(DB::raw("id, phone"))
                    ->where('id', $request->userId)
                    ->get();
        $response = [
            'success' => true,
            'data' => $user
        ];
        return response()->json($response);
    }

    public function updatePhone(Request $request) {
        $user = User::where('id', $request->userId)
                        ->update(['phone' => $request->phone]);
        $response = [
            'success' => true,
            'data' => $user
        ];
        return response()->json($response);
    }
}
