<?php

namespace App\Http\Controllers;

use App\Order;
use App\PaymentModeStatus;
use App\User;
use App\Wallet;
use App\WalletTransactions;
use DB;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    /**
     * @param Request $request
     */
    public function getWalletTransactions($user_id)
    {
        $walletTransactions = Wallet::where('user_id', $user_id)
            ->with(['wallet_transactions' => function ($query) {
                $query->orderBy('id', 'DESC');}])
        // select('*')
        // ->whereIn('transaction_type', ['R','D','C'])
        // DB::raw("wallet_id,updated_at,transaction_type,order_reference,id,created_at, CASE WHEN wallet_transactions.transaction_type = 'D' THEN wallet_transactions.amount WHEN wallet_transactions.transaction_type = 'R' THEN -1 * wallet_transactions.amount ELSE 0 end AS amount")
            ->get();

        return response()->json($walletTransactions);

    }
    public function getWalletTransactionsDateRange(Request $request)
    {
        $dateRangeResponse = WalletTransactions::where('wallet_id', $request->wallet_id)
            ->whereBetween('created_at', [$request->fromDate, $request->toDate])
            ->orderBy('id', 'DESC')
            ->get();
        $sumOfDateRange = WalletTransactions::select(DB::raw('SUM(amount) as sum'))
                            ->where('wallet_id', $request->wallet_id)
                            ->where('transaction_type', 'D')
                            ->whereBetween('created_at', [$request->fromDate, $request->toDate])
                            ->get();
        $response = [
            'success' => false,
            'dateRangeResponse' => $dateRangeResponse,
            'sumOfDateRange' => $sumOfDateRange,
        ];
        return response()->json($response);
    }

    public function getOrderDetails(Request $request)
    {
        // try {
        $orderDetails = Order::where('orders.user_id', $request->user_id)
            ->select(DB::raw('sum(orders.total) as sum, orders.restaurant_id, restaurants.name'))
            ->join('restaurants', 'restaurants.id', '=', 'orders.restaurant_id')
            ->whereNotNull('orders.restaurant_id')
            ->groupBy('orders.restaurant_id')
            ->get();

        return response()->json($orderDetails);
        // } catch (\Throwable $e) {
        //     $response = ['success' => false, 'data' => 'Please check the data again'];
        //     return response()->json($response, 201);
        // }

    }

    public function saveWalletTransaction(Request $request)
    {
        $walletDetails = Wallet::where('id', $request->w)->first();

        $orderTotal = $request->amount;
        $qrCode = json_decode($request->order_reference, true);

        $t = time();
        $unique_order_id = '';
        if ($qrCode["oid"] != '') {
            $unique_order_id = $qrCode["oid"];
        } else {
            $unique_order_id = 'MCNT' . '-' . date("m-d", $t) . '-' . strtoupper(uniqid());
        }

        //write code here to check the wallet balance and deduct

        if ($walletDetails->balance >= $orderTotal) {

            $WalletTransactions = new WalletTransactions();

            //save the wallet WalletTransactions
            $WalletTransactions->wallet_id = $walletDetails->id;
            $WalletTransactions->order_reference = $unique_order_id;
            $WalletTransactions->amount = $orderTotal;
            $WalletTransactions->transaction_type = 'D';
            $WalletTransactions->save();
            $WalletTransactionsId = $WalletTransactions->id;

            $walletDetails->user_id = $request->u;
            $walletDetails->balance = $walletDetails->balance - $orderTotal;
            $walletDetails->save();

            $order = Order::where('unique_order_id', $qrCode["oid"])->first();
            if (!empty($order)) {
                $order->orderstatus_id = 1;
                $order->transaction_id = $WalletTransactionsId;
                $order->save();
            }

            $response = [
                'success' => true,
                'data' => $walletDetails,
            ];

            return response()->json($response);

        } else {

            $response = [
                'success' => false,
                'data' => null,
            ];

            return response()->json($response);
        }

        //}
    }

    public function saveDirectWalletTransaction(Request $request)
    {
        // $walletDetails = Wallet::where('id', $request->w)->first();
        // $orderTotal = $request->amount;
        // $qrCode = json_decode($request->order_reference, true);
        // $t = time();

        // $lastOrder = Order::orderBy('id', 'desc')->first();
        // if ($lastOrder) {
        //     $lastOrderId = $lastOrder->id;
        //     $newId = $lastOrderId + 1;
        //     $uniqueId = Hashids::encode($newId);
        // } else {
        //     //first order
        //     $newId = 1;
        // }

        // $restaurantCode = $request->restaurantCode;
        // $houseAddress = $request->house;

        // $orderId = str_pad($newId, 4, '0', STR_PAD_LEFT);
        // $unique_order_id = $restaurantCode . date('md') . $orderId . $houseAddress;
        // // }

        // //write code here to check the wallet balance and deduct
        // if ($walletDetails->balance >= $orderTotal) {
        //     $WalletTransactions = new WalletTransactions();
        //     //save the wallet WalletTransactions
        //     $WalletTransactions->wallet_id = $walletDetails->id;
        //     $WalletTransactions->order_reference = $unique_order_id;
        //     $WalletTransactions->amount = $orderTotal;
        //     $WalletTransactions->transaction_type = 'D';
        //     $WalletTransactions->save();

        //     $WalletTransactionsId = $WalletTransactions->id;
        //     $walletDetails->user_id = $request->u;
        //     $walletDetails->balance = $walletDetails->balance - $orderTotal;
        //     $walletDetails->save();

        //     $newOrderForWalletTrasanction = new Order();
        //     $newOrderForWalletTrasanction->unique_order_id = $unique_order_id;
        //     $newOrderForWalletTrasanction->orderstatus_id = 7;
        //     $newOrderForWalletTrasanction->user_id = $request->u;
        //     $newOrderForWalletTrasanction->total = $orderTotal;
        //     $newOrderForWalletTrasanction->payment_mode = 'WALLET';
        //     $newOrderForWalletTrasanction->address = 'Campus';
        //     $newOrderForWalletTrasanction->restaurant_id = $request->restaurant;
        //     $newOrderForWalletTrasanction->location = $restaurantCode;
        //     $newOrderForWalletTrasanction->save();

        //     // DB::table('orders')->insert([
        //     //     ['unique_order_id' => $unique_order_id,
        //     //         'orderstatus_id' => 7,
        //     //         'user_id' => $request->u,
        //     //         'total' => $orderTotal,
        //     //         'payment_mode' => 'WALLET',
        //     //         'location' => $restaurantCode,
        //     //         'address' => 'Campus',
        //     //         'restaurant_id' => $request->restaurant,
        //     //         'created_at' => date("Y/m/d") . ' ' . date("h:i:s"),
        //     //     ],
        //     // ]);

        //     $response = [
        //         'success' => true,
        //         'data' => $walletDetails,
        //     ];
        //     return response()->json($response);

        // } else {

        $response = [
            'success' => false,
            'data' => null,
        ];

        return response()->json($response);
        // }
    }

    /**
     * @param Request $request
     */
    public function addWalletBalance(Request $request)
    {
        $wallet = new Wallet();
        $WalletTransactions = new WalletTransactions();

        $walletId = 0;
        try {

            $wallets = Wallet::where('user_id', $request->userId)->first();
            if (!empty($wallets)) {
                $wallets->balance = 0;
                $wallets->user_id = $request->userId;
                $wallets->is_active = 1;
                $wallets->balance = (float) $wallets->balance + (float) $request->walletBalance;
                $wallets->save();
                $walletId = $wallets->save();
            } else {
                $wallet->user_id = $request->userId;
                $wallet->is_active = 1;
                $wallet->balance = $request->walletBalance;
                $wallet->save();
                $walletId = $wallet->save();
            }

            $today = date("md");
            $WalletTransactions->wallet_id = $wallets->id;
            $WalletTransactions->order_reference = 'CREDITED-ON-' . $today;
            $WalletTransactions->amount = $request->walletBalance;
            $WalletTransactions->transaction_type = 'C';
            $WalletTransactions->save();

            return response()->json([
                'success' => true,
                'userName' => $request->userName,
            ], 201);
        } catch (\Throwable $th) {

        }
    }

    public function getWalletDetails()
    {
        try {

            $walletDetails = Wallet::join('users', 'users.id', '=', 'wallets.user_id')
                ->select('users.name', 'users.email', 'wallets.*')
                ->orderBy('updated_at', 'DESC')
                ->paginate(20);

            return view('admin.wallet', array(
                'details' => $walletDetails,
            ));
        } catch (\Throwable $th) {
        }
    }

    public function postSearchUsers(Request $request)
    {
        $query = $request['query'];
        $walletDetails = Wallet::where('users.name', 'LIKE', '%' . $query . '%')
            ->join('users', 'users.id', '=', 'wallets.user_id')
            ->orWhere('users.email', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();

        // $roles = Role::all();

        // $count = count($users);

        return view('admin.wallet', array(
            'details' => $walletDetails,
            // 'query' => $query,
            // 'count' => $count,
        ));
    }

    public function getUserWalletTransaction($id)
    {
        // dd($id, $orderid);
        $wallet = Wallet::where('user_id', $id)->first();
        $walletId = $wallet->id;
        $user = User::where('id', $id)->first();
        $transactions = WalletTransactions::where('wallet_id', $walletId)->orderBy('updated_at', 'DESC')->take(20)->paginate(25);
        return view('admin.userWalletTransactions', array(
            'transactions' => $transactions,
            'user' => $user,
            'walletId' => $walletId,
            'orderid' => '',
            'ticketno' => '',
        ));
    }

    public function paymentModeStatus()
    {
        $transactions = DB::table('payment_mode')->where('is_active', 1)->distinct()->get();
        if ($transactions) {
            $response = [
                'success' => true,
                'data' => $transactions,
            ];
            return response()->json($response);
        } else {
            $response = [
                'success' => false,
                'data' => null,
            ];
            return response()->json($response);
        }

    }

}
