<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model
{

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }

    public function items()
    {
        return $this->belongsTo('App\Items');
    }

    public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }

}