<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentModeStatus extends Model
{
    protected $table = 'payment_mode';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }
	// protected $fillable = [
	// 	'restaurant_id', 'changed_by','action', 'value_data'
	// ];
	
	// protected $casts = [
    //     'value_data' => 'array'
    // ];
}
