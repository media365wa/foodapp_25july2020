<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickupLocation extends Model
{
    protected $table = 'pickup_location';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }
}
