<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QrCode extends Model
{
    protected $table = 'logs';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'restaurant_id', 'changed_by','action', 'value_data'
	];
	
	protected $casts = [
        'value_data' => 'array'
    ];
}
