<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantUser extends Model
{
    protected $table = 'restaurant_user';
}
