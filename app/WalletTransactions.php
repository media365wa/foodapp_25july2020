<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletTransactions extends Model
{
    public function order()
    {
        return $this->belongsTo('App\Wallet');
    }
}
