@extends('admin.layouts.master')
@section("title") Orders - Complaints
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Open Complaints</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Order</th>
                            <th>Issue</th>
                            <th>Description</th>
                            <th>Total</th>
                            <th>Issued At</th>
                            <th>Status</th>
                            <th>Save</th>
                            {{-- <th>Refund and Save</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($openComplaints) <= 0) <td colspan="8" class='text-center'>No Complaints Reported</td>
                            @else @foreach ($openComplaints as
                            $complaint) <tr>
                                <td>
                                    <a href="{{ route('admin.getWalletTransaction', ['id'=>$complaint->user_id, 'orderId' => $complaint->unique_order_id, 'ticketno' => $complaint->id]) }}">
                                        {{ $complaint->name}}
                                    </a>
                                </td>
                                <td>
                                    {{$complaint->unique_order_id}}
                                </td>
                                <td class="text-capitalize">{{$complaint->issue_type}}</td>
                                <td>
                                    {{$complaint->issue_description}}
                                </td>
                                <td>
                                    {{ config('settings.currencyFormat') }}{{$complaint->total}}
                                </td>
                                <td>{{$complaint->created_at}}</td>
                                <form action="{{ route('admin.updateComplaintStatus') }}" method="POST" name="form1"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <td class="text-capitalize">
                                        <div class="">
                                            <select class="form-control select-search" name="is_open" required>
                                                <option value="0" class="text-capitalize" @if($complaint->is_open ==
                                                    '0') selected="selected" @endif> Open </option>
                                                <option value="1" class="text-capitalize" @if($complaint->is_open ==
                                                    '1') selected="selected" @endif> Close </option>
                                            </select>
                                            <input type="hidden" name='id' value="{{$complaint->id}}">
                                        </div>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#exampleModalCenter{{$complaint->id}}"
                                            style="color: white; cursor:pointer" class="badge badge-primary badge-icon">
                                            Save
                                        </a>
                                        <div class="modal fade" id="exampleModalCenter{{$complaint->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" id="{{$complaint->id}}"
                                                role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="font-size: 16px;">
                                                        Do you want to close the complaint with Order <span
                                                            style="font-size: 20px; color: red;">{{$complaint->unique_order_id}}</span>
                                                        ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" name="form1" class="btn btn-primary">Yes</button>
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">No</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </form>
                                {{-- <td>
                                    <form name="form2" action="{{ route('admin.refundAndUpdateComplaintStatus') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name='id' value="{{$complaint->id}}">
                                        <input type="hidden" name='total' value="{{$complaint->total}}">
                                        <a data-toggle="modal" data-target="#exampleModalCenter{{$complaint->id}}"
                                            style="color: white; cursor:pointer" class="badge badge-primary badge-icon">
                                            Refund and Save
                                        </a>
                                        <div class="modal fade" id="exampleModalCenter{{$complaint->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" id="{{$complaint->id}}"
                                                role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="font-size: 16px;">
                                                        Do you want to close the complaint with Order <span
                                                            style="font-size: 20px; color: red;">{{$complaint->unique_order_id}}</span>
                                                        ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" name="form2" class="btn btn-primary">Yes</button>
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">No</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td> --}}
                            </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $openComplaints->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Closed Complaints</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Order</th>
                            <th>Issue</th>
                            <th>Description</th>
                            <th>Total</th>
                            <th>Issued At</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    {{-- {{($closeComplaints)}} --}}
                    <tbody>
                        @if(count($closeComplaints) <= 0) <td colspan='7' class='text-center'>No Complaints Reported
                            </td> @else
                            @foreach ($closeComplaints as $closedComplaint)
                            <tr>
                                <td>{{ $closedComplaint->name}}</td>
                                <td>
                                    {{$closedComplaint->unique_order_id}}
                                </td>
                                <td class="text-capitalize">{{$closedComplaint->issue_type}}</td>
                                <td>
                                    {{$closedComplaint->issue_description}}
                                </td>
                                <td>
                                    {{ config('settings.currencyFormat') }}{{$closedComplaint->total}}
                                </td>
                                <td>{{$closedComplaint->created_at}}</td>
                                <td class="text-capitalize">
                                    @if($closedComplaint->is_open == 0) Open @else Closed @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
