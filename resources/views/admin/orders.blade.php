@extends('admin.layouts.master')
@section("title") Orders - Dashboard
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-primary btn-labeled btn-labeled-left" id="addWalletBalance"
                    data-toggle="modal" data-target="#downloadMonthlyReport">
                <b><i class="icon-download"></i></b>
                    Download Report
                </button>
            </div>
        </div>
    </div>
</div>
<div id="downloadMonthlyReport" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg"  style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Download</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{action('AdminController@exportOrders')}}" method="GET">
                    <div class="row">
                        <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>Select Shop:</label>
                        <div class="col-lg-10">
                            <select class="form-control select-search" name="restaurantId" required>
                            @foreach ($restaurants as $restaurant)
                                <option value="{{ $restaurant->id }}" class="text-capitalize" selected="selected" >{{ $restaurant->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>From:</label>
                        <div class="col-md-4">
                            <input type="datetime-local" name="from" class="form-control text-left" required>
                        </div>
                        <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>To:</label>
                        <div class="col-md-4 text-left">
                            <input type="datetime-local" name="to" class="form-control  text-left" required>
                        {{-- <input type="hidden" name="restaurantId" value="{{$restaurantId[0]}}"> --}}
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary" width="100px">
                                <i class="icon-file-download ml-1"></i>Download Report
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</br>
<div class="content">
    <form action="{{ route('admin.post.searchOrders') }}" method="GET">
        <div class="form-group form-group-feedback form-group-feedback-right search-box">
            <input type="text" class="form-control form-control-lg search-input" placeholder="Search with order id..."
                name="query">
            <div class="form-control-feedback form-control-feedback-lg">
                <button class="btn btn-default"
                    onclick="/public/admin/orders/searchOrders">Search</button>
            </div>
        </div>
        <button type="submit" class="hidden">Search</button>
        @csrf
    </form>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Restaurant Name</th>
                            <th>Status</th>
                            <th>Total</th>
                            <th>Coupon</th>
                            <th>Order Placed At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order->unique_order_id }}</td>
                            <td>{{ $order->restaurant->name }}</td>
                            <td>
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize text-left">
                                    @if ($order->orderstatus_id == 0) Pending Payment @endif
                                    @if ($order->orderstatus_id == 1) Order Placed @endif
                                    @if ($order->orderstatus_id == 2) Order Accepted @endif
                                    @if ($order->orderstatus_id == 3) Delivery Assigned @endif
                                    @if ($order->orderstatus_id == 4) Picked Up @endif
                                    @if ($order->orderstatus_id == 5) Completed @endif
                                    @if ($order->orderstatus_id == 6) Canceled @endif
                                    @if ($order->orderstatus_id == 7) Paid Via Wallet @endif
                                    @if($order->orderstatus_id > 2 && $order->orderstatus_id < 6) @if($order->
                                        accept_delivery !== null)
                                        @if($order->orderstatus_id > 2 && $order->orderstatus_id < 6) Delivery by: <b>
                                            {{ $order->accept_delivery->user->name }}</b>
                                            @endif
                                            @endif
                                            @endif
                                </span>
                            </td>
                            <td>{{ config('settings.currencyFormat') }} {{ $order->total }}</td>
                            <td>
                                @if($order->coupon_name == NULL) NONE @else
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                    {{ $order->coupon_name }}
                                </span>
                                @endif
                            </td>
                            <td>@if ($order->created_at !== null) {{ $order->created_at}} ({{ $order->created_at->diffForHumans()}})  @else - @endif</td>
                            {{-- <td>{{ $order->created_at->diffForHumans() }}</td> --}}
                            <td class="text-center">
                                <a href="{{ route('admin.viewOrder', $order->unique_order_id) }}"
                                    class="badge badge-primary badge-icon"> VIEW <i class="icon-file-eye ml-1"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#downloadSampleOrdersCsv').click(function (event) {
        event.preventDefault();
       window.location.href = "{{substr(url("/"), 0, strrpos(url("/"), '/'))}}/assets/docs/orders-sample-csv.csv";
    });
</script>
@endsection
