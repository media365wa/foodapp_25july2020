@extends('admin.layouts.master')
@section("title") Report - Raw Data
@endsection
@section('content')
<script>
function rptChange() {
  let rpt =$("#rpt").val();
  if (rpt === 'user_spend' || rpt === 'pending_refund')
    $("#shops").hide();
  else if (rpt === 'order' || rpt === 'vendor_sales')
    $("#shops").show();
 }
</script>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Raw Data</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <form action="{{action('AdminController@rawData')}}" method="GET">
        <div class="row">
            <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>Select Report:</label>
            <div class="col-lg-10">
                <select id="rpt" onchange="rptChange()" class="form-control selectpicker" name="report" required>
                  <option value="order" class="text-capitalize">Order report</option>
                  <option value="user_spend" class="text-capitalize">Consolidated student spend report</option>
                  <option value="vendor_sales" class="text-capitalize">Vendor sales report</option>
                  <option value="pending_refund" class="text-capitalize">Pending Refund report</option>
                </select>
            </div>
        </div>
        <br/>

        <div id="shops" class="row">
            <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>Select Shop:</label>
            <div class="col-lg-10">
                <select id="framework" class="form-control selectpicker" multiple required name="restaurantId[]">
                @foreach ($restaurants as $restaurant)
                    <option value="{{ $restaurant->id }}" class="text-capitalize" selected="selected" >{{ $restaurant->name }}</option>
                @endforeach
                </select>
            </div>
        </div>
        <br/>
        <div class="row">
            <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>From:</label>
            <div class="col-md-4">
                <input type="datetime-local" name="from" class="form-control text-left" required>
            </div>
            <label class="col-lg-2 col-form-label"><span class="text-danger">*</span>To:</label>
            <div class="col-md-4 text-left">
                <input type="datetime-local" name="to" class="form-control  text-left" required>
            {{-- <input type="hidden" name="restaurantId" value="{{$restaurantId[0]}}"> --}}
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary" width="100px">
                    <i class="icon-file-download ml-1"></i>Download Report
                </button>
            </div>
        </div>
    </form>
</div>
<script>
</script>
@endsection
