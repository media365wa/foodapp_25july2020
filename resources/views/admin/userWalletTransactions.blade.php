@extends('admin.layouts.master')
@section("title") User - Wallet Transactions
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Wallet Transactions</span>
                {{-- <span class="badge badge-primary badge-pill animated flipInX">"{{ $user->email }}"</span> --}}
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        {{-- <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left" id="addWalletBalance"
                    data-toggle="modal" data-target="#refundwallet">
                <b><i class="icon-gift"></i></b>
                    Refund User
                </button>
            </div>
        </div> --}}
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Order</th>
                            <th>Amount</th>
                            <th>Transaction Type</th>
                            <th>Paid on</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- {{$transactions}} --}}
                        @if(count($transactions) <= 0) <td colspan='7' class='text-center'>No Records Found
                            </td> @else
                            @foreach ($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->order_reference}}</td>
                                <td>
                                    {{ config('settings.currencyFormat') }} {{$transaction->amount}}
                                </td>
                                <td class="text-capitalize">
                                    @if($transaction->transaction_type == 'D') <span class="badge badge-danger badge-icon">Debited</span>
                                    @elseif($transaction->transaction_type == 'C') <span class="badge badge-success badge-icon">Credited</span>
                                    @elseif($transaction->transaction_type == 'R') <span class="badge badge-info badge-icon">Refunded</span>
                                    @elseif($transaction->transaction_type == 'P') <span class="badge badge-primary badge-icon">Pending Refund</span>   
                                    @endif
                                </td>
                                <td>
                                    {{$transaction->created_at->diffForHumans()}}
                                </td>
                            </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div id="refundwallet" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Refund to wallet</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.refundAndUpdateComplaintStatus'),  $user->id  }}" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Refund Amount:</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control form-control-lg" name="walletBalance" id="walletBalance" min="0" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Comment:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="comment" id="comment" required>
                        </div>
                    </div>
                    <input type="hidden" name="userId" id="userId" value="{{ $user->id }}">
                    <input type="hidden" name="userName" id="userName" value="{{ $user->name }}">
                    <input type="hidden" name="walletId" id="walletId" value="{{ $walletId }}">
                    <input type="hidden" name="orderid" id="orderid" value="{{ $orderid }}">
                    <input type="hidden" name="ticketno" id="ticketno" value="{{ $ticketno }}">
                    <div class="text-right">
                        <button id="AddWalletBalance" type="submit" class="btn btn-primary">
                        Add
                        <i class="icon-database-insert ml-1"></i></button>
                    </div>
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
    </div>
</div> --}}
<script>
    $("#AddWalletBalance").click(function (e) {
            let refundAmount = $("#walletBalance").val();
            let comment = $("#comment").val();
            let user_id = $("#userId").val();
            let token = $("#token").val();
            let name = $("#userName").val();
            let walletId = $("#walletId").val();
            let orderid = $("#orderid").val();
            let ticketno = $("#ticketno").val();
    if(walletBalance && comment !=0)
    {
            $(this).attr("disabled", "disabled").html("ADD <i class='icon-spinner9 spinner ml-1'></i>")

            $.ajax({
                type: "POST",
                url: "{{ route("admin.refundAndUpdateComplaintStatus") }}",
                data: {_token: token, refundAmount: refundAmount, comment: comment, user_id: user_id, name: name, walletId: walletId, orderid: orderid, ticketno:  ticketno},
                dataType: "JSON",
                success: function (response) {
                     //if success then reset the form and "SAVE" button and show success toast or message
                    $.jGrowl('Wallet Balance Added For ' +response.userName +' is successfull.', {
                    position: 'bottom-center',
                    header: 'SUCCESS!!!',
                    theme: 'bg-dark',
                });
                $("#AddWalletBalance").removeAttr("disabled").html("ADD <i class='icon-database-insert ml-1'></i>")
                $("#walletBalance").val("");
                $("#comment").val("");
                },
                error: function (response) {
                    //else say something went wrong and show the form again
                    $.jGrowl(response.responseJSON.message.errorInfo[2], {
                    position: 'bottom-center',
                    header: 'ERROR!!!',
                    theme: 'bg-danger',
                });
                $("#AddWalletBalance").removeAttr("disabled").html("ADD <i class='icon-database-insert ml-1'></i>")
                }
            });
    }
    else
    {
        alert('Please fill in the field')
    }
        });
</script>
@endsection
