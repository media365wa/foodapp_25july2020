@extends('admin.layouts.master')
@section("title") Users - Dashboard
@endsection
@section('content')
<style>
    #showPassword {
    cursor: pointer;
    padding: 5px;
    border: 1px solid #E0E0E0;
    border-radius: 0.275rem;
    color: #9E9E9E;
    }
    #showPassword:hover {
    color: #616161;
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                {{-- @if(empty($query)) --}}
                <span class="font-weight-bold mr-2">Wallet Balance List</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                {{-- <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewUser"
                    data-toggle="modal" data-target="#addNewUserModal">
                <b><i class="icon-user-plus"></i></b>
                Add New User
                </button> --}}
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addBulkWalletImport"
                            data-toggle="modal" data-target="#addBulkWalletImportModal">
                        <b><i class="icon-database-insert"></i></b>
                            Bulk Add Amount with Pending Refund
                        </button>
                        <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="deductBulkWalletImport"
                            data-toggle="modal" data-target="#deductBulkWalletImportModal">
                        <b><i class="icon-database-insert"></i></b>
                            Bulk Deduct Amount
                  </button>
                  {{-- <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addBulkUsersImport"
                    data-toggle="modal" data-target="#addBulkUsersImportModal">
                <b><i class="icon-database-insert"></i></b>
                    Bulk CSV Upload For Users
                </button>
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left" id="addBulkMailImport"
                    data-toggle="modal" data-target="#addBulkMailImportModal">
                <b><i class="icon-database-insert"></i></b>
                    Bulk Mail
                </button> --}}
            </div>
        </div>
    </div>
</div>
<div class="content">
    <form action="{{ route('admin.wallet.searchUsers') }}" method="GET">
        <div class="form-group form-group-feedback form-group-feedback-right search-box">
            <input type="text" class="form-control form-control-lg search-input"
                placeholder="Search with user name or email..." name="query">
            <div class="form-control-feedback form-control-feedback-lg">
                <button class="btn btn-default" onclick="http://localhost/foodapp/public/admin/wallet/searchUsers" >Search</button>
            </div>
        </div>
         <button type="submit" class="hidden">Search</button>
        @csrf
    </form>
    <div class="card">
        <div class="card-body">
            {{-- {{$details}} --}}
            <table class="table">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Email</th>
                        <th>Balance</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        {{-- <th>Save</th> --}}
                        {{-- <th>Refund and Save</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($details as
                        $detail) <tr>
                            <td>
                                <a href="{{ route('admin.getUserWalletTransaction', $detail->user_id) }}">
                                    {{ $detail->name}}
                                </a>
                            </td>
                            <td>
                                {{$detail->email}}
                            </td>
                            <td>
                                {{$detail->balance}}
                            </td>
                            <td class="text-capitalize">{{$detail->created_at->diffForHumans()}}</td>
                            <td>
                                {{$detail->updated_at->diffForHumans()}}
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            <div class="mt-3">
                {{ $details->links() }}
            </div>
        </div>
    </div>
</div>
<div id="deductBulkWalletImportModal" class="modal fade mt-5" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">CSV Bulk Upload for Amount Deduction</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.userAmountDeduction') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">CSV File: </label>
                        <div class="col-lg-10">
                            <div class="uploader">
                                <input type="file" accept=".csv" name="deduction_csv" class="form-control-uniform form-control-lg" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        Upload
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
<div id="addBulkWalletImportModal" class="modal fade mt-5" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">CSV Bulk Upload for Amount Deduction</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.userAmountAddition') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">CSV File: </label>
                        <div class="col-lg-10">
                            <div class="uploader">
                                <input type="file" accept=".csv" name="deduction_csv" class="form-control-uniform form-control-lg" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        Upload
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
