@extends('install.layout.master') 
@section('content')
<div class="box">
    <div class="installation-message text-center">
        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
        <h3>Installation Successful</h3>
    </div>
    <div class="clearfix"></div>
    <div class="visit-wrapper text-center clearfix">
        <div class="row">
            <div class="col-sm-6">
                <div class="visit text-center">
                    <div class="icon">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                    </div>
                    <a href="{{ substr(url("/"), 0, strrpos(url("/"), '/')) }}" class="btn btn-primary" target="_blank">Home Page</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="visit text-center">
                    <div class="icon">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </div>
                    {{-- later change it to route to admin dashboard --}}
                    <a href="{{ route('admin.dashboard') }}" class="btn btn-primary" target="_blank">Admin Dashboard</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection