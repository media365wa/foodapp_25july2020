@extends('admin.layouts.master')
@section("title") Orders - Complaints
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Open Complaints</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Order</th>
                            <th>Issue</th>
                            <th>Description</th>
                            <th>Total</th>
                            <th>Issued At</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($openComplaints) < 0) No Complaints Reported @else 
                            @foreach ($openComplaints as
                            $complaint) <tr>
                            <td>{{ $complaint->name}}</td>
                            <td>
                                {{$complaint->unique_order_id}}
                            </td>
                            <td class="text-capitalize">{{$complaint->issue_type}}</td>
                            <td>
                                {{$complaint->issue_description}}
                            </td>
                            <td>
                                {{ config('settings.currencyFormat') }}{{$complaint->total}}
                            </td>
                            <td>{{$complaint->created_at}}</td>
                            <td class="text-capitalize">
                                {{$complaint->issue_status}}
                            </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                {{-- <div class="mt-3">
                    {{ $openComplaints->links() }}
                </div> --}}
            </div>
        </div>
    </div>
</div>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Closed Complaints</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Order</th>
                            <th>Issue</th>
                            <th>Description</th>
                            <th>Total</th>
                            <th>Issued At</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    {{-- {{($closeComplaints)}} --}}
                    <tbody>
                        @if(count($closeComplaints) <= 0) <td colspan='7' class='text-center'>No Complaints Reported
                            </td> @else
                            @foreach ($closeComplaints as $closedComplaint)
                            <tr>
                                <td>{{ $closedComplaint->name}}</td>
                                <td>
                                    {{$closedComplaint->unique_order_id}}
                                </td>
                                <td class="text-capitalize">{{$closedComplaint->issue_type}}</td>
                                <td>
                                    {{$closedComplaint->issue_description}}
                                </td>
                                <td>
                                    {{ config('settings.currencyFormat') }}{{$closedComplaint->total}}
                                </td>
                                <td>{{$closedComplaint->created_at}}</td>
                                <td class="text-capitalize">
                                    {{$closedComplaint->issue_status}}
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
