@extends('admin.layouts.master')
@section("title")
Dashboard
@endsection
@section('content')
<div class="content container-1">
    <div class="row mt-4">
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-city"></i>
                        </div>
                        <div class="dashboard-display-number">{{ $restaurantsCount }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Restaurants</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-basket"></i>
                        </div>
                        <div class="dashboard-display-number">{{ $ordersCountToday }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Orders Processed</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-stack-star"></i>
                        </div>
                        <div class="dashboard-display-number">{{ $orderItemsCountToday }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Items Sold</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-coin-dollar"></i>
                        </div>
                        <div class="dashboard-display-number">{{ config('settings.currencyFormat') }}
                            {{ $totalEarningToday }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Earnings</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row pt-4 p-0">
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="row mt-4">
                    <div class="col-6 col-xl-3">
                        <div class="panel-heading">
                            <h4 class="panel-title pl-3 pt-3"><strong>NEW ORDERS</strong></h4>
                        </div>
                    </div>
                    <div class="col-6 col-xl-3">
                    </div>
                    <div class="col-6 col-xl-3">
                        {{-- @foreach ($restaurantIds as $item)
                            {{$item}}
                        @endforeach --}}
                    </div>
                    @if($restaurantIds[0] == 47 && $acceptAllOrders)
                        <div class="col-6 col-xl-3 mt-3">
                            <form action="{{ route('restaurant.acceptAllOrders') }}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="ids" value="{{ json_encode(array_values(array_unique($acceptAllOrders)),TRUE)}}">
                                <button style="color: white" class="badge badge-primary badge-icon" type="submit">
                                    Accept All Orders
                                    <i class="icon-checkmark3 ml-1"></i>
                                </button>
                                @csrf
                            </form>
                        </div>
                    @endif
                </div>
                <hr>
                <div id="newOrdersTable" class="table-responsive @if(!count($newOrders)) hidden @endif">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID - Name</th>
                                <th>Pickup Loc.</th>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Total</th>
                                {{-- <th>Status</th> --}}
                                <th class="text-center">Accept Order</th>
                                <th class="text-center">Decline Items</th>
                            </tr>
                        </thead>
                        <tbody id="newOrdersData">
                            @php $prevOrderId = ''; $acceptbtn = ''; $totalbtn = '';$prevName = '' @endphp
                            @foreach($newOrders as $nO)
                            <tr>
                                <td>
                                    <a href="{{ route('restaurant.viewOrder', $nO->unique_order_id) }}"
                                        class="letter-icon-title">
                                        {{ $prevOrderId != $nO->unique_order_id ? $nO->unique_order_id : "" }}
                                    </a>
                                    <span>
                                        {{ $prevOrderId != $nO->unique_order_id ?  $nO->userName : "" }}
                                        <sub>{{ $prevOrderId != $nO->unique_order_id ?  '('.$nO->phone.')' : "" }}</sub>
                                    </span>
                                    @php $prevOrderId = $nO->unique_order_id; @endphp
                                </td>
                                <td  class="text-capitalize text-center">
                                    @if ($nO->pickupLocation === null)
                                        {{ "-" }}
                                    @else
                                        {{ $nO->pickupLocation }}
                                    @endif
                                </td>
                                <td  class="text-capitalize">
                                    {{ $nO->itemNames }}
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ config('settings.currencyFormat') }}
                                        {{ $nO->price }}</span>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">
                                        @if ($totalbtn != $nO->total)
                                        {{ config('settings.currencyFormat') }} {{$nO->total}}
                                        @else
                                        {{ "" }}

                                        @endif
                                        @php $totalbtn = $nO->total; @endphp
                                    </span>
                                </td>
                                {{-- <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        NEW
                                    </span>
                                </td> --}}
                                <td class="text-center">
                                    @if ($acceptbtn != $nO->unique_order_id)
                                    <a href="{{ route('restaurant.acceptOrder', $nO->unique_order_id) }}"
                                        class="badge badge-primary badge-icon">
                                        Accept Order
                                        <i class="icon-checkmark3 ml-1"></i>
                                    </a>
                                    @else
                                    {{ "" }}
                                    @endif
                                    @php $acceptbtn = $nO->unique_order_id; @endphp
                                </td>
                                <td class="text-center">
                                    <a class="badge badge-primary badge-icon" data-toggle="modal"
                                        data-target="#exampleModalCenter{{$nO->id}}"
                                        style="color: white; cursor: pointer;">
                                        Decline Item <i class="icon-cross ml-1"></i>
                                    </a>
                                    <form action="{{ route('kitchens.declineItems') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal fade" id="exampleModalCenter{{$nO->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" id="{{$nO->id}}"
                                                role="document">
                                                <input type="hidden" name="unique_order_id" value={{$nO->unique_order_id}}>
                                                <input type="hidden" name="itemId" value={{$nO->orderitemsId}}>
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="font-size: 16px;">
                                                        Are you sure you want to remove
                                                        <span
                                                            style="font-size: 20px; color: red;">{{$nO->names}}</span>
                                                    ?</div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">No</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-3">
                        {{$newOrders->links() }}
                    </div>
                </div>
                @if(!count($newOrders))
                <div class="text-center text-muted pb-2" id="newOrdersNoOrdersMessage">
                    <h4> No orders to show</h4>
                </div>
                @endif
            </div>
        </div>
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="panel-heading">
                    <div class="row mt-4">
                        <div class="col-6 col-xl-3">
                            <div class="panel-heading">
                                <h4 class="panel-title pl-3 pt-3"><strong>PREPARING ORDERS</strong></h4>
                            </div>
                        </div>
                        <div class="col-6 col-xl-3">
                        </div>
                        <div class="col-6 col-xl-3">
                            {{-- @foreach ($restaurantIds as $item)
                                {{$item}}
                            @endforeach --}}
                        </div>
                        @if($restaurantIds[0] == 47 && $ordersReadyArray)
                            <div class="col-6 col-xl-3 mt-3">
                                <form action="{{ route('restaurant.markAllItemsAsReady') }}" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="ids" value="{{ json_encode(array_values(array_unique($ordersReadyArray)),TRUE)}}">
                                    <button style="color: white" class="badge badge-primary badge-icon" type="submit">
                                        Mark All Orders as Ready
                                        <i class="icon-checkmark3 ml-1"></i>
                                    </button>
                                    @csrf
                                </form>
                            </div>
                        @endif
                    </div>

                    <hr>
                </div>
                @if(count($acceptedOrders))
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Pickup Location</th>
                                <th>Items</th>
                                <th>Order Status</th>
                                <th class="text-center">Order Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $prevOrder = '' @endphp
                            @foreach($acceptedOrders as $aO)
                            <tr>
                                <td>
                                    @if ($prevOrder != $aO->unique_order_id)
                                    <a href="{{ route('restaurant.viewOrder', $aO->unique_order_id) }}"
                                        class="letter-icon-title">
                                        {{ $aO->unique_order_id }}
                                    </a>
                                    @else
                                    {{""}}
                                    @endif
                                    @php $prevOrder = $aO->unique_order_id; @endphp
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">
                                        @if ($aO->pickupLocation === null)
                                            {{ "-" }}
                                        @else
                                            {{ $aO->pickupLocation }}
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ $aO->names }}</span>
                                </td>
                                <td>
                                    @if ($aO->item_status == 2)
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        PREPARING
                                    </span>
                                    @elseif ($aO->item_status == 3)
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize"
                                        style="background: green; color: white">
                                        COMPLETED
                                    </span>
                                    @else
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        CHECK IN KITCHEN
                                    </span>
                                    @endif

                                </td>
                                <td class="text-center">
                                    @if ($aO->item_status == 2)
                                    <a href="{{ route('restaurant.acceptOrderItem', $aO->id, $aO->order_id) }}"
                                        class="badge badge-primary badge-icon"> Mark Item Ready <i
                                            class="icon-checkmark3 ml-1"></i>
                                    </a>
                                    @elseif ($aO->item_status == 3)
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        ITEM READY
                                    </span>
                                    @else
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        CHECK IN KITCHEN
                                    </span>
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-3">
                        {{$acceptedOrders->links() }}
                    </div>
                    @else
                    <div class="text-center text-muted pb-2">
                        <h4> No orders to show</h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="panel-heading">

                    <div class="row mt-4">
                        <div class="col-6 col-xl-3">
                            <div class="panel-heading">
                                <h4 class="panel-title pl-3 pt-3"><strong>READY ORDERS</strong></h4>
                            </div>
                        </div>
                        <div class="col-6 col-xl-3">
                        </div>
                        <div class="col-6 col-xl-3">
                            {{-- @foreach ($restaurantIds as $item)
                                {{$item}}
                            @endforeach --}}
                        </div>
                        @if($restaurantIds[0] == 47 && $ordersDeliverArray)
                            <div class="col-6 col-xl-3 mt-3">
                                <form action="{{ route('restaurant.markAllItemsAsDelivered') }}" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="ids" value="{{ json_encode(array_values(array_unique($ordersDeliverArray)),TRUE)}}">
                                    <button style="color: white" class="badge badge-primary badge-icon" type="submit">
                                        Mark All Orders as Ready
                                        <i class="icon-checkmark3 ml-1"></i>
                                    </button>
                                    @csrf
                                </form>
                            </div>
                        @endif
                    </div>
                    <hr>
                </div>
                @if(count($completedOrders))
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Payment Mode</th>
                                <th>Order Status</th>
                                <th>Ready</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($completedOrders as $cOrders)
                            <tr>
                                <td>
                                    <a href="{{ route('restaurant.viewOrder', $cOrders->unique_order_id) }}"
                                        class="letter-icon-title">{{ $cOrders->unique_order_id }}</a>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ $cOrders->names }}</span>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ config('settings.currencyFormat') }}
                                        {{ $cOrders->total }}</span>
                                </td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        {{ $cOrders->payment_mode }}</span>
                                </td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize"
                                        style="background: green; color: white">
                                        COMPLETED
                                    </span>
                                </td>
                                <td class="text-center">
                                    @if ($acceptbtn != $cOrders->unique_order_id)
                                    <a href="{{ route('restaurant.deliverOrder', $cOrders->unique_order_id) }}"
                                        class="badge badge-primary badge-icon">
                                        Delivered
                                        <i class="icon-checkmark3 ml-1"></i>
                                    </a>
                                    @else
                                    {{ "" }}
                                    @endif
                                    @php $acceptbtn = $cOrders->unique_order_id; @endphp
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="text-center text-muted pb-2">
                        <h4> No orders to show</h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    @media (min-width: 1200px){
        .container-1 {
            max-width: 1280px;
        }
	}
 </style>
<script>

    $(function () {
        var notification = document.createElement('audio');
        notification.setAttribute('src', '{{substr(url("/"), 0, strrpos(url("/"), ' / '))}}/qwickpay/assets/backend/audio/new-order.mp3');	
        notification.setAttribute('type', 'audio/mp3');
        notification.setAttribute('muted', 'muted');
        let array = @json($newOrdersJS);
        let tableDataOrd;
        setInterval(function () {
            $.ajax({
                url: '{{ route('restaurant.getNewOrders') }}',
                type: 'GET',
                dataType: 'json',
            })
                .done(function (newArray) {

                    localStorage.removeItem('prevOrder');
                    if (JSON.stringify(array) !== JSON.stringify(newArray[0].data) && (array.length) !== (newArray[0].data.length)) {

                        //if orders are not same so something here..
                        array = newArray[0].data;
                        // console.log(array)
                        //play sound
                        notification.play();
                        console.log(array, newArray[0].data);
                        var tableData = "";
                        var formData = "";

                        $.map(array, function (item, index) {
                            var cmt;
                            if(item.order_comment === null) {
                                cmt = '-';
                            } else {
                                cmt = item.order_comment;
                            }
                            if(item.pickupLocation === null) {
                                pLoc = '-';
                            } else {
                                pLoc = item.pickupLocation;
                            }
                            // console.log(item)
                            var viewOrderURL = "{{ url('/restaurant-owner/order') }}/" + item.unique_order_id;
                            // console.log(viewOrderURL)
                            var acceptedOrderURL = "{{ url('/restaurant-owner/orders/accept-order') }}/" + item.unique_order_id;
                            var declineItemOrderURL = "{{ url('/restaurant-owner/orders/decline-item') }}/"+ item.orderitemsId;
                            // var restaurantLocation = item.restaurant.name + " (" + item.restaurant.location.name + ")";
                            if(localStorage.getItem('prevOrder') == item.unique_order_id) {
                                tableData +='<tr><td>  </td>';
                            } else {
                                tableData += '<tr><td> <a href="' + viewOrderURL + '" class="letter-icon-title">' + item.unique_order_id + '</a> ' + item.userName + '<spab><sub>('+item.phone+')</sub></span>'+' </td>';
                            }
                            tableData += '<td class="text-capitalize text-center">' + pLoc + '</td>';
                            tableData += '<td class="text-capitalize">' + item.itemNames + '</td>';
                            tableData += '<td> {{ config('settings.currencyFormat') }} ' + item.ip + '</td>';
                            if(localStorage.getItem('prevOrder') == item.unique_order_id) {
                                tableData += '<td>  </td>';
                                tableData += '<td> </td>';
                                // tableData += '<td>  </td>';
                            } else {
                                tableData += '<td><span class="text-semibold no-margin">{{ config('settings.currencyFormat') }} ' + item.total + '</span></td>';
                                // tableData += '<td> '+ cmt  +'  </td>';
                                // tableData +='<td><span class="badge badge-flat border-grey-800 text-default text-capitalize"> NEW </span></td>';
                                tableData += '<td class="text-center"> <a href="' + acceptedOrderURL + '" class="badge badge-primary badge-icon"> Accept Order <i class="icon-checkmark3 ml-1"></i></a> </td>';
                            }

                            tableData += '<td class="text-center"> <a href="' + declineItemOrderURL + '" style="color: white; cursor: pointer;" class="badge badge-primary badge-icon"> Decline Item <i class="icon-cross ml-1"></i></a> </td></tr>';

                            $('#newOrdersData').html(tableData);
                            // $('#formData').html(formData);
                            $('#formData').removeClass(formData);
                            $('#newOrdersTable').removeClass('hidden')
                            $('#newOrdersNoOrdersMessage').remove();
                            localStorage.setItem('prevOrder', item.unique_order_id);
                            item.unique_order_id = item.unique_order_id
                        });
                    }
                })
                .fail(function () {
                    console.log("error");
                })
        }, 5000);
    });
</script>
@endsection
