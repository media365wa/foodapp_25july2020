@extends('admin.layouts.master')
@section('title') Delivery Settings
@endsection
<style>
    .disable-switch {
        opacity: 0.5;
        pointer-events: none;
    }

</style>
@section('content')
    @if (count($pickupLocationData))
        <div class="content">
            <div class="col-md-12">
                <div class="card" style="min-height: 100vh;">
                    <div class="card-body">
                        <form action="{{ route('admin.saveSettings') }}" method="POST" enctype="multipart/form-data">
                            <div class="d-lg-flex justify-content-lg-left">
                                <ul class="nav nav-pills flex-column mr-lg-3 wmin-lg-250 mb-lg-0">
                                    <li class="nav-item">
                                        <a href="#pickupLocations1" class="nav-link active" data-toggle="tab">
                                            <i class="icon-gear mr-2"></i>
                                            Delivery Locations
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content" style="width: 100%; padding: 0 25px;">
                                    <div class="tab-pane fade show active" id="pickupLocations1">
                                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                                            Pickup Locations
                                        </legend>
                                        <div class="" id="paymentGatewaySettings">
                                            @php
                                                $pickupLocations = count($pickupLocationData);
                                            @endphp
                                            @foreach ($pickupLocationData as $location)
                                                <div class="form-group row" id="locationsData">
                                                    <label class="col-lg-5 col-form-label"><strong>{{ $location->name }}
                                                        </strong>({{ $location->landmark }})</label>
                                                    <div class="col-lg-6 mt-2">
                                                        <label>
                                                            <input value="{{ $location->id }}" type="checkbox"
                                                                class="switchery-primary pickup-location-toggle" @if ($location->is_active) checked="checked" @endif
                                                                @if ($location->is_active)
                                                            checked="checked"
                                            @endif
                                            name="{{ $location->name }}">
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="content">
            <div class="card">
                <div class="card-body text-center">
                    Not required/needed for this login.
                </div>
            </div>
        </div>
    @endif
    <script>
        $(function() {

            function setSwitchery(switchElement, checkedBool) {
                if ((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
                    switchElement.setPosition(true);
                    switchElement.handleOnchange(true);
                }
            }

            $('.form-control-uniform').uniform();

            // Display color formats
            $(".colorpicker-show-input").spectrum({
                showInput: true
            });

            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery-primary'));
                elems.forEach(function(html) {
                    var switchery = new Switchery(html, {
                        color: '#2196F3'
                    });
                });
            } else {
                var elems = document.querySelectorAll('.switchery-primary');
                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], {
                        color: '#2196F3'
                    });
                }
            }

            $('.summernote-editor').summernote({
                height: 200,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

            $('.pickup-location-toggle').click(function(event) {
                var pickupLocationId = $(this).val();
                var token = $("#csrf").val();
                console.log(pickupLocationId);
                $.ajax({
                        url: '{{ route('restaurant.togglePickupLocation') }}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            id: pickupLocationId,
                            _token: token
                        },
                    })
                    .done(function() {
                        $.jGrowl("Pickup Location Updated", {
                            position: 'bottom-center',
                            header: 'SUCCESS 👌',
                            theme: 'bg-success',
                        });
                    })
                    .fail(function() {
                        $.jGrowl("Something went wrong! ", {
                            position: 'bottom-center',
                            header: 'Wooopsss ⚠️',
                            theme: 'bg-warning',
                        });
                        $('#paymentGatewaysData :input[value="' + pickupLocationId + '"]');
                    })
            });
        });
    </script>
@endsection
