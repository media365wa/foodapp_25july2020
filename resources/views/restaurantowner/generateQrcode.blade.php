@extends('admin.layouts.master')
@section('title') Generate QR Code
@endsection
@section('content')
    <div class="page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                {{-- <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">TOTAL</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
            </div>
        </div>
    </div>
    @if ($restaurants[0]->id == 64 || $restaurants[0]->id == 65 || $restaurants[0]->id == 66)
        <div class="content">
            <div class="card">
                <div class="card-body">
                    <div class="panel-heading">
                        <h4 class="panel-title pl-3"><strong>Generate QR Code</strong></h4>
                        <hr>
                    </div>
                    <form action="javascript:void(0);">
                        <div class="form-group row">
                            <div class="col-12 col-xl-4 mb-2">
                                <select class="form-control select-search" name="restaurant_id" required id="restaurant_id"
                                    style="">
                                    @foreach ($restaurants as $restaurant)
                                        <option value="{{ $restaurant->id }}" name="{{ $restaurant->id }}"
                                            class="text-capitalize">
                                            {{ $restaurant->name }} ({{ $restaurant->location->name }})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-xl-4 mb-2">
                                <select class="form-control select-search" name="item_id" required id="item_id" style="">
                                    @foreach ($items as $item)
                                        <option value="{{ $item->id }}" name="{{ $item->id }}"
                                            class="text-capitalize">
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-xl-4 mb-2">
                                <button type="submit" onClick="changeFunc();" class="btn btn-primary"
                                    style="width: 100%">Generate QR Code</button>
                            </div>
                        </div>
                    </form>
                    <br>


                    <div class="content">
                        <div class="row">
                            <div class="col-xl-8">
                                <div class="sidebar-category mt-4"
                                    style="box-shadow: 0 1px 6px 1px rgba(0, 0, 0, 0.05);background-color: #fff;">
                                    <div class="category-content" id="printThis">
                                        <div href="#" class="btn btn-block content-group"
                                            style="text-align: left; background-color: #8360c3; color: #fff; border-radius: 0;">
                                            <strong style="font-size: 1.3rem;">QR Code</strong>
                                            <a href="javascript:void(0)" id="printButton" class="btn btn-sm"
                                                style="color: #fff; border: 1px solid #ccc; float: right;">Print</a>
                                        </div>
                                        <div class="p-3 ">
                                            <div style="text-align: -webkit-center;" id="qrcode">

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label no-margin text-semibold mr-2"><strong>Restaurant
                                                        Name:
                                                    </strong></label>
                                                {{ $restaurant->name }}
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label no-margin text-semibold mr-2"><strong>Payment
                                                        Mode:
                                                    </strong></label>
                                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                                    QR Code Payment
                                                </span>
                                            </div>
                                            <hr>

                                            <div class="text-right">

                                                <div class="form-group">
                                                    <div class="clearfix"></div>
                                                    <div class="row">
                                                        <div class="col-md-12 p-2 mb-3"
                                                            style="background-color: #f7f8fb; float: right; text-align: left;">
                                                            <div class="text-uppercase text-left"
                                                                style="text-decoration: underline;">
                                                                <strong>Items :</strong>
                                                            </div>
                                                            <br>
                                                            <div>
                                                                <div class="d-flex mb-1 align-items-start"
                                                                    style="font-size: 1rem;" id="itemName">
                                                                    <strong class="mr-2" style="width: 100%;"></strong>
                                                                </div>
                                                            </div>
                                                            <div class="form-group text-right">
                                                                <label class="control-label no-margin text-semibold mr-2">
                                                                    <strong>TOTAL</strong>
                                                                </label>
                                                                <h3 id="itemPrice">
                                                                    <strong> {{ config('settings.currencyFormat') }}
                                                                    </strong>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="content">
            <div class="card">
                <div class="card-body text-center">
                    Not required/needed for this login.
                </div>
            </div>
        </div>
    @endif

@endsection

<script>
    var restaurantId;
    var itemId;
    var items = {!! json_encode($items->toArray(), JSON_HEX_TAG) !!};
    var itemName1;
    var itemPrice1;

    function changeFunc() {
        document.getElementById("qrcode").innerHTML = '';
        var restaurant = document.getElementById("restaurant_id");
        var item = document.getElementById("item_id");
        restaurantId = restaurant.options[restaurant.selectedIndex].value;
        itemId = item.options[item.selectedIndex].value;
        itemArr = items.filter(function(a) {
            if (a.id == itemId) {
                itemName1 = a.name;
                itemPrice1 = a.price;
            }
        });
        document.getElementById('itemName').innerHTML = itemName1;
        document.getElementById('itemPrice').innerHTML = itemPrice1;
        // alert(restaurantId+itemId);
        var qrcode = new QRCode("qrcode");

        function makeCode() {
            var elText = '{"restaurant_id":"' + restaurantId + '", "item_id":"' + itemId + '",  "item_name":"' +
                itemName1 + '", "item_price":"' + itemPrice1 + '"}';
            qrcode.makeCode(elText);
        }
        makeCode();
        $('#printButton').on('click', function() {
            $('#printThis').printThis();
        })
    }
</script>
