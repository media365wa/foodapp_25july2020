@extends('admin.layouts.master')
@section("title") Logs 
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            {{-- <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">TOTAL</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <div class="panel-heading">
                    <h4 class="panel-title pl-3"><strong>Logs</strong></h4>
                    <hr>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Changed By</th>
                            <th>Action </th>
                            <th>Prev. Name - New Name</th>
                            <th>Prev. Price - New Price</th>
                            <th>Prev. Qty. - New Qty. </th>
                            <th>Changed On</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($logs as $log)
                            @foreach($log->value_data as $data)
                            <tr>
                                <td class="text-capitalize">{{$log->name}}</td>
                                @foreach ( $data as $key => $value)
                                    <td class="text-capitalize"> 
                                        @if($key == 'changedOn')
                                        {{ \Carbon\Carbon::parse($value)->isoFormat('MMM Do YYYY')}}<span style="font-size: 0.6rem">({{ \Carbon\Carbon::parse($value)->diffForHumans() }})</span>
                                            {{-- {{date('d-m-Y h:m:s', strtotime($value))}} --}}
                                            {{-- {{ \Carbon\Carbon::parse($value)->diffForHumans() }} --}}
                                            
                                        @elseif($key == 'action')
                                            <span>
                                                @if($value == 'edit')
                                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize text-left" style="background: red; color: white">{{$value}}</span>
                                                @elseif($value == 'new')
                                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize text-left" style="background: green; color: white">
                                                        {{$value}}
                                                    </span>
                                                @endif
                                            </span>
                                        @else
                                            {{$value}}
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $('#downloadSampleOrdersCsv').click(function (event) {
        event.preventDefault();
      window.location.href = "{{substr(url("/"), 0, strrpos(url("/"), '/'))}}/assets/docs/orders-sample-csv.csv";
    });
    
</script>
<script type="text/javascript">
    $(function() {
  $('input[name="datetimes"]').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY/MM/DD hh:mm:ss'
    }
  });
});
function getInputValue(){
            
var month = document.getElementById('month').value;
console.log(month)
        }

</script>
@endsection
