@extends('admin.layouts.master')
@section("title") Kitchen Status
Dashboard
@endsection
@section('content')
<style>
  .boxshadow {
    -webkit-box-shadow: 0px 0px 17px 12px rgba(191,191,191,1);
    -moz-box-shadow: 0px 0px 17px 12px rgba(191,191,191,1);
    box-shadow: 0px 0px 17px 12px rgba(191,191,191,1);
  }

}
</style>
<body>
<br>
  <div class="container">
    <h3 class="font-weight-bold mr-2 text-uppercase">New orders <span class="badge badge-primary badge-pill animated flipInX">{{ count($kitchenItemsInProgress) }}</span></h3>


    <ul class="row list-unstyled ">
      @foreach ( $kitchenItemsInProgress  as $kit )

          <li class="col-md-4">
            <div class="card">
              <div class="card-body text-dark" style="background-color: #1CBA00; ">
                <div class="float-right" >{{ $kit->created_at->diffForHumans() }}<br>

                  <a class="close text-light " href="{{ route('restaurant.acceptOrderItem', $kit->id) }}" onclick="refreshPage()"><strong><i class="icon-checkmark2" style="color: solidblack; font-size: 24px;"></i></strong></a>
                </div><br><br>

                <h5 class="text-uppercase" style="font-color: red;"><b></b> </h5>

                  <h5 class="text-uppercase">Order ID: <b>{{ !empty($kit-> order) ? $kit->order->unique_order_id:'' }}</b> </h5>
                  <h2 class="" > {{ $kit ->name}} x {{ $kit->quantity }}</h2>
                <br>
              </div>
            </div>
          </li>

      @endforeach
  </ul>
  <h2>Completed Orders: </h2>
  <ul class="row list-unstyled">
    @foreach ($kitchenItemsCompleted as $kitItemsCompleted)
    <li class="col-md-3">
      <div class="card">
        <div class="card-body text-dark" style="background-color: #d8d8d8;">
          <h6 class="text-uppercase">Order ID: <b>{{ !empty($kitItemsCompleted-> order) ? $kitItemsCompleted->order->unique_order_id:'' }}</b> </h6>

            <h4 class="">{{ $kitItemsCompleted ->name}} x
              {{ $kitItemsCompleted->quantity }}</h4>
          <br>
        </div>
      </div>
     </li>
    @endforeach
   </div>
 </ul>

 <br><br>
</div>

</body>


<script type="text/javascript">

  $('.close').click(function(){
    var $target = $(this).parents('li');
    $target.hide('slow', function(){ $target.remove(); });
  })

  function refreshPage(){
			location.reload();
	}
</script>

@endsection
