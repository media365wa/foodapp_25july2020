<?php

/* API ROUTES */

Route::post('/get-settings', [
    'uses' => 'SettingController@getSettings',
]);

Route::post('/search-location/{query}', [
    'uses' => 'LocationController@searchLocation',
]);

Route::post('/popular-locations', [
    'uses' => 'LocationController@popularLocations',
]);

Route::post('/promo-slider', [
    'uses' => 'PromoSliderController@promoSlider',
]);

Route::post('/get-restaurants/{location}', [
    'uses' => 'RestaurantController@getRestaurants',
]);

Route::post('/get-restaurant-info/{slug}', [
    'uses' => 'RestaurantController@getRestaurantInfo',
]);

Route::post('/get-restaurant-info-by-id/{id}', [
    'uses' => 'RestaurantController@getRestaurantInfoById',
]);

Route::post('/get-restaurant-items/{slug}', [
    'uses' => 'RestaurantController@getRestaurantItems',
]);

Route::post('/apply-coupon', [
    'uses' => 'CouponController@applyCoupon',
]);

Route::post('/get-pages', [
    'uses' => 'PageController@getPages',
]);

Route::post('/search-restaurants', [
    'uses' => 'RestaurantController@searchRestaurants',
]);

Route::post('/select-restaurant', [
    'uses' => 'RestaurantController@selectRestaurant',
]);

Route::post('/update-user-password/{id}', [
    'uses' => 'UserController@updateUserPassword'
]);

Route::post('/check-old-password/{id}', [
    'uses' => 'UserController@checkUserPassword'
]);

Route::post('/get-wallet-date-range/{id}', [
    'uses' => 'WalletController@getWalletTransactionsDateRange',
]);

Route::post('/view-order/{id}', [
    'uses' => 'RestaurantOwnerController@viewOrderById',
]);

Route::post('/get-order-details/{id}', [
    'uses' => 'WalletController@getOrderDetails',
]);
Route::post('/get-total-amount-spent', [
    'uses' => 'RestaurantController@getTotalWithMaxLimit',
]);
Route::post('/get-waiting-list', [
    'uses' => 'OrderController@getWaitingList',
]);
Route::post('/update-user-address', [
    'uses' => 'AddressController@updateAddress',
]);
Route::post('/report-issue', [
    'uses' => 'UserController@reportIssue',
]);
Route::post('/get-tickets', [
    'uses' => 'UserController@getTickets',
]);
Route::post('/get-notices', [
    'uses' => 'RestaurantController@getNotice',
]);
Route::post('/place-order', [
    'uses' => 'OrderController@placeOrder',
]);
Route::post('/check-email', [
    'uses' => 'UserController@checkMail',
]);
Route::post('/verify-otp', [
    'uses' => 'UserController@verifyOtp',
]);
Route::post('/change-password', [
    'uses' => 'UserController@changePassword',
]);
Route::post('/contact-us', [
    'uses' => 'AdminController@getContactUs',
]);
Route::post('/faq', [
    'uses' => 'AdminController@getFaq',
]);
Route::post('/paymentmode_status', [
    'uses' => 'WalletController@paymentModeStatus'
]);
// Route::post('/check-status', [
//     'uses' => 'RestaurantController@checkStatus',
// ]);

Route::post('/mess-qr', [
    'uses' => 'OrderController@messQr',
]);
Route::post('/save-wallet', [
    'uses' => 'WalletController@saveWalletTransaction',
]);

Route::post('/save-direct-wallet-transacctions', [
    'uses' => 'WalletController@saveDirectWalletTransaction',
]);
Route::post('/get-wallet-transactions/{id}', [
    'uses' => 'WalletController@getWalletTransactions',
]);
Route::post('/get-payment-gateways', [
    'uses' => 'PaymentController@getPaymentGateways',
]);
Route::post('/update-user-info', [
    'uses' => 'UserController@updateUserInfo',
]);
Route::post('/get-pickup-location', [
    'uses' => 'RestaurantOwnerController@getPickupLocation',
]);
Route::post('/get-user-details', [
    'uses' => 'UserController@getUserDetails',
]);
Route::post('/update-phone', [
    'uses' => 'UserController@updatePhone',
]);


/* Protected Routes for Loggedin users */
Route::group(['middleware' => ['jwt.auth']], function () {
    Route::post('/save-notification-token', [
        'uses' => 'NotificationController@saveToken',
    ]);

    

    Route::get('/get-addresses/{id}', [
        'uses' => 'AddressController@getAddresses',
    ]);
    Route::post('/save-address', [
        'uses' => 'AddressController@saveAddress',
    ]);
    
    // Route::post('/save-wallet', [
    //     'uses' => 'WalletController@saveWalletTransaction',
    // ]);
    // Route::post('/mess-qr', [
    //     'uses' => 'OrderController@messQr',
    // ]);
    // Route::post('/save-direct-wallet-transacctions', [
    //     'uses' => 'WalletController@saveDirectWalletTransaction',
    // ]);
    Route::post('/delete-address', [
        'uses' => 'AddressController@deleteAddress',
    ]);
    

    // Route::post('/update-user-password/{id}', [
    //     'uses' => 'UserController@updateUserPassword'
    // ]);

    Route::post('/update-user-personal-details/{id}', [
        'uses' => 'UserController@updateUserPersonalDetails'
    ]);

    


    
    Route::post('/set-default-address', [
        'uses' => 'AddressController@setDefaultAddress',
    ]);
    Route::post('/get-orders', [
        'uses' => 'OrderController@getOrders',
    ]);

    Route::post('/get-order-byid/{id}', [
        'uses' => 'OrderController@getOrderByID',
    ]);

    // Route::post('/get-wallet-transactions/{id}', [
    //     'uses' => 'WalletController@getWalletTransactions',
    // ]);
    

    Route::post('/get-order-items', [
        'uses' => 'OrderController@getOrderItems',
    ]);

    Route::post('/delivery/get-delivery-orders', [
        'uses' => 'DeliveryController@getDeliveryOrders',
    ]);

    Route::post('/delivery/get-single-delivery-order', [
        'uses' => 'DeliveryController@getSingleDeliveryOrder',
    ]);

    Route::post('/delivery/set-delivery-guy-gps-location', [
        'uses' => 'DeliveryController@setDeliveryGuyGpsLocation',
    ]);

    Route::post('/delivery/get-delivery-guy-gps-location', [
        'uses' => 'DeliveryController@getDeliveryGuyGpsLocation',
    ]);

    Route::post('/delivery/accept-to-deliver', [
        'uses' => 'DeliveryController@acceptToDeliver',
    ]);

    Route::post('/delivery/pickedup-order', [
        'uses' => 'DeliveryController@pickedupOrder',
    ]);

    Route::post('/delivery/deliver-order', [
        'uses' => 'DeliveryController@deliverOrder',
    ]);
    
    Route::post('/save-user-availabilty', [
        'uses' => 'UserAvailabilityController@saveUserAvailabilty',
    ]);

    
});
/* END Protected Routes */

/* Auth Routes */
Route::post('/login', [
    'uses' => 'UserController@login',
]);

Route::post('/register', [
    'uses' => 'UserController@register',
]);

Route::post('/delivery/login', [
    'uses' => 'DeliveryController@login',
]);
/* END Auth Routes */
