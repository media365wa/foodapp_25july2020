import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from "../Elements/BackWithSearch";
const packageJson = require('../../../../package.json');

const appVersion = packageJson.version;

const jsonData = {
  version: appVersion
};

var jsonContent = JSON.stringify(jsonData);

class AboutUs extends Component {
    render() {
        // console.log(JSON.parse(jsonContent))
        return (
            <React.Fragment>
                <div className="input-group-prepend">
                    <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title="About Us"
                    disbale_search={true}
                    />
                </div>

                <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
                    <div className="text-center">
                        <img src="../../../../assets/img/favicons/favicon-48x48.png" style={{ background:"transparent", border:"none" }} alt="qwickpay.in" className="img-thumbnail"></img>
                    </div>
                    <div className="text-center" style={{ fontSize:"10px" }}>
                        v{JSON.parse(jsonContent).version}
                    </div>
                    <br/>
                    <p>
                    “Against the backdrop of the rapidly evolving world of online-payments, <strong><u>QwickPay</u></strong> is a startup
                        founded by <strong><u>IIM,Bangalore</u></strong> alumni who aspire to change the landscape of cashless payment
                        systems. <br></br><br></br>Founded in 2020 – The need for contactless
                        and cashless payment solutions is greater than ever before. Leveraging closed-loop payment
                        systems, QwickPay is revolutionizing payment systems for shops and restaurants in school and
                        college campuses. By facilitating online payments and convenience of ordering through digital
                        solutions, QwickPay is taking a leap forward into the future of the payments industry.”
                    </p>
                </div>
            </React.Fragment>
        );
    }
}
export default connect(
)(AboutUs);
