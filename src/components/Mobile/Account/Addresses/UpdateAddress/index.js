import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import BackWithSearch from "../../../Elements/BackWithSearch";
import { updateAddress } from "../../../../../services/addresses/actions";
class UpdateAddress extends Component {
  state = {
    id: localStorage.getItem("addressId"),
    address: localStorage.getItem("addressLine"),
    house: localStorage.getItem("addressHouse"),
    landmark: localStorage.getItem("addressLandmark"),
    tag: localStorage.getItem("addressTag"),
    error: false,
    showMessage: false,
  };
  handleInput = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    // this.props.updateAddress(this.state.id, this.state.address,this.state.house,this.state.landmark ,this.state.tag );
    if (
      (this.state.address !== "" &&
        this.state.house !== "" &&
        this.state.landmark !== "") ||
      this.state.tag !== ""
    ) {
      this.setState({ error: false });
      this.props.updateAddress(
        this.state.id,
        this.state.address,
        this.state.house,
        this.state.landmark,
        this.state.tag
      );
      this.setState({ showMessage: true });
    } else {
      this.setState({ error: true });
      this.setState({ showMessage: false });
    }
  };

  render() {
    return (
      <React.Fragment>
        <BackWithSearch
          boxshadow={true}
          has_title={true}
          title={localStorage.getItem("accountManageAddress")}
          disbale_search={true}
        />
        <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group m-0 ">
              <label className="col-12 edit-address-input-label">
                {localStorage.getItem("editAddressAddress")}
              </label>
              <div className="col-md-9">
                <input
                  type="text"
                  name="address"
                  defaultValue={localStorage.getItem("addressLine")}
                  onChange={this.handleInput}
                  className="form-control edit-address-input"
                />
              </div>
              <label className="col-12 edit-address-input-label">
                Room/House (Ex: J001)
              </label>
              <div className="col-md-9">
                <input
                  type="text"
                  name="house"
                  defaultValue={localStorage.getItem("addressHouse")}
                  onChange={this.handleInput}
                  className="form-control edit-address-input"
                />
              </div>
              <label className="col-12 edit-address-input-label">
                {localStorage.getItem("editAddressLandmark")}
              </label>
              <div className="col-md-9">
                <input
                  type="text"
                  name="landmark"
                  defaultValue={localStorage.getItem("addressLandmark")}
                  onChange={this.handleInput}
                  className="form-control edit-address-input"
                />
              </div>
              <label className="col-12 edit-address-input-label">
                {localStorage.getItem("editAddressTag")}
              </label>
              <div className="col-md-9">
                <input
                  type="text"
                  name="tag"
                  onChange={this.handleInput}
                  defaultValue={localStorage.getItem("addressTag")}
                  className="form-control edit-address-input edit-address-tag"
                  placeholder={localStorage.getItem("addressTagPlaceholder")}
                />
              </div>
            </div>
            {!this.state.showMessage ? (
              <button
                type="submit"
                className="btn-save-address"
                style={{ backgroundColor: localStorage.getItem("storeColor") }}
              >
                {localStorage.getItem("buttonSaveAddress")}
                <Ink duration={200} />
              </button>
            ) : (
              <div className="col-md-12 text-center">
                <span class="alert alert-success" role="alert">
                  <i className="si si-check" />
                  Address saved successfully
                </span>
              </div>
            )}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  updateAddress,
})(UpdateAddress);
