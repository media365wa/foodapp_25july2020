import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from "../../../Elements/BackWithSearch";
import { changePassword } from "../../../../../services/user/actions";
import Logout from "../../Logout";
import DelayLink from "../../../../helpers/delayLink";
class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      confirmPassword: "",
      showLogoutMessage: false,
    };
    // console.log(this.props);
  }
  handleInputNewPassword = (event) => {
    this.setState({ password: event.target.value });
  };
  handleInputConfirmPassword = (event) => {
    this.setState({ confirmPassword: event.target.value });
  };
  handleLogin = (event) => {
    const { password, confirmPassword } = this.state;
    // perform all neccassary validations
    if (password !== confirmPassword) {
      alert("Passwords don't match");
    } else {
      alert("Password correct");
      event.preventDefault();
      this.setState({ loading: true });
      this.props.changePassword(
        this.props.user.data.id,
        this.props.user.data.email,
        this.props.user.data.auth_token,
        this.state.confirmPassword
      );
      this.setState({ showLogoutMessage: true });
    }
  };
  render() {
    const { user } = this.props;
    // console.log(user);
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Personal Details"
            disbale_search={true}
          />
        </div>
        <div className="bg-white">
          <form onSubmit={this.handleLogin}>
            <div className="form-group px-15 pt-30 mt-50 pb-20">
              <label className="col-12 edit-address-input-label">
                New Password
              </label>
              <div className="col-md-9">
                <input
                  type="password"
                  name="newPpassword"
                  onChange={this.handleInputNewPassword}
                  className="form-control edit-address-input"
                  placeholder="New Password"
                  required
                />
              </div>
            </div>
            <div className="form-group px-15">
              <label className="col-12 edit-address-input-label">
                Confirm Password
              </label>
              <div className="col-md-9">
                <input
                  type="password"
                  name="confirmPassword"
                  onChange={this.handleInputConfirmPassword}
                  className="form-control edit-address-input"
                  placeholder="Confirm Password"
                  required
                />
              </div>
            </div>
            <div className="mt-20 px-15 pt-15 button-block">
              <button
                type="submit"
                className="btn btn-main"
                style={{ backgroundColor: localStorage.getItem("storeColor") }}
              >
                Change Password
              </button>
            </div>
          </form>
          <br></br>
          <div className="col-12">
            {this.state.showLogoutMessage === true ? (
              <div className="row">
                <div className="col-md-12">
                  <div className="alert alert-success text-center" role="alert">
                    The details are updated.
                  </div>
                </div>
                <div className="col-md-12">
                  <span className="blockquote-footer">
                    Please to Logout and login again to save the changes.
                  </span>
                  <div>
                    <br></br>
                    <DelayLink to={"/"} delay={200}>
                      <div className="display-flex">
                        <div className="flex-auto logout-text">
                          Go Home
                        </div>
                      </div>
                    </DelayLink>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* {this.state.showLogoutMessage === false && (
              <div className="row">
                <div className="col-md-12">
                  <div className="alert alert-danger text-center" role="alert">
                    Error.
                  </div>
                </div>
              </div>
            )} */}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, {
  changePassword,
})(ChangePassword);
