import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from "../../../../Elements/BackWithSearch";
import { changePassword } from "../../../../../../services/user/actions";
import Logout from "../../../Logout";
import Axios from "axios";
import DelayLink from "../../../../../helpers/delayLink";
import { CHANGE_PASSWORD } from "../../../../../../configs/index";

class NewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      success: false,
      error: false,
      confirmPassword: "",
      showMessage: false,
    };
  }

  handleEmailInput = (event) => {
    this.setState({ email: this.props.email });
  };

  passwordService = () => {
    const { password, confirmPassword } = this.state;
    if (password !== confirmPassword) {
      alert("Passwords don't match");
    } else {
      Axios.post(CHANGE_PASSWORD, {
        email: this.props.email,
        password: this.state.password,
      })
        .then((response) => response)
        .then((json) =>
          this.setState({
            success: json.data.success,
            showMessage: true,
            password: "",
            confirmPassword: "",
          })
        )
        .catch(function (error) {
          console.log(error);
        });
      // this.setState({ showLogoutMessage: true });
    }

    // console.log(this.state.email);
  };
  handleInputNewPassword = (event) => {
    this.setState({ password: event.target.value });
  };
  handleInputConfirmPassword = (event) => {
    this.setState({ confirmPassword: event.target.value });
  };

  handleChangePassword = (event) => {
    event.preventDefault();
    if (this.props.email) {
      this.passwordService();
    }
  };
  render() {
    const { email } = this.props;
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Change Password"
            disbale_search={true}
          />
        </div>
        <div className="bg-white">
          <form onSubmit={this.handleChangePassword}>
            <div className="form-group px-15 pt-30 mt-50 pb-20">
              <label className="col-12 edit-address-input-label">
                New Password
              </label>
              <div className="col-md-9">
                <input
                  type="password"
                  name="newPpassword"
                  onChange={this.handleInputNewPassword}
                  className="form-control edit-address-input"
                  placeholder="New Password"
                  required
                />
              </div>
            </div>
            <div className="form-group px-15">
              <label className="col-12 edit-address-input-label">
                Confirm Password
              </label>
              <div className="col-md-9">
                <input
                  type="password"
                  name="confirmPassword"
                  onChange={this.handleInputConfirmPassword}
                  className="form-control edit-address-input"
                  placeholder="Confirm Password"
                  required
                />
              </div>
            </div>
            <div className="mt-20 px-15 pt-15 button-block">
              <button
                type="submit"
                className="btn btn-main"
                style={{ backgroundColor: localStorage.getItem("storeColor") }}
              >
                Change Password
              </button>
            </div>
          </form>
          <br></br>
          <div className="col-12">
            {this.state.success === true ? (
              <div className="row">
                <div className="col-md-12">
                  <div className="alert alert-success text-center" role="alert">
                    The details are updated.
                  </div>
                </div>
                <div className="col-md-12">
                    <br></br>
                    <DelayLink to={"/"} delay={200}>
                      <div className="display-flex">
                        <div className="flex-auto logout-text">Go Home</div>
                      </div>
                    </DelayLink>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* {this.state.showLogoutMessage === false && (
              <div className="row">
                <div className="col-md-12">
                  <div className="alert alert-danger text-center" role="alert">
                    Error.
                  </div>
                </div>
              </div>
            )} */}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, {
  changePassword,
})(NewPassword);
