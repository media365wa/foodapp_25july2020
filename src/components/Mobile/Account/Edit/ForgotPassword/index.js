import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from "../../../Elements/BackWithSearch";
import { changePassword } from "../../../../../services/user/actions";
import Logout from "../../Logout";
import Axios from "axios";
import DelayLink from "../../../../helpers/delayLink";
import { CHECK_EMAIL } from "../../../../../configs/index";
import { VERIFY_OTP } from "../../../../../configs/index";
import NewPassword from "./NewPassword/index";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      verifyMail: "",
      success: false,
      error: false,
      otpSuccess: false,
      confirmPassword: "",
      showMessage: false,
      showOtpMessage: false,
      passwordEvent: false,
      loading: false,
      disabled: false,
      disabledMail: false,
    };
  }

  handleEmailInput = (event) => {
    this.setState({ email: event.target.value });
  };
  handleVerifyInput = (event) => {
    this.setState({ verifyMail: event.target.value });
  };
  checkMail = () => {
    Axios.post(CHECK_EMAIL, {
      email: this.state.email,
    })
      .then((response) => response)
      .then((json) =>
        this.setState({
          success: json.data.success,
          showMessage: true,
          loading: !this.state.loading,
          disabled: !this.state.disabled,
        })
      )
      .catch(function (error) {
        console.log(error);
      });
    // if (this.state.success == true) {
    //   this.setState({ disabledMail: true });
    // }
  };
  verifyOtpService = () => {
    Axios.post(VERIFY_OTP, {
      email: this.state.email,
      otp: this.state.verifyMail,
    })
      .then((response) => response)
      .then((json) =>
        this.setState({
          otpSuccess: json.data.success,
          showOtpMessage: true,
        })
      )
      .catch(function (error) {
        console.log(error);
      });
  };

  handleCheckEmail = (event) => {
    event.preventDefault();
    if (this.state.email) {
      this.setState({
        loading: true,
        disabled: true,
      });
      this.checkMail();
      //   this.setState({ loading: !this.state.loading, disabled: !this.state.disabled });
    }
  };
  handleVerifyMail = (event) => {
    event.preventDefault();
    if (this.state.verifyMail) {
      this.verifyOtpService();
    }
  };
  render() {
    let disabledMail = this.state.success == true ? true : false;
    let bgColor;
    if (this.state.disabled) {
      bgColor = "#dddddd";
    } else {
      bgColor = localStorage.getItem("storeColor");
    }
    if (this.state.otpSuccess) {
      return <NewPassword email={this.state.email} />;
    }
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Verify Email"
            disbale_search={true}
          />
        </div>
        <div className="bg-white">
          <form onSubmit={this.handleCheckEmail}>
            <div className="form-group px-15 pt-30 mt-50 pb-10">
              <label className="col-12 edit-address-input-label">
                Email ID
              </label>
              <div className="col-md-9">
                <input
                  type="email"
                  name="email"
                  onChange={this.handleEmailInput}
                  className="form-control edit-address-input"
                  placeholder="Email"
                  required
                  disabled={disabledMail}
                />
              </div>
            </div>
            {this.state.loading == true ? (
              <div className=" text-center">
                <span className="mt-10 px-15 pt-5 button-block">
                  Please wait..
                </span>
              </div>
            ) : (
              ""
            )}
            {this.state.success == false ? (
              <div className="mt-10 px-15 pt-5 button-block">
                <button
                  type="submit"
                  className="btn btn-main"
                  style={{
                    backgroundColor: bgColor,
                  }}
                  disabled={this.state.disabled}
                >
                  Submit
                </button>
              </div>
            ) : (
              ""
            )}
          </form>

          <br></br>

          {/*SUCCESS MESSAGE */}
          <div className="col-12">
            {this.state.showMessage === true ? (
              <div>
                {this.state.success == true ? (
                  <div className="row">
                    <div className="col-md-12">
                      <form onSubmit={this.handleVerifyMail}>
                        <div
                          className="alert alert-success text-center"
                          role="alert"
                        >
                          OTP has been mailed. Please check your mail
                        </div>
                        <div className="form-group px-5 pb-1">
                          <label className="col-12 edit-address-input-label">
                            OTP
                          </label>
                          <div className="col-md-9">
                            <input
                              type="text"
                              name="otp"
                              onChange={this.handleVerifyInput}
                              className="form-control edit-address-input"
                              placeholder="OTP"
                              required
                            />
                            <input
                              type="hidden"
                              name="emailId"
                              value={this.state.email}
                              className="form-control edit-address-input"
                              placeholder="OTP"
                              required
                            />
                          </div>
                        </div>
                        <div className="mt-10 px-15 pt-5 button-block">
                          <button
                            type="submit"
                            className="btn btn-main"
                            style={{
                              backgroundColor: localStorage.getItem(
                                "storeColor"
                              ),
                            }}
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                ) : (
                  <div className="alert alert-danger text-center" role="alert">
                    Check the mail again
                  </div>
                )}
                {this.state.showOtpMessage ? (
                  <div className="col-12">
                    {this.state.otpSuccess ? (
                      <div></div>
                    ) : (
                      <div
                        className="alert alert-danger text-center"
                        role="alert"
                      >
                        Please check the OTP again.
                      </div>
                    )}
                  </div>
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, {
  changePassword,
})(ForgotPassword);
