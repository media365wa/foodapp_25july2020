import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from "../../../Elements/BackWithSearch";
import { updateUserPersonalDetails } from "../../../../../services/user/actions";
import Footer from "../../../Footer";
import { logoutUser } from "../../../../../services/user/actions";
import DelayLink from "../../../../helpers/delayLink";
import Axios from "axios";
import { UPDATE_USER_PERSONAL_DETAILS_URL } from "../../../../../configs";
import { Redirect } from "react-router";
class UpdateUserDetails extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
  }

  state = {
    item: {
      name: this.props.user.data.name,
      email: this.props.user.data.email,
      phone: this.props.user.data.phone,
      roll_number: this.props.user.data.roll_number,
      token: this.props.user.data.auth_token,
      showLogoutMessage: false,
      loading: false,
      redirect: false,
      seconds: 0,
    },
  };

  logout() {
    localStorage.clear();
    window.location.href = "/";
  }

  handleChange = (e) => {
    e.persist();
    this.setState((prevState) => ({
      item: { ...prevState.item, [e.target.name]: e.target.value },
    }));
    // console.log(this.state.item);
  };

  handleUpdateDetails = (event) => {
    // console.log(this.state.item);
    this.setState({ userId: this.state.item.name });
    this.setState({ roll_number: this.state.item.roll_number });
    this.setState({ email: this.props.user.data.email });
    this.setState({ token: this.props.user.data.auth_token });
    this.setState({ loading: true });
    // perform all neccassary validations
    if (
      this.state.item.name == "" ||
      this.state.item.email == "" ||
      this.state.item.phone == ""
    ) {
      alert("Please fill all the details");
    } else {
      // event.preventDefault();

      this.updateUserPersonalDetails(
        this.props.user.data.id,
        this.state.item.name,
        this.state.item.email,
        this.state.item.phone,
        this.state.item.roll_number,
        this.props.user.data.auth_token
      );

      // this.timeout = setTimeout(() => this.setState({ redirect: true }), 5000);
    }
  };
  updateUserPersonalDetails = (
    user_id,
    name,
    email,
    phone,
    roll_number,
    token
  ) => {
    Axios.post(
      UPDATE_USER_PERSONAL_DETAILS_URL + "/" + user_id + "?token=" + token,
      {
        token: token,
        name: name,
        email: email,
        phone: phone,
        roll_number: roll_number,
        user_id: user_id,
      }
    )
      .then((response) => {
        const res = response.data;
        this.setState({ loading: false });
        this.setState({ showLogoutMessage: res.success });
        // return dispatch({ type: UPDATE_USER_PERSONAL_DETAILS, payload: user });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  render() {
    const { user } = this.props;
    if (this.state.showLogoutMessage === true) {
      return <Redirect to={"/cart"} />;
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <div className="height-100 overlay-loading">
            <div>
              <img
                src="/assets/img/loading-food.gif"
                alt={localStorage.getItem("pleaseWaitText")}
              />
            </div>
          </div>
        ) : (
          <React.Fragment>
            <div className="input-group-prepend">
              <BackWithSearch
                boxshadow={true}
                has_title={true}
                title="Edit Personal Details"
                disbale_search={true}
              />
            </div>
            {/* <form onSubmit={this.handleUpdateDetails}> */}
            <div className="bg-white pt-80 pb-80 height-100-percent">
              <div className="col-12">
                <div className="form-row">
                  <label className="col-sm-2 col-form-label" htmlFor="name">
                    Name:{" "}
                  </label>
                  <input
                    className="form-control col-sm-10"
                    id="name"
                    type="text"
                    name="name"
                    required
                    onChange={this.handleChange}
                    autoFocus
                    placeholder="Name"
                    defaultValue={user.data.name}
                  />
                </div>
                <div className="form-row">
                  <label className="col-sm-2 col-form-label" htmlFor="name">
                    Email:{" "}
                  </label>
                  <input
                    className="form-control col-sm-10"
                    id="email"
                    type="text"
                    required
                    name="email"
                    onChange={this.handleChange}
                    autoFocus
                    placeholder="Email"
                    disabled
                    defaultValue={user.data.email}
                  />
                </div>
                <div className="form-row">
                  <label className="col-sm-2 col-form-label" htmlFor="name">
                    Phone:{" "}
                  </label>
                  <input
                    className="form-control col-sm-10"
                    id="phone"
                    type="number"
                    required
                    name="phone"
                    autoFocus
                    onChange={this.handleChange}
                    placeholder="Phone"
                    defaultValue={user.data.phone}
                  />
                </div>
                <div className="form-row">
                  <label className="col-sm-2 col-form-label" htmlFor="name">
                    Roll Number:{" "}
                  </label>
                  <input
                    className="form-control col-sm-10"
                    id="roll_number"
                    type="text"
                    required
                    name="roll_number"
                    autoFocus
                    onChange={this.handleChange}
                    placeholder="Roll Number"
                    disabled
                    defaultValue={
                      user.data.roll_number ? user.data.roll_number : ""
                    }
                  />
                </div>
                <br></br>

                <div className="form-row">
                  <button
                    onClick={this.handleUpdateDetails}
                    className="btn btn-primary col-sm-2 col-form-label"
                    type="submit"
                  >
                    Update
                  </button>
                </div>
                <br></br>
                {this.state.showLogoutMessage === true && (
                  <div className="row">
                    <div className="col-md-12">
                      <div
                        className="alert alert-success text-center"
                        role="alert"
                      >
                        The details are updated.
                      </div>
                    </div>
                    <div className="col-md-12">
                      <span className="blockquote-footer">
                        Please to Logout and login again to save the changes.
                      </span>
                      <br></br>
                      <DelayLink to={"/my-account"} delay={200}>
                        <div className="display-flex">
                          <div className="flex-auto border-0">
                            <h6>My Account</h6>
                          </div>
                        </div>
                      </DelayLink>
                    </div>
                  </div>
                )}
                {this.state.showLogoutMessage === false && (
                  <div className="row">
                    <div className="col-md-12">
                      <div className="alert alert-success" role="alert">
                        Error.
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>

            {/* </form> */}

            <Footer />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
  // }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, {
  updateUserPersonalDetails,
  logoutUser,
})(UpdateUserDetails);
