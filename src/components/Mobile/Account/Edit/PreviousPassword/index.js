import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from "../../../Elements/BackWithSearch";
import DelayLink from "../../../../helpers/delayLink";
import Axios from "axios";
import { CHECK_OLD_PASSWORD } from "../../../../../configs";
import { changePassword } from "../../../../../services/user/actions";
class PreviousPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      isCorrect: false,
      showCountMessage: true,
      showErrorMessage: false,
      showSuccessMessage: false,
      disabledvalue: true,
      togglePassword: false,
      color: "#d3d3d3",
      password: "",
      confirmPassword: "",
      showLogoutMessage: false,
    };
    this.toggleShow = this.toggleShow.bind(this);
    // console.log(this.props);
  }
  handleInputPassword = (event) => {
    this.setState({ oldPassword: event.target.value });
    this.setState({ isCorrect: false });
    event.preventDefault();
    const re = new RegExp("[0-9a-zA-Z]{5,}");
    const isOk = re.test(this.state.oldPassword);
    if (isOk === false) {
      this.setState({ disabledvalue: true });
    } else {
      this.setState({ disabledvalue: false });
      this.setState({ showCountMessage: false });
    }
  };
  toggleShow() {
    this.setState({ togglePassword: !this.state.togglePassword });
  }
  handleInputNewPassword = (event) => {
    this.setState({ password: event.target.value });
  };
  handleInputConfirmPassword = (event) => {
    this.setState({ confirmPassword: event.target.value });
  };
  handleNewPassword = (event) => {
    const { password, confirmPassword } = this.state;
    // perform all neccassary validations
    if (password !== confirmPassword) {
      alert("Passwords don't match");
    } else {
      alert("Password correct");
      event.preventDefault();
      this.setState({ loading: true });
      this.props.changePassword(
        this.props.user.data.id,
        this.props.user.data.email,
        this.props.user.data.auth_token,
        this.state.confirmPassword
      );
      this.setState({ showLogoutMessage: true });
    }
  };

  handleOldPassword = (event) => {
    // perform all neccassary validations
    if (this.state.oldPassword.length < 6) {
      // this.setState({showCountMessage: true})
    } else {
      event.preventDefault();
      Axios.post(
        CHECK_OLD_PASSWORD +
          "/" +
          this.props.user.data.id +
          "?token=" +
          this.props.user.data.auth_token,
        {
          userId: this.props.user.data.id,
          password: this.state.oldPassword,
          accessToken: this.props.user.data.auth_token,
        }
      )
        .then((response) => {
          const successType = response.data;
          if (successType.success) {
            this.setState({ isCorrect: successType.success });
          }
          this.state.isCorrect === false
            ? this.setState({
                showErrorMessage: true,
                showSuccessMessage: false,
              })
            : this.setState({
                showSuccessMessage: true,
                showErrorMessage: false,
              });
          // console.log(this.state.showErrorMessage);
        })
        .catch(function (error) {
          console.log(error);
        });
      // this.setState({ showLogoutMessage: true });
    }
  };
  render() {
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Change Password"
            disbale_search={false}
          />
        </div>
        <div className="bg-white">
          <form onSubmit={this.handleOldPassword}>
            <div className="form-group px-15 pt-30 mt-50 pb-20">
              <label className="col-12 edit-address-input-label">
                Old Password
              </label>
              <div className="col-md-9">
                <input
                  pattern=".{0}|.{6,}"
                  type={
                    this.state.togglePassword === false ? "password" : "text"
                  }
                  name="Oldpassword"
                  onChange={this.handleInputPassword}
                  className="form-control edit-address-input"
                  placeholder="Old Password"
                  required
                />
                
              </div>
              {this.state.showSuccessMessage === false ? (
                <div className="">
                  {this.state.oldPassword ? (
                  <div className="text-right">
                    <a
                      onClick={this.toggleShow}
                      style={{
                        cursor: "pointer",
                        fontSize: "12px",
                        color: "#0366d6 ",
                      }}
                    >
                      {this.state.togglePassword === false ? "Show" : "Hide"}
                    </a>
                  </div>
                ) : (
                  ""
                )}
                  <ul>
                    <li
                      style={{
                        fontSize: "12px",
                      }}
                    >
                      Password must more than 6 characters!
                    </li>
                  </ul>
                </div>
              ) : (
                ""
              )}

              {this.state.isCorrect == false ? (
                <div>
                  {this.state.showErrorMessage === true ? (
                    <div
                      className="alert alert-danger text-center"
                      role="alert"
                    >
                      Please check the password and try again.
                    </div>
                  ) : (
                    ""
                  )}
                  <button
                    type="submit"
                    className="btn btn-main"
                    style={{
                      backgroundColor:
                        this.state.disabledvalue === false
                          ? localStorage.getItem("storeColor")
                          : "#d3d3d3",
                    }}
                    disabled={this.state.disabledvalue}
                  >
                    Check Password
                  </button>
                </div>
              ) : (
                <div>
                  {this.state.showSuccessMessage === true ? (
                    <div>
                      <div
                        className="alert alert-success text-center"
                        role="alert"
                      >
                        Password correct. Please enter a new password below.
                      </div>
                      <div className="bg-white">
                        <form onSubmit={this.handleNewPassword}>
                          <div className="form-group ">
                            <label className="col-12 edit-address-input-label">
                              New Password
                            </label>
                            <div className="col-md-9">
                              <input
                                type="password"
                                name="newPpassword"
                                onChange={this.handleInputNewPassword}
                                className="form-control edit-address-input"
                                placeholder="New Password"
                                required
                              />
                            </div>
                          </div>
                          <div className="form-group ">
                            <label className="col-12 edit-address-input-label">
                              Confirm Password
                            </label>
                            <div className="col-md-9">
                              <input
                                type="password"
                                name="confirmPassword"
                                onChange={this.handleInputConfirmPassword}
                                className="form-control edit-address-input"
                                placeholder="Confirm Password"
                                required
                              />
                            </div>
                          </div>
                          <div className="mt-20 px-15 pt-15 button-block">
                            <button
                              type="submit"
                              className="btn btn-main"
                              style={{
                                backgroundColor: localStorage.getItem(
                                  "storeColor"
                                ),
                              }}
                            >
                              Change Password
                            </button>
                          </div>
                        </form>
                        <br></br>
                        <div className="col-12">
                          {this.state.showLogoutMessage === true ? (
                            <div className="row">
                              <div className="col-md-12">
                                <div
                                  className="alert alert-success text-center"
                                  role="alert"
                                >
                                  The details are updated.
                                </div>
                              </div>
                              <div className="col-md-12">
                                <span className="blockquote-footer">
                                  Please to Logout and login again to save the
                                  changes.
                                </span>
                                <div>
                                  <br></br>
                                  <DelayLink to={"/"} delay={200}>
                                    <div className="display-flex">
                                      <div className="flex-auto logout-text">
                                        Go Home
                                      </div>
                                    </div>
                                  </DelayLink>
                                </div>
                              </div>
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              )}
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, { changePassword })(PreviousPassword);
