import React, { Component } from "react";
import Moment from "react-moment";
import { formatPrice } from "../../../../helpers/formatPrice";
import DelayLink from "../../../../helpers/delayLink";
import BackWithSearch from "../../../Elements/BackWithSearch";
class OrderStatus extends Component {
  __getOrderStatus = (id) => {
    if (id === 0) {
      return "Pending Payment";
    }
    if (id === 1) {
      return "Order Placed";
    }
    if (id === 2) {
      return "Preparing Order";
    }
    if (id === 3) {
      return "Delivery Guy Assigned";
    }
    if (id === 4) {
      return "Order Picked Up";
    }
    if (id === 5) {
      return "Delivered";
    }
    if (id === 6) {
      return "Canceled";
    }
    if (id === 7) {
      return "Paid From Wallet";
    }
    if (id === 8) {
      return "Declined";
    }
  };
  _getTotalItemCost = (item) => {
    let itemCost = parseFloat(item.price) * item.quantity;
    if (item.order_item_addons.length) {
      item.order_item_addons.map((addon) => {
        itemCost += parseFloat(addon.addon_price) * item.quantity;
        return itemCost;
      });
    }
    return formatPrice(itemCost);
  };
  handleClick = (e, order) => {
    localStorage.setItem("orderWithIssue", JSON.stringify(order));
  };
  render() {
    const { order } = this.props;
    console.log(this.props)
    return (
      <React.Fragment>
        <BackWithSearch
          boxshadow={true}
          has_title={true}
          title='Order Status'
          disbale_search={true}
        />
        <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
            <div className="mb-20">
          <div className="display-flex">
              as
          </div>
        </div>
        </div>
      </React.Fragment>
    );
  }
}

export default OrderStatus;
