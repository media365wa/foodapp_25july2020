import React, { Component } from "react";
import { connect } from "react-redux";
import Footer from "../../../Footer";
import { VIEW_ORDER_URL } from "../../../../../configs/index";
import Moment from "react-moment";
import Axios from "axios";
import BackWithSearch from "../../../Elements/BackWithSearch";
class ViewOrderDetails extends Component {
  restaurantId;
  constructor(props) {
    super(props);
    this.state = { order: [], restaurants: [], redirect: false };
    this.getShopName = this.getShopName.bind(this);
  }
  state = {
    loading: false,
  };
  componentDidMount() {
    this.setState({ loading: true });
    this.getOrderDetails();
    this.setState({ loading: false });
  }
  getOrderDetails = () => {
    Axios.post(VIEW_ORDER_URL + "/" + localStorage.getItem("OrderReference"))
      .then((response) => response)
      .then((json) =>
        this.setState({
          order: json.data,
        })
      )
      .catch(function (error) {
        console.log(error);
      });
  };

  // getRestaurants = () => {
  //   Axios.post(SELECT_RESTAURANTS_URL)
  //     .then((response) => response)
  //     .then((json) =>
  //       this.setState({
  //         restaurants: json.data,
  //       })
  //     )
  //     // console.log()
  //     .catch(function (error) {
  //       console.log(error);
  //     });
  // };

  getShopName = (restuarntIds) => {
    // e.preventDefault();
    // console.log(this.state.restaurants);
    console.log(restuarntIds)
    let getList = localStorage.getItem("ShopList");
    let list = JSON.parse(getList);
    let rest = list.filter((x) => x.id == restuarntIds);
    console.log(rest)
    return rest ? rest[0].name : "";
  };

  render() {
    const { order } = this.state;
    // console.log(order);
    return (
      <React.Fragment>
        {" "}
        {this.state.loading ? (
          <div className="height-100 overlay-loading">
            <div>
              <img
                src="/assets/img/loading-food.gif"
                alt={localStorage.getItem("pleaseWaitText")}
              />
            </div>
          </div>
        ) : (
          <React.Fragment>
            <div className="input-group-prepend">
              <BackWithSearch
                boxshadow={true}
                has_title={true}
                title="ORDER DETAILS"
                disbale_search={true}
              />
            </div>
            <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
              <div>
                <div className="display-flex pb-5">
                  <div className="flex-auto text-left">
                    <b>ORD-REF:</b>{" "}
                    <span className="text-white bg-dark">
                      {localStorage.getItem("OrderReference")}
                    </span>
                  </div>{" "}
                </div>
                <div className="display-flex pb-5">
                  <div className="flex-auto text-left">
                    <b>DATE:</b>{" "}
                    <span className="text-white bg-dark">
                      <Moment format="DD-MM-YYYY ">
                        {localStorage.getItem("OrderDate")}
                      </Moment>
                    </span>
                  </div>{" "}
                </div>
                <div className="display-flex pb-5">
                  <div className="flex-auto text-left">
                    <b>SHOP:</b>{" "}
                    <span className="text-white bg-dark">
                      {order.slice(0, 1).map((ord, index) =>
                        ord.unique_order_id ? (
                          <span
                            className="text-white bg-dark"
                            key={ord.restaurant_id}
                          >
                            {this.getShopName(ord.restaurant_id)}
                          </span>
                        ) : (
                          "d"
                        )
                      )}
                    </span>
                  </div>{" "}
                </div>
                <div className="display-flex pb-5">
                  <div className="flex-auto text-left">
                    <b>PAYMENT MODE:</b>{" "}
                    {order.slice(0, 1).map((ord, index) =>
                      ord.unique_order_id ? (
                        <span
                          className="text-white bg-dark"
                          key={ord.payment_mode}
                        >
                          {ord.payment_mode}
                        </span>
                      ) : (
                        "d"
                      )
                    )}
                  </div>{" "}
                </div>
                <hr></hr>
                <div className="display-flex pb-5  bg-light">
                  <div className="flex-auto text-left ">
                    <b>ITEMS</b>
                  </div>{" "}
                  <div className="flex-auto text-center ">
                    <b>PRICE * QTY</b>
                  </div>{" "}
                  <div className="flex-auto text-right ">
                    <b>TOTAL</b>
                  </div>{" "}
                </div>
                <br></br>
                {order.map((item, index) =>
                  item.item_id ? (
                    item.declined == 0 ? (
                      <div key={item.id} className="display-flex pb-5">
                        <div className="flex-auto text-left ">
                          <div key={item.item_id}>{item.name}</div>
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto text-center ">
                          <div key={item.item_id}>
                            {item.price} * {item.quantity}
                          </div>
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto text-right ">
                          <div key={item.item_id}>
                            + {item.price * item.quantity}
                          </div>
                        </div>{" "}
                      </div>
                    ) : (
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left ">
                          <div key={item.item_id} className="text-danger">
                            {item.name}
                            <br />
                            <h6 className="text-danger">(declined)</h6>
                          </div>
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto text-center ">
                          <div key={item.item_id} className="text-danger">
                            {item.price} * {item.quantity}
                          </div>
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto text-right ">
                          <div key={item.item_id} className="text-danger">
                            - {item.price * item.quantity}
                          </div>
                        </div>{" "}
                      </div>
                    )
                  ) : (
                    "Not Available"
                  )
                )}

                <hr></hr>
                <div className="display-flex pb-5">
                  <div className="flex-auto text-right ">
                    <b>GRAND TOTAL:</b> {localStorage.getItem("currencyFormat")}{" "}
                    {order.slice(0, 1).map((ord, index) =>
                      ord.unique_order_id ? (
                        <span className="text-danger h4" key={ord.total}>
                          {" "}
                          {ord.total}
                        </span>
                      ) : (
                        " "
                      )
                    )}
                  </div>
                </div>
              </div>
            </div>

            <Footer />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  restaurants: state.restaurants.restaurants,
  wallets: state.wallets.wallets,
  user: state.user.user,
});

export default connect(mapStateToProps, {})(ViewOrderDetails);
