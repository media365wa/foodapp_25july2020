import React, { Component } from "react";
import { connect } from "react-redux";
// import Ink from "react-ink";
import BackWithSearch from "../../Elements/BackWithSearch";
import DelayLink from "../../../helpers/delayLink";
import Footer from "../../Footer";
import Axios from "axios";
import { PAYMENTMODE_STATUS } from "../../../../configs/index";
class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: [],
      loading: false,
    };
    this.getPaymentDetails();
  }
  //   componentDidMount() {
  //     this.getPaymentDetails();
  //   }
  getPaymentDetails = () => {
    this.setState({ loading: true });
    Axios.post(PAYMENTMODE_STATUS)
      .then((response) =>
        this.setState({ mode: response.data, loading: false })
      )
      .catch(function (error) {
        console.log(error);
      });
  };

  modeFunction(mode) {
    return mode.map((item, index) => {
      const { id, name, is_active, display_name, route } = item;
      return is_active == 1 ? (
        <DelayLink key={id} to={route} delay={200}>
          <div className="col-12 text-center">
            <div className="block ">
              <div className="block-content ">
                <button type="button" className="btn btn-light btn-block">
                  {display_name}
                </button>
              </div>
            </div>
          </div>
        </DelayLink>
      ) : (
        ""
      );
    });
  }

  render() {
    // console.log(this.state.mode);
    const { mode } = this.state;
    console.log(mode.data);
    return (
      <React.Fragment>
        <React.Fragment>
          <div className="input-group-prepend">
            <BackWithSearch
              boxshadow={true}
              has_title={true}
              title="Pay"
              disbale_search={true}
            />
          </div>
          {mode.data ? (
            <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
              {this.modeFunction(mode.data)}
            </div>
          ) : (
            <div className="height-100 overlay-loading">
              <div>
                <img
                  src="/assets/img/loading-food.gif"
                  alt={localStorage.getItem("pleaseWaitText")}
                />
              </div>
            </div>
          )}
          <Footer />
        </React.Fragment>
      </React.Fragment>
    );
  }
}
export default connect()(Payment);
