import React, { Component } from "react";
import DelayLink from "../../../helpers/delayLink";
import { connect } from "react-redux";
import BackWithSearch from "../../Elements/BackWithSearch";
import Footer from "../../Footer";
import { Link } from "react-router-dom";
class PersonalDeatils extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
  }
  render() {
    const { user } = this.props;
    // console.log(user);
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Personal Details"
            disbale_search={true}
          />
        </div>
        <div className="bg-white pt-80 pb-80 height-100-percent">
          <div className="col-12">
            <div className="form-row">
              <label className="col-sm-2 col-form-label" htmlFor="name">
                Name:{" "}
              </label>
              <input
                className="form-control col-sm-10"
                id="name"
                type="text"
                autoFocus
                placeholder="Name"
                defaultValue={user.data.name}
                disabled
              />
            </div>
            <div className="form-row">
              <label className="col-sm-2 col-form-label" htmlFor="name">
                Email:{" "}
              </label>
              <input
                className="form-control col-sm-10"
                id="email"
                type="text"
                autoFocus
                placeholder="Email ID"
                defaultValue={user.data.email}
                disabled
              />
            </div>
            <div className="form-row">
              <label className="col-sm-2 col-form-label" htmlFor="name">
                Phone:{" "}
              </label>
              <input
                className="form-control col-sm-10"
                id="phone"
                type="text"
                autoFocus
                placeholder="Phone"
                defaultValue={user.data.phone}
                disabled
              />
            </div>
            <div className="form-row">
              <label className="col-sm-2 col-form-label" htmlFor="name">
                Roll Number:{" "}
              </label>
              <input
                className="form-control col-sm-10"
                id="roll_number"
                type="text"
                autoFocus
                placeholder="Roll Number"
                defaultValue={user.data.roll_number? user.data.roll_number:""}
                disabled
              />
            </div>
            <div className="form-row">
              <label className="col-sm-2 col-form-label" htmlFor="name">
                Address:{" "}
              </label>

              <input
                className="form-control col-sm-10"
                id="name"
                type="text"
                autoFocus
                defaultValue={
                  user.data.default_address == null
                    ? ""
                    : [
                        user.data.default_address.address,
                        user.data.default_address.house,
                        user.data.default_address.landmark,
                      ]
                }
                disabled
              />

              <span className="blockquote-footer">
                Visit "Manage Address" tab under "My Account" or{" "}
                <Link to="/my-addresses"> <span className="text-danger">Click Here</span> </Link>.
              </span>
            </div>
            <br></br>

            <DelayLink to={"/update-user-details"} delay={200}>
              <div className="form-row">
                <button className="btn btn-primary col-sm-2 col-form-label">
                  Edit
                </button>
              </div>
            </DelayLink>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(PersonalDeatils);
