import React, { Component } from "react";
import { connect } from "react-redux";
// import ContentLoader from "react-content-loader";
import { Redirect } from "react-router";
import BackWithSearch from "../../Elements/BackWithSearch";
import QrReader from "react-qr-reader";
// import { formatPrice } from "../../../helpers/formatPrice";
import {
  saveWalletTransaction,
} from "../../../../services/wallet/actions";
import { getSingleOrder } from "../../../../services/orders/actions";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
class QRCode extends Component {
  state = {
    loading: false,
    result: "",
    amounttopay: "0.00",
    paymentStatus: "pending",
    amountErr: false,
    walletBalLowErr: false,
    qrCodeOrder: false,
    open: false,
  };

  componentDidMount() {
    // const { user } = this.props;
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  handleScan = (data) => {
    if (data) {
      // console.log(data);
      var a = JSON.parse(data);
      var amnt = parseFloat(a.totalAmount);
      if (amnt > 0) {
        this.setState({
          qrCodeOrder: true,
        });
        this.setState({
          amounttopay: amnt,
        });
      } else {
        this.setState({
          qrCodeOrder: false,
        });
      }
      this.setState({
        result: data,
      });
    }
  };
  handleError = (err) => {
    console.error(err);
  };

  handleInput = (event) => {
    alert(event.target.value);

    this.setState({ qrCodeOrder: false });
    var amnt = event.target.value;
    this.setState({ amounttopay: amnt });
    localStorage.setItem("amounttopay", amnt);

    if (amnt > 0) {
      this.setState({ amountErr: false });
    }
    // else {
    //   this.setState({ amountErr: true });
    // }

    const { wallets } = this.props;
    if (amnt <= wallets[0].balance) {
      this.setState({ walletBalLowErr: false });
    } else {
      this.setState({ walletBalLowErr: true });
    }
  };

  handlePayFromWallet = (walletBal, wallet_id) => {
    var amntNum = parseFloat(this.state.amounttopay);
    var walletBlnce = parseFloat(walletBal);
    // var amnttopay = parseFloat(this.state.amounttopay);
    // if (amnttopay > 0) {
    this.setState({
      amountErr: false,
    });

    if (amntNum <= walletBlnce) {
      this.setState({
        walletBalLowErr: false,
      });
      const { user, wallets } = this.props;
      if (user.success) {
        this.setState({ loading: true });
        this.props.saveWalletTransaction(
          user.data.auth_token,
          user.data.id,
          wallets[0].id,
          this.state.result,
          amntNum
        );
        this.setState({ loading: false });
        this.setState({paymentStatus: true});
      }
    } else {
      this.setState({
        walletBalLowErr: true,
      });
    }
    // }
    // else {
    // this.setState({
    //   amountErr: true,
    // });
    // }
  };

  render() {
    const { open } = this.state;
    if (window.innerWidth > 768) {
      return <Redirect to="/" />;
    }

    if (localStorage.getItem("storeColor") === null) {
      return <Redirect to={"/"} />;
    }
    const { wallets } = this.props;
    // const previewStyle = {
    //   height: 240,
    //   width: 320,
    // };
    return (
      <React.Fragment>
        {/* {this.state.amountErr && (
          <div className="auth-error" style={{ marginLeft: "-0.7rem" }}>
            <div className="error-shake">Please enter an amount to pay.</div>
          </div>
        )} */}
        {this.state.walletBalLowErr && (
          <div className="auth-error" style={{ marginLeft: "-0.7rem" }}>
            <div className="error-shake">
              There is not enough balance on your wallet
            </div>
          </div>
        )}
        {this.state.loading ? (
          <div className="height-100 overlay-loading">
            <div>
              <img
                src="/assets/img/loading-food.gif"
                alt={localStorage.getItem("pleaseWaitText")}
              />
            </div>
          </div>
        ) : (
          <React.Fragment>
            <BackWithSearch
              boxshadow={true}
              has_title={true}
              title="Scan to Pay"
              disbale_search={true}
            />
            <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
              {this.state.result == "" && (
                <div>
                  <QrReader
                    delay={300}
                    onError={this.handleError}
                    onScan={this.handleScan}
                    style={{ width: "100%" }}
                  />
                  <p>{this.state.result}</p>
                </div>
              )}

              {this.state.result != "" &&
                this.state.paymentStatus == "pending" && (
                  <div>
                    {
                      <label className="col-12 edit-address-input-label"></label>
                    }
                    <div className="input-group mb-20">
                      <div className="input-group-prepend">
                        <button className="btn apply-coupon-btn font-size-h3 font-w600">
                          {localStorage.getItem("currencyFormat")}
                        </button>
                      </div>
                      {this.state.qrCodeOrder == true && (
                        <input
                          className="form-control apply-coupon-input font-size-h3 font-w600"
                          type="number"
                          readOnly
                          value={this.state.amounttopay || ""}
                        />
                      )}
                      {this.state.qrCodeOrder == false && (
                        <input
                          className="form-control apply-coupon-input font-size-h3 font-w600"
                          type="number"
                          autoFocus
                          placeholder="Enter Amount"
                          onChange={this.handleInput}
                          value={this.state.amounttopay || ""}
                        />
                      )}
                    </div>

                    <div>
                      <div className="block block-link-shadow text-right shadow-light">
                        <div className="block-content block-content-full clearfix">
                          <div className="float-left mt-10">
                            <i className="si si-wallet fa-3x text-body-bg-dark" />
                          </div>
                          <div className="font-size-h4">Wallet Balance</div>
                          <div className="font-size-h3 font-w600">
                            {localStorage.getItem("currencyFormat")}
                            {wallets[0].balance}
                          </div>
                          <div className="font-size-sm font-w600 text-uppercase text-muted">
                            Pay With Wallet - {this.state.amounttopay}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex flex-row bd-highlight mb-3">
                      <button
                        className="p-2 btn btn-dark col-sm-2 col-form-label"
                        onClick={this.onOpenModal}
                        disabled={!this.state.amounttopay}
                      >
                        Pay
                      </button>
                      <Modal open={open} onClose={this.onCloseModal} center>
                        <br></br>
                        <div className="form-control apply-coupon-input font-size-h3 font-w600">
                          <span>Confirm to pay from Wallet</span>
                          <br></br>
                          <span>
                            <b>Amount:</b>{" "}
                          </span>
                          {localStorage.getItem("currencyFormat")}{" "}
                          {this.state.amounttopay}
                          <br></br>
                        </div>
                        <div className="d-flex flex-row bd-highlight mb-3">
                          <button
                            className="btn btn-danger col-sm-5"
                            onClick={this.onCloseModal}
                          >
                            Cancel
                          </button>
                          <button
                            className="btn btn-dark col-sm-4"
                            onClick={() => {
                              this.handlePayFromWallet(
                                wallets[0].balance,
                                wallets[0].id
                              );
                              this.onCloseModal();
                            }}
                          >
                            Yes
                          </button>
                        </div>
                      </Modal>
                    </div>
                  </div>
                )}
              {this.state.result != "" && this.state.paymentStatus == true && (
                <div className="row">
                  <div className="col-md-12">
                    <div className="block block-link-shadow">
                      <div className="block-content block-content-full clearfix py-0">
                        <div className="float-right">
                          <img
                            src="/assets/img/order-placed.gif"
                            className="img-fluid img-avatar"
                            alt="success"
                          />
                        </div>
                        <div
                          className="float-left mt-20"
                          style={{ width: "75%" }}
                        >
                          <div className="font-w600 font-size-h4 mb-5">
                            Success
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              {this.state.result != "" && this.state.paymentStatus == false && (
                <div className="row">
                  <div className="col-md-12">
                    <div className="block block-link-shadow">
                      <div className="block-content block-content-full clearfix py-0">
                        <div className="float-right">
                          <img
                            src="/assets/img/order-canceled.png"
                            className="img-fluid img-avatar"
                            alt="Failed"
                          />
                        </div>
                        <div
                          className="float-left mt-20"
                          style={{ width: "75%" }}
                        >
                          <div className="font-w600 font-size-h4 mb-5">
                            Failed
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user,
  wallets: state.wallets.wallets,
});

export default connect(mapStateToProps, {
  saveWalletTransaction,
  getSingleOrder,
})(QRCode);
