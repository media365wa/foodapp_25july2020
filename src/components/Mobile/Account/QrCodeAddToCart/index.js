import React, { Component } from "react";
import { connect } from "react-redux";
// import ContentLoader from "react-content-loader";
import { Redirect } from "react-router";
import BackWithSearch from "../../Elements/BackWithSearch";
import QrReader from "react-qr-reader";
// import { formatPrice } from "../../../helpers/formatPrice";
import { messQr } from "../../../../services/wallet/actions";
import { getSingleOrder } from "../../../../services/orders/actions";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import { SAVE_QR_CODE } from "./actionTypes";
import { SAVE_QR_CODE_MESS } from "../../../../configs";
import { getWalletTransactions } from "../../../../services/wallet/actions";
import DelayLink from "../../../helpers/delayLink";
import Moment from "react-moment";
import Axios from "axios";
class CartQrCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      result: "",
      amounttopay: "0.00",
      restaurantId: "",
      itemId: "",
      itemName: "",
      itemPrice: "",
      paymentStatus: "pending",
      amountErr: false,
      walletBalLowErr: false,
      qrCodeOrder: false,
      open: false,
      quantity: 1,
      total: 0,
      success: true,
      message: "",
      newOrder: "",
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const { user, wallets } = this.props;
    this.props.getWalletTransactions(user.data.auth_token, user.data.id);
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  handleScan = (data) => {
    console.log(data);
    if (data) {
      // console.log(data);
      var a = JSON.parse(data);
      var amnt = parseFloat(a.item_price);
      var restaurnatId = parseFloat(a.restaurant_id);
      var itemId = parseFloat(a.item_id);
      var itemName1 = a.item_name;
      if (amnt > 0) {
        this.setState({
          qrCodeOrder: true,
        });
        this.setState({
          amounttopay: amnt,
        });
        this.setState({
          restaurantId: restaurnatId,
        });
        this.setState({
          itemId: itemId,
        });
        this.setState({
          itemName: itemName1,
        });
      } else {
        this.setState({
          qrCodeOrder: false,
        });
      }
      this.setState({
        result: data,
      });
    }
  };
  handleError = (err) => {
    console.error(err);
  };

  handleInput = (event) => {
    alert(event.target.value);

    this.setState({ qrCodeOrder: false });
    var amnt = event.target.value;
    this.setState({ amounttopay: amnt });
    localStorage.setItem("amounttopay", amnt);

    if (amnt > 0) {
      this.setState({ amountErr: false });
    }

    const { wallets } = this.props;
    if (amnt <= wallets[0].balance) {
      this.setState({ walletBalLowErr: false });
    } else {
      this.setState({ walletBalLowErr: true });
    }
  };

  handleChange(e) {
    const val = e.target.value;
    this.setState({
      quantity: val,
    });
  }

  getMessage() {
    if (this.state.message === "order_placed") {
      return "Order Placed Successfully";
    } else if (this.state.message === "less_quantity") {
      return "Less Quantity";
    } else if (this.state.message === "less_balance") {
      return "Less Balance";
    } else {
      return "Please contact the admin.";
    }
  }

  handlePayFromWallet = (walletBal, wallet_id) => {
    var amntNum = parseFloat(this.state.amounttopay);
    var walletBlnce = parseFloat(walletBal);
    var payment_method = "messQrCode";
    this.setState({
      amountErr: false,
    });

    if (amntNum <= walletBlnce) {
      this.setState({
        walletBalLowErr: false,
      });
      const { user, wallets } = this.props;
      if (user.success) {
        this.setState({ loading: true });
        Axios.post(SAVE_QR_CODE_MESS, {
          token: user.data.auth_token,
          u: user.data.id,
          w: wallets[0].id,
          order_reference: this.state.result,
          amount: amntNum,
          restaurant_id: this.state.restaurantId,
          item_id: this.state.itemId,
          quantity: this.state.quantity,
          payment_method: "messQrCode",
        })
          .then((response) => response)
          .then((json) =>
            this.setState({
              success: json.data.success,
              message: json.data.message,
              newOrder: json.data.data,
              loading: false,
              paymentStatus: true,
            })
          )
          .catch(function (error) {
            console.log(error);
          });
        // this.setState({ loading: false });
        // this.setState({ paymentStatus: true });
      }
    } else {
      this.setState({
        walletBalLowErr: true,
      });
    }
  };

  displayOrder(newOrder) {
    console.log(newOrder);
    const {
      unique_order_id,
      restaurantName,
      itemName,
      itemPrice,
      quantity,
      total,
      payment_mode,
      created_at,
    } = newOrder;
    return (
      <React.Fragment>
        <div className="display-flex pb-5">
          <div className="flex-auto text-left">
            <b>Order Reference:</b>{" "}
            <span>{unique_order_id}</span>
          </div>{" "}
        </div>
        <div className="display-flex pb-5">
          <div className="flex-auto text-left">
            <b>Date:</b>{" "}
            <span>
              <Moment format="DD-MM-YYYY ">{created_at}</Moment>
            </span>
          </div>{" "}
        </div>
        <div className="display-flex pb-5">
          <div className="flex-auto text-left">
            <b>Restaurant Name: </b>{" "}
            <span>{restaurantName}</span>
          </div>{" "}
        </div>
        <div className="display-flex pb-5">
          <div className="flex-auto text-left">
            <b>Payment Mode: </b>{" "}
            <span>{payment_mode}</span>
          </div>{" "}
        </div>
      <hr></hr>
      <div className="display-flex pb-5">
        <div className="text-left" style={{ width: "50%" }}>
          <b>ITEMS</b>
        </div>{" "}
        <div className="text-center" style={{ width: "30%" }}>
          <b>PRICE * QTY</b>
        </div>{" "}
        <div className="text-right" style={{ width: "20%" }}>
          <b>TOTAL</b>
        </div>{" "}
      </div>
      <br></br>
      <div className="col-12">
        <div className="row">
          <div className="text-left" style={{ width: "50%" }}>
            {itemName}
            <br></br>
          </div>{" "}
          <div className="flex-auto text-center" style={{ width: "30%" }}>
            {itemPrice} * {quantity}
            <br></br>
          </div>{" "}
          <div className="text-right" style={{ width: "20%" }}>{localStorage.getItem("currencyFormat")} {itemPrice * quantity}</div>{" "}
    {/*}  <br />
      <div className="row">
      <b className="flex-auto text-right">GRAND TOTAL : {localStorage.getItem("currencyFormat")} {itemPrice * quantity}</b>{" "}
      </div> */}
        </div>
      </div>
      </React.Fragment>
    );
  }

  render() {
    const { open, newOrder } = this.state;
    if (window.innerWidth > 768) {
      return <Redirect to="/" />;
    }

    if (localStorage.getItem("storeColor") === null) {
      return <Redirect to={"/"} />;
    }
    const { wallets } = this.props;
    // const previewStyle = {
    //   height: 240,
    //   width: 320,
    // };
    return (
      <React.Fragment>
        {this.state.walletBalLowErr && (
          <div className="auth-error" style={{ marginLeft: "-0.7rem" }}>
            <div className="error-shake">
              There is not enough balance on your wallet
            </div>
          </div>
        )}
        {this.state.loading ? (
          <div className="height-100 overlay-loading">
            <div>
              <img
                src="/assets/img/loading-food.gif"
                alt={localStorage.getItem("pleaseWaitText")}
              />
            </div>
          </div>
        ) : (
          <React.Fragment>
            <BackWithSearch
              boxshadow={true}
              has_title={true}
              title="Scan to Pay"
              disbale_search={true}
            />
            <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
              {this.state.result == "" && (
                <div>
                  <QrReader
                    delay={300}
                    onError={this.handleError}
                    onScan={this.handleScan}
                    style={{ width: "100%" }}
                  />
                  <p>{this.state.result}</p>
                </div>
              )}

              {this.state.result != "" &&
                this.state.paymentStatus == "pending" && (
                  <div>
                    <div>
                      <div className="input-group mb-20">
                        <div className="input-group-prepend">
                          <div className="btn apply-coupon-btn">Item:</div>
                        </div>
                        {this.state.qrCodeOrder == true && (
                          <input
                            className="form-control  font-w600"
                            type="text"
                            readOnly
                            value={this.state.itemName || ""}
                          />
                        )}
                      </div>
                    </div>
                    <div className="input-group mb-20">
                      {this.state.qrCodeOrder == true && (
                        <div style={{ display: "-webkit-inline-box" }}>
                          <div>
                            <div className="input-group-prepend">
                              <div className="btn apply-coupon-btn">Price:</div>
                            </div>
                          </div>
                          <input
                            className="form-control font-size-h3 font-w600"
                            type="text"
                            readOnly
                            style={{ width: "100px" }}
                            value={
                              localStorage.getItem("currencyFormat") +
                                this.state.amounttopay *
                                  (1 * this.state.quantity) || ""
                            }
                          />
                          <div className="input-group-prepend">
                            <div className="btn apply-coupon-btn">* Qty:</div>
                          </div>
                          <select
                            onChange={this.handleChange}
                            className="form-control font-size-h4 font-w600"
                            style={{ width: "100px" }}
                          >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                        </div>
                      )}

                      {this.state.qrCodeOrder == false && (
                        <input
                          className="form-control apply-coupon-input font-size-h3 font-w600"
                          type="number"
                          autoFocus
                          placeholder="Enter Amount"
                          onChange={this.handleInput}
                          value={
                            this.state.amounttopay * this.state.quantity || ""
                          }
                        />
                      )}
                    </div>

                    <div>
                      <div className="block block-link-shadow text-right shadow-light">
                        <div className="block-content block-content-full clearfix">
                          <div className="float-left mt-10">
                            <i className="si si-wallet fa-3x text-body-bg-dark" />
                          </div>
                          <div className="font-size-h4">Wallet Balance</div>
                          <div className="font-size-h3 font-w600">
                            {localStorage.getItem("currencyFormat")}
                            {wallets[0].balance}
                          </div>
                          <div className="font-size-sm font-w600 text-uppercase text-muted">
                            Pay With Wallet -{" "}
                            {this.state.amounttopay * this.state.quantity}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex flex-row bd-highlight mb-3">
                      <button
                        className="p-2 btn btn-dark col-sm-2 col-form-label"
                        onClick={this.onOpenModal}
                        disabled={!this.state.amounttopay * this.state.quantity}
                      >
                        Pay
                      </button>
                      <Modal open={open} onClose={this.onCloseModal} center>
                        <br></br>
                        <div className="form-control apply-coupon-input font-size-h3 font-w600">
                          <span>Confirm to pay from Wallet</span>
                          <br></br>
                          <span>
                            <b>Amount:</b>{" "}
                          </span>
                          {localStorage.getItem("currencyFormat")}{" "}
                          {this.state.amounttopay * this.state.quantity}
                          <br></br>
                        </div>
                        <div className="d-flex flex-row bd-highlight mb-3">
                          <button
                            className="btn btn-danger col-sm-5"
                            onClick={this.onCloseModal}
                          >
                            Cancel
                          </button>
                          <button
                            className="btn btn-dark col-sm-4"
                            onClick={() => {
                              this.handlePayFromWallet(
                                wallets[0].balance,
                                wallets[0].id
                              );
                              this.onCloseModal();
                            }}
                          >
                            Yes
                          </button>
                        </div>
                      </Modal>
                    </div>
                  </div>
                )}
              {this.state.result != "" && this.state.paymentStatus == true && (
                <div>
                  {this.state.message === "less_quantity" ||
                  this.state.message === "less_balance" ? (
                    <div className="row">
                      <div className="col-md-12">
                        <div className="block block-link-shadow">
                          <div className="block-content block-content-full clearfix py-0 bg-light">
                            <div className="float-right">
                              <img
                                src="/assets/img/order-canceled.png"
                                className="img-fluid img-avatar"
                                alt="Failed"
                              />
                            </div>
                            <div
                              className="float-left mt-20"
                              style={{ width: "75%" }}
                            >
                              <div className="font-w600 font-size-h4 mb-5">
                                <div>{this.getMessage()}</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : this.state.message == "order_placed" ? (
                    <div>
                                <div style={{ width: "100%" }}>
                                  <div>{this.displayOrder(newOrder)}</div>
                                </div>

					  <hr></hr>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-placed.gif"
                                  className="img-fluid img-avatar"
                                  alt="success"
                                />
                              </div>
                              <div>
                                <div className="font-w600 font-size-h4 mb-5">
                                  <div>
									  <br></br>
                                    <div      style={{
                                              color: localStorage.getItem("cartColorBg")
                                          }}
                                    >{this.getMessage()}</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  ) : (
                    <div className="row">
                      <div className="col-md-12">
                        <div className="block block-link-shadow">
                          <div className="block-content block-content-full clearfix py-0">
                            <div className="text-center">
                              {this.getMessage()}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              )}
              {/* {this.state.result != "" && this.state.paymentStatus == false && (
                <div className="row">
                  <div className="col-md-12">
                    <div className="block block-link-shadow">
                      <div className="block-content block-content-full clearfix py-0">
                        <div className="float-right">
                          <img
                            src="/assets/img/order-canceled.png"
                            className="img-fluid img-avatar"
                            alt="Failed"
                          />
                        </div>
                        <div
                          className="float-left mt-20"
                          style={{ width: "75%" }}
                        >
                          <div className="font-w600 font-size-h4 mb-5">
                            {this.state.message}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )} */}
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user,
  wallets: state.wallets.wallets,
});

export default connect(mapStateToProps, {
  messQr,
  getSingleOrder,
  getWalletTransactions,
})(CartQrCode);
