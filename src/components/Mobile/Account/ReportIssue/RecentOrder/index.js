import React, { Component } from "react";
import { connect } from "react-redux";
import Moment from "react-moment";
import DelayLink from "../../../../helpers/delayLink";

const RecentOrder = (props) => {
  // console.log(props.latestOrder.user_id);
  return (
    <React.Fragment>
      <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
        <div>
          <h5 className="text-uppercase">Tickets</h5>
          <DelayLink to={"/view-tickets"} delay={200}>
            <div
              style={{ color: "#6666FF" }}
            >
              View Tickets
            </div>
          </DelayLink>
        </div>
        <br />
        <hr />
        <div>
          <h5 className="text-uppercase">Recent Order</h5>
          <div className="block block-link-shadow text-right shadow-dark">
            <div className="block-content block-content-full clearfix">
              <div className="float-left mt-5">
                <div
                  className=""
                  style={{ color: localStorage.getItem("storeColor") }}
                >
                  {props.latestOrder.unique_order_id} <br />
                </div>
              </div>
              <div className="text-right mt-5">
                <Moment format="DD-MM-YYYY h:ma">
                  {props.latestOrder.created_at}
                </Moment>
              </div>
              <div className="text-left text-capitalize">
                {props.latestOrder.orderitems
                  ? props.latestOrder.orderitems.map((item, index) => (
                      <div key={index} style={{ fontSize: "12px" }}>
                        {item.name}
                      </div>
                    ))
                  : ""}
              </div>
              <hr></hr>
              <DelayLink to={"/submit-report"} delay={200}>
                <div onClick={props.handleClicks.bind(this, props.latestOrder)} style={{ color: "#6666FF" }} className='text-left'>
                  Having issues with this Order?
                </div>
              </DelayLink>
              <div className="text-right font-size-h5">
                Total: {localStorage.getItem("currencyFormat")}{" "}
                {props.latestOrder.total}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect()(RecentOrder);
