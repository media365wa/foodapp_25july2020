import React, { Component } from "react";
import { connect } from "react-redux";
import Moment from "react-moment";
import Axios from "axios";
import "react-responsive-modal/styles.css";
import BackWithSearch from "../../../Elements/BackWithSearch";
import { Modal } from "react-responsive-modal";
import Select from "react-select";
import { REPORT_ISSUE } from "../../../../../configs/index";
class SubmitReport extends Component {
  constructor(props) {
    super(props);
    var order = JSON.parse(localStorage.getItem("orderWithIssue"));
    this.state = {
      uniqueOrderId: order.unique_order_id,
      userId: order.user_id,
      issueType: "",
      issueDescription: "",
      totalAmount: order.total,
      isClearable: true,
      hoverBlue: false,
      showMsg: false,
      showErrorMsg: false,
      ticketExist: false,
      ticketId: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleTextAreaInput = this.handleTextAreaInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  state = {
    open: false,
  };
  handleChange = (issueType) => {
    if (issueType === null) {
      this.setState((state) => ({
        isClearable: !state.isClearable,
        issueType: "",
      }));
    } else {
      this.setState({ issueType }, () =>
        console.log(`Option selected:`, this.state.issueType.value)
      );
      this.setState((state) => ({ isClearable: !state.isClearable }));
    }
  };
  handleTextAreaInput(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      issueDescription: value,
    });
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  toggleClearable = () =>
    this.setState((state) => ({ isClearable: !state.isClearable }));
  changeColor() {
    this.setState({ hoverBlue: !this.state.hoverBlue });
  }
  reportIssue = (userId, uniqueOrderId, issueType, issueDescription, total) => {
    Axios.post(REPORT_ISSUE, {
      userId: userId,
      uniqueOrderId: uniqueOrderId,
      issueType: issueType,
      issueDescription: issueDescription,
      total: total,
    })
      .then((response) =>
        response.data.success == true
          ? this.setState({
              showMsg: true,
              ticketId: response.data.id,
              issueDescription: "",
              issueType: "",
            })
          : response.data.success == "exist"
          ? this.setState({ showMsg: false, ticketExist: true, ticketId: response.data.id })
          : this.setState({ showMsg: false, showErrorMsg: true })
      )
      // .then((json) => this.setState({ selectedRest: json.data }))
      .catch(function (error) {
        console.log(error);
      });
  };
  handleSubmit(event) {
    this.reportIssue(
      this.state.userId,
      this.state.uniqueOrderId,
      this.state.issueType,
      this.state.issueDescription,
      this.state.totalAmount
    );
  }
  componentDidMount() {}
  render() {
    let btn_class = this.state.hoverBlue ? "#2684FF" : "#c7c7c7";
    const options = [
      { value: "amount", label: "Amount" },
      { value: "order", label: "Order" },
    ];
    const {
      open,
      issueDescription,
      isClearable,
      issueType,
      customStyle,
    } = this.state;
    // console.log(this.state);
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Submit Report"
            disbale_search={true}
          />
        </div>
        <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
          {/* <form> */}
          <div className="col-12">
            <div className="form-row">
              <div className="text-uppercase ">
                <span style={{ fontSize: "18px" }}>
                  Order No:{" "}
                  <input
                    style={{ color: "red" }}
                    disabled
                    defaultValue={this.state.uniqueOrderId}
                  />{" "}
                </span>
              </div>
            </div>
            <br />
            <div className="form-row" style={{ display: "block" }}>
              <div className="">
                <span style={{ fontSize: "18px" }}>
                  ISSUE WITH:{" "}
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    defaultValue={"default"}
                    name="color"
                    isClearable={isClearable}
                    onChange={this.handleChange}
                    value={issueType}
                    options={options}
                  />
                </span>
              </div>
            </div>
            <br />
            <div className="form-row">
              <div className="text-uppercase ">
                <span style={{ fontSize: "18px" }}>
                  Descrption: <br />
                  <textarea
                    value={this.state.issueDescription}
                    placeholder="Describe your issue here"
                    onChange={this.handleTextAreaInput}
                    style={{
                      width: "310px",
                      borderColor: btn_class,
                      borderRadius: "3%",
                    }}
                    required
                  />{" "}
                </span>
              </div>
            </div>
            <br />
            <button
              className="p-2 btn btn-dark col-sm-2 col-form-label"
              onClick={this.onOpenModal}
              disabled={issueDescription && issueType ? false : true}
            >
              Submit
            </button>
            <br />
            <br />
            {this.state.showMsg == true ? (
              <div className="alert alert-success text-center" role="alert">
                Report submitted successfully.
                <br />
                {this.state.ticketId ? (
                  <div>Ticket No: {this.state.ticketId} </div>
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
            <br />
            <br />
            {this.state.showErrorMsg == true ? (
              <div className="alert alert-danger text-center" role="alert">
                View the tickets and if its not updated, please submit again.
              </div>
            ) : (
              ""
            )}
            {this.state.ticketExist == true ? (
              <div className="alert alert-danger text-center" role="alert">
                Ticket already exist. <br/>
                Ticket No: {this.state.ticketId} <br/>
                Please contact Admin.
              </div>
            ) : (
              ""
            )}
          </div>
          <Modal open={open} onClose={this.onCloseModal} center>
            <br></br>
            <div className="form-control apply-coupon-input font-size-h3 font-w600">
              <span>Are you sure you want to report this issue?</span>
              <br></br>
            </div>
            <div className="d-flex flex-row bd-highlight mb-3">
              <button
                className="btn btn-danger col-sm-5"
                onClick={this.onCloseModal}
              >
                Cancel
              </button>
              <button
                type="submit"
                className="btn btn-dark col-sm-4"
                onClick={() => {
                  this.onCloseModal();
                  this.handleSubmit();
                }}
              >
                Yes
              </button>
            </div>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
  orders: state.orders.orders,
});
export default connect(mapStateToProps)(SubmitReport);
