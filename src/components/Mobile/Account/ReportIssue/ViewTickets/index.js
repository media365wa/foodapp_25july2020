 import React, { Component } from "react";
import DelayLink from "../../../../helpers/delayLink";
import { connect } from "react-redux";
import BackWithSearch from "../../../Elements/BackWithSearch";
import Axios from "axios";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import { GET_TICKETS } from "../../../../../configs/index";
import Table from "react-bootstrap/Table";
class ViewTickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tickets: [],
    };
    this.getTickets(this.props.user.data.id);
  }
  __getOrderStatus = (id) => {
    if (id == 0) {
      return "Open";
    }
    if (id == 1) {
      return "Closed";
    }
  };
  getTickets = (userId) => {
    Axios.post(GET_TICKETS, {
      userId: userId,
    })
      .then((response) => this.setState({ tickets: response.data }))
      .catch(function (error) {
        console.log(error);
      });
  };
  renderTableData(tickets) {
    return tickets.map((ticket, index) => {
      const { id, unique_ticket_id, issue_type, issue_description, is_open, updated_at } = ticket; //destructuring
      return (
        <tr key={id}>
          <td>{unique_ticket_id}</td>
          <td className="text-capitalize">{issue_type}</td>
          {/* <td className='text-capitalize'>{issue_description}</td> */}
          <td className={is_open == 0 ? "text-danger" : "text-success"}>
            {this.__getOrderStatus(is_open)}
          </td>
          <td>
            <Moment format="DD-MM-YYYY h:ma">{updated_at}</Moment>
          </td>
        </tr>
      );
    });
  }
  render() {
    const { tickets } = this.state;
    // console.log(tickets);
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Tickets"
            disbale_search={true}
          />
        </div>
        {tickets.data ? (
          <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
            <Table responsive bordered size hover>
              <thead>
                <tr>
                  <th>Tkt</th>
                  <th>Type</th>
                  {/* <th>Desc.</th> */}
                  <th>Status</th>
                  <th>Updated</th>
                </tr>
              </thead>
              <tbody>{this.renderTableData(tickets.data)}</tbody>
            </Table>
          </div>
        ) : (
          <div>No Tickets Found</div>
        )}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(ViewTickets);
