import React, { Component } from "react";
import { connect } from "react-redux";
import Moment from "react-moment";
import Axios from "axios";
import BackWithSearch from "../../Elements/BackWithSearch";
import { getOrders } from "../../../../services/orders/actions";

import RecentOrder from "./RecentOrder";
class ReportIssue extends Component {
  constructor(props) {
    super(props);
    this.state = { latestOrder: [] };

    const { user } = this.props;
    if (user.success) {
      this.props.getOrders(user.data.auth_token, user.data.id);
    }
  }
  componentDidMount() {
    // console.log(this.props.orders.length);
    if (this.props.orders.length > 0) {
      this.setState({ latestOrder: this.props.orders[0] });
    }
  }
  handleClick = (order) => {
    localStorage.setItem("orderWithIssue", JSON.stringify(order));
  };
  render() {
    var latestOrder;
    this.props.orders[0]
      ? (latestOrder = this.props.orders[0])
      : (latestOrder = []);
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Report an Issue"
            disbale_search={true}
          />
        </div>
        <RecentOrder latestOrder={latestOrder} handleClicks={this.handleClick}/>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
  orders: state.orders.orders,
});
export default connect(mapStateToProps, { getOrders })(ReportIssue);
