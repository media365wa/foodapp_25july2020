import React, { Component } from "react";
import { connect } from "react-redux";
// import { debounce } from "../../../helpers/debounce";
import { Redirect } from "react-router";
import Footer from "../../Footer";
// import Meta from "../../../helpers/meta";
// import RNPickerSelect from 'react-native-picker-select';
import Axios from "axios";
// import Ink from "react-ink";
// import { formatPrice } from "../../../helpers/formatPrice";
import BackWithSearch from "../../Elements/BackWithSearch";
import {
  getWalletTransactions,
  saveWalletTransaction,
  saveDirectWalletTransaction,
} from "../../../../services/wallet/actions";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import { GET_TOTAL_AMOUNT_SPENT_WITHIN_LIMIT } from "../../../../configs/index";
class SelectRestaurant extends Component {
  restaurantId;
  constructor(props) {
    super(props);
    // const { users, wallets } = this.props;
    this.state = { redirect: false };
    this.handleChange = this.handleChange.bind(this);
    this.state.restaurantList = JSON.parse(localStorage.getItem("ShopList"));
  }
  state = {
    loading: false,
    result: "",
    amounttopay: "",
    paymentStatus: "pending",
    amountErr: false,
    walletBalLowErr: false,
    restaurantId: "",
    open: false,
    name: "",
    restaurantList: [],
    selectedRest: [],
    maxCapLimit: 0,
    maxCapLimitReached: false,
    maxCapLimitFrom: "",
  };
  // getWalletTotalWithLimit = (userId, restaurantId, maxLimitDateFrom) => {
  //   Axios.post(GET_TOTAL_AMOUNT_SPENT_WITHIN_LIMIT, {
  //     userId: userId,
  //     restaurantId: restaurantId,
  //     maxLimitFrom: maxLimitDateFrom,
  //   })
  //     .then((response) => response)
  //     .then(
  //       (json) => this.setState({ selectedRest: json.data }))
  //     .catch(function (error) {
  //       console.log(error);
  //     });
  // };
  componentDidMount() {
    this.setState({ loading: true });
    this.setState({ loading: false });
  }

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  handleError = (err) => {
    console.error(err);
  };
  handleChange(evt) {
    if (evt.target.value != "") {
      this.setState({ amounttopay: "" });
      localStorage.setItem("RestaurantChosen", evt.target.value);
      evt.preventDefault();
      let rest = this.state.restaurantList.filter(
        (restaurantList) =>
          restaurantList.id == Number(localStorage.getItem("RestaurantChosen"))
      );
      this.setState({
        name: rest[0].name,
      });
      // this.setState({ maxCapLimitFrom: rest[0].max_cap_from });
      // rest[0].max_cap_from
      //   ? localStorage.setItem("capLimit", rest[0].max_cap)
      //   : localStorage.removeItem("capLimit");
      // this.getWalletTotalWithLimit(
      //   this.props.user.data.id,
      //   rest[0].id,
      //   rest[0].max_cap_from
      // );
      // this.setState({name: rest[0].name});
      // const restaurantSpendings = JSON.parse(
      //   localStorage.getItem("OrderSpendings")
      // );
      // this.setState({ maxCapLimitReached: false });
    } else {
      this.setState({ name: "" });
    }
  }

  handleInput = (event) => {
    console.log(event.target.value);
    var amnt = event.target.value;
    const { wallets } = this.props;
    this.setState({ amounttopay: amnt });
    // localStorage.setItem("amounttopay", amnt);
    // if (amnt) {
    //   // console.log(localStorage.getItem("capLimit"));
    //   if (localStorage.getItem("capLimit") === "0") {
    //     this.setState({ maxCapLimit: 0 });
    //     localStorage.removeItem("capLimit");
    //     this.setState({ maxCapLimitReached: false });
    //   } else {
    //     this.setState({
    //       maxCapLimit: JSON.parse(localStorage.getItem("capLimit")),
    //     });
    //   }
    //   if (this.state.selectedRest[0].sum != null) {
    //     JSON.parse(amnt) + JSON.parse(this.state.selectedRest[0].sum) >
    //     this.state.maxCapLimit
    //       ? this.setState({ maxCapLimitReached: true })
    //       : this.setState({ maxCapLimitReached: false });
    //   } else {
    //     this.setState({ maxCapLimitReached: false });
    //   }
    // }

    // if (amnt > 0) {
    //   this.setState({ amountErr: false });
    // } else {
    //   this.setState({ amountErr: true });
    // }

    // if (amnt <= wallets[0].balance) {
    //   this.setState({ walletBalLowErr: false });
    // } else {
    //   this.setState({ walletBalLowErr: true });
    // }
  };

  handlePayFromWallet = (walletBal, wallet_id) => {
    let res = this.state.restaurantList.filter(
      (x) => x.id == Number(localStorage.getItem("RestaurantChosen"))
    );

    let loc = res[0].code;
    var amntNum = parseFloat(this.state.amounttopay);
    var walletBlnce = parseFloat(walletBal);
    var amnttopay = parseFloat(this.state.amounttopay);
    var restaurantId = Number(localStorage.getItem("RestaurantChosen"));
    var restaurantCode = loc;
    if (amnttopay > 0) {
      this.setState({
        amountErr: false,
      });

      if (amntNum <= walletBlnce) {
        this.setState({
          walletBalLowErr: false,
        });
        const { user, wallets } = this.props;
        if (user.success) {
          this.props.saveDirectWalletTransaction(
            user.data.auth_token,
            user.data.id,
            user.data.default_address.house,
            wallets[0].id,
            this.state.result,
            amntNum,
            restaurantId,
            restaurantCode
          );
          localStorage.removeItem("RestaurantChosen");

          this.setState({ paymentStatus: true });
          this.timeout = setTimeout(
            () => this.setState({ redirect: true }),
            3000
          );
        }
      } else {
        this.setState({
          walletBalLowErr: true,
        });
      }
    } else {
      this.setState({
        amountErr: true,
      });
    }
  };

  render() {
    const { open } = this.state;
    const { wallets } = this.props;
    if (this.state.redirect) {
      return <Redirect to="/my-wallet" />;
    } else {
      return (
        <React.Fragment>
          {this.state.loading ? (
            <div className="height-100 overlay-loading">
              <div>
                <img
                  src="/assets/img/loading-food.gif"
                  alt={localStorage.getItem("pleaseWaitText")}
                />
              </div>
            </div>
          ) : (
            <React.Fragment>
              <div>
                <div className="input-group-prepend">
                  <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title="Pay From Wallet"
                    disbale_search={true}
                  />
                </div>

                <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
                  <select
                    className="form-control apply-coupon-input font-size-h3 font-w600"
                    name="restaurantId"
                    id="restaurantId"
                    onChange={this.handleChange}
                    key="1"
                    required
                  >
                    <option
                      defaultValue=""
                      placeholder="Select Restaurant"
                      disabled
                      selected
                    >
                      Select Restaurant
                    </option>
                    {this.state.restaurantList.length == 0
                      ? "Loading Restaurants"
                      : this.state.restaurantList.map((restaurants, index) => (
                          <option value={restaurants.id} key={restaurants.id}>
                            {restaurants.name}
                          </option>
                        ))}
                  </select>

                  {this.state.restaurantList.length == 0 ? (
                    "Loading Restaurants"
                  ) : (
                    <input
                      className="form-control apply-coupon-input font-size-h3 font-w600"
                      type="number"
                      autoFocus
                      placeholder="Enter Amount"
                      onChange={this.handleInput}
                      required
                      value={this.state.amounttopay || ""}
                    />
                  )}

                  {/* {this.state.result != '' && this.state.paymentStatus=='pending' &&( */}
                  <div>
                    <div className="block block-link-shadow text-right shadow-light">
                      <div className="block-content block-content-full clearfix">
                        <div className="float-left mt-10">
                          <i className="si si-wallet fa-3x text-body-bg-dark" />
                        </div>
                        <div className="font-size-h4">Wallet Balance</div>
                        <div className="font-size-h3 font-w600">
                          {localStorage.getItem("currencyFormat")}
                          {wallets[0].balance}
                        </div>
                        <div className="font-size-sm font-w600 text-uppercase text-muted">
                          Pay - {localStorage.getItem("currencyFormat")}{" "}
                          {this.state.amounttopay}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex flex-row bd-highlight mb-3">
                    <button
                      className="p-2 btn btn-dark col-sm-2 col-form-label"
                      onClick={this.onOpenModal}
                      disabled={!this.state.amounttopay || !this.state.name}
                    >
                      Pay
                    </button>
                    {/* {this.state.maxCapLimitReached ? (
                      <div className="p-2 col-sm-2 col-form-label">
                        <div className="alert alert-danger text-center">
                          <strong>Max Cap Reached</strong>
                        </div>
                      </div>
                    ) : (
                      <button
                        className="p-2 btn btn-dark col-sm-2 col-form-label"
                        onClick={this.onOpenModal}
                        disabled={!this.state.amounttopay || !this.state.name}
                      >
                        Pay
                      </button>
                    )} */}

                    <Modal open={open} onClose={this.onCloseModal} center>
                      <br></br>
                      <div className="form-control apply-coupon-input font-size-h3 font-w600">
                        <span>Confirm to pay from Wallet</span>
                        <br></br>
                        <span>
                          <b>Shop:</b>{" "}
                        </span>
                        {this.state.name ? (
                          this.state.name
                        ) : (
                          <span className="text-danger">Not Selected</span>
                        )}
                        <br></br>
                        <span>
                          <b>Amount:</b>{" "}
                        </span>
                        {localStorage.getItem("currencyFormat")}{" "}
                        {this.state.amounttopay ? (
                          this.state.amounttopay
                        ) : (
                          <span className="text-danger">
                            Please add amount{" "}
                          </span>
                        )}
                        <br></br>
                      </div>
                      <div className="d-flex flex-row bd-highlight mb-3">
                        <button
                          className="btn btn-danger col-sm-5"
                          onClick={this.onCloseModal}
                        >
                          Cancel
                        </button>
                        <button
                          className="btn btn-dark col-sm-4"
                          disabled={!this.state.name || !this.state.amounttopay}
                          onClick={() => {
                            this.handlePayFromWallet(
                              wallets[0].balance,
                              wallets[0].id
                            );
                            this.onCloseModal();
                          }}
                        >
                          Yes
                        </button>
                      </div>
                    </Modal>
                  </div>

                  {this.state.result != "" && this.state.paymentStatus == true && (
                    <div className="row">
                      <div className="col-md-12">
                        <div className="block block-link-shadow">
                          <div className="block-content block-content-full clearfix py-0">
                            <div className="float-right">
                              <img
                                src="/assets/img/order-placed.gif"
                                className="img-fluid img-avatar"
                                alt="success"
                              />
                            </div>
                            <div
                              className="float-left mt-20"
                              style={{ width: "75%" }}
                            >
                              <div className="font-w600 font-size-h4 mb-5">
                                Success
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {this.state.result != "" &&
                    this.state.paymentStatus == false && (
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-canceled.png"
                                  className="img-fluid img-avatar"
                                  alt="Failed"
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  Failed
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                </div>
              </div>
              <Footer />
            </React.Fragment>
          )}
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  restaurants: state.restaurants.restaurants,
  wallets: state.wallets.wallets,
  user: state.user.user,
  restaurant_info: state.items.restaurant_info,
});

export default connect(mapStateToProps, {
  getWalletTransactions,
  saveWalletTransaction,
  saveDirectWalletTransaction,
})(SelectRestaurant);
