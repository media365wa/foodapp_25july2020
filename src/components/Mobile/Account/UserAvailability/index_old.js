import React, { Component } from "react";
import Ink from "react-ink";
import { Redirect } from "react-router";
import BackWithSearch from "../../Elements/BackWithSearch";

const typeData = ['Breakfast','Lunch','Snacks','Dinner'];

class UserAvailability extends Component {
    state = {
        daterange: "",
        type: "",
        error: false
    };

    handleInput = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    };

    handleSubmit = event => {
        event.preventDefault();
        if (
            this.state.daterange !== "" &&
            this.state.type !== ""
        ) {
            this.setState({ error: false });
            const { user } = this.props;
            this.props.savevailability(user.data.id, user.data.auth_token, this.state);
        } else {
            this.setState({ error: true });
        }
    };

    render() {

          if (window.innerWidth > 768) {
              return <Redirect to="/" />;
          }

          if (localStorage.getItem("storeColor") === null) {
              return <Redirect to={"/"} />;
          }
          return (
              <React.Fragment>

                  {this.state.loading ? (
                      <div className="height-100 overlay-loading">
                          <div>
                              <img
                                  src="/assets/img/loading-food.gif"
                                  alt={localStorage.getItem("pleaseWaitText")}
                              />
                          </div>
                      </div>
                  ) : (
                      <React.Fragment>
                          <BackWithSearch
                              boxshadow={true}
                              has_title={true}
                              title="User Availability"
                              disbale_search={true}
                          />
                          <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">



                          <div className="form-group m-0 mt-30 mb-30">
                            <label className="col-12 add-date-input-label">
                                Date Range
                            </label>
                            <div className="col-md-9">
                                <input
                                    type="date"
                                    name="daterange"
                                      onChange={this.handleInput}
                                    className="form-control add-date-input"
                                />
                            </div>
                            <label className="col-12 add-type-select-label">
                                Type
                            </label>
                            <div className="col-md-9">
                            <select
                                name="type"
                                  multiple onChange={this.handleInput}
                              >
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch">Lunch</option>
                                <option value="snacks">Snacks</option>
                                <option value="dinner">Dinner</option>
                              </select>
                            </div>
                          </div>
                          <button
                              type="submit"
                              className="btn-save-userAvailability"
                              style={{ backgroundColor: localStorage.getItem("storeColor") }}
                          >
                              Save Availability
                              <Ink duration={200} />
                          </button>

                    </div>
                      </React.Fragment>
                  )}
              </React.Fragment>
          );
    }
}
export default UserAvailability;
