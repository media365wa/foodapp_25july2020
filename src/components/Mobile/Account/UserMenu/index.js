import React, { Component } from "react";
import Collapsible from "react-collapsible";
// import Popup from "reactjs-popup";

import DelayLink from "../../../helpers/delayLink";

class UserMenu extends Component {
  render() {
    // const { pages } = this.props;
    return (
      <React.Fragment>
        <Collapsible
          trigger={localStorage.getItem("accountMyAccount")}
          transitionTime={200}
        >
          <div className="category-list-item">
            <DelayLink to={"/personal-details"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-info" />
                </div>
                <div className="flex-auto border-0">Personal Details</div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
          <div className="category-list-item">
            <DelayLink to={"/old-password"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-lock" />
                </div>
                <div className="flex-auto border-0">Change Password</div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
          <div className="category-list-item">
            <DelayLink to={"/my-addresses"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-home" />
                </div>
                <div className="flex-auto border-0">
                  {localStorage.getItem("accountManageAddress")}
                </div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
          {/* <div className="category-list-item">
            <DelayLink to={"/my-orders"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-basket-loaded" />
                </div>
                <div className="flex-auto border-0">
                  {localStorage.getItem("accountMyOrders")}
                </div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div> */}
          {/* <div className="category-list-item">
                        <DelayLink to={"/user-availabilty"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                   Availability
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div> */}
          {/*<div className="category-list-item">
                        <DelayLink to={"/my-wallet"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                    Wallet Transactions
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                    <div className="category-list-item">
                        <DelayLink to={"/qrcode"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                   Scan to Pay
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                    <div className="category-list-item">
                        <DelayLink to={"/select-restaurant"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                   Pay from Wallet
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div> */}
        </Collapsible>
        <Collapsible trigger="Order Status" transitionTime={200}>
          <div className="category-list-item">
            <DelayLink to={"/running-order"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-refresh" />
                </div>
                <div className="flex-auto border-0">Order Status</div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
          <div className="category-list-item">
            <DelayLink to={"/my-orders"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-basket-loaded" />
                </div>
                <div className="flex-auto border-0">
                  {localStorage.getItem("accountMyOrders")}
                </div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
        </Collapsible>
        <Collapsible trigger="Help &amp; Support" transitionTime={200}>
          <div className="category-list-item">
            <DelayLink to={"/faq"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-question" />
                </div>
                <div className="flex-auto border-0">FAQ</div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
          <div className="category-list-item">
            <DelayLink to={"/contact-us"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-phone" />
                </div>
                <div className="flex-auto border-0">Contact Us</div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
          <div className="category-list-item">
            <DelayLink to={"/report-issue"} delay={200}>
              <div className="display-flex">
                <div className="mr-10 border-0">
                  <i className="si si-exclamation" />
                </div>
                <div className="flex-auto border-0">Report an Issue</div>
                <div className="flex-auto text-right">
                  <i className="si si-arrow-right" />
                </div>
              </div>
            </DelayLink>
          </div>
        </Collapsible>
        {/* <div className="block-content bg-white">
          <DelayLink to={"/contact-us"} delay={200}>
            <div className="display-flex">
              <div className="flex-auto border-0">
                <h6>Contact Us</h6>
              </div>
            </div>
          </DelayLink>
        </div> */}
        {/* <div className="block-content bg-white">
          <DelayLink to={"/contact-us"} delay={200}>
            <div className="display-flex">
              <div className="flex-auto border-0">
                <h6>Help &amp; Support</h6>
              </div>
            </div>
          </DelayLink>
        </div> */}
        <div className="block-content bg-white">
          <DelayLink to={"/about-us"} delay={200}>
            <div className="display-flex">
              <div className="flex-auto border-0">
                <h6>About Us</h6>
              </div>
            </div>
          </DelayLink>
        </div>
      </React.Fragment>
    );
  }
}

export default UserMenu;
