import React, { Component } from "react";
import { connect } from "react-redux";
import DelayLink from "../../../../helpers/delayLink";
import Axios from "axios";
import Moment from "react-moment";
import { GET_WALLET_DATE_RANGE } from "../../../../../configs/index";
import BackWithSearch from "../../../Elements/BackWithSearch";
import { Pie } from "react-chartjs-2";
import Footer from "../../../Footer";
class WalletTransactionsDateRange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: {},
      fromDate: "",
      toDate: "",
      transactions: [],
      walletId: "",
      user_id: "",
      show: false,
      total: "",
    };
    this.handleFromDate = this.handleFromDate.bind(this);
    this.handleToDate = this.handleToDate.bind(this);
    this.handleGetTransaction = this.handleGetTransaction.bind(this);
  }

  handleFromDate(event) {
    let fD = event.target.value;
    this.setState({ fromDate: fD });
  }
  handleToDate(event) {
    let tD = event.target.value;
    this.setState({ toDate: tD });
    this.setState({ walletId: this.props.wallets[0].id });
  }

  handleGetTransaction() {
    Axios.post(GET_WALLET_DATE_RANGE + "/" + this.props.user.data.id, {
      user_id: this.props.user.data.id,
      wallet_id: this.state.walletId,
      fromDate: this.state.fromDate + " 00:00:00",
      toDate: this.state.toDate + " 23:59:59",
    })
      .then((response) => response)
      .then((json) =>
        this.setState({
          transactions: json.data.dateRangeResponse,
          total: json.data.sumOfDateRange[0].sum,
        })
      )

      // console.log()
      .catch(function (error) {
        console.log(error);
      });
    this.setState({ show: true });
  }

  _getTransactionFormat = (item) => {
    if (item.transaction_type === "D") {
      return "red";
    } else {
      return "green";
    }
  };

  _getTransactionSign = (item) => {
    if (item.transaction_type === "D") {
      return "-";
    } else {
      return "+";
    }
  };

  _getWalletBalanceColor(walletBalance) {
    if (walletBalance >= 0) {
      return "green";
    } else {
      return "red";
    }
  }

  _walletStatus = (wallet) => {
    if (wallet.is_active === 1) {
      return "Active";
    } else {
      return "InActive";
    }
  };
  getRandomColor = function () {
    var letters = "0123456789ABCDEF".split("");
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };
  getRandomColorEachEmployee = function (count) {
    var data = [];
    for (var i = 0; i < count; i++) {
      data.push(this.getRandomColor());
    }
    return data;
  };
  handleClick = (e, orderRef, createdAt) => {
    localStorage.setItem("OrderReference", orderRef);
    localStorage.setItem("OrderDate", createdAt);
  };

  //   componentDidMount() {
  //     const { walletGraph, user } = this.props;
  //     console.log(this.props.wallet);
  //     let labelName = [];
  //     let plotvalue = [];
  //     // this.props.wallet.wallet_transactions
  //     //   .slice(0, 5)
  //     //   .map(
  //     //     (item) => (
  //     //       item.transaction_type === "D"
  //     //         ? labelName.push(item.created_at)
  //     //         : null,
  //     //       item.transaction_type === "D" ? plotvalue.push(item.amount) : null
  //     //     )
  //     //   );
  //     // console.log(this.props.wallet);
  //     // let l = ['Visits',  'Categories',  'Categories',  'Data 4'];
  //     // let c = ['10',  '2',  '0',  '4'];
  //     this.setState({
  //       Data: {
  //         labels: labelName,
  //         datasets: [
  //           {
  //             label: "Spends",
  //             data: plotvalue,
  //             backgroundColor: this.getRandomColorEachEmployee(plotvalue.length),
  //           },
  //         ],
  //       },
  //     });
  //   }

  render() {
    const { wallets } = this.props;
    console.log(this.state);
    // console.log(wallets);
    // console.log(this.state.transactions);
    const options = {
      animation: {
        animateScale: true,
      },
      legend: {
        display: false,
        position: "left",
      },
    };
    return (
      <React.Fragment>
        <BackWithSearch
          boxshadow={true}
          has_title={true}
          title="Search Transaction"
          disbale_search={true}
        />
        <div className="ml-5 bg-white pt-80 pb-80 height-100-percent">
          <div className="display-flex">
            <div className="">
              <div className="d-flex flex-row ">
                <h4 className=" text-uppercase ">
                  From:{" "}
                  <input
                    onChange={this.handleFromDate}
                    className=" text-uppercase"
                    type="date"
                    style={{ width: "165px" }}
                  ></input>
                </h4>
                <h4 className="text-uppercase ">
                  To:{" "}
                  <input
                    onChange={this.handleToDate}
                    className=" text-uppercase"
                    type="date"
                    style={{ width: "165px" }}
                  ></input>
                </h4>
              </div>
              {/* <div className="d-flex flex-row bd-highlight ">
                <button
                  className="p-2 btn btn-dark col-sm-2 col-form-label"
                  onClick={this.handleGetTransaction}
                >
                  Search
                </button>
              </div> */}
              <div className="display-flex">
                <div className="flex-auto">
                  <div className="text-uppercase mt-10">
                    <div className="flex-auto mr-10">
                      <button
                        className="btn btn-dark col-sm-4"
                        onClick={this.handleGetTransaction}
                      >
                        Search
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br></br>
          <div>
            {this.state.transactions.id ? this.state.transactions.id : ""}
            {this.state.show == false ? (
              <div>
                <div className="display-flex pb-5">
                  <div className="flex-auto flex-fill text-left">Date</div>
                  <div className="flex-auto flex-fill text-center">OrderID</div>
                  <div className="flex-auto flex-fill text-right">Amount</div>
                </div>
                {wallets[0].wallet_transactions.slice(0, 7).map((item) => (
                  <div className="display-flex pb-5" key={item.id}>
                    <span className="flex-auto flex-fill text-left">
                      <Moment format="DD-MM-YYYY ">{item.created_at}</Moment>
                    </span>
                    {item.transaction_type === "D" ? (
                      <DelayLink to={"/view-order"} delay={200}>
                        <div className="flex-auto flex-fill text-center text-primary">
                          <div
                            onClick={(e) =>
                              this.handleClick(
                                e,
                                item.order_reference,
                                item.created_at
                              )
                            }
                          >
                            {item.order_reference}
                          </div>{" "}
                        </div>
                      </DelayLink>
                    ) : item.transaction_type === "R" ? (
                      <div className="flex-auto flex-fill text-center">
                        <div>{item.order_reference}</div>{" "}
                      </div>
                    ) : item.transaction_type === "C" ? (
                      <div className="flex-auto flex-fill text-center">
                        <div>{item.order_reference}</div>{" "}
                      </div>
                    ) : (
                      "-"
                    )}
                    <div
                      className="flex-auto flex-fill text-right"
                      style={{ color: this._getTransactionFormat(item) }}
                    >
                      {this._getTransactionSign(item)}
                      {localStorage.getItem("currencyFormat")}
                      {item.amount}
                    </div>
                    <hr />
                  </div>
                ))}
              </div>
            ) : (
              <div>
                {this.state.total ? (
                  <div className="display-flex pb-5">
                    <div className="h4">
                      Total Spent: {localStorage.getItem("currencyFormat")}{" "}
                      <span className="h3" style={{ color: "red" }}>
                        {this.state.total}
                      </span>
                    </div>
                  </div>
                ) : (
                  ""
                )}
                {this.state.transactions ? (
                  <div className="display-flex pb-5">
                    <div className="flex-auto text-left">Date</div>
                    <div className="flex-auto text-center">OrderID</div>
                    <div className="flex-auto text-right">Amount</div>
                  </div>
                ) : (
                  ""
                )}

                <div>
                  {this.state.transactions.id ? this.state.transactions.id : ""}
                  {this.state.transactions.length == 0 ? (
                    <div className="display-flex pb-5">
                      <div className="flex-auto text-left">
                        No transactions found
                      </div>
                    </div>
                  ) : (
                    this.state.transactions.map((transactions, index) => (
                      <div className="display-flex pb-5" key={transactions.id}>
                        <span className="flex-auto text-left">
                          <Moment format="DD-MM-YYYY ">
                            {transactions.created_at}
                          </Moment>
                        </span>
                        {/* <DelayLink to={"/view-order"} delay={200}>
                          <div className="flex-auto text-left text-primary">
                            {transactions.order_reference}
                          </div>
                        </DelayLink> */}
                        {transactions.transaction_type === "D" ? (
                          <DelayLink to={"/view-order"} delay={200}>
                            <div className="flex-auto flex-fill text-center text-primary">
                              <div
                                onClick={(e) =>
                                  this.handleClick(
                                    e,
                                    transactions.order_reference,
                                    transactions.created_at
                                  )
                                }
                              >
                                {transactions.order_reference}
                              </div>{" "}
                            </div>
                          </DelayLink>
                        ) : transactions.transaction_type === "R" ? (
                          <div className="flex-auto flex-fill text-center">
                            <div>{transactions.order_reference}</div>{" "}
                          </div>
                        ) : transactions.transaction_type === "C" ? (
                          <div className="flex-auto flex-fill text-center">
                            <div>{transactions.order_reference}</div>{" "}
                          </div>
                        ) : (
                          "-"
                        )}
                        <div
                          className="flex-auto text-right"
                          style={{
                            color: this._getTransactionFormat(transactions),
                          }}
                        >
                          {this._getTransactionSign(transactions)}
                          {localStorage.getItem("currencyFormat")}
                          {transactions.amount}
                        </div>
                        <hr />
                      </div>
                    ))
                  )}
                </div>
              </div>
            )}
            <hr></hr>
          </div>

          {/* <div className="display-flex pb-5" >
              <span className="flex-auto text-left">
                <Moment format="DD-MM-YYYY "> </Moment>
              </span>
              <div className="flex-auto text-left"> </div>
              <div
                className="flex-auto text-right"
              >
              </div>
              <hr />
            </div> */}
        </div>

        <Pie data={this.state.Data} options={options} />
        <Footer />
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user.user,
  wallets: state.wallets.wallets,
});
// const mapStateToProps = state => ({

// });
export default connect(mapStateToProps)(WalletTransactionsDateRange);
