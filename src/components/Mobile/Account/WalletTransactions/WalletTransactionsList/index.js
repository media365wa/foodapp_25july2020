import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getWalletTransactions,
  getWalletAnalytics,
  getOrderDetails,
} from "../../../../../services/wallet/actions";
import Moment from "react-moment";
import DelayLink from "../../../../helpers/delayLink";
import { SELECT_RESTAURANTS_URL } from "../../../../../configs/index";
import { Pie } from "react-chartjs-2";
import Axios from "axios";
class WalletTransactionsList extends Component {
  static contextTypes = {
    router: () => null,
  };
  constructor(props) {
    super(props);

    this.state = {
      Data: {},
      orderDetails: [],
      errMessage: false
    };
  }

  getRestaurants = () => {
    Axios.post(SELECT_RESTAURANTS_URL)
      .then((response) => response)
      .then((json) =>
        localStorage.setItem("ShopList", JSON.stringify(json.data))
      )
      .catch(function (error) {
        console.log(error);
      });
  };

  _getTransactionFormat = (item) => {
    if (item.transaction_type === "D" || item.transaction_type === "P") {
      return "red";
    } else {
      return "green";
    }
  };

  _getTransactionSign = (item) => {
    if (item.transaction_type === "D" || item.transaction_type === "P") {
      return "-";
    } else {
      return "+";
    }
  };

  _getWalletBalanceColor(walletBalance) {
    if (walletBalance >= 0) {
      return "green";
    } else {
      return "red";
    }
  }

  _walletStatus = (wallet) => {
    if (wallet.is_active === 1) {
      return "Active";
    } else {
      return "InActive";
    }
  };
  getRandomColor = function () {
    var letters = "0123456789ABCDEF".split("");
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };
  getRandomColorEachEmployee = function (count) {
    var data = [];
    for (var i = 0; i < count; i++) {
      data.push(this.getRandomColor());
    }
    return data;
  };
  handleClick = (e, orderRef, createdAt) => {
    localStorage.setItem("OrderReference", orderRef);
    localStorage.setItem("OrderDate", createdAt);
  };
  componentDidMount() {
    let plotvalue = [];
    let labelName = [];
    // localStorage.removeItem("OrderSpendings")
    let spendings = JSON.parse(localStorage.getItem("OrderSpendings"));
    if (spendings) {
      spendings.map(
        (item) => (
          item.restaurant_id != null
            ? labelName.push(item.name)
            : null,
          item.restaurant_id != null ? plotvalue.push(item.sum) : null
        )
      );
    } else {
      this.setState({errMessage: true})
    }
    labelName = [...new Set(labelName)];
    if (labelName && plotvalue) {
      this.setState({
        Data: {
          labels: labelName,
          datasets: [
            {
              label: "Spends",
              data: plotvalue,
              backgroundColor: this.getRandomColorEachEmployee(
                plotvalue.length
              ),
            },
          ],
        },
      });
    }
    this.getRestaurants();
  }
  render() {
    const { wallet } = this.props;
    let sum = 0;
    const options = {
      animation: {
        animateScale: true,
      },
      legend: {
        display: false,
        position: "left",
      },
    };

    return (
      <React.Fragment>
        <div className="mb-20">
          <div className="display-flex pb-5">
            <div className="flex-auto text-left">
              Balance: {localStorage.getItem("currencyFormat")}{" "}
              <span className="h4">{wallet.balance}</span>
            </div>
            <div className="flex-auto text-right">
              Total Recharge done: {localStorage.getItem("currencyFormat")}{" "}
              <span className="h4">
                {this.props.wallet.wallet_transactions.forEach((item) => {
                  item.transaction_type === "C"
                    ? (sum += parseFloat(item.amount))
                    : (sum = sum);
                })}
                <span>{sum}</span>
              </span>
            </div>
            {/* <div className="flex-auto text-right">
              <DelayLink to={"/my-wallet-date-range"} delay={200}>
                <div className="flex-auto">
                  <button className="btn btn-dark col-sm-4">Search Date</button>
                </div>
              </DelayLink>
            </div> */}
          </div>
          <div className="display-flex">
            <div className="flex-auto">
              <div className="text-uppercase mt-10">
                <DelayLink to={"/my-wallet-date-range"} delay={200}>
                  <div className="flex-auto">
                    <button className="btn btn-dark col-sm-4">
                      Search Date
                    </button>
                  </div>
                </DelayLink>
              </div>
            </div>
          </div>
          {/* <div className="display-flex">
            <div className="flex-auto">s
              <div className="text-uppercase mt-10"></div>
            </div>
            <div className="flex-auto">
              <div className="text-uppercase mt-10">
                <p></p>
              </div>
            </div>
          </div> */}
          {/* <div className="display-flex">

          </div> */}

          <hr />
          {wallet.wallet_transactions.length != 0 && (
            <div className="display-flex pb-5">
              <div className="flex-auto flex-fill text-left">Date</div>
              <div className="flex-auto flex-fill text-center">OrderID</div>
              <div className="flex-auto flex-fill text-right">Type</div>
              <div className="flex-auto flex-fill text-right">Amount</div>
            </div>
          )}
          {wallet.wallet_transactions.length === 0 && (
            <div className="text-center mt-50 font-w600 text-muted">
              No Wallet Transactions
              {localStorage.getItem("noWalletTransactions")}
            </div>
          )}
          <hr />
          {wallet.wallet_transactions.slice(0, 5).map((item) => (
            <div className="display-flex pb-5" key={item.id}>
              <span className="flex-auto flex-fill text-left">
                <Moment format="DD-MM-YYYY ">{item.created_at}</Moment>
              </span>
              {item.transaction_type === "D" ? (
                <DelayLink to={"/view-order"} delay={200}>
                  <div className="flex-auto flex-fill text-center text-primary">
                    <div
                      onClick={(e) =>
                        this.handleClick(
                          e,
                          item.order_reference,
                          item.created_at
                        )
                      }
                    >
                      {item.order_reference}
                    </div>{" "}
                  </div>
                </DelayLink>
              ) : item.transaction_type === "R" ? (
                <div
                  className="flex-auto flex-fill text-center"
                  style={{ color: this._getTransactionFormat(item) }}
                >
                  <div>{item.order_reference}</div>{" "}
                </div>
              ) : item.transaction_type === "C" ? (
                <div
                  className="flex-auto flex-fill text-center"
                  style={{ color: this._getTransactionFormat(item) }}
                >
                  <div>{item.order_reference}</div>{" "}
                </div>
              ) : item.transaction_type === "P" ? (
                <div
                  className="flex-auto flex-fill text-center"
                  style={{ color: this._getTransactionFormat(item) }}
                >
                  <div>{item.order_reference}</div>{" "}
                </div>
              ) : (
                "-"
              )}
              <div className="flex-auto flex-fill text-right">
                {item.transaction_type}
              </div>
              <div
                className="flex-auto flex-fill text-right"
                style={{ color: this._getTransactionFormat(item) }}
              >
                {this._getTransactionSign(item)}
                {localStorage.getItem("currencyFormat")}
                {item.amount}
              </div>
              <hr />
            </div>
          ))}
        </div>
        {!this.state.errMessage ? <Pie data={this.state.Data} options={options} />:"Please try again later." }


      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  getWalletTransactions,
  getWalletAnalytics,
  getOrderDetails,
})(WalletTransactionsList);
