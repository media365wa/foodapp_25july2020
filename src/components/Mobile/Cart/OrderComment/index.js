import React, { Component } from "react";
import Select from "react-select";
import { GET_PICKUP_LOCATION } from "../../../../configs/index";
import Axios from "axios";
class OrderComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: "",
      deliveryTime: "",
      restaurant_id: this.props.restaurant_id,
      location: "",
      isClearable: true,
      pickupLocations: "",
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.setState({ comment: localStorage.getItem("orderComment") });
    this.setState({ comment: localStorage.getItem("orderDeliveryTime") });
    this.getPickupLocation();
  }

  getPickupLocation = () => {
    Axios.post(GET_PICKUP_LOCATION, {
      restaurant_id: this.state.restaurant_id,
    })
      .then((response) => this.getLocations(response.data.data))
      .catch(function (error) {
        console.log(error);
      });
  };

  getLocations(data) {
    const loc = {};
    const arr = [];
    console.log(this.props);
    if (data.length > 0) {
      data.forEach((element) => {
        arr.push({ value: element.id, label: element.name });
      });
      arr.push(loc);
      this.setState({ pickupLocation: arr });
      return arr;
    } else {
      return arr;
    }
  }

  handleInput = (event) => {
    this.setState({ comment: event.target.value });
    localStorage.setItem("orderComment", event.target.value);
  };

  handleChange = (location) => {
    if (location === null) {
      this.setState((state) => ({
        isClearable: !state.isClearable,
        location: "",
      }));
    } else {
      this.setState({ location }, () =>
        console.log(`Option location:`, this.state.location.value)
      );
      // let val = location.value;
      // let showVal = false;
      localStorage.setItem("pickupLocation", location.value);
      // if(this.state.pickupLocation != null && val) {
      //   showVal = true;
      //   this.props.checkForValue(val, showVal, this.state.pickupLocation);
      // }

      this.setState((state) => ({ isClearable: !state.isClearable }));
    }
  };

  render() {
    const { open, pickupLocation, isClearable, location, customStyle } =
      this.state;
      localStorage.setItem("pickedLocation", this.state.location);
      localStorage.setItem("pickupLocation", this.state.pickupLocation);

    return (
      <React.Fragment>
        <div className="block-content block-content-full bg-white mb-200">
          {pickupLocation ? (
            <div className="form-row" style={{ display: "block" }}>
              <h2 className="bill-detail-text m-0">Select a pickup point * </h2>
              <span style={{ fontSize: "12px" }}>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  defaultValue={"default"}
                  name="color"
                  isClearable={isClearable}
                  onChange={this.handleChange}
                  value={location}
                  options={pickupLocation}
                  required
                />
              </span>
              { (localStorage.getItem("pickedPoint")) ? (
              <div
                className="alert alert-danger text-center"
                role="alert">
                Please select a pickup point.
              </div>
              ) : (
                ""
              )}
              <hr></hr>
            </div>
          ) : (
            ""
          )}
          <div className="form-row" style={{ display: "block" }}>
            <h2 className="item-text">
              <input
                className="form-control  "
                type="text"
                placeholder="Add any suggestion/comment (Optional)"
                onChange={this.handleInput}
                value={this.state.comment || ""}
                // required={required}
              />
            </h2>
          </div>

          {/* <div className="form-group row">
            <label className="col-lg-3 col-form-label">
              Pick delivery time (Optional):
            </label>
            <div className="col-lg-9">
              <input
                type="time"
                name="deliveryTime"
                onChange={this.handleChange}
                className="form-control"
              />
            </div>
          </div> */}
        </div>
      </React.Fragment>
    );
  }
}

export default OrderComment;
