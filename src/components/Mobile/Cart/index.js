import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";

import CartItems from "./CartItems";
import { updateCart } from "../../../services/total/actions";
import { updateUserInfo } from "../../../services/user/actions";
import { addProduct, removeProduct } from "../../../services/cart/actions";
import { getRestaurantInfoById } from "../../../services/items/actions";
import BackWithSearch from "../../Mobile/Elements/BackWithSearch";
import RestaurantInfoCart from "./RestaurantInfoCart";
import OrderComment from "./OrderComment";
import BillDetails from "./BillDetails";
import Coupon from "./Coupon";
import CartCheckoutBlock from "./CartCheckoutBlock";
import Footer from "../Footer";
import Meta from "../../helpers/meta";
import { getWalletTransactions } from "../../../services/wallet/actions";
import { GET_TOTAL_AMOUNT_SPENT_WITHIN_LIMIT } from "../../../configs/index";
import { GET_ORDER_WAITING_LIST } from "../../../configs/index";
import { GET_PICKUP_LOCATION } from "../../../configs/index";
import Axios from "axios";
class Cart extends Component {
  constructor(props) {
    super(props);
  }
  static contextTypes = {
    router: () => null,
  };

  state = {
    loading: true,
    selectedRestSum: 0,
    waitingList: 0,
    waitingListState: false,
    selectedRestaurant: "",
    pickupLocations: "",
    showCheckout: true,
  };

  componentDidMount() {
    if (this.props.cartProducts.length) {
      document.getElementsByTagName("body")[0].classList.add("bg-grey");
    }
    const activeRestaurant = JSON.parse(
      localStorage.getItem("activeRestaurant")
    );
    const { user, cartProducts } = this.props;
    console.log(this.props);
    this.props.getRestaurantInfoById(activeRestaurant);
    if (user.success) {
      this.props.updateUserInfo(user.data.id, user.data.auth_token);
      this.props.getWalletTransactions(user.data.auth_token, user.data.id);
      // this.getPickupLocation(cartProducts[0].restaurant_id);
      this.setState({ loading: false });
      // console.log(this.props.wallets);
    } else {
      this.setState({ loading: false });
      return (
        //redirect to login page if not loggedin
        <Redirect to={"/login"} />
      );
    }
  }

  addProduct = (product) => {
    const { cartProducts, updateCart } = this.props;
    let productAlreadyInCart = false;

    cartProducts.forEach((cp) => {
      if (cp.id === product.id) {
        if (
          JSON.stringify(cp.selectedaddons) ===
          JSON.stringify(product.selectedaddons)
        ) {
          cp.quantity += 1;
          productAlreadyInCart = true;
        }
      }
    });

    if (!productAlreadyInCart) {
      cartProducts.push(product);
    }

    updateCart(cartProducts);
  };

  removeProduct = (product) => {
    const { cartProducts, updateCart } = this.props;
    const index = cartProducts.findIndex(
      (p) =>
        p.id === product.id && JSON.stringify(p) === JSON.stringify(product)
    );
    //if product is in the cart then index will be greater than 0
    if (index >= 0) {
      cartProducts.forEach((cp) => {
        if (cp.id === product.id) {
          if (JSON.stringify(cp) === JSON.stringify(product)) {
            if (cp.quantity === 1) {
              //if quantity is 1 then remove product from cart
              cartProducts.splice(index, 1);
            } else {
              //else decrement the quantity by 1
              cp.quantity -= 1;
            }
          }
        }
      });

      updateCart(cartProducts);
    }
  };

  componentWillUnmount() {
    clearInterval(this.interval);
    document.getElementsByTagName("body")[0].classList.remove("bg-grey");
  }

  // checkPickup = (val, showVal, pickupLoc) => {
  //   console.log('====================================');
  //   console.log(pickupLoc);
  //   console.log('====================================');
  //   if (pickupLoc.length > 0) {
  //     if (val != 0 && showVal) {
  //       this.state.showCheckout = true;
  //       console.log(this.state.showCheckout);
  //     } else {
  //       this.state.showCheckout = false;
  //     }
  //   } else {
  //     this.state.showCheckout = true;
  //   }
  // };

  render() {
    console.log(this.state);
    if (window.innerWidth > 768) {
      return <Redirect to="/" />;
    }
    if (localStorage.getItem("storeColor") === null) {
      return <Redirect to={"/"} />;
    }
    if (!this.props.cartProducts.length) {
      document.getElementsByTagName("body")[0].classList.remove("bg-grey");
    }
    const { user, cartTotal, cartProducts, restaurant_info } = this.props;
    console.log(restaurant_info);
    if (!user.success) {
      return (
        //redirect to login page if not loggedin
        <Redirect to={"/login"} />
      );
    }
    return (
      <React.Fragment>
        <Meta
          seotitle={localStorage.getItem("cartPageTitle")}
          seodescription={localStorage.getItem("seoMetaDescription")}
          ogtype="website"
          ogtitle={localStorage.getItem("seoOgTitle")}
          ogdescription={localStorage.getItem("seoOgDescription")}
          ogurl={window.location.href}
          twittertitle={localStorage.getItem("seoTwitterTitle")}
          twitterdescription={localStorage.getItem("seoTwitterDescription")}
        />
        {this.state.loading ? (
          <div className="height-100 overlay-loading">
            <div>
              <img
                src="/assets/img/loading-food.gif"
                alt={localStorage.getItem("pleaseWaitText")}
              />
            </div>
          </div>
        ) : (
          <React.Fragment>
            <BackWithSearch
              boxshadow={true}
              has_title={true}
              title={localStorage.getItem("cartPageTitle")}
              disbale_search={true}
            />
            {cartProducts.length ? (
              <React.Fragment>
                <div>
                  <RestaurantInfoCart restaurant={restaurant_info} />
                  <div className="block-content block-content-full bg-white pb-5">
                    <h2
                      className="item-text"
                      style={{ marginBottom: "-20px!important" }}
                    >
                      {localStorage.getItem("cartItemsInCartText")}
                    </h2>
                    {cartProducts.map((item, index) => (
                      <CartItems
                        item={item}
                        addProduct={this.addProduct}
                        removeProduct={this.removeProduct}
                        key={item.name + item.id + index}
                      />
                    ))}
                  </div>
                </div>

                <div>
                  <BillDetails total={cartTotal.totalPrice} />
                </div>

                <div
                    style={{
                      position: "fixed",
                      zIndex: 9,
                    }}>
                      <CartCheckoutBlock
                      cart_page={this.context.router.route.location.pathname}
                    />
                </div>
                <div>
                  <OrderComment restaurant_id={cartProducts[0].restaurant_id} />
                </div>
              </React.Fragment>
            ) : (
              <div className="bg-white cart-empty-block">
                <img
                  className="cart-empty-img"
                  src="/assets/img/cart-empty.png"
                  alt={localStorage.getItem("cartEmptyText")}
                />
                <h2 className="cart-empty-text mt-50 text-center">
                  {localStorage.getItem("cartEmptyText")}
                </h2>
                <Footer active_cart={true} />
              </div>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  restaurant_info: state.items.restaurant_info,
  cartProducts: state.cart.products,
  cartTotal: state.total.data,
  user: state.user.user,
  wallets: state.wallets.wallets,
});

export default connect(mapStateToProps, {
  updateUserInfo,
  updateCart,
  addProduct,
  removeProduct,
  getRestaurantInfoById,
  getWalletTransactions,
})(Cart);
