import React, { Component } from "react";
import { connect } from "react-redux";
import Axios from "axios";
import { GET_CONTACT_US } from "../../../configs/index";
// import Ink from "react-ink";GET_CONTACT_US
import BackWithSearch from "../Elements/BackWithSearch";

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.getContactMails();
  }
  getContactMails() {
    Axios.post(GET_CONTACT_US)
      .then(
        (response) => {
          let mails;
          mails = response.data.mail;
          this.setState({ data: mails });
        }
      )
      .catch(function (error) {
        console.log(error);
      });
  }

  renderData(details) {
    return details.map((detail, index) => {
      const { id, name, email } = detail; //destructuring
      return (
        <li key={id}>
          {name}
          <h6>
            <a
              className="text-primary"
              href={
                "mailto:" +
                email +
                "?subject=[Help]%20Issue with the application&cc=cashless.hmc@iimb.ac.in;"
              }
            >
              {email}
            </a>
          </h6>
        </li>
      );
    });
  }

  render() {
    console.log(this.state.data);
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="Contact Us"
            disbale_search={true}
          />
        </div>
        <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
          <h4 className="">You can contact by mailing us at:</h4>
          <br></br>
          <h4 className="">
            <ul style={{ listStyleType: "none" }}>
              {this.state.data ? (
                <div>
                  {this.renderData(this.state.data)}
                  <br></br>
                </div>
              ) : (
                ""
              )}
            </ul>
          </h4>
        </div>
      </React.Fragment>
    );
  }
}
export default connect()(ContactUs);
