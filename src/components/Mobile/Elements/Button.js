import React, { Component } from "react";

class Button extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="mt-20 p-10 button-block">
                    <button
                        className="btn btn-main"
                        style={{ backgroundColor: localStorage.getItem("storeColor") }}
                    >
                        {this.props.buttonText}
                    </button>
                </div>
            </React.Fragment>
        );
    }
}

export default Button;
