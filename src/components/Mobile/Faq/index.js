import React, { Component } from "react";
import { connect } from "react-redux";
import Axios from "axios";
import { GET_FAQ } from "../../../configs/index";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";
import BackWithSearch from "../Elements/BackWithSearch";

class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.getContactMails();
  }
  getContactMails() {
    Axios.post(GET_FAQ)
      .then((response) => {
        let faqs;
        faqs = response.data.faq;
        this.setState({ data: faqs });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  renderData(questions) {
    return (
      <Accordion allowZeroExpanded>
        {questions.map((data) => (
          <AccordionItem key={data.id}>
            <AccordionItemHeading>
              <AccordionItemButton>{data.question}</AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
              <p>{data.answer}</p>
            </AccordionItemPanel>
          </AccordionItem>
        ))}
      </Accordion>
    );
  }

  render() {
    return (
      <React.Fragment>
        <div className="input-group-prepend">
          <BackWithSearch
            boxshadow={true}
            has_title={true}
            title="FAQ"
            disbale_search={true}
          />
        </div>
        <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
          {this.state.data.length > 0 ? (
            <div>
              {this.renderData(this.state.data)}
              <br></br>
            </div>
          ) : (
            <div className="text-center">Please check later</div>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default connect()(Faq);
