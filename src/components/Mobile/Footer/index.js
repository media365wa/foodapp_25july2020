import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import Jello from "react-reveal/Jello";

class Footer extends Component {
    state = {
        active_wallet: false,
        active_nearme: false,
        active_explore: false,
        active_cart: false,
        active_account: false
    };

    componentDidMount() {
        if (this.props.active_nearme === true) {
            this.setState({ active_nearme: true });
        }
        if (this.props.active_explore === true) {
            this.setState({ active_explore: true });
        }
        if (this.props.active_cart === true) {
            this.setState({ active_cart: true });
        }
        if (this.props.active_account === true) {
            this.setState({ active_account: true });
        }
        if (this.props.active_wallet === true) {
            this.setState({ active_wallet: true });
        }
    }

    render() {
        const { cartTotal } = this.props;
        localStorage.removeItem("pickedPoint");
        return (
            <React.Fragment>
                <p>{this.state.active}</p>
                <div className="content pt-10 py-5 font-size-xs clearfix footer-fixed">
                    <NavLink to="/my-wallet" className="col footer-links">
                        <i className="si si-wallet fa-2x" /> <br />
                        <span className={this.state.active_wallet ? "active-footer-tab" : ""}>
                            {this.state.active_wallet ? (
                                <Jello>Wallet</Jello>
                            ) : (
                                <span>Wallet</span>
                            )}
                        </span>
                    </NavLink>
                    <NavLink to="/" className="col footer-links">
                        <i className="si si-pointer fa-2x" /> <br />
                        <span className={this.state.active_nearme ? "active-footer-tab" : ""}>
                            {this.state.active_nearme ? (
                                <Jello>Shops</Jello>
                            ) : (
                                <span> Shops</span>
                            )}
                        </span>
                    </NavLink>

                    <NavLink to="/payment" className="col footer-links">
                        <i className="si si-camera fa-2x" /> <br />
                        <span className={this.state.active_explore ? "active-footer-tab" : ""}>
                            {this.state.active_explore ? (
                                <Jello>Pay</Jello>
                            ) : (
                                <span>Pay</span>
                            )}
                        </span>
                    </NavLink>
                    <NavLink to="/cart" className="col footer-links">
                        <i className="si si-bag fa-2x" /> <br />
                        <span className={this.state.active_cart ? "active-footer-tab" : ""}>
                            {this.state.active_cart ? (
                                <Jello>{localStorage.getItem("footerCart")}</Jello>
                            ) : (
                                <span> {localStorage.getItem("footerCart")}</span>
                            )}
                            <span
                                className="cart-quantity-badge"
                                style={{ backgroundColor: localStorage.getItem("cartColorBg") }}
                            >
                                {cartTotal.productQuantity}
                            </span>
                        </span>
                    </NavLink>
                    <NavLink to="/my-account" className="col footer-links">
                        <i className="si si-user fa-2x" /> <br />
                        <span className={this.state.active_account ? "active-footer-tab" : ""}>
                            {this.state.active_account ? (
                                <Jello>{localStorage.getItem("footerAccount")}</Jello>
                            ) : (
                                <span> {localStorage.getItem("footerAccount")}</span>
                            )}
                        </span>
                    </NavLink>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cartTotal: state.total.data
});

export default connect(
    mapStateToProps,
    {}
)(Footer);
