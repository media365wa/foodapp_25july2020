import React, { Component } from "react";
import Ink from "react-ink";
import Popup from "reactjs-popup";

class Customization extends Component {
    _processAddons = product => {
        let addons = [];
        addons["selectedaddons"] = [];

        let radio = document.querySelectorAll("input[type=radio]:checked");
        for (let i = 0; i < radio.length; i++) {
            addons["selectedaddons"].push({
                addon_category_name: radio[i].name,
                addon_id: radio[i].getAttribute("data-addon-id"),
                addon_name: radio[i].getAttribute("data-addon-name"),
                price: radio[i].value
            });
        }

        let checkboxes = document.querySelectorAll("input[type=checkbox]:checked");

        for (let i = 0; i < checkboxes.length; i++) {
            addons["selectedaddons"].push({
                addon_category_name: checkboxes[i].name,
                addon_id: checkboxes[i].getAttribute("data-addon-id"),
                addon_name: checkboxes[i].getAttribute("data-addon-name"),
                price: checkboxes[i].value
            });
        }

        this.props.addProduct(Object.assign(addons, product));
    };
    render() {
        const { product } = this.props;
        return (
            <React.Fragment>
                <Popup
                    onOpen={() => {
                        document.getElementsByTagName("body")[0].classList.add("noscroll");
                        document
                            .getElementsByClassName("popup-overlay")[0]
                            .classList.add("customizable-scroll");
                    }}
                    onClose={() =>
                        document.getElementsByTagName("body")[0].classList.remove("noscroll")
                    }
                    trigger={
                        <button
                            type="button"
                            className="btn btn-add-remove"
                            style={{
                                color: localStorage.getItem("cartColorBg")
                            }}
                        >
                            <span className="btn-inc">+</span>
                            <Ink duration="500" />
                        </button>
                    }
                    modal
                    closeOnDocumentClick
                >
                    {close => (
                        <div className="pages-modal">
                            <div onClick={close} className="close-modal-header text-right">
                                <span className="close-modal-icon">&times;</span>
                            </div>

                            <div
                                style={{
                                    marginTop: "5rem",
                                    textAlign: "left"
                                }}
                            >
                                <h3>{localStorage.getItem("customizationHeading")}</h3>
                                {product.addon_categories.map(addon_category => (
                                    <div key={addon_category.id} class="addon-category-block">
                                        <React.Fragment>
                                            <p className="addon-category-name">
                                                {addon_category.name}
                                            </p>
                                            {addon_category.addons.length && (
                                                <React.Fragment>
                                                    {addon_category.addons.map((addon, index) => (
                                                        <React.Fragment key={addon.id}>
                                                            <div className="form-group addon-list">
                                                                <input
                                                                    type={
                                                                        addon_category.type ===
                                                                        "SINGLE"
                                                                            ? "radio"
                                                                            : "checkbox"
                                                                    }
                                                                    className={
                                                                        addon_category.type ===
                                                                        "SINGLE"
                                                                            ? "magic-radio"
                                                                            : "magic-checkbox"
                                                                    }
                                                                    name={addon_category.name}
                                                                    data-addon-id={addon.id}
                                                                    data-addon-name={addon.name}
                                                                    value={addon.price}
                                                                    defaultChecked={
                                                                        addon_category.type ===
                                                                            "SINGLE" &&
                                                                        index === 0 &&
                                                                        true
                                                                    }
                                                                />
                                                                {addon_category.type ===
                                                                    "SINGLE" && (
                                                                    <label
                                                                        htmlFor={addon.name}
                                                                    ></label>
                                                                )}

                                                                <label
                                                                    className="text addon-label"
                                                                    htmlFor={addon.name}
                                                                >
                                                                    {addon.name}{" "}
                                                                    {localStorage.getItem(
                                                                        "currencyFormat"
                                                                    )}
                                                                    {addon.price}{" "}
                                                                </label>
                                                            </div>
                                                        </React.Fragment>
                                                    ))}
                                                </React.Fragment>
                                            )}
                                            <hr />
                                        </React.Fragment>
                                    </div>
                                ))}
                                <button
                                    className="btn btn-lg btn-customization-done"
                                    onClick={() => {
                                        this._processAddons(product);
                                        close();
                                    }}
                                    style={{
                                        backgroundColor: localStorage.getItem("cartColorBg"),
                                        color: localStorage.getItem("cartColorText")
                                    }}
                                >
                                    {localStorage.getItem("customizationDoneBtnText")}
                                </button>
                            </div>
                        </div>
                    )}
                </Popup>
            </React.Fragment>
        );
    }
}

export default Customization;
