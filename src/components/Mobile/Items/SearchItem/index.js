import React, { Component } from "react";
import ContentLoader from "react-content-loader";
import ProgressiveImage from "react-progressive-image";
import BackWithSearch from "../../Elements/BackWithSearch";
// import SelectSearch from 'react-select-search/dist/cjs/index.js';
// import Dropdown from "react-dropdown";
// import "react-dropdown/style.css";
// import Select from "react-select";
import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";

export default class SearchItem extends Component {
  state = {
    value: "",
    selected: [],
  };

  handleChange = (selectedOptions) => {
    // console.log(selectedOptions);
    let selectedItem;
    const { items } = this.props.data;
    if (selectedOptions.length > 0) {
      for (const key in items) {
        let list = this.props.data.items[key];
        for (let index = 0; index < list.length; index++) {
          const element = list[index];
          if (element.id === selectedOptions[0].value) {
            selectedItem = element;
          }
        }
      }
      this.props.onSearch(selectedItem);
    } else {
      this.props.onSearch(selectedItem);
    }
  };

  render() {
    const { items } = this.props.data;
    const { value } = this.state;
    const options = [];
    for (let key in items) {
      var list = items[key];
      for (var j = 0; j < list.length; j++) {
        options.push({ value: list[j].id, label: list[j].name });
      }
    }
    return (
      <React.Fragment>
        <div className="height-10 bg-white">
          <div className="">
            <div className="block-content block-content-full">
              <Typeahead
                id="basic-typeahead-single"
                onChange={this.handleChange}
                options={options}
                placeholder="Search for an item"
                // selected={value}
                clearButton
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

// export default SearchItem;
