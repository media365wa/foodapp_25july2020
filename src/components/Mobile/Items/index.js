import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import Axios from "axios";
import {
  getRestaurantInfo,
  getRestaurantItems,
  resetItems,
  resetInfo,
} from "../../../services/items/actions";
import FloatCart from "../FloatCart";
import ItemList from "./ItemList";
import RestaurantInfo from "./RestaurantInfo";
// import ImportantNotice from "./ImportantNotice";
import SearchItem from "./SearchItem";
import Meta from "../../helpers/meta";
class Items extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: "",
      filteredItemsArray: this.props.restaurant_items,
      items: {},
      searchDisplay: false,
    };
    this.onSearch = this.onSearch.bind(this);
  }
  onSearch = this.onSearch.bind(this);
  componentDidMount() {
    //if currentLocation doesnt exists in localstorage then redirect the user to firstscreen
    //else make API calls
    if (localStorage.getItem("currentLocation") === null) {
      this.props.history.push("/");
    } else {
      //call to promoSlider API to fetch
      this.props.getRestaurantInfo(this.props.match.params.restaurant);
      this.props.getRestaurantItems(this.props.match.params.restaurant);
      this.setState({ filteredItemsArray: this.props.restaurant_items });
      // console.log(this.props.restaurant_items)
    }
  }
  componentWillUnmount() {
    this.props.resetItems();
    this.props.resetInfo();
  }
  onSearch(search) {
    let objItems = {};
    let item,
      arr = [];
    if (search) {
      this.setState({ searchDisplay: true });
      for (const key in this.props.restaurant_items.items) {
        let list = this.props.restaurant_items.items[key];
        list.filter(function (e) {
          if (e.id === search.id) {
            arr[0] = e;
            objItems[key] = arr;
            item = objItems;
          }
        });
      }
      if (item) {
        this.setState({ items: item });
      }
    } else {
      this.setState({ searchDisplay: false });
    }
  }
  render() {
    localStorage.setItem(
      "activeRestaurantLimitFrom",
      this.props.restaurant_info.max_cap_from
    );
    if (window.innerWidth > 768) {
      return <Redirect to="/" />;
    }
    if (localStorage.getItem("storeColor") === null) {
      return <Redirect to={"/"} />;
    }
    // console.log(this.state);
    let filteredItemsList = {
      items: this.state.items,
      recommended: this.state.items,
    };
    // console.log(this.props.restaurant_items);
    return (
      <React.Fragment>
        <Meta
          seotitle={`${
            this.props.restaurant_info.name
          } | ${localStorage.getItem("seoMetaTitle")}`}
          seodescription={localStorage.getItem("seoMetaDescription")}
          ogtype="website"
          ogtitle={`${this.props.restaurant_info.name} | ${localStorage.getItem(
            "seoOgTitle"
          )}`}
          ogdescription={localStorage.getItem("seoOgDescription")}
          ogurl={window.location.href}
          twittertitle={`${
            this.props.restaurant_info.name
          } | ${localStorage.getItem("seoTwitterTitle")}`}
          twitterdescription={localStorage.getItem("seoTwitterDescription")}
        />
        <div key={this.props.match.params.restaurant}>
          <RestaurantInfo
            history={this.props.history}
            restaurant={this.props.restaurant_info}
          />
          {/* <ImportantNotice data={this.props.restaurant_info.id} /> */}
          <SearchItem
            data={this.props.restaurant_items}
            onSearch={this.onSearch}
          />
          {this.state.searchDisplay == true ? (
            <ItemList data={filteredItemsList} />
          ) : (
            <ItemList data={this.props.restaurant_items} />
          )}
          {/* <ItemList data={this.props.restaurant_items} /> */}
        </div>
        <div>
          <FloatCart />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  restaurant_info: state.items.restaurant_info,
  restaurant_items: state.items.restaurant_items,
});

export default connect(mapStateToProps, {
  getRestaurantInfo,
  getRestaurantItems,
  resetItems,
  resetInfo,
})(Items);
