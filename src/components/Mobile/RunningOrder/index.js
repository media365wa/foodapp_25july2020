import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";

import { updateUserInfo } from "../../../services/user/actions";
import BackWithSearch from "../../Mobile/Elements/BackWithSearch";
import Map from "./Map";
import Meta from "../../helpers/meta";
import Moment from "react-moment";

class RunningOrder extends Component {
  state = {
    updatedUserInfo: false,
  };
  static contextTypes = {
    router: () => null,
  };

  __refreshOrderStatus = () => {
    const { user } = this.props;
    if (user.success) {
      this.refs.refreshButton.setAttribute("disabled", "disabled");
      this.props.updateUserInfo(user.data.id, user.data.auth_token);
      this.setState({ updatedUserInfo: true });
      this.refs.btnSpinner.classList.remove("hidden");
      setTimeout(() => {
        if (this.refs.refreshButton) {
          this.refs.btnSpinner.classList.add("hidden");
        }
      }, 5 * 1000);
      setTimeout(() => {
        if (this.refs.refreshButton) {
          if (this.refs.refreshButton.hasAttribute("disabled")) {
            this.refs.refreshButton.removeAttribute("disabled");
          }
        }
      }, 5 * 1000);
    }
  };

  componentDidMount() {
    const { user } = this.props;
    if (user.success) {
      if (user.running_order === null) {
        this.props.updateUserInfo(user.data.id, user.data.auth_token);
      }
    }

    this.refreshSetInterval = setInterval(() => {
      this.__refreshOrderStatus();
    }, 10 * 1000);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user.running_order === null) {
      this.context.router.history.push("/");
    }
  }

  componentWillUnmount() {
    clearInterval(this.refreshSetInterval);
  }

  render() {
    if (window.innerWidth > 768) {
      return <Redirect to="/" />;
    }
    if (localStorage.getItem("storeColor") === null) {
      return <Redirect to={"/"} />;
    }

    const { user } = this.props;
    if (!user.success) {
      return <Redirect to={"/"} />;
    }

    let cartProducts = localStorage.getItem("cartProducts");
    cartProducts = JSON.parse(cartProducts);
    // console.log("cartProducts : " + JSON.stringify(cartProducts));
    // if (user.running_order.deliver_status === 1) {
    //     return <Redirect to={"/"} />;
    // }

    return (
      <React.Fragment>
        <Meta
          seotitle={localStorage.getItem("seoMetaTitle")}
          seodescription={localStorage.getItem("seoMetaDescription")}
          ogtype="website"
          ogtitle={localStorage.getItem("seoOgTitle")}
          ogdescription={localStorage.getItem("seoOgDescription")}
          ogurl={window.location.href}
          twittertitle={localStorage.getItem("seoTwitterTitle")}
          twitterdescription={localStorage.getItem("seoTwitterDescription")}
        />
        <BackWithSearch
          boxshadow={true}
          has_title={true}
          title="ORDER STATUS"
          disbale_search={true}
          back_to_home={true}
        />
        {user.running_order && cartProducts && (
          <React.Fragment>
            <div className="bg-white height-100 pt-50">
              <div>
                {localStorage.getItem("showMap") === "true" && (
                  <Map
                    restaurant_latitude={user.running_order.restaurant.latitude}
                    restaurant_longitude={
                      user.running_order.restaurant.longitude
                    }
                    order_id={user.running_order.id}
                    orderstatus_id={user.running_order.orderstatus_id}
                  />
                )}
                <div className="mt-15 mb-200">
                  {user.running_order.orderstatus_id === 1 && (
                    <React.Fragment>
                    <div className="col-md-12">
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Order Reference:</b>{" "}
                          <span>{user.running_order.unique_order_id}</span>
                        </div>{" "}
                      </div>
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Date:</b>{" "}
                          <span>
                            <Moment format="DD-MM-YYYY ">{user.running_order.created_at}</Moment>
                          </span>
                        </div>{" "}
                      </div>
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Restaurant Name: </b>{" "}
                          <span>{user.running_order.restaurant.name}</span>
                        </div>{" "}
                      </div>
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Payment Mode: </b>{" "}
                          <span>{user.running_order.payment_mode}</span>
                        </div>{" "}
                      </div>
                      <hr/>
                      </div>
                      <div className="col-md-12">
                    <div className="display-flex pb-5">
                      <div className="flex-auto text-left" style={{ width: "55%" }}>
                        <b>ITEMS</b>
                      </div>{" "}
                      <div className="flex-auto text-right" style={{ width: "25%" }}>
                        <b>PRICE * QTY</b>
                      </div>{" "}
                      <div className="flex-auto text-right" style={{ width: "20%" }}>
                        <b>TOTAL</b>
                      </div>{" "}
                    </div>
                    <br></br>
                    <div className="col-md-12">

                    {cartProducts.map((item, index) => (
                      <div className="row">
                        <React.Fragment key={item.name + index}>
                        <div className="flex-auto text-left" style={{ width: "55%" }}>
                          {item.name}
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto flex-auto text-right" style={{ width: "25%" }}>
                          {item.price} * {item.quantity}
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto text-right" style={{ width: "20%" }}>{localStorage.getItem("currencyFormat")} {item.price * item.quantity}</div>{" "}

                        </React.Fragment>
                      </div>
                    ))}
                    <br />
                    <div className="row">
                    <b className="flex-auto text-right">GRAND TOTAL : {localStorage.getItem("currencyFormat")} {user.running_order.total}</b>{" "}
                    </div>

                    </div>
                    <hr/>
                    </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-placed.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5"      style={{
                                          color: localStorage.getItem("cartColorBg")
                                      }}
                                >
                                  {localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPlacedSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </React.Fragment>
                  )}
                  {user.running_order.orderstatus_id === 2 && (
                    <React.Fragment>
                    <div className="col-md-12">
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Order Reference:</b>{" "}
                          <span>{user.running_order.unique_order_id}</span>
                        </div>{" "}
                      </div>
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Date:</b>{" "}
                          <span>
                            <Moment format="DD-MM-YYYY ">{user.running_order.created_at}</Moment>
                          </span>
                        </div>{" "}
                      </div>
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Restaurant Name: </b>{" "}
                          <span>{user.running_order.restaurant.name}</span>
                        </div>{" "}
                      </div>
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left">
                          <b>Payment Mode: </b>{" "}
                          <span>{user.running_order.payment_mode}</span>
                        </div>{" "}
                      </div>
                      <hr/>
                      </div>
                      <div className="col-md-12">
                    <div className="display-flex pb-5">
                      <div className="flex-auto text-left" style={{ width: "55%" }}>
                        <b>ITEMS</b>
                      </div>{" "}
                      <div className="flex-auto text-right" style={{ width: "25%" }}>
                        <b>PRICE * QTY</b>
                      </div>{" "}
                      <div className="flex-auto text-right" style={{ width: "20%" }}>
                        <b>TOTAL</b>
                      </div>{" "}
                    </div>
                    <br></br>
                    <div className="col-md-12">

                    {cartProducts.map((item, index) => (
                      <div className="row">
                        <React.Fragment key={item.name + index}>
                        <div className="flex-auto text-left" style={{ width: "55%" }}>
                          {item.name}
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto flex-auto text-right" style={{ width: "25%" }}>
                          {item.price} * {item.quantity}
                          <br></br>
                        </div>{" "}
                        <div className="flex-auto text-right" style={{ width: "20%" }}>{localStorage.getItem("currencyFormat")} {item.price * item.quantity}</div>{" "}

                        </React.Fragment>
                      </div>
                    ))}
                    <br />
                    <div className="row">
                    <b className="flex-auto text-right">GRAND TOTAL : {localStorage.getItem("currencyFormat")} {user.running_order.total}</b>{" "}
                    </div>

                    </div>
                    <hr/>
                  </div>

                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-preparing.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPreparingTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderPreparingTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPreparingSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    {/*  <hr className="m-0" />
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-placed.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPlacedSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr className="m-0" />
                    */}
                    </React.Fragment>
                  )}
                  {user.running_order.orderstatus_id === 3 && (
                    <React.Fragment>
                      <div className="col-md-12">
                        <div className="display-flex pb-5">
                          <div className="flex-auto text-left">
                            <b>Order Reference:</b>{" "}
                            <span>{user.running_order.unique_order_id}</span>
                          </div>{" "}
                        </div>
                        <div className="display-flex pb-5">
                          <div className="flex-auto text-left">
                            <b>Date:</b>{" "}
                            <span>
                              <Moment format="DD-MM-YYYY ">{user.running_order.created_at}</Moment>
                            </span>
                          </div>{" "}
                        </div>
                        <div className="display-flex pb-5">
                          <div className="flex-auto text-left">
                            <b>Restaurant Name: </b>{" "}
                            <span>{user.running_order.restaurant.name}</span>
                          </div>{" "}
                        </div>
                        <div className="display-flex pb-5">
                          <div className="flex-auto text-left">
                            <b>Payment Mode: </b>{" "}
                            <span>{user.running_order.payment_mode}</span>
                          </div>{" "}
                        </div>
                        <hr/>
                        </div>
                        <div className="col-md-12">
                      <div className="display-flex pb-5">
                        <div className="flex-auto text-left" style={{ width: "55%" }}>
                          <b>ITEMS</b>
                        </div>{" "}
                        <div className="flex-auto text-right" style={{ width: "25%" }}>
                          <b>PRICE * QTY</b>
                        </div>{" "}
                        <div className="flex-auto text-right" style={{ width: "20%" }}>
                          <b>TOTAL</b>
                        </div>{" "}
                      </div>
                      <br></br>
                      <div className="col-md-12">

                      {cartProducts.map((item, index) => (
                        <div className="row">
                          <React.Fragment key={item.name + index}>
                          <div className="flex-auto text-left" style={{ width: "55%" }}>
                            {item.name}
                            <br></br>
                          </div>{" "}
                          <div className="flex-auto flex-auto text-right" style={{ width: "25%" }}>
                            {item.price} * {item.quantity}
                            <br></br>
                          </div>{" "}
                          <div className="flex-auto text-right" style={{ width: "20%" }}>{localStorage.getItem("currencyFormat")} {item.price * item.quantity}</div>{" "}

                          </React.Fragment>
                        </div>
                      ))}
                      <br />
                      <div className="row">
                      <b className="flex-auto text-right">GRAND TOTAL : {localStorage.getItem("currencyFormat")} {user.running_order.total}</b>{" "}
                      </div>

                      </div>
                      <hr/>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-onway.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderDeliveryAssignedTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderDeliveryAssignedTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderDeliveryAssignedSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr/>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-preparing.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPreparingTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderPreparingTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPreparingSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/*<hr className="m-0" />
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-placed.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPlacedSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr className="m-0" />
                      */}
                      </React.Fragment>
                  )}
                  {user.running_order.orderstatus_id === 5 && (
                    <React.Fragment>
                        <div className="col-md-12">
                          <div className="display-flex pb-5">
                            <div className="flex-auto text-left">
                              <b>Order Reference:</b>{" "}
                              <span>{user.running_order.unique_order_id}</span>
                            </div>{" "}
                          </div>
                          <div className="display-flex pb-5">
                            <div className="flex-auto text-left">
                              <b>Date:</b>{" "}
                              <span>
                                <Moment format="DD-MM-YYYY ">{user.running_order.created_at}</Moment>
                              </span>
                            </div>{" "}
                          </div>
                          <div className="display-flex pb-5">
                            <div className="flex-auto text-left">
                              <b>Restaurant Name: </b>{" "}
                              <span>{user.running_order.restaurant.name}</span>
                            </div>{" "}
                          </div>
                          <div className="display-flex pb-5">
                            <div className="flex-auto text-left">
                              <b>Payment Mode: </b>{" "}
                              <span>{user.running_order.payment_mode}</span>
                            </div>{" "}
                          </div>
                          <hr/>
                        </div>
                          <div className="col-md-12">
                        <div className="display-flex pb-5">
                          <div className="flex-auto text-left" style={{ width: "55%" }}>
                            <b>ITEMS</b>
                          </div>{" "}
                          <div className="flex-auto text-right" style={{ width: "25%" }}>
                            <b>PRICE * QTY</b>
                          </div>{" "}
                          <div className="flex-auto text-right" style={{ width: "20%" }}>
                            <b>TOTAL</b>
                          </div>{" "}
                        </div>
                        <br></br>
                        <div className="col-md-12">

                        {cartProducts.map((item, index) => (
                          <div className="row">
                            <React.Fragment key={item.name + index}>
                            <div className="flex-auto text-left" style={{ width: "55%" }}>
                              {item.name}
                              <br></br>
                            </div>{" "}
                            <div className="flex-auto flex-auto text-right" style={{ width: "25%" }}>
                              {item.price} * {item.quantity}
                              <br></br>
                            </div>{" "}
                            <div className="flex-auto text-right" style={{ width: "20%" }}>{localStorage.getItem("currencyFormat")} {item.price * item.quantity}</div>{" "}

                            </React.Fragment>
                          </div>
                        ))}
                        <br />
                        <div className="row">
                        <b className="flex-auto text-right">GRAND TOTAL : {localStorage.getItem("currencyFormat")} {user.running_order.total}</b>{" "}
                        </div>

                        </div>
                        <hr/>
                        </div>
                      {/* <div className="row">
                                                <div className="col-md-12">
                                                    <div className="font-size-h4 mb-5 pl-15">
                                                        {localStorage.getItem(
                                                            "runningOrderDeliveryPin"
                                                        )}{" "}
                                                        <span className="font-w600 btn-deliverypin">
                                                            {this.props.user.data.delivery_pin}
                                                        </span>
                                                    </div>
                                                    <hr />
                                                    <div className="block block-link-shadow">
                                                        <div className="block-content block-content-full clearfix py-0">
                                                            <div className="float-right">
                                                                <img
                                                                    src="/assets/img/order-onway.gif"
                                                                    className="img-fluid img-avatar"
                                                                    alt={localStorage.getItem(
                                                                        "runningOrderOnwayTitle"
                                                                    )}
                                                                    style={{
                                                                        transform: "scaleX(-1)"
                                                                    }}
                                                                />
                                                            </div>
                                                            <div
                                                                className="float-left mt-20"
                                                                style={{ width: "75%" }}
                                                            >
                                                                <div className="font-w600 font-size-h4 mb-5">
                                                                    {localStorage.getItem(
                                                                        "runningOrderOnwayTitle"
                                                                    )}
                                                                </div>
                                                                <div className="font-size-sm text-muted">
                                                                    {localStorage.getItem(
                                                                        "runningOrderOnwaySub"
                                                                    )}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="m-0" /> */}
                      <div className="row">
                        <div className="col-md-12">
                          <div className="block block-link-shadow">
                            <div className="block-content block-content-full clearfix py-0">
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-placed.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  Order has been prepared
                                </div>
                                <div className="font-size-sm text-muted">
                                  You can pick your order at the shop
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                  {/*    <div className="row">
                        <div
                          className="col-md-12 "
                          style={{ backgroundColor: "lightgray" }}
                        >
                          <div className="block block-link-shadow ">
                            <div
                              style={{ backgroundColor: "lightgray" }}
                              className="block-content block-content-full clearfix py-0"
                            >
                              <div className="float-right">
                                <img
                                  src="/assets/img/order-preparing.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPreparingTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderPreparingTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPreparingSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr className="m-0" />
                      <div className="row">
                        <div
                          style={{ backgroundColor: "lightgray" }}
                          className="col-md-12"
                        >
                          <div className="block block-link-shadow">
                            <div
                              style={{ backgroundColor: "lightgray" }}
                              className="block-content block-content-full clearfix py-0"
                            >
                              <div
                                className="float-right"
                                style={{ backgroundColor: "lightgray" }}
                              >
                                <img
                                  src="/assets/img/order-placed.gif"
                                  className="img-fluid img-avatar"
                                  alt={localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                />
                              </div>
                              <div
                                className="float-left mt-20"
                                style={{ width: "75%" }}
                              >
                                <div className="font-w600 font-size-h4 mb-5">
                                  {localStorage.getItem(
                                    "runningOrderPlacedTitle"
                                  )}
                                </div>
                                <div className="font-size-sm text-muted">
                                  {localStorage.getItem(
                                    "runningOrderPlacedSub"
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr className="m-0" />
                      */}
                    </React.Fragment>
                  )}
                  {user.running_order.orderstatus_id === 6 && (

                    <div className="row">
                      <div className="col-md-12">
                        <div className="block block-link-shadow">
                          <div className="block-content block-content-full clearfix py-0">
                            <div className="float-right">
                              <img
                                src="/assets/img/order-canceled.png"
                                className="img-fluid img-avatar"
                                alt={localStorage.getItem(
                                  "runningOrderCanceledTitle"
                                )}
                                style={{ transform: "scaleX(-1)" }}
                              />
                            </div>
                            <div
                              className="float-left mt-20"
                              style={{ width: "75%" }}
                            >
                              <div className="font-w600 font-size-h4 mb-5">
                                {localStorage.getItem(
                                  "runningOrderCanceledTitle"
                                )}
                              </div>
                              <div className="font-size-sm text-muted">
                                {localStorage.getItem(
                                  "runningOrderCanceledSub"
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr className="m-0" />
                    </div>
                  )}
                </div>
              </div>
              <div>
                <button
                  className="btn btn-lg btn-refresh-status"
                  ref="refreshButton"
                  onClick={this.__refreshOrderStatus}
                  style={{
                    backgroundColor: localStorage.getItem("cartColorBg"),
                    color: localStorage.getItem("cartColorText"),
                  }}
                >
                  {localStorage.getItem("runningOrderRefreshButton")}{" "}
                  <span ref="btnSpinner" className="hidden">
                    <i className="fa fa-refresh fa-spin" />
                  </span>
                </button>
              </div>
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user,
});

export default connect(mapStateToProps, { updateUserInfo })(RunningOrder);
