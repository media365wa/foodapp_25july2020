import React, { Component } from "react";
import Axios from "axios";
import { Modal } from "react-responsive-modal";
import { UPDATE_PHONE } from "../../../configs/index";
import "react-responsive-modal/styles.css";
import axios from "axios";
import { Redirect } from "react-router";
class OrderComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: this.props.phone,
      open: true,
      userId: this.props.userId,
      showMessage: "",
      success: false
    };
    this.handlePhoneNumber = this.handlePhoneNumber.bind(this);
  }

  componentDidMount() {}
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });

  };

  updatePhoneNumber = () => {
    var regx = /^[10-11]\d{9}$/;
    if (regx.test(this.state.phone) === false) {
      this.setState({ showMessage: "Please enter a valid number." });
    } else {
      axios
        .post(UPDATE_PHONE, {
          userId: this.state.userId,
          phone: this.state.phone,
        })
        .then((response) => {
            this.setState({success: response.data.success});
          if (response.data.success) {
            this.onCloseModal();
          } else {
            this.setState({
              showMessage: "Please enter a valid number.",
            });
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  handlePhoneNumber = (event) => {
    event.preventDefault();
    this.setState({ phone: event.target.value });
  };
  render() {
    const { phone } = this.props;
    console.log(this.props);
    console.log(this.state.phone);
    if(this.state.success) {
        return <Redirect to="/" />;
    }
    return (
      <React.Fragment>
        <Modal open={this.state.open} onClose={this.onCloseModal} center>
          <br></br>
          <div className="form-control apply-coupon-input font-size-h3 font-w600">
            <span>Update your phone number.</span>
            <br></br>
            <span>
              <b>Phone:</b>{" "}
            </span>
            <input
              onChange={this.handlePhoneNumber}
              type="tel"
              name="phone"
            ></input>
            <br></br>
            {this.state.showMessage ? (
              <span style={{ color: "red" }}>{this.state.showMessage}</span>
            ) : (
              ""
            )}
          </div>
          <div className="d-flex flex-row bd-highlight mb-3">
            <button
              className="btn btn-danger col-sm-5"
              onClick={this.onCloseModal}
            >
              Cancel
            </button>
            <button
              className="btn btn-dark col-sm-4"
              onClick={() => {
                this.updatePhoneNumber();
              }}
            >
              Yes
            </button>
          </div>
        </Modal>{" "}
      </React.Fragment>
    );
  }
}

export default OrderComment;
