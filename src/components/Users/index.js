import React, { Component } from "react";

import { connect } from "react-redux";

import { fetchUsers } from "../../services/user/actions";
import User from "./User";
import Logout from "./Logout";

class Users extends Component {
	state = {
		logged_in: false,
		email: localStorage.getItem("email")
	};
	componentDidMount() {
		if (localStorage.getItem("logged_in") === "true") {
			this.setState({ logged_in: true });
		} else {
			this.props.fetchUsers();
		}
	}

	getLoginTime = () => {
		return localStorage.getItem("time");
	};

	render() {
		return (
			<React.Fragment>
				{this.state.logged_in ? (
					<div className="col-md-12 loggedin-block">
						<div className="block block-themed">
							<div className="block-header">
								<h3 className="block-title">You are Loggedin</h3>
								<div className="block-options">
									<button type="button" className="btn-block-option">
										<i className="si si-check" />
									</button>
								</div>
							</div>
							<div className="block-content">
								<h3 className="text-muted login-time">
									Login Time: {this.getLoginTime()}
								</h3>
								<Logout email={this.state.email} />
							</div>
						</div>
					</div>
				) : (
					<div className="container">
						<div className="text-center">
							<img
								src="/assets/img/logo.png"
								alt="leadwalnut"
								className="mb-20 mt-20"
							/>
						</div>
						<h3 className="py-20 text-muted mt-20 text-center">
							Select your profile to login
						</h3>
						<User user={this.props.users} />
					</div>
				)}
			</React.Fragment>
		);
	}
}
const mapStateToProps = state => ({
	users: state.users.users
});

export default connect(
	mapStateToProps,
	{ fetchUsers }
)(Users);
