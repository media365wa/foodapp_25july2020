import { WEBSITE_URL } from "./website";

export const GET_SETTINGS_URL = WEBSITE_URL + "/public/api/get-settings";
export const SEARCH_LOCATIONS_URL = WEBSITE_URL + "/public/api/search-location";
export const GET_POPULAR_LOCATIONS_URL = WEBSITE_URL + "/public/api/popular-locations";
export const GET_PROMO_SLIDER_URL = WEBSITE_URL + "/public/api/promo-slider";
export const GET_RESTAURANTS_URL = WEBSITE_URL + "/public/api/get-restaurants";
export const GET_USER_DETAILS = WEBSITE_URL + "/public/api/get-user-details";
export const UPDATE_PHONE = WEBSITE_URL + "/public/api/update-phone";
export const GET_RESTAURANT_INFO_URL = WEBSITE_URL + "/public/api/get-restaurant-info";
export const GET_RESTAURANT_INFO_BY_ID_URL = WEBSITE_URL + "/public/api/get-restaurant-info-by-id";
export const GET_RESTAURANT_ITEMS_URL = WEBSITE_URL + "/public/api/get-restaurant-items";
export const APPLY_COUPON_URL = WEBSITE_URL + "/public/api/apply-coupon";
export const LOGIN_USER_URL = WEBSITE_URL + "/public/api/login";
export const CHECK_EMAIL = WEBSITE_URL + "/public/api/check-email";
export const VERIFY_OTP = WEBSITE_URL + "/public/api/verify-otp";
export const CHANGE_PASSWORD = WEBSITE_URL + "/public/api/change-password";
export const REGISTER_USER_URL = WEBSITE_URL + "/public/api/register";
export const GET_PAGES_URL = WEBSITE_URL + "/public/api/get-pages";
export const SEARCH_RESTAURANTS_URL = WEBSITE_URL + "/public/api/search-restaurants";
export const GET_ADDRESSES_URL = WEBSITE_URL + "/public/api/get-addresses";
export const SAVE_ADDRESS_URL = WEBSITE_URL + "/public/api/save-address";
export const DELETE_ADDRESS_URL = WEBSITE_URL + "/public/api/delete-address";
export const UPDATE_USER_INFO_URL = WEBSITE_URL + "/public/api/update-user-info";
export const UPDATE_USER_PERSONAL_DETAILS_URL = WEBSITE_URL + "/public/api/update-user-personal-details";
export const UPDATE_USER_ADDRESS_URL = WEBSITE_URL + "/public/api/update-user-address";
export const UPDATE_USER_PASSWORD_URL = WEBSITE_URL + "/public/api/update-user-password";
export const CHECK_OLD_PASSWORD = WEBSITE_URL + "/public/api/check-old-password";
export const PLACE_ORDER_URL = WEBSITE_URL + "/public/api/place-order";
export const SET_DEFAULT_URL = WEBSITE_URL + "/public/api/set-default-address";
export const GET_ORDERS_URL = WEBSITE_URL + "/public/api/get-orders";
export const GET_SINGLE_ORDER_URL = WEBSITE_URL + "/public/api/get-order-byid";
export const GET_WALLET_URL = WEBSITE_URL + "/public/api/get-wallet-transactions";
export const SAVE_WALLET_URL = WEBSITE_URL + "/public/api/save-wallet";
export const GET_WALLET_DATE_RANGE = WEBSITE_URL + "/public/api/get-wallet-date-range";
export const GET_PAYMENT_GATEWAYS_URL = WEBSITE_URL + "/public/api/get-payment-gateways";
export const NOTIFICATION_TOKEN_URL = WEBSITE_URL + "/public/api/save-notification-token";
export const SAVE_AVAILABILITY_URL = WEBSITE_URL + "/public/api/save-user-availabilty";
export const PAYMENTMODE_STATUS = WEBSITE_URL + "/public/api/paymentmode_status";
export const VIEW_ORDER_URL = WEBSITE_URL + "/public/api/view-order";
export const ORDER_DETAILS = WEBSITE_URL + "/public/api/get-order-details";
export const GET_TOTAL_AMOUNT_SPENT_WITHIN_LIMIT = WEBSITE_URL + "/public/api/get-total-amount-spent";
export const REPORT_ISSUE = WEBSITE_URL + "/public/api/report-issue";
export const GET_TICKETS = WEBSITE_URL + "/public/api/get-tickets";
export const GET_CONTACT_US = WEBSITE_URL + "/public/api/contact-us";
export const GET_FAQ = WEBSITE_URL + "/public/api/faq";
export const GET_ORDER_WAITING_LIST = WEBSITE_URL + "/public/api/get-waiting-list";
export const GET_NOTICES = WEBSITE_URL + "/public/api/get-notices";
export const GET_PICKUP_LOCATION = WEBSITE_URL + "/public/api/get-pickup-location";
/* Delivery URLs */
export const LOGIN_DELIVERY_USER_URL = WEBSITE_URL + "/public/api/delivery/login";
export const UPDATE_DELIVERY_USER_INFO_URL = WEBSITE_URL + "/public/api/delivery/update-user-info";
export const GET_DELIVERY_ORDERS_URL = WEBSITE_URL + "/public/api/delivery/get-delivery-orders";
export const GET_SINGLE_DELIVERY_ORDER_URL =
    WEBSITE_URL + "/public/api/delivery/get-single-delivery-order";
export const SEND_DELIVERY_GUY_GPS_LOCATION_URL =
    WEBSITE_URL + "/public/api/delivery/set-delivery-guy-gps-location";
export const GET_DELIVERY_GUY_GPS_LOCATION_URL =
    WEBSITE_URL + "/public/api/delivery/get-delivery-guy-gps-location";
export const ACCEPT_TO_DELIVER_URL = WEBSITE_URL + "/public/api/delivery/accept-to-deliver";
export const PICKEDUP_ORDER_URL = WEBSITE_URL + "/public/api/delivery/pickedup-order";
export const DELIVER_ORDER_URL = WEBSITE_URL + "/public/api/delivery/deliver-order";
export const SELECT_RESTAURANTS_URL = WEBSITE_URL + "/public/api/select-restaurant";
export const SAVE_DIRECT_WALLET_TRANSACTIONS = WEBSITE_URL + "/public/api/save-direct-wallet-transacctions"
export const SAVE_QR_CODE_MESS = WEBSITE_URL + "/public/api/mess-qr"




