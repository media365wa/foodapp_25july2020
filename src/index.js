import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Redirect } from "react-router";
import Loadable from "react-loadable";
import Loading from "./components/helpers/loading";
import withTracker from "./withTracker";
import 'react-bootstrap-typeahead/css/Typeahead.css';
import Root from "./Root";
import 'react-accessible-accordion/dist/fancy-example.css';
import App from "./components/App";
import Home from "./components/Mobile/Home";
// import CacheBuster from "./CacheBuster";

// import NotFound from "./components/NotFound";
const NotFound = Loadable({
    loader: () => import("./components/NotFound"),
    loading: () => <Loading />
});

// import Location from "./components/Mobile/Location";
const Location = Loadable({
    loader: () => import("./components/Mobile/Location"),
    loading: () => <Loading />
});

// import Items from "./components/Mobile/Items";
const Items = Loadable({
    loader: () => import("./components/Mobile/Items"),
    loading: () => <Loading />
});

// import Login from "./components/Mobile/Auth/Login";
const Login = Loadable({
    loader: () => import("./components/Mobile/Auth/Login"),
    loading: () => <Loading />
});

// import Register from "./components/Mobile/Auth/Register";
const Register = Loadable({
    loader: () => import("./components/Mobile/Auth/Register"),
    loading: () => <Loading />
});

// import CartPage from "./components/Mobile/Cart";
const CartPage = Loadable({
    loader: () => import("./components/Mobile/Cart"),
    loading: () => <Loading />
});

// import Account from "./components/Mobile/Account";
const Account = Loadable({
    loader: () => import("./components/Mobile/Account"),
    loading: () => <Loading />
});

// import Explore from "./components/Mobile/Explore";
const Explore = Loadable({
    loader: () => import("./components/Mobile/Explore"),
    loading: () => <Loading />
});

// import Addresses from "./components/Mobile/Account/Addresses";
const Addresses = Loadable({
    loader: () => import("./components/Mobile/Account/Addresses"),
    loading: () => <Loading />
});

const WalletTransactions = Loadable({
    loader: () => import("./components/Mobile/Account/WalletTransactions"),
    loading: () => <Loading />
});

const WalletTransactionsDateRange = Loadable({
    loader: () => import("./components/Mobile/Account/WalletTransactions/WalletTransactionDateRange"),
    loading: () => <Loading />
});

const QrCode = Loadable({
    loader: () => import("./components/Mobile/Account/QrCode"),
    loading: () => <Loading />
});

const UserAvailability = Loadable({
    loader: () => import("./components/Mobile/Account/UserAvailability"),
    loading: () => <Loading />
});

// import Checkout from "./components/Mobile/Checkout";
const Checkout = Loadable({
    loader: () => import("./components/Mobile/Checkout"),
    loading: () => <Loading />
});

// import RunningOrder from "./components/Mobile/RunningOrder";
const RunningOrder = Loadable({
    loader: () => import("./components/Mobile/RunningOrder"),
    loading: () => <Loading />
});

// import Orders from "./components/Mobile/Account/Orders";
const Orders = Loadable({
    loader: () => import("./components/Mobile/Account/Orders"),
    loading: () => <Loading />
});

/* Delivery */
// import Delivery from "./components/Delivery";
const Delivery = Loadable({
    loader: () => import("./components/Delivery"),
    loading: () => <Loading />
});

// import DeliveryLogin from "./components/Delivery/Login";
const DeliveryLogin = Loadable({
    loader: () => import("./components/Delivery/Login"),
    loading: () => <Loading />
});

// import DeliveryOrders from "./components/Delivery/Orders";
const DeliveryOrders = Loadable({
    loader: () => import("./components/Delivery/Orders"),
    loading: () => <Loading />
});

// import ViewOrder from "./components/Delivery/ViewOrder";
const ViewOrder = Loadable({
    loader: () => import("./components/Delivery/ViewOrder"),
    loading: () => <Loading />
});

const SelectRestaurant = Loadable({
    loader: () => import("./components/Mobile/Account/SelectRestaurant"),
    loading: () => <Loading />
});

const ContactUs = Loadable({
    loader: () => import("./components/Mobile/ContactUs"),
    loading: () => <Loading />
});

const AboutUs = Loadable({
    loader: () => import("./components/Mobile/AboutUs"),
    loading: () => <Loading />
});

const Payment = Loadable({
    loader: () => import("./components/Mobile/Account/Payment"),
    loading: () => <Loading />
});
const PersonalDeatils = Loadable({
    loader: () => import("./components/Mobile/Account/PersonalDetails"),
    loading: () => <Loading />
});
const ChangePassword = Loadable({
    loader: () => import("./components/Mobile/Account/Edit/ChangePassword"),
    loading: () => <Loading />
});

const UpdateUserDetails = Loadable({
    loader: () => import("./components/Mobile/Account/Edit/PersonalDetails"),
    loading: () => <Loading />
});

const UserMenu = Loadable({
    loader: () => import("./components/Mobile/Account/UserMenu"),
    loading: () => <Loading />
});

const ViewOrderDetails = Loadable({
    loader: () => import("./components/Mobile/Account/Orders/ViewOrder"),
    loading: () => <Loading />
});

const UpdateAddress = Loadable({
    loader: () => import("./components/Mobile/Account/Addresses/UpdateAddress"),
    loading: () => <Loading />
});

const PreviousPassword = Loadable({
    loader: () => import("./components/Mobile/Account/Edit/PreviousPassword"),
    loading: () => <Loading />
});

const ReportIssue = Loadable({
    loader: () => import("./components/Mobile/Account/ReportIssue"),
    loading: () => <Loading />
});

const SubmitReport = Loadable({
    loader: () => import("./components/Mobile/Account/ReportIssue/SubmitReport"),
    loading: () => <Loading />
});

const ViewTickets = Loadable({
    loader: () => import("./components/Mobile/Account/ReportIssue/ViewTickets"),
    loading: () => <Loading />
});

const ForgotPassword = Loadable({
    loader: () => import("./components/Mobile/Account/Edit/ForgotPassword"),
    loading: () => <Loading />
});
const Faq = Loadable({
    loader: () => import("./components/Mobile/Faq"),
    loading: () => <Loading />
});
const OrderStatus = Loadable({
    loader: () => import("./components/Mobile/Account/Orders/OrderStatus"),
    loading: () => <Loading />
});
const CartQrCode = Loadable({
    loader: () => import("./components/Mobile/Account/QrCodeAddToCart"),
    loading: () => <Loading />
});
const ScrollToTop = () => {
    window.scrollTo(0, 0);
    return null;
};

ReactDOM.render(
    <Root>
        <BrowserRouter>
            <React.Fragment>
                <Route component={ScrollToTop} />
                <Switch>
                    <Route
                        exact
                        strict
                        path="/:url*"
                        render={props => <Redirect to={`${props.location.pathname}/`} />}
                    />
                    <Route path={"/"} exact component={withTracker(App)} />
                    <Route path={"/search-location"} exact component={withTracker(Location)} />
                    <Route path={"/:location/restaurants"} exact component={withTracker(Home)} />
                    <Route
                        path={"/:location/restaurants/:restaurant"}
                        exact
                        component={withTracker(Items)}
                    />
                    <Route path={"/explore"} exact component={withTracker(Explore)} />
                    <Route path={"/login"} exact component={withTracker(Login)} />
                    <Route path={"/register"} exact component={withTracker(Register)} />
                    <Route path={"/my-account"} exact component={withTracker(Account)} />
                    <Route path={"/my-addresses"} exact component={withTracker(Addresses)} />
                    <Route path={"/my-wallet"} exact component={withTracker(WalletTransactions)} />
                    <Route path={"/my-wallet-date-range"} exact component={withTracker(WalletTransactionsDateRange)} />

                    <Route path={"/qrcode"} exact component={withTracker(QrCode)} />
                    <Route path={"/mess-qr"} exact component={withTracker(CartQrCode)} />
                    <Route path={"/user-availabilty"} exact component={withTracker(UserAvailability)} />
                    <Route path={"/select-restaurant"} exact component={withTracker(SelectRestaurant)} />
                    <Route path={"/checkout"} exact component={withTracker(Checkout)} />
                    <Route path={"/running-order"} exact component={withTracker(RunningOrder)} />
                    <Route path={"/my-orders"} exact component={withTracker(Orders)} />
                    <Route path={"/cart"} exact component={withTracker(CartPage)} />
                    <Route path={"/contact-us"} exact component={withTracker(ContactUs)} />
                    <Route path={"/about-us"} exact component={withTracker(AboutUs)} />
                    <Route path={"/payment"} exact component={withTracker(Payment)} />
                    <Route path={"/personal-details"} exact component={withTracker(PersonalDeatils)} />
                    <Route path={"/change-password"} exact component={withTracker(ChangePassword)} />
                    <Route path={"/old-password"} exact component={withTracker(PreviousPassword)} />
                    <Route path={"/forgot-password"} exact component={withTracker(ForgotPassword)} />
                    <Route path={"/update-user-details"} exact component={withTracker(UpdateUserDetails)} />
                    <Route path={"/user-info"} exact component={withTracker(UserMenu)} />
                    <Route path={"/view-order"} exact component={withTracker(ViewOrderDetails)} />
                    <Route path={"/update-address"} exact component={withTracker(UpdateAddress)} />
                    <Route path={"/report-issue"} exact component={withTracker(ReportIssue)} />
                    <Route path={"/submit-report"} exact component={withTracker(SubmitReport)} />
                    <Route path={"/view-tickets"} exact component={withTracker(ViewTickets)} />
                    <Route path={"/faq"} exact component={withTracker(Faq)} />
                    <Route path={"/check-status"} exact component={withTracker(OrderStatus)} />
                    {/* Delivery Routes */}
                    <Route path={"/delivery"} exact component={Delivery} />
                    <Route path={"/delivery/login"} exact component={DeliveryLogin} />
                    <Route path={"/delivery/orders"} exact component={DeliveryOrders} />
                    <Route path={"/delivery/orders/:unique_order_id"} exact component={ViewOrder} />
                    <Route path={"/delivery/completed-orders"} exact component={Delivery} />

                    {/* Common Routes */}
                    <Route component={NotFound} />
                </Switch>
            </React.Fragment>
        </BrowserRouter>
    </Root>,
    document.getElementById("root")
);
