import { PLACE_ORDER } from "./actionTypes";
import { PLACE_ORDER_URL } from "../../configs";

import Axios from "axios";
import { updateCart } from "../total/actions";

export const placeOrder = (
    user,
    order,
    coupon,
    location,
    order_comment,
    delivery_time,
    total,
    method,
    payment_token
) => (dispatch, getState) => {
    Axios.post(PLACE_ORDER_URL, {
        token: user.data.auth_token,
        user: user,
        order: order,
        coupon: coupon,
        location: location,
        order_comment: order_comment,
        pickup_location: localStorage.getItem("pickupLocation"),
        delivery_time: '',
        total: delivery_time,
        method: total,
        payment_token: payment_token,
        restaurantCode: localStorage.getItem("restaurantCode")
    })
        .then(response => {
            const checkout = response.data;

            if (checkout.success) {
                dispatch({ type: PLACE_ORDER, payload: checkout });

                const state = getState();
                // console.log(state);
                const cartProducts = state.cart.products;
                // const user = state.user.user;
                localStorage.removeItem("orderComment");
                localStorage.removeItem("orderDeliveryTime");
                localStorage.removeItem("pickupLocation");

                for (let i = cartProducts.length - 1; i >= 0; i--) {
                    // remove all items from cart
                    cartProducts.splice(i, 1);
                }

                dispatch(updateCart(cartProducts));
            }
        })
        .catch(function(error) {
            console.log(error);
        });
};
