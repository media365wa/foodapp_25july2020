import { SEARCH_RESTAURANTS } from "./actionTypes";
import { SELECT_RESTAURANTS } from "./actionTypes";
import { SEARCH_RESTAURANTS_URL } from "../../configs";
import { SELECT_RESTAURANTS_URL } from "../../configs/index";
import { GET_NOTICES } from "../../configs/index";
import Axios from "axios";

export const searchRestaurants = (query, location) => (dispatch) => {
  Axios.post(SEARCH_RESTAURANTS_URL, {
    q: query,
    location: location,
  })
    .then((response) => {
      const restaurants = response.data;
      return dispatch({ type: SEARCH_RESTAURANTS, payload: restaurants });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getRestaurants = () => (dispatch) => {
  // Axios.post(SELECT_RESTAURANTS_URL )
  //     .then(response => {
  //         const restaurants = response.data;
  //         return dispatch({ type: SEARCH_RESTAURANTS, payload: restaurants });
  //     })
  //     .catch(function(error) {
  //         console.log(error);
  //     });

  Axios.post(SELECT_RESTAURANTS_URL)
    .then((response) => {
      localStorage.setItem("ShopList", JSON.stringify(response.data));
    })
    // console.log()
    .catch(function (error) {
      console.log(error);
    });
};

export const importantNotices = () => (dispatch) => {
  Axios.post(GET_NOTICES, { })
    .then((response) => {
      localStorage.setItem("notices", JSON.stringify(response.data));
    })
  //   .then((json) => this.setState({ selectedRest: json.data }))
    .catch(function (error) {
      console.log(error);
    });
};
