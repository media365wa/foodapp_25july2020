import {
  UPDATE_CART
} from "./actionTypes";
import {
  formatPrice
} from "../../components/helpers/formatPrice";

export const updateCart = cartProducts => dispatch => {
  let productQuantity = cartProducts.reduce((sum, p) => {
    // p.quantity = p.originalQuantity;
    // sum += p.quantity;
    if (p.quantity <= p.originalQuantity) {
      sum++;
      return sum;
    } else {
      
      p.quantity = (p.originalQuantity !== undefined) ? parseInt(p.originalQuantity) : p.quantity;
      alert('No More Quantities Avaliable In Cart');
      sum++;
      return sum;
      // return parseInt(p.originalQuantity);
    }
  }, 0);

  let totalPrice = cartProducts.reduce((sum, p) => {
    if (p.quantity <= p.originalQuantity) {
      let addonTotal = 0;
      if (p.selectedaddons) {
        p.selectedaddons.map(addonArray => {
          addonTotal += parseFloat(addonArray.price);
          return addonTotal;
        });
      }
      sum += p.price * p.quantity + addonTotal * p.quantity;
      sum = parseFloat(sum);
      formatPrice(sum);
      return sum;
    } else {
      let addonTotal = 0;
      if (p.selectedaddons) {
        p.selectedaddons.map(addonArray => {
          addonTotal += parseFloat(addonArray.price);
          return addonTotal;
        });
      }
      //sum += p.price * p.originalQuantity + addonTotal * p.originalQuantity;
      sum += p.price * p.quantity + addonTotal * p.quantity;
      sum = parseFloat(sum);
      formatPrice(sum);
      return sum;
    }
  }, 0);

  let cartTotal = {
    productQuantity,
    totalPrice
  };

  dispatch({
    type: UPDATE_CART,
    payload: cartTotal
  });
};