import {
  LOGIN_USER,
  REGISTER_USER,
  LOGOUT_USER,
  UPDATE_USER_INFO,
  UPDATE_USER_PASSWORD,
  UPDATE_USER_PERSONAL_DETAILS,
} from "./actionTypes";
import {
  LOGIN_USER_URL,
  REGISTER_USER_URL,
  UPDATE_USER_INFO_URL,
  UPDATE_USER_PASSWORD_URL,
  UPDATE_USER_PERSONAL_DETAILS_URL,
} from "../../configs";

import Axios from "axios";

export const loginUser = (name, email, password, accessToken) => (dispatch) => {
  Axios.post(LOGIN_USER_URL, {
    name: name,
    email: email,
    password: password,
    accessToken: accessToken,
  })
    .then((response) => {
      const user = response.data;
      return dispatch({ type: LOGIN_USER, payload: user });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const registerUser = (name, email, phone, password) => (dispatch) => {
  Axios.post(REGISTER_USER_URL, {
    name: name,
    email: email,
    phone: phone,
    password: password,
  })
    .then((response) => {
      const user = response.data;
      return dispatch({ type: REGISTER_USER, payload: user });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const logoutUser = (user) => (dispatch) => {
  user = [];
  dispatch({
    type: LOGOUT_USER,
    payload: user,
  });
};

export const updateUserInfo = (user_id, token) => (dispatch) => {
  Axios.post(UPDATE_USER_INFO_URL, {
    token: token,
    user_id: user_id,
  })
    .then((response) => {
      const user = response.data;
      return dispatch({ type: UPDATE_USER_INFO, payload: user });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const changePassword = (user_id, email, authToken, password) => (
  dispatch
) => {
  Axios.post(UPDATE_USER_PASSWORD_URL + "/" + user_id + "?token=" + authToken, {
    user_id: user_id,
    email: email,
    authToken: authToken,
    password: password,
  })
    .then((response) => {
      const user = response.data;
      return dispatch({ type: UPDATE_USER_PASSWORD, payload: user });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const updateUserPersonalDetails = (
  user_id,
  name,
  email,
  phone,
  roll_number,
  token
) => (dispatch) => {
  Axios.post(
    UPDATE_USER_PERSONAL_DETAILS_URL + "/" + user_id + "?token=" + token,
    {
      token: token,
      name: name,
      email: email,
      phone: phone,
      roll_number: roll_number,
      user_id: user_id,
    }
  )
    .then((response) => {
      const user = response.data;
      return dispatch({ type: UPDATE_USER_PERSONAL_DETAILS, payload: user });
    })
    .catch(function (error) {
      console.log(error);
    });
};

// export const updateAddress = (id, address, landmark, tag) => dispatch => {
//     Axios.post(UPDATE_USER_ADDRESS_URL +"/"+ id, {
//         address_id: id,
//         address_address: address,
//         address_address: landmark,
//         address_tag: tag
//     })
//         .then(response => {
//             const address = response.data;
//             return dispatch({ type: UPDATE_USER_ADDRESS, payload: address });
//         })
//         .catch(function(error) {
//             console.log(error);
//         });
// };
