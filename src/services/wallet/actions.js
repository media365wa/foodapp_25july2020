import {
  GET_WALLET_DETAILS,
  SAVE_WALLET_TRANSACTION,
  GET_WALLET_ANALYTICS,
  SAVE_DIRECT_WALLET_TRANSACTION,
  GET_ORDER_DETAILS,
  SAVE_QR_CODE

} from "./actionTypes";
import {
  GET_WALLET_URL,
  SAVE_WALLET_URL,
  SAVE_DIRECT_WALLET_TRANSACTIONS,
  SAVE_QR_CODE_MESS

} from "../../configs";
import { ORDER_DETAILS } from "../../configs/index";
import Axios from "axios";

export const getWalletTransactions = (token, user_id) => (dispatch) => {
  Axios.post(GET_WALLET_URL + "/" + user_id + "?token=" + token)
    .then((response) => {
      const wallets = response.data;
      return dispatch({ type: GET_WALLET_DETAILS, payload: wallets });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getWalletAnalytics = (token, user_id) => (dispatch) => {
  Axios.post(
    GET_WALLET_URL + "/" + user_id + "?token=" + token + "&d=" + new Date()
  )
    .then((response) => {
      const wallets = response.data;
      return dispatch({ type: GET_WALLET_ANALYTICS, payload: wallets });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const saveWalletTransaction = (token, u, w, order_reference, amount) => (
  dispatch
) => {
  //console.log(saveWalletTransaction);
  Axios.post(SAVE_WALLET_URL, {
    token: token,
    u: u,
    w: w,
    order_reference: order_reference,
    amount: amount,
  })
    .then((response) => {
      const wallettransaction = response.data;
      return dispatch({
        type: SAVE_WALLET_TRANSACTION,
        payload: wallettransaction,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const messQr = (token, u, w, order_reference, amount, restaurant_id,
  item_id, quantity, payment_method) => (
  dispatch
) => {
  //console.log(saveWalletTransaction);
  Axios.post(SAVE_QR_CODE_MESS, {
    token: token,
    u: u,
    w: w,
    order_reference: order_reference,
    amount: amount,
    restaurant_id: restaurant_id,
    item_id: item_id,
    quantity: quantity,
    payment_method: 'messQrCode'
  })
    .then((response) => {
      const wallettransaction = response.data;
      return dispatch({
        type: SAVE_QR_CODE,
        payload: wallettransaction,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const saveDirectWalletTransaction = (
  token,
  u,
  house,
  w,
  order_reference,
  amount,
  restaurantId,
  restaurantCode
) => (dispatch) => {
  //console.log(saveWalletTransaction);
  Axios.post(SAVE_DIRECT_WALLET_TRANSACTIONS, {
    token: token,
    u: u,
    house: house,
    w: w,
    order_reference: order_reference,
    amount: amount,
    restaurant: restaurantId,
    restaurantCode: restaurantCode,
  })
    .then((response) => {
      const wallettransaction = response.data;
      return dispatch({
        type: SAVE_DIRECT_WALLET_TRANSACTION,
        payload: wallettransaction,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getOrderDetails = (user_id) => (dispatch) => {
  Axios.post(ORDER_DETAILS + "/" + user_id, {
    user_id: user_id,
  })
    .then((response) => {
        const orderDetails = response.data;
        localStorage.setItem('OrderSpendings', JSON.stringify(response.data))
        return dispatch({
          type: GET_ORDER_DETAILS,
          payload: orderDetails,
        });
    })
    // .then((json) =>
    //   json.data.map(
    //     (item) => (
    //       item.restaurant_id != null
    //         ? labelName.push(item.restaurant_id)
    //         : null,
    //       item.restaurant_id != null ? plotvalue.push(item.sum) : null
    //     )
    //   )
    // )
    .catch(function (error) {
      console.log(error);
    });
};
